import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;

import distance_matrix.ExpandableDistanceMatrix;
import model_info.EntityInfo;
import non_model_entity.DataPoint;
import support.SupportFunction;


public class Main {
	private static final int MAX_CHUNK = 100;
	
	public static void main(String[] args) {
		EntityInfo info = new EntityInfo("./info");
		try {
			// We continue to expand the current distance matrix
			// The reason is Google API will block your API key at sometime, and you have to choose 
			// .. another code, and then continue the expanding process with the new key.
			String fileName = "distance-matrix-[A].json";
			savingDistanceMatrixFromGoogleMap(info.getPatientDataPoints(), fileName);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void savingDistanceMatrixFromGoogleMap(ArrayList<DataPoint> pidCoords, String fileName) throws InterruptedException {
		int count = 0;
		int failCount = 0;
		int countCheck = 0;
		
		String[] apiKeys;
		try {
			apiKeys = importApiKeys("ApiKeys.txt");
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		int maxTries = apiKeys.length * 5;
		int currentApiKeyIdx = 0;
		
		System.out.println("Getting matrix distance data from Google Map ... ");
				
		ExpandableDistanceMatrix distanceMatrix = new ExpandableDistanceMatrix(fileName);
		
		int n = pidCoords.size();
				
		System.out.println(" (Total matrix elements : " + distanceMatrix.size() + ")");
		try {
			SupportFunction.saveFile(distanceMatrix.generateJsonArray().toString(3), fileName);
		} catch (JSONException e1) {
			e1.printStackTrace();
			throw new RuntimeException();
		}
		
		ArrayList<DataPoint> destinations = new ArrayList<>();
		for (int i = 0 ; i < n ; ++i) {
			DataPoint origin = pidCoords.get(i);
			destinations = new ArrayList<>();
			for (int j = i ; j < n ; ++j) {
				++countCheck;
				
				DataPoint dest = pidCoords.get(j);
				
				if (origin.getId().equals(dest.getId())) {
					distanceMatrix.addCoordinatePair(origin.getId(), dest.getId(), 0.0);
					continue;
				}
				
				if (!distanceMatrix.hasPair(origin.getId(), dest.getId())) {
					destinations.add(dest);
				}
				
				if (destinations.size() == MAX_CHUNK) {
					boolean flag = false;
					while (!flag && currentApiKeyIdx < maxTries){
						int k = currentApiKeyIdx % apiKeys.length;
						String url = googleMapServiceUrlGenerator(origin, destinations, apiKeys[k]);
						TimeUnit.SECONDS.sleep(1);
						flag = distanceMatrix.expandingDistanceMatrix(url, origin, destinations);
						if (!flag)
							++failCount;
						++count;					
						++currentApiKeyIdx;
					}

					if (flag || currentApiKeyIdx >= maxTries) {
						System.out.println(" (Total matrix elements : " + distanceMatrix.size() + ")");
						try {
							SupportFunction.saveFile(distanceMatrix.generateJsonArray().toString(3), fileName);
						} catch (JSONException e) {
							e.printStackTrace();
							throw new RuntimeException();
						}
					}
					destinations = new ArrayList<>();
				}
			}
			
			if (destinations.size() > 0) {
				boolean flag = false;
				while (!flag && currentApiKeyIdx < maxTries){
					int k = currentApiKeyIdx % apiKeys.length;
					String url = googleMapServiceUrlGenerator(origin, destinations, apiKeys[k]);
					TimeUnit.SECONDS.sleep(1);
					flag = distanceMatrix.expandingDistanceMatrix(url, origin, destinations);
					if (!flag)
						++failCount;
					++count;					
					++currentApiKeyIdx;
				}

				if (flag || currentApiKeyIdx >= maxTries) {
					System.out.println(" (Total matrix elements : " + distanceMatrix.size() + ")");
					SupportFunction.saveFile(distanceMatrix.generateJsonArray().toString(), fileName);
				}
				destinations = new ArrayList<>();
			}
		}
		
		System.out.println(" (Total matrix elements : " + distanceMatrix.size() + ")");
		SupportFunction.saveFile(distanceMatrix.generateJsonArray().toString(), fileName);
		
		System.out.println("Total expanding processes : " + count);
		System.out.println("Total fails processes : " + failCount);
		System.out.println("Total check : " + countCheck);
		System.out.println("Matrix size : " + distanceMatrix.size());		
	}
	
	public static String googleMapServiceUrlGenerator(DataPoint originCoord, ArrayList<DataPoint> destCoords, String apiKey) {
		if (destCoords.size() > MAX_CHUNK) {
			System.out.println("Error : Over limit-rate (more than 100)");
			return null;
		}
		
		String origins = originCoord.getCoordinate().getX() + "," + originCoord.getCoordinate().getY();
		String destinations = "";
		
		int n = destCoords.size();

		for (int i = 0 ; i < n ; ++i) {
			destinations += destCoords.get(i).getCoordinate().getX() + "," + destCoords.get(i).getCoordinate().getY();
			if (i < n - 1) {
				destinations += "|";
			}
		}
		return "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + origins + "&destinations=" + destinations + "&key=" + apiKey;
	}
	
	
	
	private static String[] importApiKeys(String apiKeysFile) throws IOException {
		BufferedReader br = null;
		br = new BufferedReader(new FileReader(apiKeysFile));
		ArrayList<String> keys = new ArrayList<>();
	    String line = br.readLine();
	    while (line != null) {
	    	keys.add(line);
	    	line = br.readLine();
	    }
	    br.close();
	    
	    String [] keysArray = new String [keys.size()];
    	for (int i = 0 ; i < keysArray.length ; ++i) 
    		keysArray[i] = keys.get(i);
    	
    	return keysArray; 
	}
}
