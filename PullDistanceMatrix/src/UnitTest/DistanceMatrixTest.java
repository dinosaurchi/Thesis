package UnitTest;

import org.junit.Assert;
import org.junit.Test;

import distance_matrix.IndirectedDistanceMatrix;

public class DistanceMatrixTest {

	@Test
	public void test() {
		IndirectedDistanceMatrix m = new IndirectedDistanceMatrix("distance-matrx-[20170513-153629].json");
		Assert.assertEquals(m.getDistance("P049735", "P087819"), new Double(620.0 / 60.0));
	}

}
