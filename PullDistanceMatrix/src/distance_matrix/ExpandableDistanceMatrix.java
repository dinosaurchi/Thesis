package distance_matrix;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import distance_matrix.IndirectedDistanceMatrix;
import non_model_entity.DataPoint;
import non_model_entity.IndirectedPair;
import support.SupportFunction;

public class ExpandableDistanceMatrix extends IndirectedDistanceMatrix{
	
	public ExpandableDistanceMatrix(String distanceMatrixFileName) {
		super(distanceMatrixFileName);
	}
	
	public void addCoordinatePair(String id1, String id2, double time) {
		if (!hasPair(id1, id2))
			super.addPair(new IndirectedPair<String, String>(id1, id2), time);
	}
	
	public boolean expandingDistanceMatrix(String stringUrl, DataPoint origin, ArrayList<DataPoint> destinations) {
		String str = new String();
		try {
			URL url = new URL(stringUrl);
			
			// read from the URL
			Scanner scan = new Scanner(url.openStream());
		    while (scan.hasNext())
		        str += scan.nextLine();
		    scan.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	    // build a JSON object
	    JSONObject obj = SupportFunction.parseJsonObjectFromString(str);
	    try {
			if (!obj.getString("status").equals("OK")) {
				System.out.println("Error: Rows");
			    return false;
			}
	 
		    String id1 = origin.getId();
		    
		    // get the first result
		    JSONArray rows = obj.getJSONArray("rows");
		    for (int i = 0 ; i < rows.length() ; ++i) {
		    	JSONArray elements = (rows.getJSONObject(i)).getJSONArray("elements");
		    	for (int j = 0 ; j < elements.length() ; ++j) {
		    		JSONObject e = elements.getJSONObject(j);
		    		if (!e.getString("status").equals("OK")) {
		    			System.out.println("Error: element " + j);
		    	        return false;
		    		}
		    		String id2 = destinations.get(j).getId();
		    		double time = e.getJSONObject("duration").getInt("value");
		    		addCoordinatePair(id1, id2, time / 60.0);
		    	}
		    }
	    
	    } catch (JSONException e1) {
			e1.printStackTrace();
		}
	    
	    System.out.println("Successful!");
	    return true;
	}
	
	public boolean hasPair(String id1, String id2) {
		if (super.constainsPair(new IndirectedPair<String, String>(id1, id2)))
			return true;
		return false;
	}
}
