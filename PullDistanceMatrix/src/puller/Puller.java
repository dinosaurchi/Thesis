package puller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;

import distance_matrix.ExpandableDistanceMatrix;
import non_model_entity.DataPoint;
import support.SupportFunction;

public class Puller {
	private static final int WAITING_TIME = 3;
	private static final int MAX_CHUNK = 100;
	
	private final String [] googleApiKeys;
	
	public Puller(String apiKeysFilePath) {
		this(importApiKeys(apiKeysFilePath));
	}
	
	public Puller(String [] apiKeys) {
		if (apiKeys == null)
			throw new NullPointerException();
		this.googleApiKeys = apiKeys;
	}
	
	public boolean updateDistanceMatrix(ArrayList<DataPoint> dataPoints, String outputDirPath) {
		if (dataPoints == null)
			throw new NullPointerException();
		if (outputDirPath == null)
			throw new NullPointerException();
		
		int count = 0;
		int failCount = 0;
		int countCheck = 0;
		
		int maxTries = googleApiKeys.length * 5;
		int currentApiKeyIdx = 0;
		
		System.out.println("Getting matrix distance data from Google Map ... ");
				
		String distanceMatrixFilePath = outputDirPath + "/" + "distance-matrix.json";
		File f = new File(distanceMatrixFilePath);
		if (!f.exists()) {
			try {
				SupportFunction.saveFile((new JSONArray()).toString(3), distanceMatrixFilePath);
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		
		ExpandableDistanceMatrix distanceMatrix = new ExpandableDistanceMatrix(distanceMatrixFilePath);
		
		int n = dataPoints.size();
				
		System.out.println(" (Total matrix elements : " + distanceMatrix.size() + ")");
		try {
			SupportFunction.saveFile(distanceMatrix.generateJsonArray().toString(3), distanceMatrixFilePath);
		} catch (JSONException e1) {
			e1.printStackTrace();
			throw new RuntimeException();
		}
		
		ArrayList<DataPoint> destinations = new ArrayList<>();
		for (int i = 0 ; i < n ; ++i) {
			DataPoint origin = dataPoints.get(i);
			destinations = new ArrayList<>();
			for (int j = i ; j < n ; ++j) {
				++countCheck;
				
				DataPoint dest = dataPoints.get(j);
				
				if (origin.getId().equals(dest.getId())) {
					distanceMatrix.addCoordinatePair(origin.getId(), dest.getId(), 0.0);
					continue;
				}
				
				if (!distanceMatrix.hasPair(origin.getId(), dest.getId())) {
					destinations.add(dest);
				}
				
				if (destinations.size() == MAX_CHUNK) {
					boolean flag = false;
					while (!flag && currentApiKeyIdx < maxTries){
						int k = currentApiKeyIdx % googleApiKeys.length;
						String url = googleMapServiceUrlGenerator(origin, destinations, googleApiKeys[k]);
						try {
							TimeUnit.SECONDS.sleep(WAITING_TIME);
						} catch (InterruptedException e) {
							e.printStackTrace();
							throw new RuntimeException();
						}
						flag = distanceMatrix.expandingDistanceMatrix(url, origin, destinations);
						if (!flag)
							++failCount;
						++count;					
						++currentApiKeyIdx;
					}

					if (flag || currentApiKeyIdx >= maxTries) {
						System.out.println(" (Total matrix elements : " + distanceMatrix.size() + ")");
						try {
							SupportFunction.saveFile(distanceMatrix.generateJsonArray().toString(3), distanceMatrixFilePath);
						} catch (JSONException e) {
							e.printStackTrace();
							throw new RuntimeException();
						}
					}
					destinations = new ArrayList<>();
				}
			}
			
			if (destinations.size() > 0) {
				boolean flag = false;
				while (!flag && currentApiKeyIdx < maxTries){
					int k = currentApiKeyIdx % googleApiKeys.length;
					String url = googleMapServiceUrlGenerator(origin, destinations, googleApiKeys[k]);
					try {
						TimeUnit.SECONDS.sleep(WAITING_TIME);
					} catch (InterruptedException e) {
						e.printStackTrace();
						throw new RuntimeException();
					}
					flag = distanceMatrix.expandingDistanceMatrix(url, origin, destinations);
					if (!flag)
						++failCount;
					++count;					
					++currentApiKeyIdx;
				}

				if (flag || currentApiKeyIdx >= maxTries) {
					System.out.println(" (Total matrix elements : " + distanceMatrix.size() + ")");
					try {
						SupportFunction.saveFile(distanceMatrix.generateJsonArray().toString(3), distanceMatrixFilePath);
					} catch (JSONException e) {
						e.printStackTrace();
						throw new RuntimeException();
					}
				}
				destinations = new ArrayList<>();
			}
		}
		
		System.out.println(" (Total matrix elements : " + distanceMatrix.size() + ")");
		try {
			SupportFunction.saveFile(distanceMatrix.generateJsonArray().toString(3), distanceMatrixFilePath);
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		
		System.out.println("Total expanding processes : " + count);
		System.out.println("Total fails processes : " + failCount);
		System.out.println("Total check : " + countCheck);
		System.out.println("Matrix size : " + distanceMatrix.size() + " / " + countCheck);		
		
		return distanceMatrix.size() == countCheck;
	}
	
	private static String googleMapServiceUrlGenerator(DataPoint originCoord, ArrayList<DataPoint> destCoords, String apiKey) {
		if (destCoords.size() > MAX_CHUNK) {
			System.out.println("Error : Over limit-rate (more than 100)");
			return null;
		}
		
		String origins = originCoord.getCoordinate().getX() + "," + originCoord.getCoordinate().getY();
		String destinations = "";
		
		int n = destCoords.size();

		for (int i = 0 ; i < n ; ++i) {
			destinations += destCoords.get(i).getCoordinate().getX() + "," + destCoords.get(i).getCoordinate().getY();
			if (i < n - 1) {
				destinations += "|";
			}
		}
		return "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + origins + "&destinations=" + destinations + "&key=" + apiKey;
	}
	
	private static String[] importApiKeys(String apiKeysFile) {
		if (apiKeysFile == null)
			throw new NullPointerException();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(apiKeysFile));
			ArrayList<String> keys = new ArrayList<>();
		    String line = br.readLine();
		    while (line != null) {
		    	keys.add(line);
		    	line = br.readLine();
		    }
		    br.close();
		    
		    String [] keysArray = new String [keys.size()];
	    	for (int i = 0 ; i < keysArray.length ; ++i) 
	    		keysArray[i] = keys.get(i);
	    	return keysArray; 
		} 
		catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
}
