package generator;

import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;

import global.KEY_MAP;
import info.ClscInfo;
import info.NursesInfo;
import info.PatientsInfo;
import info.VisitsInfo;
import model_entity.Nurse;
import support.SupportFunction;

public class DataHeaderGenerator {
	private final PatientsInfo patientsInfo = new PatientsInfo();
	private final NursesInfo nursesInfo = new NursesInfo();
	private final ArrayList<VisitsInfo> visitsInfos = new ArrayList<>();
	private final ClscInfo clscInfo;
	
	public DataHeaderGenerator(JSONArray clscInfoArray) {
		clscInfo = new ClscInfo();
		clscInfo.merge(clscInfoArray);
	}
	
	public void addDayVisitInfo(JSONArray patientsInfoArray, JSONArray nursesInfoArray, JSONArray visitsInfoArray) {
		if (patientsInfoArray == null)
			throw new NullPointerException();
		if (nursesInfoArray == null)
			throw new NullPointerException();
		if (visitsInfoArray == null)
			throw new NullPointerException();
		
		this.patientsInfo.merge(patientsInfoArray);
		this.nursesInfo.merge(nursesInfoArray);
		visitsInfoArray = assignRandomlyForNoFollowUpPatients(visitsInfoArray, this.nursesInfo.getValues(KEY_MAP.NURSE_ID));
		VisitsInfo temp = new VisitsInfo();
		temp.merge(visitsInfoArray);
		this.visitsInfos.add(temp);
	}
	
	private JSONArray assignRandomlyForNoFollowUpPatients(JSONArray casesInfoArray, ArrayList<String> values) {
		for (int i = 0 ; i < casesInfoArray.length() ; ++i) {
			try {
				String nid = casesInfoArray.getJSONObject(i).getString(KEY_MAP.NURSE_ID);
				if (nid.equals(Nurse.EMPTY_NURSE)) {
					Random rand = new Random();
					nid = values.get(rand.nextInt(values.size()));
					casesInfoArray.getJSONObject(i).put(KEY_MAP.NURSE_ID, nid);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return casesInfoArray;
	}

	private JSONArray generatePatientsJsonInfo() {
		return patientsInfo.generateJsonInfos();
	}
	
	private JSONArray generateNursesJsonInfo() {
		return nursesInfo.generateJsonInfos();
	}
	
	private JSONArray generateClscJsonInfo() {
		return clscInfo.generateJsonInfos();
	}

	private String getDailyVisitInfoFilePath(int day) {
		return "day-" + day + "-" + visitsInfos.get(day - 1).getLength() + ".json";
	}
	
	private String getClscInfoFilePath() {
		return "clsc-info-" + clscInfo.getLength() + ".json";
	}

	private String getNurseInfoFilePath() {
		return "nurse-info-" + nursesInfo.getLength() + ".json";
	}

	private String getPatientsInfoFilePath() {
		return "patient-info-" + patientsInfo.getLength() + ".json";
	}
	
	public void exportInfo(String infoDirectoryName) {
		System.out.println("Exporting info to " + infoDirectoryName + " ... ");
		SupportFunction.createDirectory(infoDirectoryName);
		try {
			SupportFunction.saveFile(generatePatientsJsonInfo().toString(3), infoDirectoryName + "/" + getPatientsInfoFilePath());
			SupportFunction.saveFile(generateNursesJsonInfo().toString(3), infoDirectoryName + "/" + getNurseInfoFilePath());
			SupportFunction.saveFile(generateClscJsonInfo().toString(3), infoDirectoryName + "/" + getClscInfoFilePath());
			
			String visitInfoDirPath = infoDirectoryName + "/visit-info";
			SupportFunction.createDirectory(visitInfoDirPath);
			for (int i = 0 ; i < visitsInfos.size(); ++i) {
				VisitsInfo info = visitsInfos.get(i);
				SupportFunction.saveFile(info.generateJsonInfos().toString(3), visitInfoDirPath + "/" + getDailyVisitInfoFilePath(i + 1));
			}
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		System.out.println("Finished exporting info to " + infoDirectoryName);
	}
}
