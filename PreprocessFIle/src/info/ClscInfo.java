package info;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import global.KEY_MAP;
import model_entity.CLSC;
import parser.ClscInfoExcelParser;

public class ClscInfo extends MergeableInfo {

	public ClscInfo() {
		super(new String [] {
			KEY_MAP.CLSC_ID,
			KEY_MAP.COORDINATE_X,
			KEY_MAP.COORDINATE_Y,
			KEY_MAP.ADDRESS
		});
	}

	@Override
	protected void mergeInfo(ArrayList<InfoBundle<String>> currentInfos, InfoBundle<String> newInfo) {
		ClscInfoExcelParser.mergeClscs(currentInfos, newInfo);
	}

	@Override
	protected JSONObject getFormatedJsonObject(JSONObject infoJsonObject) {
		if (infoJsonObject == null)
			throw new NullPointerException();
		try {
			CLSC clsc = new CLSC(infoJsonObject.getString(KEY_MAP.CLSC_ID), 
								 Double.parseDouble(infoJsonObject.getString(KEY_MAP.COORDINATE_X)), 
								 Double.parseDouble(infoJsonObject.getString(KEY_MAP.COORDINATE_Y)));
			return clsc.generateJsonObject();
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

}
