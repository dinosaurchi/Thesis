package info;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;


public class InfoBundle<A> {
	private Map<String, A> data = new HashMap<>();
	
	public boolean addInfo (String key, A value) {
		if (key == null)
			throw new RuntimeException("Null input");
		if (value == null)
			throw new RuntimeException("Null input");
		
		if (data.containsKey(key)) {
			throw new RuntimeException("Duplicated info key");
		}
		
		data.put(key, value);
		
		return true;
	}
	
	public boolean updateInfo (String key, A newValue) {
		if (key == null)
			throw new RuntimeException("Null input");
		if (newValue == null)
			throw new RuntimeException("Null input");
		
		if (!data.containsKey(key)) {
			throw new RuntimeException("Not existing info key");
		}
		
		data.put(key, newValue);
		
		return true;
	}
	
	public A getValue(String key) {
		assert(key != null);
		
		if (data.containsKey(key)) {
			return data.get(key);
		}
		throw new RuntimeException("Not existing key : " + key);
	}
	
	public JSONObject generateJsonObject() {
		JSONObject obj = new JSONObject();
		for (String key : data.keySet()) {
			try {
				obj.put(key, data.get(key));
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		return obj;
	}
	
	public void removeKey(String key) {
		if (key == null)
			throw new NullPointerException();
		if (data.containsKey(key))
			data.remove(key);
	}

	public boolean hasKey(String key) {
		if (key == null)
			throw new NullPointerException();
		return data.containsKey(key);
	}

	public ArrayList<String> getKeys() {
		return new ArrayList<>(this.data.keySet());
	}
}
