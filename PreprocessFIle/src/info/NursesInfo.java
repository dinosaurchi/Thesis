package info;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import global.KEY_MAP;
import model_entity.Nurse;
import parser.NurseExcelInfoParser;

public class NursesInfo extends MergeableInfo {

	public NursesInfo() {
		super(new String [] {
			KEY_MAP.NURSE_ID,
			KEY_MAP.NURSE_COLOR
		});
	}

	@Override
	protected void mergeInfo(ArrayList<InfoBundle<String>> currentInfos, InfoBundle<String> newInfo) {
		NurseExcelInfoParser.mergeNurses(currentInfos, newInfo);
	}

	@Override
	protected JSONObject getFormatedJsonObject(JSONObject infoJsonObject) {
		if (infoJsonObject == null)
			throw new NullPointerException();
		try {
			Nurse nurse = new Nurse(infoJsonObject.getString(KEY_MAP.NURSE_ID));
			return nurse.generateJsonObject();
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
}
