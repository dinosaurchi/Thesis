package info;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import global.KEY_MAP;
import model_entity.Visit;
import parser.VisitExcelInfoParser;

public class VisitsInfo extends MergeableInfo {		
	public VisitsInfo() {
		super(new String [] {
			KEY_MAP.NURSE_ID,
			KEY_MAP.SERVICE_TIME,
			KEY_MAP.VISIT_TYPE,
			KEY_MAP.PATIENT_ID,
		});
	}

	@Override
	protected void mergeInfo(ArrayList<InfoBundle<String>> currentInfos, InfoBundle<String> newInfo) {
		if (currentInfos == null)
			throw new NullPointerException();
		if (newInfo == null)
			throw new NullPointerException();
		
		VisitExcelInfoParser.mergeVisits(currentInfos, newInfo);
	}

	@Override
	protected JSONObject getFormatedJsonObject(JSONObject infoJsonObject) {
		if (infoJsonObject == null)
			throw new NullPointerException();
		try {
			Visit visit = new Visit(infoJsonObject.getString(KEY_MAP.PATIENT_ID), 
									infoJsonObject.getString(KEY_MAP.NURSE_ID), 
									infoJsonObject.getString(KEY_MAP.VISIT_TYPE), 
									Double.parseDouble(infoJsonObject.getString(KEY_MAP.SERVICE_TIME)));
			return visit.generateJsonObject();
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
}
