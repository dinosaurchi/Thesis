package info;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import support.SupportFunction;


public abstract class MergeableInfo {
	
	private final ArrayList<InfoBundle<String>> infos = new ArrayList<>();
	private final String [] acceptableKeys;
	
	public MergeableInfo(String [] acceptableKeys) {
		if (acceptableKeys == null)
			throw new NullPointerException();
				
		if (hasDuplication(acceptableKeys)) 
			throw new RuntimeException("Duplicated keys");
		
		this.acceptableKeys = acceptableKeys;
	}
	
	private static boolean hasDuplication(String[] acceptableKeys) {
		for (int i = 0 ; i < acceptableKeys.length ; ++i) {
			for (int j = i + 1 ; j < acceptableKeys.length ; ++j) {
				if (acceptableKeys[i].equals(acceptableKeys[j]))
					return true;
			}
		}
		return false;
	}

	public void merge(JSONArray infosArray) {
		if (infosArray == null)
			throw new NullPointerException();
		if (infosArray.length() == 0)
			throw new RuntimeException();
		
		ArrayList<InfoBundle<String>> dayInfos = createDayInfoBundle(infosArray, acceptableKeys);
		for (int i = 0 ; i < dayInfos.size() ; ++i) {
			mergeInfo(infos, dayInfos.get(i));
		}
	}
	
	abstract protected void mergeInfo(ArrayList<InfoBundle<String>> currentInfos, InfoBundle<String> newInfo);
	
	protected static ArrayList<InfoBundle<String>> createDayInfoBundle(JSONArray infosArray, String [] acceptableKeys) {
		if (infosArray == null)
			throw new NullPointerException();
		if (acceptableKeys == null)
			throw new NullPointerException();
		
		ArrayList<InfoBundle<String>> result = new ArrayList<>();
		
		for (int i = 0 ; i < infosArray.length() ; ++i) {
			JSONObject obj;
			try {
				obj = infosArray.getJSONObject(i);
				InfoBundle<String> bundle = new InfoBundle<>();
				
				Iterator<?> iter = obj.keys();
				
				while (iter.hasNext()) {
					String key = iter.next().toString();
					bundle.addInfo(key, obj.get(key).toString());
				}
				
				for (String k : acceptableKeys) {
					if (!SupportFunction.contains(bundle.getKeys().toArray(), k))
						throw new RuntimeException("Missed key : " + k);
				}					
				
				result.add(bundle);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		
		return result;
	}
	
	public JSONArray generateJsonInfos() {
		if (infos == null || infos.size() < 1)
			throw new RuntimeException("Nothing to generate");
		
		JSONArray array = new JSONArray();
		for (int i = 0 ; i < infos.size() ; ++i) {
			InfoBundle<String> info = infos.get(i);
			for (String key : info.getKeys()) {
				if (!SupportFunction.contains(acceptableKeys, key))
					info.removeKey(key);
			}
			array.put(getFormatedJsonObject(info.generateJsonObject()));
		}
		
		return array;
	}
	
	abstract protected JSONObject getFormatedJsonObject(JSONObject infoJsonObject);

	public ArrayList<String> getValues(String key) {
		if (key == null)
			throw new NullPointerException();
		
		ArrayList<String> results = new ArrayList<>();
		for (InfoBundle<String> info : infos) {
			results.add(info.getValue(key));
		}
		return results;
	}
	
	public long getLength() {
		if (infos == null)
			return 0;
		return this.infos.size();
	}
}
