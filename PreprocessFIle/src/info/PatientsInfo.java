package info;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import global.KEY_MAP;
import model_entity.Patient;
import parser.PatientExcelInfoParser;

public class PatientsInfo extends MergeableInfo{

	public PatientsInfo() {
		super(new String [] {
			KEY_MAP.PATIENT_ID,
			KEY_MAP.COORDINATE_X,
			KEY_MAP.COORDINATE_Y,
			KEY_MAP.ADDRESS,
			KEY_MAP.IS_FOLLOW_UP_IMPORTANT
		});
	}

	@Override
	protected void mergeInfo(ArrayList<InfoBundle<String>> currentInfos, InfoBundle<String> newInfo) {		
		PatientExcelInfoParser.mergePatients(currentInfos, newInfo);
	}

	@Override
	protected JSONObject getFormatedJsonObject(JSONObject infoJsonObject) {
		if (infoJsonObject == null)
			throw new NullPointerException();
		try {
			Patient p = new Patient(infoJsonObject.getString(KEY_MAP.PATIENT_ID), 
									Double.parseDouble(infoJsonObject.getString(KEY_MAP.COORDINATE_X)), 
									Double.parseDouble(infoJsonObject.getString(KEY_MAP.COORDINATE_Y)), 
									Boolean.parseBoolean(infoJsonObject.getString(KEY_MAP.IS_FOLLOW_UP_IMPORTANT)));
			return p.generateJsonObject();
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
}
