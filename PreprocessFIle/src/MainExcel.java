import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;

import info.ClscInfo;
import info.MergeableInfo;
import info.NursesInfo;
import info.PatientsInfo;
import parser.PatientExcelInfoParser;
import parser.ClscInfoExcelParser;
import parser.ExcelInfoParser;
import parser.ExcelMatrixDistanceParser;
import parser.NurseExcelInfoParser;
import support.SupportFunction;

public class MainExcel {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		XSSFWorkbook myExcelBook = new XSSFWorkbook("reformated_data_new.xlsx"); 
		parseExcelFile(myExcelBook);
	}

	private static void parseExcelFile(XSSFWorkbook myExcelBook) throws FileNotFoundException {
		ArrayList<JSONArray> array = new ArrayList<>();
		array.add(parsePatientInfo(myExcelBook.getSheet("day1")));
		array.add(parsePatientInfo(myExcelBook.getSheet("day2")));
		array.add(parsePatientInfo(myExcelBook.getSheet("day3")));
		
		MergeableInfo daysInfo = new PatientsInfo();
		for (int i = 0 ; i < array.size() ; ++i) {
			daysInfo.merge(array.get(i));
		}
		String fileName = "patient-info-" + daysInfo.getLength() + ".json";
	    SupportFunction.saveFile(daysInfo.generateJsonInfos().toString(), fileName);
		
	    // ------------------------
	    
	    array = new ArrayList<>();
		array.add(parseNurseInfo(myExcelBook.getSheet("nurses_day1")));
		array.add(parseNurseInfo(myExcelBook.getSheet("nurses_day2")));
		array.add(parseNurseInfo(myExcelBook.getSheet("nurses_day3")));
		
		daysInfo = new NursesInfo();
		for (int i = 0 ; i < array.size() ; ++i) {
			daysInfo.merge(array.get(i));
		}
		fileName = "nurse-info-" + daysInfo.getLength() + ".json";
	    SupportFunction.saveFile(daysInfo.generateJsonInfos().toString(), fileName);
	    
	    // -----------------------
	    
	    array = new ArrayList<>();
		array.add(parseClscInfo(myExcelBook.getSheet("clsc")));
		
		daysInfo = new ClscInfo();
		for (int i = 0 ; i < array.size() ; ++i) {
			daysInfo.merge(array.get(i));
		}
		fileName = "clsc-info-" + daysInfo.getLength() + ".json";
	    SupportFunction.saveFile(daysInfo.generateJsonInfos().toString(), fileName);
	    
	    // ------------------------
	   //parseMatrixDistance(myExcelBook.getSheet("distance_matrix"));
	}

	private static void parseMatrixDistance(XSSFSheet sheet) throws FileNotFoundException {
		ExcelInfoParser infoParser = new ExcelMatrixDistanceParser();
	    JSONArray infoList = ((ExcelMatrixDistanceParser) infoParser).getJsonArray(sheet);
	    String fileName = "distance-matrix" + infoList.length() + ".json";
	    SupportFunction.saveFile(infoList.toString(), fileName);
	}

	private static JSONArray parseNurseInfo(XSSFSheet sheet) throws FileNotFoundException {
		NurseExcelInfoParser infoParser = new NurseExcelInfoParser();
		return infoParser.getJsonArray(sheet);
	}
	
	private static JSONArray parseClscInfo(XSSFSheet sheet) throws FileNotFoundException {
		ClscInfoExcelParser infoParser = new ClscInfoExcelParser();
		return infoParser.getJsonArray(sheet);
	}

	private static JSONArray parsePatientInfo(XSSFSheet sheet) throws FileNotFoundException {
		PatientExcelInfoParser infoParser = new PatientExcelInfoParser();
		return infoParser.getJsonArray(sheet);
	}
}
