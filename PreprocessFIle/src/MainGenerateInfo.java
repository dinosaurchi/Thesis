import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;

import generator.DataHeaderGenerator;
import parser.ClscInfoExcelParser;
import parser.NurseExcelInfoParser;
import parser.PatientExcelInfoParser;
import parser.VisitExcelInfoParser;
import support.SupportFunction;

public class MainGenerateInfo {
	public static void main(String[] args) throws IOException {
		XSSFWorkbook myExcelBook = new XSSFWorkbook("reformated_data_new.xlsx"); 
		DataHeaderGenerator generator = new DataHeaderGenerator(parseClscInfo(myExcelBook.getSheet("clsc")));
		for (int i = 1 ; i <= 3 ; ++i) {
			generator.addDayVisitInfo(parsePatientInfo(myExcelBook.getSheet("day" + i)),
										parseNurseInfo(myExcelBook.getSheet("nurses_day" + i)),
										parseVisitInfo(myExcelBook.getSheet("day" + i)));
		}
		generator.exportInfo("info-" + SupportFunction.getCurrentTimeStamp());
	}
	
	private static JSONArray parseNurseInfo(XSSFSheet sheet) throws FileNotFoundException {
		NurseExcelInfoParser infoParser = new NurseExcelInfoParser();
		return infoParser.getJsonArray(sheet);
	}
	
	private static JSONArray parseClscInfo(XSSFSheet sheet) throws FileNotFoundException {
		ClscInfoExcelParser infoParser = new ClscInfoExcelParser();
		return infoParser.getJsonArray(sheet);
	}

	private static JSONArray parsePatientInfo(XSSFSheet sheet) throws FileNotFoundException {
		PatientExcelInfoParser infoParser = new PatientExcelInfoParser();
		return infoParser.getJsonArray(sheet);
	}
	
	private static JSONArray parseVisitInfo(XSSFSheet sheet) throws FileNotFoundException {
		VisitExcelInfoParser infoParser = new VisitExcelInfoParser();
		return infoParser.getJsonArray(sheet);
	}
}
