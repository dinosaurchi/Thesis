package parser;

import java.util.ArrayList;
import org.json.JSONArray;

import global.KEY_MAP;
import info.InfoBundle;

public class VisitExcelInfoParser extends ExcelInfoParser {

	public VisitExcelInfoParser() {
		super(new Integer [] {3, 5, 8});
	}

	@Override
	protected JSONArray postProcessing(JSONArray result) {
		return result;
	}

	@Override
	public void mergeInfos(ArrayList<InfoBundle<String>> infosList, InfoBundle<String> newInfo) {
		Double t = Double.parseDouble(newInfo.getValue(KEY_MAP.COORDINATE_X));
		if (t.equals(0.0))
			return;
		
		t = Double.parseDouble(newInfo.getValue(KEY_MAP.COORDINATE_Y));
		if (t.equals(0.0))
			return;
		
		mergeVisits(infosList, newInfo);
	}

	public static void mergeVisits(ArrayList<InfoBundle<String>> infosList, InfoBundle<String> newInfo) {
		infosList.add(newInfo);
	}
}
