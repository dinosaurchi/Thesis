package parser;
import java.util.ArrayList;

import org.json.JSONArray;

import global.KEY_MAP;
import info.InfoBundle;

public class PatientExcelInfoParser extends ExcelInfoParser {	
	
	public PatientExcelInfoParser() {
		super(new Integer[] {5});
	}

	public static void mergePatients(ArrayList<InfoBundle<String>> patientsInfoList, InfoBundle<String> newPatientInfo) {
		for (int i = 0 ; i < patientsInfoList.size() ; ++i) {
			if (patientsInfoList.get(i).getValue(KEY_MAP.PATIENT_ID).equals(newPatientInfo.getValue(KEY_MAP.PATIENT_ID)))
				return;
		}
		patientsInfoList.add(newPatientInfo);
	}

	@Override
	public void mergeInfos(ArrayList<InfoBundle<String>> infosList, InfoBundle<String> newInfo) {
		Double t = Double.parseDouble(newInfo.getValue(KEY_MAP.COORDINATE_X));
		if (t.equals(0.0))
			return;
		
		t = Double.parseDouble(newInfo.getValue(KEY_MAP.COORDINATE_Y));
		if (t.equals(0.0))
			return;
		
		mergePatients(infosList, newInfo);
	}

	@Override
	protected JSONArray postProcessing(JSONArray array) {
		return array;
	}

}
