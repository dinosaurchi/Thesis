package parser;
import java.util.ArrayList;

import org.json.JSONArray;

import global.KEY_MAP;
import info.InfoBundle;

public class ExcelMatrixDistanceParser extends ExcelInfoParser {
	
	public ExcelMatrixDistanceParser() {
		super(new Integer [] {0, 1});
	}

	private static void mergeMatrixDistances(ArrayList<InfoBundle<String>> matrixDistanceMap, 
			InfoBundle<String> distanceMapEntity) {
		
		for (int i = 0 ; i < matrixDistanceMap.size() ; ++i) {
			matrixDistanceMap.get(i).getValue(KEY_MAP.PATIENT_2_ID).equals(distanceMapEntity.getValue(KEY_MAP.PATIENT_2_ID));
			
			// Check if this matrix-distance model_entity has been already added
			if (matrixDistanceMap.get(i).getValue(KEY_MAP.PATIENT_1_ID).equals(distanceMapEntity.getValue(KEY_MAP.PATIENT_1_ID)) && 
					matrixDistanceMap.get(i).getValue(KEY_MAP.PATIENT_2_ID).equals(distanceMapEntity.getValue(KEY_MAP.PATIENT_2_ID))) {				
				return;
			}
		}
		
		// If not, add it to the matrix-distance
		matrixDistanceMap.add(distanceMapEntity);
	}
	
	@Override
	public void mergeInfos(ArrayList<InfoBundle<String>> infosList, InfoBundle<String> newInfo) {
		mergeMatrixDistances(infosList, newInfo);
	}

	@Override
	protected JSONArray postProcessing(JSONArray result) {
		return result;
	}
}
