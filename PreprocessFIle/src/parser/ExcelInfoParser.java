package parser;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.json.JSONArray;

import info.InfoBundle;
import support.SupportFunction;

public abstract class ExcelInfoParser {	
	private final Integer [] ignoredCols;
	
	public ExcelInfoParser(Integer [] ignoredCols) {
		if (ignoredCols == null)
			throw new NullPointerException();
		this.ignoredCols = ignoredCols;
	}
	
	protected InfoBundle<String> parseEntity(XSSFRow row, final ArrayList<String> keys) {
		assert(row != null);
		assert(keys != null);
		assert(ignoredCols != null);
		assert(row.getPhysicalNumberOfCells() == keys.size()) 
		: "Cells = " + row.getPhysicalNumberOfCells() + ", keys = " + keys.size();
		
		InfoBundle<String> result = new InfoBundle<String>();
		for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
			if (SupportFunction.contains(ignoredCols, i)) {
				continue;
			}
				
            XSSFCell cell = row.getCell(i);
            if (cell != null) {
            	String cellVal = getCellValue(cell);
            	if (cellVal != null) {
            		result.addInfo(keys.get(i), cellVal);
            	}
            }
        }
		
		return result;
	}
	
	protected String getCellValue(XSSFCell cell) {		
		switch (cell.getCellType()) {
		case XSSFCell.CELL_TYPE_STRING:
			return cell.getStringCellValue();
			
		case XSSFCell.CELL_TYPE_FORMULA:
			if (cell.getCachedFormulaResultType() == XSSFCell.CELL_TYPE_STRING)
				return cell.getRichStringCellValue().getString();
			else if (cell.getCachedFormulaResultType() == XSSFCell.CELL_TYPE_NUMERIC)
				return String.valueOf(cell.getNumericCellValue());
			else if (cell.getCachedFormulaResultType() == XSSFCell.CELL_TYPE_BOOLEAN)
				return String.valueOf(cell.getBooleanCellValue());
			break;

		case XSSFCell.CELL_TYPE_NUMERIC:
			return String.valueOf(cell.getNumericCellValue());
			
		default:
			break;
		}
		throw new RuntimeException();
	}
	
	protected ArrayList<String> parseKeysList(XSSFRow row) {
		ArrayList<String> keys = new ArrayList<>();
		
		for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
			XSSFCell cell = row.getCell(i);
			if (cell != null) {
				String cellVal = getCellValue(cell);
            	if (cellVal != null) {
            		keys.add(cellVal);
            	}
			}
		}
		
		return keys;
	}
	
	public JSONArray getJsonArray(XSSFSheet sheet) {
		assert(sheet != null);
				
		int rows = sheet.getPhysicalNumberOfRows();
	    	    
	    System.out.println("Start parsing Info from sheet : " + sheet.getSheetName());
	    
	    // Parse the first row as a keys-list
	    ArrayList<String> keys = parseKeysList(sheet.getRow(0));
	    
	    ArrayList<InfoBundle<String>> infosList = new ArrayList<>();
	    
	    for (int r = 1; r < rows; r++) {
	    	XSSFRow row = sheet.getRow(r);
	        if (row != null) {
	        	InfoBundle<String> newInfo = parseEntity(row, keys);
	        	mergeInfos(infosList, newInfo);
	        }
	        if (r % 100 == 0)
	        	System.out.println("Processed row " + r);
	    }
	    
	    System.out.println("Total " + infosList.size() + " unique info entity's IDs");
	    
	    JSONArray result = new JSONArray();
	    for (InfoBundle<String> info : infosList) {
	    	result.put(info.generateJsonObject());
	    }
	    assert(result != null);
	    return postProcessing(result);
	}
	
	abstract protected JSONArray postProcessing(JSONArray result);

	abstract public void mergeInfos(ArrayList<InfoBundle<String>> infosList, InfoBundle<String> newInfo);
}
