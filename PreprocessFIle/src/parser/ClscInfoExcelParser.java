package parser;

import java.util.ArrayList;

import org.json.JSONArray;

import global.KEY_MAP;
import info.InfoBundle;

public class ClscInfoExcelParser extends ExcelInfoParser {

	public ClscInfoExcelParser() {
		super(new Integer [] {});
	}

	@Override
	public void mergeInfos(ArrayList<InfoBundle<String>> infosList, InfoBundle<String> newInfo) {
		mergeClscs(infosList, newInfo);
	}
	
	public static void mergeClscs(ArrayList<InfoBundle<String>> clscsInfoList, InfoBundle<String> newClscInfo) {
		Double t = Double.parseDouble(newClscInfo.getValue(KEY_MAP.COORDINATE_X));
		if (t.equals(0.0))
			throw new RuntimeException();
		
		t = Double.parseDouble(newClscInfo.getValue(KEY_MAP.COORDINATE_Y));
		if (t.equals(0.0))
			throw new RuntimeException();
		
		// Check if this CLSC info has been already added
		for (int i = 0 ; i < clscsInfoList.size() ; ++i) {
			if (clscsInfoList.get(i).getValue(KEY_MAP.CLSC_ID).equals(newClscInfo.getValue(KEY_MAP.CLSC_ID))) {
				throw new RuntimeException("Duplicated CLSCs");
			}
		}
		
		// If not, add it to the CLSC info list 
		clscsInfoList.add(newClscInfo);
	}

	@Override
	protected JSONArray postProcessing(JSONArray result) {
		return result;
	}
}
