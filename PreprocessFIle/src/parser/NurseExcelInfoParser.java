package parser;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;

import global.KEY_MAP;
import info.InfoBundle;
import model_entity.Nurse;

public class NurseExcelInfoParser extends ExcelInfoParser {
	
	public NurseExcelInfoParser() {
		super(new Integer[] {0, 1});
	}

	public static void mergeNurses(ArrayList<InfoBundle<String>> nursesInfoList, InfoBundle<String> newNurseInfo) {
		if (nursesInfoList == null)
			throw new NullPointerException();
		if (newNurseInfo == null)
			throw new NullPointerException();
		
		if (newNurseInfo.getValue(KEY_MAP.NURSE_ID).equals(Nurse.EMPTY_NURSE))
			throw new RuntimeException("Cannot have empty nurse");
		
		for (int i = 0 ; i < nursesInfoList.size() ; ++i) {
			// Check if this nurse info has been already added
			if (nursesInfoList.get(i).getValue(KEY_MAP.NURSE_ID).equals(newNurseInfo.getValue(KEY_MAP.NURSE_ID))) {
				return;
			}
		}
		
		// If not, add it to the nurse info list with a randomly generated color
		if (!newNurseInfo.hasKey(KEY_MAP.NURSE_COLOR)) {
			Random rand = new Random();
			Color color = new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
			newNurseInfo.addInfo(KEY_MAP.NURSE_COLOR, String.valueOf(color.getRGB()));
		}
		nursesInfoList.add(newNurseInfo);		
	}
	
	@Override
	public void mergeInfos(ArrayList<InfoBundle<String>> infosList, InfoBundle<String> newInfo) {
		mergeNurses(infosList, newInfo);
	}

	@Override
	protected JSONArray postProcessing(JSONArray result) {
		return result;
	}
}
