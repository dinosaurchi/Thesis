import java.util.ArrayList;

import clustering.Cluster;
import clustering.KMeanAlgorithm;
import clustering.SimpleKMeanAlgorithm;
import distance_matrix.IndirectedDistanceMatrix;
import model_info.EntityInfo;
import model_info.VivianeModelInfo;
import non_model_entity.DataPoint;

public class TerminateHookTest {

	public static void main(String[] args) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				throw new RuntimeException("Catched!");
			}
		});
		
		int i = 0;
		while (i++ < 1000) {
			System.out.println("Iter = " + i);
			EntityInfo entityInfo = new EntityInfo("./info");
			IndirectedDistanceMatrix dm = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
			VivianeModelInfo info = new VivianeModelInfo(entityInfo, dm, null);
			
			ArrayList<DataPoint> dataPoints = info.getDataPointsList();
			
			KMeanAlgorithm kMean = new SimpleKMeanAlgorithm(16);
			ArrayList<Cluster> clusters = kMean.clustering(dataPoints, 30);
		}
	}

}
