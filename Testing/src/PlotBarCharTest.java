import javax.swing.JFrame;

import distance_matrix.IndirectedDistanceMatrix;
import model.VivianeModel;
import model_info.EntityInfo;
import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import nurse_districting_problem.NurseDistrictingProblem;
import optimizer_creator.OptimizerCreator;
import support.ExtendedPlotter;
import support.SupportFunction;
import support.Timer;

public class PlotBarCharTest {

	public static void main(String[] args) {
		optimizeKMeanTest();
	}
	
	private static void optimizeKMeanTest() {
		Timer timer = new Timer();
		System.out.print("Loading data : ");
		timer.restart();
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix distanceMatrix = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, 
													distanceMatrix, 
													null);
		VivianeModel model = new VivianeModel(info);
		NurseDistrictingProblem program = new NurseDistrictingProblem(model);
		System.out.println(timer.elapsed() + " seconds");
		
		timer.restart();
		System.out.print("Start optimizing : ");
		OptimizerCreator<HomecareAssignmentState, VivianeModel> creator = new OptimizerCreator<>();
		program.setOptimizer(creator.createSimpleKMean(model, 1000, 16));
		program.solve();
		System.out.println(timer.elapsed() + " seconds");
		
		timer.restart();
		System.out.print("Start plotting : ");
		JFrame frame = ExtendedPlotter.plotBarCharWorkload(model, (HomecareAssignmentState) program.getLastState(), true);
		System.out.println(timer.elapsed() + " seconds");
		
		timer.restart();
		System.out.print("Start saving : ");
		SupportFunction.savePlot(frame, "test_bar_char");
		System.out.println(timer.elapsed() + " seconds");
		
		System.out.println("Finished");
	}
}
