import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import distance_matrix.IndirectedDistanceMatrix;
import model.VivianeModel;
import model_info.EntityInfo;
import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import neighborhood.VoronoiMap;
import support.Timer;

public class PerformanceTest {
	public static void main(String[] args) {
		//testCheckConsistencyTime();
		//testBigIntegerPerformance();
		//testLongPerformance();
		//testSynchronizedPerformance();
		//testUnmodificableList();
		//testObjectiveFunctionPerformance();
		testHomehealthcareAssignmentPerformance();
	}
	
	private static void testHomehealthcareAssignmentPerformance() {
		HomecareAssignmentState a = new HomecareAssignmentState("assignment-m1.json");
		ArrayList<String> nids = a.getNurseIds();
		ArrayList<String> pids = a.getPatientIds();
		
		int count = 0;
		Timer timer = new Timer();
		System.out.println("Start testing");
		timer.restart();
		
		int i = 0;
		while (i++ < 10) {
			HomecareAssignmentState curState = a;
			for (String nid : nids) {
				for (String pid : pids) {
					curState = curState.assignPatientToNurse(pid, nid);
					++count;
				}
			}
		}
		
		double time = timer.elapsed();
		System.out.println("Total tests : " + count);
		System.out.println("Finished : " + time + " s");
	}
	
	private static void testUnmodificableList() {
		ArrayList<String> temp = new ArrayList<>();
		temp.add("1243");
		temp.add("12342343");
		temp.add("1243243");
		List<String> list = Collections.unmodifiableList(temp);
		
		try {
			list.add("32");
		}
		catch (UnsupportedOperationException e) {
			
		}
		
		Set<String> s = new HashSet<>();
		s.add("123");
		System.out.println("Set's size = " + s.size());
		s.add("124");
		System.out.println("Set's size = " + s.size());
		s.add("123");
		System.out.println("Set's size = " + s.size());
	}
	
	private static void testObjectiveFunctionPerformance() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix dm = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		HomecareAssignmentState a = new HomecareAssignmentState("assignment-m1.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, dm, new VoronoiMap(entityInfo.getPatientDataPoints()));
		VivianeModel s1 = new VivianeModel (info);
		HomecareAssignmentState a2 = new HomecareAssignmentState("assignment-m2.json");
		
		int i = 0;
		final int TOTAL_TESTS = 100;
		System.out.println("Start testing");
		long startTime = System.currentTimeMillis();
		
		while (i++ < TOTAL_TESTS) {
			System.out.println(s1.getObjectiveScore(a2));
			//s1.getObjectiveScore(a2);
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Total test : " + TOTAL_TESTS);
		System.out.println("Total test time : " + (endTime - startTime) + " ms");
	}
	
	private static class TestClass {
		private static long a = -1;
		private static final Object obj = new Object();
		public static synchronized long getNext() {
			return ++a;
		}
	}
	
	private static void testSynchronizedPerformance() {
		TestClass c = new TestClass();
		
		long i = 0;
		final long TOTAL_TESTS = 9999999;
		System.out.println("Start testing");
		long startTime = System.currentTimeMillis();
		while (i++ < TOTAL_TESTS) {
			long t = TestClass.getNext();
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Total test : " + TOTAL_TESTS);
		System.out.println("Total test time : " + (endTime - startTime) + " ms");
	}
	
	private static void testLongPerformance() {
		long li = -1;
		
		long i = 0;
		final long TOTAL_TESTS = 9999999;
		System.out.println("Start testing");
		long startTime = System.currentTimeMillis();
		while (i++ < TOTAL_TESTS) {
			++li;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Total test : " + TOTAL_TESTS);
		System.out.println("Total test time : " + (endTime - startTime) + " ms");
	}
	
	private static void testBigIntegerPerformance() {
		BigInteger bi = new BigInteger("-1");
		
		long i = 0;
		final long TOTAL_TESTS = 9999999;
		BigInteger t = new BigInteger("1");
		System.out.println("Start testing");
		long startTime = System.currentTimeMillis();
		while (i++ < TOTAL_TESTS) {
			bi.add(t);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Total test : " + TOTAL_TESTS);
		System.out.println("Total test time : " + (endTime - startTime) + " ms");
	}

	private static void testCheckConsistencyTime() {
		EntityInfo info = new EntityInfo("./info");
		HomecareAssignmentState a2 = new HomecareAssignmentState("assignment-m2.json");
		
		int i = 0;
		final int TOTAL_TESTS = 10000;
		System.out.println("Start testing");
		long startTime = System.currentTimeMillis();
		while (i++ < TOTAL_TESTS) {
			info.isConsistent(a2);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Total test : " + TOTAL_TESTS);
		System.out.println("Total test time : " + (endTime - startTime) + " ms");
	}
}
