package UnitTest;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import neighborhood.AbstractNeighborhoodMap;
import neighborhood.VoronoiMap;
import non_model_entity.Coordinate2D;
import non_model_entity.DataPoint;
import support.SupportFunction;

public class NeighborhoodMapTest {

	@Test
	public void simpleVoronoiMapTest() {
		ArrayList<DataPoint> coordinates = new ArrayList<>();
		coordinates.add(new DataPoint("P1", new Coordinate2D(0.45, 0.33)));
		coordinates.add(new DataPoint("P2", new Coordinate2D(0.35, 2.33)));
		coordinates.add(new DataPoint("P3", new Coordinate2D(0.25, 3.33)));
		coordinates.add(new DataPoint("P4", new Coordinate2D(0.15, 4.33)));
		coordinates.add(new DataPoint("P5", new Coordinate2D(0.95, 3.33)));
		
		AbstractNeighborhoodMap adjMap = new VoronoiMap(coordinates);
		Assert.assertTrue(adjMap.getNeighborIds("P1") != null);
		Assert.assertTrue(adjMap.getNeighborIds("P2") != null);
		Assert.assertTrue(adjMap.getNeighborIds("P3") != null);
		Assert.assertTrue(adjMap.getNeighborIds("P4") != null);
		Assert.assertTrue(adjMap.getNeighborIds("P5") != null);
		Assert.assertTrue(adjMap.getNeighborIds("P6") == null);
	}
	
	@Test
	public void generalVoronoiMapTest() {
		ArrayList<TestInfo> testInfos = loadTestingInfo("VoronoiJsonTestCases.json");
		
		ArrayList<DataPoint> coordinates = generateCoordinates(testInfos);
		Assert.assertTrue(coordinates.size() > 0);
		
		AbstractNeighborhoodMap adjMap = new VoronoiMap(coordinates);
		
		for (TestInfo t : testInfos) {
			String id1 = t.info.getId();
			ArrayList<String> adjacents = adjMap.getNeighborIds(id1); 
			Assert.assertTrue(t.adjacentIds.containsAll(adjacents));
			Assert.assertTrue(adjacents.containsAll(t.adjacentIds));
		}
	}
	
	public static class TestInfo {
		public DataPoint info;
		public ArrayList<String> adjacentIds = new ArrayList<>();
	}
	
	public static ArrayList<DataPoint> generateCoordinates(ArrayList<TestInfo> testInfos) {
		ArrayList<DataPoint> coords = new ArrayList<>();
		
		for (TestInfo t : testInfos)
			coords.add(t.info);
		
		return coords;
	}
		
	public static ArrayList<TestInfo> loadTestingInfo (String testingInfoJsonFileName) {
		ArrayList<TestInfo> result = new ArrayList<>();
		JSONArray array = SupportFunction.parseJsonArrayFromFile(testingInfoJsonFileName);
		
		for (int i = 0 ; i < array.length() ; ++i) {
			try {
				JSONObject obj = array.getJSONObject(i);
				String id = null;
				id = obj.getString("id");
				if (id == null || result.contains(id)) {
					throw new RuntimeException("Duplicated id = " + id +" in test info file: " + testingInfoJsonFileName);
				}
				
				TestInfo t = new TestInfo();
				t.info = new DataPoint(id, new Coordinate2D(
										Double.parseDouble(obj.get("x").toString()), 
										Double.parseDouble(obj.get("y").toString())));
				
				JSONArray adjList = (JSONArray) obj.get("adjacent");
				for (int j = 0 ; j < adjList.length() ; ++j) {
					int numId = Integer.parseInt(adjList.get(j).toString());
					t.adjacentIds.add(new String("P" + numId));
				}
				// A point is an adjacent point of itself
				t.adjacentIds.add(id);
				result.add(t);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
}
