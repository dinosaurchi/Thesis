package UnitTest;

import java.util.ArrayList;

import org.junit.Test;

import distance_matrix.IndirectedDistanceMatrix;
import model.VivianeModel;
import model_info.EntityInfo;
import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import move.UnitMove;
import move_generator.SimpleMoveGenerator;
import nurse_districting_problem.NurseDistrictingProblem;
import optimizer.SimpleTabuSearch;
import solution_reporter.VivianeModelSolutionReporter;
import support.SupportFunction;

public class SolutionRecordTest {

	@Test
	public void generalTest() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix distanceMatrix = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, 
													distanceMatrix, 
													null);
		VivianeModel model = new VivianeModel(info);
		model.setWeights(50.7, 72.4, 15.0, 25.3, 15.6);
		NurseDistrictingProblem program = new NurseDistrictingProblem(model);
		program.setOptimizer(new SimpleTabuSearch<>(new SimpleMoveGenerator(model), 10, new ArrayList<UnitMove<HomecareAssignmentState>>()));
		
		VivianeModelSolutionReporter record = new VivianeModelSolutionReporter(model);
		program.solve();
		
		//Assert.assertTrue(model.getNurseAverageDailyWorkload(program.getLastState(), "N1426") == 219.72411477411478);
		//Assert.assertTrue(model.getObjectiveScore(program.getLastState()) == 42651.02152837192);
		
//		JSONObject jsonRecord = record.generateSummary(program.getLastState(), 
//														program.getLastRunningTime(), 
//														program.getLastTotalIterations());
//		
//		try {
//			SupportFunction.saveFile(VivianeModelSolutionReporter.getContextInfo(jsonRecord).toString(3), "context-info.json");
//			SupportFunction.saveFile(VivianeModelSolutionReporter.getDetails(jsonRecord).toString(3), "details.json");
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//		
		record.exportReport("report-" + SupportFunction.getCurrentTimeStamp(), program.getLastRunningInfo());
	}
}
