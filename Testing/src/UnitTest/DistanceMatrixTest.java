package UnitTest;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import distance_matrix.IndirectedDistanceMatrix;

public class DistanceMatrixTest {

	@Test
	public void copyConstructorTest() {
		IndirectedDistanceMatrix m1 = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		IndirectedDistanceMatrix m2 = m1.clone();
		Assert.assertTrue(m1.equals(m2));
		Assert.assertTrue(m2.equals(m1));
	}
	
	@Test
	public void equalTest() {
		// The matrix distance in this case is from a 401x401 matrix
		IndirectedDistanceMatrix m1 = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		IndirectedDistanceMatrix m2 = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		
		Assert.assertTrue(m1.equals(m2));
		Assert.assertTrue(m2.equals(m1));
		
		ArrayList<String> ids = m1.getIdsList();
		int count = 0;
		for (int i = 0 ; i < ids.size() ; ++i) {
			String id1 = ids.get(i);
			for (int j = 0 ; j < ids.size() ; ++j) {
				String id2 = ids.get(j);
				if (id1.equals(id2)) {
					Assert.assertTrue(m1.getDistance(id1, id2) == 0);
				}
				else {
					Assert.assertTrue(m1.getDistance(id1, id2) >= 0);
				}
				++count;
			}
		}
		System.out.println("Total time element test cases : " + count);
	}
	
	@Test
	public void generalTest() {
		IndirectedDistanceMatrix m = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		
		Assert.assertTrue(m.getDistance("P041259", "P041259") == 0.0);
		Assert.assertTrue(m.getDistance("P049341", "P049341") == 0.0);
		Assert.assertFalse(m.getDistance("P049341", "P049341") == 0.000000001);
		
		Assert.assertTrue(m.getDistance("CLSC", "CLSC") != null);
		Assert.assertTrue(m.getDistance("CLSC", "P049341") != null);
		Assert.assertTrue(m.getDistance("CLSC", "P049341") == m.getDistance("P049341", "CLSC"));
		Assert.assertTrue(m.getDistance("CLSC", "CLSC") == 0.0);
		
		Assert.assertTrue(m.getDistance("P031379", "P011717") == 10.1);
		Assert.assertTrue(m.getDistance("P011717", "P031379") == 10.1);
		Assert.assertFalse(m.getDistance("P031379", "P007950") == 11.81);
		
		Assert.assertTrue(m.getDistance("P007950", "P031379") == 6.416666666666667);
		Assert.assertTrue(m.getDistance("P007950", "P031379") == m.getDistance("P031379", "P007950"));
		Assert.assertFalse(m.getDistance("P007950", "P031379") == 6.416666766666667);
		Assert.assertFalse(m.getDistance("P007950", "P031379") == 6.416666666666567);
		
		Assert.assertFalse(m.getDistance("P031379", "P049341") == m.getDistance("P049341", "P011717"));
		
		try {
			m.getDistance("F042344", "P086617");
			Assert.assertTrue(false);
		}
		catch (RuntimeException r) {
			Assert.assertTrue(true);
		}
		finally {
			
		}
	}

}
