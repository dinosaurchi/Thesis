package UnitTest;

import java.util.ArrayList;

import org.junit.Test;

import move.UnitMove;
import distance_matrix.IndirectedDistanceMatrix;
import global.CHI_CONSTANT;
import model.VivianeModel;
import model_info.EntityInfo;
import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import move_generator.SimpleMoveGenerator;
import non_model_entity.Coordinate2D;
import non_model_entity.DataPoint;
import non_model_entity.DirectedPair;
import nurse_districting_problem.NurseDistrictingProblem;
import optimizer.SimpleTabuSearch;
import plotting.Plotter;
import support.ExtendedPlotter;
import support.SupportFunction;

public class PlottingPointsTest {

	@Test
	public void plottingWithDense() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix distanceMatrix = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, 
													distanceMatrix, 
													null);
		ArrayList<Coordinate2D> coords = SupportFunction.convertToCoordinate2DsList(info.getDataPointsList());
		SupportFunction.savePlot(Plotter.plot2DPointsWithDense(coords, CHI_CONSTANT.FRAME_INFO, false), "TestPlottingDense");
	}
	
	@Test
	public void plottingSolution() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix distanceMatrix = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, 
													distanceMatrix, 
													null);
		VivianeModel model = new VivianeModel(info);
		model.setWeights(50.7, 72.4, 15.0, 25.3, 15.6);
		NurseDistrictingProblem program = new NurseDistrictingProblem(model);
		program.setOptimizer(new SimpleTabuSearch<>(new SimpleMoveGenerator(model), 10, new ArrayList<UnitMove<HomecareAssignmentState>>()));		
		program.solve();
		
		SupportFunction.savePlot(Plotter.plotSolution(model, program.getBestState(), CHI_CONSTANT.FRAME_INFO, false), "TestPlottingSolution");
	}
	
	@Test
	public void plottingWorkload() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix distanceMatrix = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, 
													distanceMatrix, 
													null);
		VivianeModel model = new VivianeModel(info);
		model.setWeights(50.7, 72.4, 15.0, 25.3, 15.6);
		NurseDistrictingProblem program = new NurseDistrictingProblem(model);
		program.setOptimizer(new SimpleTabuSearch<>(new SimpleMoveGenerator(model), 10, new ArrayList<UnitMove<HomecareAssignmentState>>()));		
		program.solve();
		
		SupportFunction.savePlot(ExtendedPlotter.plotBarCharWorkload(model, program.getBestState(), false), "TestPlottingWorkload");
	}
	
	@Test
	public void plottingSelectedHighlightTest() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix distanceMatrix = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, 
													distanceMatrix, 
													null);
		
		ArrayList<DataPoint> dps = info.getDataPointsList();
		SupportFunction.savePlot(Plotter.plotHighlightSelectedPoints(getSelected(dps), CHI_CONSTANT.FRAME_INFO, false), 
								"TestPlottingSelectedHighlight");
	}

	private ArrayList<DirectedPair<Coordinate2D, Boolean>> getSelected(ArrayList<DataPoint> dps) {
		ArrayList<Integer> idxs = new ArrayList<>();
		idxs.add(1);
		idxs.add(15);
		idxs.add(28);
		idxs.add(42);
		idxs.add(53);
		idxs.add(76);
		idxs.add(155);
		idxs.add(100);
		idxs.add(134);
		idxs.add(150);
		ArrayList<DirectedPair<Coordinate2D, Boolean>> result = new ArrayList<>();
		for (int i = 0 ; i < dps.size() ; ++i) {
			if (idxs.contains(i))
				result.add(new DirectedPair<Coordinate2D, Boolean>(dps.get(i).getCoordinate(), true));
			else
				result.add(new DirectedPair<Coordinate2D, Boolean>(dps.get(i).getCoordinate(), false));
		}
		return result;
	}
}
