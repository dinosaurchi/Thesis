package UnitTest;

import java.util.ArrayList;

import javax.swing.JFrame;

import org.junit.Assert;
import org.junit.Test;

import clustering.Cluster;
import clustering.KMeanAlgorithm;
import clustering.SimpleKMeanAlgorithm;
import distance_matrix.IndirectedDistanceMatrix;
import global.CHI_CONSTANT;
import model.VivianeModel;
import model_info.EntityInfo;
import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import move.UnitMove;
import move_generator.SimpleMoveGenerator;
import non_model_entity.DataPoint;
import nurse_districting_problem.NurseDistrictingProblem;
import optimizer.SimpleTabuSearch;
import optimizer_creator.OptimizerCreator;
import plotting.Plotter;
import support.ExtendedPlotter;
import support.SupportFunction;

public class OptimizerTest {

	@Test
	public void kMeanSimpleTest() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix dm = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, dm, null);
		
		ArrayList<DataPoint> dataPoints = info.getDataPointsList();
		
		KMeanAlgorithm kMean = new SimpleKMeanAlgorithm(16);
		ArrayList<Cluster> clusters = kMean.clustering(dataPoints, 20);
		Assert.assertEquals(16, clusters.size());
	}
	
	@Test
	public void plotKMeanTest() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix distanceMatrix = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, 
													distanceMatrix, 
													null);
		VivianeModel model = new VivianeModel(info);
		NurseDistrictingProblem program = new NurseDistrictingProblem(model);
		
		OptimizerCreator<HomecareAssignmentState, VivianeModel> creator = new OptimizerCreator<>();
		program.setOptimizer(creator.createSimpleKMean(model, 200, 16));
		program.solve();
		
		Plotter.plotSolution(model, program.getLastState(), CHI_CONSTANT.FRAME_INFO, true);		
		System.out.println("Finished");
	}
	
	@Test
	public void plotBarCharWorkloadTest() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix distanceMatrix = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, 
													 distanceMatrix, 
													 null);
		VivianeModel model = new VivianeModel(info);
		NurseDistrictingProblem program = new NurseDistrictingProblem(model);
		
		OptimizerCreator<HomecareAssignmentState, VivianeModel> creator = new OptimizerCreator<>();
		program.setOptimizer(creator.createSimpleKMean(model, 200, 16));
		program.solve();
		
		ExtendedPlotter.plotBarCharWorkload(model, (HomecareAssignmentState) program.getLastState(), true);
	}
	
	@Test
	public void TabuSearchTest() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix distanceMatrix = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, 
													distanceMatrix, 
													null);
		VivianeModel model = new VivianeModel(info);
		NurseDistrictingProblem program = new NurseDistrictingProblem(model);
		
		HomecareAssignmentState initialState = program.getLastState();
		
		program.setOptimizer(new SimpleTabuSearch<>(new SimpleMoveGenerator(model), 10, new ArrayList<UnitMove<HomecareAssignmentState>>()));
		program.solve();
		
		Assert.assertFalse(initialState.equals(program.getLastState()));
		
//		double temp = model.getNurseAverageDailyWorkload(program.getLastState(), "N1426");
//		Assert.assertTrue("Value = " + temp, temp == 219.72411477411478);
		//Assert.assertTrue(model.getObjectiveScore(program.getLastState()) == 42651.02152837192);
		
		int count = getTotalPatients(program.getLastState());
		Assert.assertEquals(155, count);
		
		JFrame frame = ExtendedPlotter.plotBarCharWorkload(model, (HomecareAssignmentState) program.getLastState(), true);
		SupportFunction.savePlot(frame, "tabu-plot");
	}

	private int getTotalPatients(HomecareAssignmentState state) {
		int count = 0;
		for (String nid : state.getNurseIds()) {
			ArrayList<String> pids = state.getAssignedPatientIdsList(nid);
			count += pids.size();
		}
		return count;
	}
}
