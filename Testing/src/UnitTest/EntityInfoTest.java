package UnitTest;
import org.junit.Assert;
import org.junit.Test;

import model_info.EntityInfo;

public class EntityInfoTest {

	@Test
	public void generalTest() {
		EntityInfo s1 = new EntityInfo("./info");
		Assert.assertEquals(s1.getPatientsListSize(), 155);
		Assert.assertEquals(s1.getNursesListSize(), 16);
		System.out.println("Passed loading info 1");
		
		EntityInfo s2 = new EntityInfo("./info");
		Assert.assertTrue(s2.getPatientsListSize() == 155);
		Assert.assertTrue (s2.getNursesListSize() == 16);
		Assert.assertTrue (s2.equals(s1) && s1.equals(s2));
		System.out.println("Passed loading info 2");
		
		s1 = s2.clone();
		Assert.assertTrue (s2.equals(s1) && s1.equals(s2));
		System.out.println("Passed copying info 2");
		
		System.out.println("Finished testing");
	}

	@Test
	public void patientTest() {
		EntityInfo info = new EntityInfo("./info");
		for (String pid1 : info.getPatientIds()) {
			for (String pid2 : info.getPatientIds()) {
				if (pid1.equals(pid2)) {
					Assert.assertTrue (info.getPatient(pid1).equals(info.getPatient(pid2)) &&
							info.getPatient(pid2).equals(info.getPatient(pid1)));
				}
				else {
					Assert.assertTrue (!info.getPatient(pid1).equals(info.getPatient(pid2)) && 
							!info.getPatient(pid2).equals(info.getPatient(pid1)));
				}
			}
		}
	}
	
	@Test
	public void nurseTest() {
		EntityInfo info = new EntityInfo("./info");
		for (String nid1 : info.getNurseIds()) {
			for (String nid2 : info.getNurseIds()) {
				if (nid1.equals(nid2)) {
					Assert.assertTrue (info.getNurse(nid1).equals(info.getNurse(nid2)) &&
							info.getNurse(nid2).equals(info.getNurse(nid1)));
				}
				else {
					Assert.assertTrue (!info.getNurse(nid1).equals(info.getNurse(nid2)) && 
							!info.getNurse(nid2).equals(info.getNurse(nid1)));
				}
			}
		}
	}
}
