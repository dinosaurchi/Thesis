package UnitTest;

import org.junit.Test;

import experiment.ExperimentHandler;
import support.SupportFunction;

public class ExperimentHandlerTest {

	@Test
	public void test() {
		ExperimentHandler handler = new ExperimentHandler(true);
		handler.performExperiments("test-cases", "report-" + SupportFunction.getCurrentTimeStamp());
	}

}
