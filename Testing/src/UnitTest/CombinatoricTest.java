package UnitTest;

import java.util.ArrayList;
import java.util.Stack;

import org.junit.Test;

public class CombinatoricTest {
	
	private void getCombinationImpl(ArrayList<String> source, int start, int r, Stack<String> trace, ArrayList<ArrayList<String>> result) {	
		for (int i = start ; i < source.size() ; ++i) {
			trace.push(source.get(i));
			if (trace.size() == r) 
				result.add(new ArrayList<>(trace));
			else
				getCombinationImpl(source, i + 1, r, trace, result);
			trace.pop();
		}
	}
	
	private ArrayList<ArrayList<String>> getCombination(ArrayList<String> source, int r) {
		ArrayList<ArrayList<String>> result = new ArrayList<>();
		getCombinationImpl(source, 0, r, new Stack<String>(), result);
		return result;
	}
	
	@Test
	public void test() {
		ArrayList<String> source = new ArrayList<>();
		source.add("AA");
		source.add("aA");
		source.add("Ba");
		source.add("BB");
		source.add("CB");
		source.add("cc");
		source.add("AA");
		source.add("BB");
		source.add("BB");
		source.add("CB");
		source.add("cc");
		source.add("AA");
		source.add("BB");
		
		ArrayList<ArrayList<String>> result = getCombination(source, 8);
		for (ArrayList<String> comb : result) {
			for (String s : comb) {
				System.out.print(s + " ");
			}
			System.out.println();
		}
		
		System.out.println("Size : " + result.size());
	}

}
