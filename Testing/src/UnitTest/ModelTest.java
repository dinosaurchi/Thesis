package UnitTest;
import org.junit.Assert;
import org.junit.Test;

import distance_matrix.IndirectedDistanceMatrix;
import model.VivianeModel;
import model_info.EntityInfo;
import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import neighborhood.VoronoiMap;
import support.SupportFunction;

public class ModelTest {	
	
	@Test
	public void objectiveFunctionTest() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix dm = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		HomecareAssignmentState a = new HomecareAssignmentState("assignment-m1.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, dm, null);
		VivianeModel s1 = new VivianeModel (info);		
		VivianeModel s2 = s1.clone();
		
		HomecareAssignmentState a2 = new HomecareAssignmentState("assignment-m2.json");
		Assert.assertTrue(s1.equals(s2) && s2.equals(s1));
		
		Assert.assertTrue(s1.getObjectiveScore(a2) > 0);
		Assert.assertTrue(s1.getObjectiveScore(a2) == s2.getObjectiveScore(a2));
		Assert.assertTrue(s1.getObjectiveScore(a) != s2.getObjectiveScore(a2));
	}
	
	@Test
	public void copyConstructorTest() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix dm = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, dm, null);
		
		VivianeModel s1 = new VivianeModel(info);
		VivianeModel s2 = s1.clone();
		Assert.assertTrue (s1.equals(s2));
	}
	
	@Test
	public void generalTest() {
		EntityInfo entityInfo = new EntityInfo("./info");
		IndirectedDistanceMatrix dm = new IndirectedDistanceMatrix("distance-matrix-156-156.json");
		VivianeModelInfo info = new VivianeModelInfo(entityInfo, dm, new VoronoiMap(entityInfo.getPatientDataPoints()));
		VivianeModel s1 = new VivianeModel (info);
		
		String content = s1.getModelInfo().getPreviousMonthAssignment().generateStateJsonArray().toString();
		SupportFunction.saveFile(content, "assignment-m1.json");
		
		VivianeModel s2 = s1.clone();
		Assert.assertTrue (s2.equals(s1) && s1.equals(s2));
		
		HomecareAssignmentState a = new HomecareAssignmentState("assignment-m1.json");
		Assert.assertTrue (a.equals(s1.getModelInfo().getPreviousMonthAssignment()) && s1.getModelInfo().getPreviousMonthAssignment().equals(a));
		
		a = new HomecareAssignmentState("assignment-m2.json");
		info = new VivianeModelInfo(entityInfo, dm, null);
		s2 = new VivianeModel (info);
		Assert.assertTrue (!s2.equals(s1) && !s1.equals(s2));
		
		info = new VivianeModelInfo(entityInfo, dm, null);
		s1 = new VivianeModel(info);
		info = new VivianeModelInfo(entityInfo, dm, null);
		s2 = new VivianeModel(info);
		Assert.assertTrue (s2.equals(s1) && s1.equals(s2));
	}
}
