package UnitTest;

import org.json.JSONArray;
import org.junit.Test;

import generator.DataHeaderGenerator;
import support.SupportFunction;

public class InfoGeneratorTest {

	@Test
	public void test() {
		JSONArray clscArray = SupportFunction.parseJsonArrayFromFile("clsc-info-1.json");
		DataHeaderGenerator generator = new DataHeaderGenerator(clscArray);
		JSONArray patientsArray = SupportFunction.parseJsonArrayFromFile("patient-info-155.json");
		JSONArray nursesArray = SupportFunction.parseJsonArrayFromFile("nurse-info-16.json");
		JSONArray visitsArray = SupportFunction.parseJsonArrayFromFile("visit-info-241.json");
		
		generator.addDayVisitInfo(patientsArray, nursesArray, visitsArray);
		generator.addDayVisitInfo(patientsArray, nursesArray, visitsArray);
		
		String dirName = "info-" + SupportFunction.getCurrentTimeStamp();
		generator.exportInfo(dirName);
	}
}
