package UnitTest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;

import non_model_entity.DirectedPair;
import non_model_entity.IndirectedPair;
import non_model_entity.Pair;

public class EntityPairTest {
	@Test
	public void indirectedPairTest() {
		IndirectedPair<String, String> p1 = new IndirectedPair<String, String>("A1", "B2");

		assertingEqual(p1, new IndirectedPair<String, String>("A1", "B2"));
		assertingEqual(p1, new IndirectedPair<String, String>("B2", "A1"));
		assertingNotEqual(p1, new IndirectedPair<String, String>("A2", "B2"));
		assertingNotEqual(p1, new IndirectedPair<String, String>("A1", "B1"));
		assertingNotEqual(p1, new IndirectedPair<String, String>("A2", "B1"));
		
		IndirectedPair<String, String> p2 = new IndirectedPair<String, String>("DASDASDA3287432RKDJASNJSDASDASDAS", "423423DJAHSJDHAS2EJ23H");
		assertingNotEqual(p1, p2);
		assertingEqual(p2, new IndirectedPair<String, String>("DASDASDA3287432RKDJASNJSDASDASDAS", "423423DJAHSJDHAS2EJ23H"));
		assertingEqual(p2, new IndirectedPair<String, String>("423423DJAHSJDHAS2EJ23H", "DASDASDA3287432RKDJASNJSDASDASDAS"));
		
		p2 = new IndirectedPair<String, String>("123456", "654321");
		assertingNotEqual(p2, new IndirectedPair<String, String>("123456", "123456"));
		assertingNotEqual(p2, new IndirectedPair<String, String>("654321", "654321"));
		assertingEqual(p2, new IndirectedPair<String, String>("654321", "123456"));
		
		System.out.println("Finished testing");
	}
	
	@Test
	public void directedPairTest1() {
		DirectedPair<String, String> p1 = new DirectedPair<String, String>("A1", "B2");

		assertingEqual(p1, new DirectedPair<String, String>("A1", "B2"));
		assertingNotEqual(p1, new DirectedPair<String, String>("B2", "A1"));
		assertingNotEqual(p1, new DirectedPair<String, String>("A2", "B2"));
		assertingNotEqual(p1, new DirectedPair<String, String>("A1", "B1"));
		assertingNotEqual(p1, new DirectedPair<String, String>("A2", "B1"));
		
		DirectedPair<String, String> p2 = new DirectedPair<String, String>("DASDASDA3287432RKDJASNJSDASDASDAS", "423423DJAHSJDHAS2EJ23H");
		assertingNotEqual(p1, p2);
		assertingEqual(p2, new DirectedPair<String, String>("DASDASDA3287432RKDJASNJSDASDASDAS", "423423DJAHSJDHAS2EJ23H"));
		assertingNotEqual(p2, new DirectedPair<String, String>("423423DJAHSJDHAS2EJ23H", "DASDASDA3287432RKDJASNJSDASDASDAS"));
		
		p2 = new DirectedPair<String, String>("123456", "654321");
		assertingNotEqual(p2, new DirectedPair<String, String>("123456", "123456"));
		assertingNotEqual(p2, new DirectedPair<String, String>("654321", "654321"));
		assertingNotEqual(p2, new DirectedPair<String, String>("654321", "123456"));
		
		System.out.println("Finished testing");
	}
	
	@Test
	public void directedPairTest2() {
		ArrayList<String> temp1 = new ArrayList<String>();
		temp1.add("123");
		temp1.add("345");
		DirectedPair<String, ArrayList<String>> p1 = 
						new DirectedPair<String, ArrayList<String>>("A1", temp1);
		
		ArrayList<String> temp2 = new ArrayList<>();
		temp2.add("123");
		temp2.add("345");
		DirectedPair<String, ArrayList<String>> p2 = 
				new DirectedPair<String, ArrayList<String>>("A1", temp2);
		
		Assert.assertTrue(p1.equals(p2) && p2.equals(p1));
		
		temp2.add("123");
		
		Assert.assertTrue(!p1.equals(p2) && !p2.equals(p1));
		
		System.out.println("Finished testing");
	}

	private void assertingEqual(Pair<String, String> p1, Pair<String, String> p2) {
		Map<Pair<String, String>, Double> m = new HashMap<>();
		m.put(p1, new Double(1.5));
		Assert.assertTrue(p1.equals(p2) && p2.equals(p1));
		Assert.assertTrue(m.containsKey(p2));
		Assert.assertTrue(m.containsKey(p1));		
	}
	
	private void assertingNotEqual(Pair<String, String> p1, Pair<String, String> p2) {
		Map<Pair<String, String>, Double> m = new HashMap<>();
		m.put(p1, new Double(1.5));
		Assert.assertTrue(!p1.equals(p2) && !p2.equals(p1));
		Assert.assertTrue(!m.containsKey(p2));
		Assert.assertTrue(m.containsKey(p1));
	}
}
