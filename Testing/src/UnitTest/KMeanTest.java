package UnitTest;

import java.util.ArrayList;

import org.json.JSONArray;
import org.junit.Test;

import clustering.Cluster;
import clustering.SimpleKMeanAlgorithm;
import global.CHI_CONSTANT;
import model_entity.Patient;
import non_model_entity.DataPoint;
import plotting.Plotter;
import support.SupportFunction;

public class KMeanTest {

	@Test
	public void test() {
		SimpleKMeanAlgorithm alg = new SimpleKMeanAlgorithm(9);
		JSONArray array = SupportFunction.parseJsonArrayFromFile("patient-info-155.json");
		ArrayList<Patient> patients = Patient.parsePatientsListFromJsonArray(array);
		ArrayList<Cluster> clusters = alg.clustering(getDataPoints(patients), 300);
		SupportFunction.savePlot(Plotter.plotClusters(clusters, CHI_CONSTANT.FRAME_INFO, false), "clustering");
		SupportFunction.savePlot(Plotter.plotClusters(clusters, CHI_CONSTANT.FRAME_INFO, false), "clustering2");
		SupportFunction.savePlot(Plotter.plotClusters(clusters, CHI_CONSTANT.FRAME_INFO, false), "clustering3");
	}

	private ArrayList<DataPoint> getDataPoints(ArrayList<Patient> patients) {
		ArrayList<DataPoint> dps = new ArrayList<>();
		for (Patient p : patients) 
			dps.add(new DataPoint(p.getId(), p.getCoordinate()));
		return dps;
	}

}
