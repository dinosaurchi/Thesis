import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SortTest {
	public static void main(String[] args) {
		ArrayList<String> temp = new ArrayList<>();
		temp.add("ABC");
		temp.add("AAA");
		temp.add("CAB");
		temp.add("BCC");
		
		Collections.sort(temp, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				if (o1.charAt(1) > o2.charAt(1))
					return 1;
				if (o1.charAt(1) < o2.charAt(1))
					return -1;
				return 0;
			}
		});
		
		for (String a : temp)
			System.out.println(a);
	}
}
