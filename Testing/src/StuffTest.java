import model_state.HomecareAssignmentState;
import move.OneMove;

public class StuffTest {
	
	public static abstract class A {
		private int a;
		private String s;
		public A(int a) {
			this.a = a;
			System.out.println("Constructor A");
		}
		public A(String s) {
			this.s = s;
		}
		abstract void go();
	}
	
	public static class B extends A {
		public B(int a) {
			super(a);
		}
		
		@Override
		void go() {
			System.out.println("Class B");
		}
	}
	
	public static class C extends B {
		public C(int a) {
			super(a);
			// TODO Auto-generated constructor stub
		}

		@Override
		void go() {
			System.out.println("Class C");
		}
	}
	
	public static void main(String[] args) {
		System.out.println(OneMove.class.getName());
		System.out.println((new OneMove<HomecareAssignmentState>("", "")).getClass().getCanonicalName());
		System.out.println((new OneMove<HomecareAssignmentState>("", "")).getClass().getName());
		
		A a = new B(1);
		a.go();
		A b = new C(1);
		b.go();
		B c = new C(1);
		c.go();
	}
}
