import org.junit.Test;

import experiment.Tester;
import support.SupportFunction;

public class TesterTest {

	@Test
	public void test() {
		Tester tester = new Tester();
		tester.performTest("./bug-cases/", 
						   "report-" + SupportFunction.getCurrentTimeStamp());
	}

}
