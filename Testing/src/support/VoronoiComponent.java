package support;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.List;
import javax.swing.JPanel;

import voronoi.GraphEdge;

public class VoronoiComponent extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int GRAPH_HEIGHT = 500;
	public static final int GRAPH_WIDTH  = 500;
	public static final int SHIFT_WIDTH = 30;
	public static final int SHIFT_HEIGHT = 30;
	
	private List<GraphEdge> edges = null;
	private String [] ids;
	private double [] xs; 
	private double [] ys;
	
	public static class ScalingInfo {
		public double xRange;
		public double yRange;
		public double xLeftMost;
		public double yBottom;
		public double width;
		public double height;
		
		public double getScalingX(double x) {
			return ((x - xLeftMost) / xRange) * width;
		}
		
		public double getScalingY(double y) {
			return ((y - yBottom) / yRange) * height;
		}
	}
	
	public VoronoiComponent (List<GraphEdge> edges, String [] ids, double [] xs, double [] ys) {
		this.edges = edges;
		this.xs = xs;
		this.ys = ys;
		this.ids = ids;
	}
	
	public void paintComponent(Graphics gh) {
		super.paintComponent(gh);
	    Graphics2D g2d = (Graphics2D) gh;
	    
	    //scalePatientsToSizePatientsList(GRAPH_WIDTH, GRAPH_HEIGHT);
	    // We will scale it after reading all the data (see function 'initializeReadingData')
	    
	    double x1, y1, x2, y2;
	    
	    ScalingInfo info = scalePatientsToSizeDistrictsList(edges, xs, ys);
	    
    	g2d.setStroke(new BasicStroke(0.25f));
	    for (GraphEdge e : edges) {
	    	
	    	x1 = info.getScalingX(e.x1);
			y1 = info.getScalingY(e.y1);
			
			x2 = info.getScalingX(e.x2);
			y2 = info.getScalingY(e.y2);
	    		
			double length = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
			System.out.println(ids[e.site1] + " - " + ids[e.site2] + " : " + length);
	  	    g2d.draw(new Line2D.Double(x1 + SHIFT_WIDTH, y1 + SHIFT_HEIGHT, x2 + SHIFT_WIDTH, y2+ SHIFT_HEIGHT));
	    }
	    
    	g2d.setStroke(new BasicStroke(4f));
    	double x,y ;
	    for (int i = 0 ; i < xs.length ; ++i) {
	    	x = info.getScalingX(xs[i]);
			y = info.getScalingY(ys[i]);
			g2d.draw(new Line2D.Double(x + SHIFT_WIDTH, y + SHIFT_HEIGHT, x + SHIFT_WIDTH, y + SHIFT_HEIGHT));
			g2d.drawString(ids[i] + "", (float) x + SHIFT_WIDTH, (float)y + SHIFT_HEIGHT);
	    }
	    
	}
	
	public static ScalingInfo scalePatientsToSizeDistrictsList (List<GraphEdge> edges, double [] xs, double [] ys){
		if (edges == null || edges.size() == 0)
			return null;
		
		double width = GRAPH_WIDTH, height = GRAPH_HEIGHT;
		
		double xRightMost = xs[0];
		double xLeftMost = xs[0];
		
		double yBottom = ys[0];
		double yUpper = ys[0];
		
//		for (GraphEdge e : edges) {
//			double x = e.x1;
//			if (x > xRightMost) {
//				xRightMost = x;
//			}
//			
//			if (x < xLeftMost) {
//				xLeftMost = x;
//			}
//			x = e.x2;
//			if (x > xRightMost) {
//				xRightMost = x;
//			}
//			
//			if (x < xLeftMost) {
//				xLeftMost = x;
//			}
//			
//			double y = e.y1;
//			if (y < yBottom) {
//				yBottom = y;
//			}
//			
//			if (y > yUpper) {
//				yUpper = y;
//			}
//			y = e.y2;
//			if (y < yBottom) {
//				yBottom = y;
//			}
//			
//			if (y > yUpper) {
//				yUpper = y;
//			}
//		}
		for (int i = 0 ; i < xs.length; ++i) {
			double x = xs[i];
			if (x > xRightMost) {
				xRightMost = x;
			}
			
			if (x < xLeftMost) {
				xLeftMost = x;
			}
			
			double y = ys[i];
			if (y < yBottom) {
				yBottom = y;
			}
			
			if (y > yUpper) {
				yUpper = y;
			}
		}
			
		ScalingInfo info = new VoronoiComponent.ScalingInfo();
		info.width = width;
		info.height = height;
		info.xRange = xRightMost - xLeftMost;
		info.yRange = yUpper -  yBottom;
		info.xLeftMost = xLeftMost;
		info.yBottom = yBottom;
		
		return info;	
	}
}
