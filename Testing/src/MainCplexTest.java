import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.cplex.IloCplex;

public class MainCplexTest {
	
	public static void solve() {
		int n = 4; //cargos
        int m = 3; //compartments
        double[] p = {310.0, 380.0, 350.0, 285.0}; //profit
        double[] v = {480.0, 650.0, 580.0, 390.0}; //volume per ton of cargo
        double[] a = {18.0, 15.0, 23.0, 12.0}; // available weight
        
        double[] c = {10.0, 16.0, 8.0}; // weight capacity of compartment
        double[] V = {6800.0, 8700.0, 5300.0}; //volume capacity of compartment
        
        try {
			IloCplex cplex = new IloCplex();
			
			// variables
			IloIntVar[][] x = new IloIntVar[n][];
			for (int i = 0 ; i < n ; ++i) {
				x[i] = cplex.boolVarArray(m);
			}
			
			IloIntVar y = cplex.intVar(0, Integer.MAX_VALUE, "y");
			
			// Expression
			IloLinearNumExpr[] usedWeightCapacities = new IloLinearNumExpr[m];
			IloLinearNumExpr[] usedVolumeCapacities = new IloLinearNumExpr[m];
			
			for (int j = 0 ; j < m ; ++j) {
				usedWeightCapacities[j] = cplex.linearNumExpr();
				usedVolumeCapacities[j] = cplex.linearNumExpr();
				for (int i = 0 ; i < n ; ++i) {
					usedWeightCapacities[j].addTerm(1, x[i][j]);
					usedVolumeCapacities[j].addTerm(v[i], x[i][j]);
				}
			}
			
			IloLinearNumExpr objective = cplex.linearNumExpr();
			for (int j = 0 ; j < m ; ++j) {
				for (int i = 0 ; i < n ; ++i) {
					objective.addTerm(p[i], x[i][j]);
				}				
			}
			
			cplex.addMaximize(objective);
			
			// Constraints 
			for (int i = 0 ; i < n; ++i) {
				cplex.addLe(cplex.sum(x[i]), a[i]); 
			}
			for (int j = 0 ; j < m ; ++j) {
				cplex.addLe(usedWeightCapacities[j], c[j]);
				cplex.addLe(usedVolumeCapacities[j], V[j]);
				//cplex.addEq(cplex.prod(1/c[j], usedWeightCapacities[j]), y);
			}
			
			// solve
			cplex.setOut(null);
			if (cplex.solve()) {
				System.out.println("Objective = " + cplex.getObjValue());
				//System.out.println("y = " + cplex.getValue(y));
				for (IloIntVar[] nv : x) {
					for (double val : cplex.getValues(nv)) {
						System.out.print(val + "	");
					}
					System.out.println();
				}
			}
			else {
				System.out.println("Error!");
			}
			cplex.end();
			
			
		} catch (IloException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
        
	}
	
	public static void main(String[] args) {
		solve();
	}
}
