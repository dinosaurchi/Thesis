import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import UnitTest.NeighborhoodMapTest;
import non_model_entity.DataPoint;
import support.SupportFunction;
import support.VoronoiComponent;
import voronoi.GraphEdge;
import voronoi.Voronoi;

public class MainTest {

	public static void main(String[] args) {
		
		JSONArray array = SupportFunction.parseJsonArrayFromFile("VoronoiJsonTestCases.json");
		for (int i = 0 ; i < array.length() ; ++i) {
			try {
				JSONObject obj = array.getJSONObject(i);
				System.out.println(obj.get("id").getClass());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static void drawVoronoi() {
		/*
		 * 	Conclusion : we can avoid any GraphEdge has length = 0;
		 * 
		 * 
		 */
		
		ArrayList<NeighborhoodMapTest.TestInfo> testInfos = NeighborhoodMapTest.loadTestingInfo("VoronoiJsonTestCases.json");
		ArrayList<DataPoint> coordinates = NeighborhoodMapTest.generateCoordinates(testInfos);
		
		Collections.sort(coordinates, new Comparator<DataPoint>() {
			@Override
			public int compare(DataPoint coord1, DataPoint coord2) {
				if (coord1.getCoordinate().getX() > coord2.getCoordinate().getX())
					return 1;
				if (coord1.getCoordinate().getX() < coord2.getCoordinate().getX())
					return -1;
				if (coord1.getCoordinate().getY() > coord2.getCoordinate().getY())
					return 1;
				if (coord1.getCoordinate().getY() < coord2.getCoordinate().getY())
					return -1;
				return 0;
			}
		});

    	ArrayList<DataPoint> blocks = getIdsCommonCoordinates(coordinates);
    	
    	// Make all the patients in the same block become neighbors to each others ...
    	// .. and find the max-min positions in term of x and y coordinates
    	
    	double[] xs = getXcoordinateValues(blocks), ys = getYcoordinateValues(blocks); 
    	assert ys.length == xs.length : "Different length between x values and y values";
    	
    	String [] ids = getIds(blocks);
    	
    	double minX = SupportFunction.min(xs), maxX = SupportFunction.max(xs);
    	double minY = SupportFunction.min(ys), maxY = SupportFunction.max(ys);
    	
    	final double scalePercentage = 0.20;
    	final double scaleUp = 1 + scalePercentage;
    	final double scaleDown = 1 - scalePercentage;
    	
    	Voronoi v = new Voronoi(Double.MIN_VALUE);
		List<GraphEdge> edges = v.generateVoronoi(xs, ys, minX * scaleDown, maxX* scaleUp, minY* scaleDown, maxY* scaleUp);
		
		
		JFrame frame = new JFrame("Voronoi");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		VoronoiComponent comp = new VoronoiComponent(edges, ids, xs, ys);
		frame.add(comp);
		frame.setSize(VoronoiComponent.GRAPH_WIDTH + VoronoiComponent.SHIFT_WIDTH * 3, VoronoiComponent.GRAPH_HEIGHT + VoronoiComponent.SHIFT_HEIGHT * 3);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	private static String[] getIds(ArrayList<DataPoint> blocks) {
		final int n = blocks.size();
		String[] ids = new String [n]; 
    	for (int t = 0 ; t < n ; ++t) {
    		ids[t] = blocks.get(t).getId();
    	}
    	assert ids != null && ids.length == n;
    	return ids;
	}

	private static double[] getXcoordinateValues(ArrayList<DataPoint> blocks) {
		if (blocks == null)
			throw new RuntimeException("Null input");
		
		final int n = blocks.size();
		double[] xValuesIn = new double [n]; 
    	for (int t = 0 ; t < n ; ++t) {
    		xValuesIn[t] = blocks.get(t).getCoordinate().getX();
    	}
    	assert xValuesIn != null && xValuesIn.length == n;
    	return xValuesIn;
	}
	
	private static double[] getYcoordinateValues(ArrayList<DataPoint> blocks) {
		if (blocks == null)
			throw new RuntimeException("Null input");
		
		final int n = blocks.size();
		double[] yValuesIn = new double [n]; 
    	for (int t = 0 ; t < n ; ++t) {
    		yValuesIn[t] = blocks.get(t).getCoordinate().getY();
    	}
    	assert yValuesIn != null && yValuesIn.length == n;
    	return yValuesIn;
	}
	
	private static ArrayList<DataPoint> getIdsCommonCoordinates(
			ArrayList<DataPoint> coordinates) {
		
		ArrayList<DataPoint> blocks = new ArrayList<>();
		Double previousX = new Double(0);
		Double previousY = new Double(0);
    	for (int i = 0 ; i < coordinates.size(); ++i) {
    		Double x = coordinates.get(i).getCoordinate().getX();
    		Double y = coordinates.get(i).getCoordinate().getY();
	    	
	    	if (!x.equals(previousX) || !y.equals(previousY)) {
	    		blocks.add(coordinates.get(i));
	    	}
	    	
	    	previousX = x;
	    	previousY = y;
    	}
    	return blocks;		
	}

	private static void stuff() {
		/*
		 	Conclusion : MapPatientId.txt has duplicated P033389 at lines 79 and 609 
		 */
		
		Map<String, String> patientIdsMap = new HashMap<>();
		try (BufferedReader br = new BufferedReader(new FileReader("MapPatientId.txt"))) {
		    String line = br.readLine();
		    while (line != null) {
		    	String [] parts = line.split(" ");
		    	patientIdsMap.put(parts[0], parts[1]);
		        line = br.readLine();
		    }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ArrayList<String> pidsList = new ArrayList<>(patientIdsMap.values());
		for (int i = 0 ; i < pidsList.size() ; ++i) {
			String temp = pidsList.get(i);
			for (int j = i + 1 ; j < pidsList.size() ; ++j) {
				if (temp.equals(pidsList.get(j))) {
					System.out.println("Duplicated " +  temp + " at " + i + ", " + j);
					return;
				}
			}
		}
		System.out.println("No duplicated Id");
	}
}
