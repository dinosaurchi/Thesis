import json
import os
import matplotlib.pyplot as plt
import numpy as np

def getDistribution(data, bins_range):
	hist, bin_edges = np.histogram(data, bins=bins_range, density=True)
	#print bin_edges
	#print [hist[i] / sum(hist) for i in range(0, len(hist))]

	bin_midpoints = bin_edges[:-1] + np.diff(bin_edges)/2
	#print (bin_edges)
	cdf = np.cumsum(hist)
	values = np.random.rand(len(data))
	value_bins = np.searchsorted(cdf, values)
	random_from_cdf = bin_midpoints[value_bins]
	return random_from_cdf

info_dir = "./generated-info-A-20170629-163413"

patients = json.load(open(info_dir + '/patient-info-171.json'))
isFollowUpPatients = {}
for p in patients:
	if p['isFollowUpImportant'] == 'true':
		isFollowUpPatients[p['pid']] = True
	else:
		isFollowUpPatients[p['pid']] = False

visitDirPath = info_dir + '/' + 'visit-info-0.7-[0.3_0.3_0.3_0.099999964]'
timesAll = []
timesFollowUp = []
timeNotFollowUp = []
for f in os.listdir(visitDirPath):
	if f.endswith('.json'):
		with open(visitDirPath + '/' + f) as file:
			data = json.load(file)
			for i in range(0, len(data)):
				t = float(data[i]['serviceTime'])
				timesAll.append(t)
				if isFollowUpPatients[data[i]['pid']] == True:
					timesFollowUp.append(t)
				else:
					timeNotFollowUp.append(t)

bins_range = np.arange(10.0, 70.1, 1.0)
#random_from_cdf = getDistribution(timesAll, bins_range)

plt.subplot(131)
plt.hist(timesAll, bins=bins_range)
plt.title("All Treat. time")
plt.xlabel("Treatment time")
plt.ylabel("Occurence")

plt.subplot(132)
plt.hist(timesFollowUp, bins=bins_range)
plt.title("Followup Treat. time")
plt.xlabel("Treatment time")
plt.ylabel("Occurence")

plt.subplot(133)
plt.hist(timeNotFollowUp, bins=bins_range)
plt.title("Not Followup Treat. time")
plt.xlabel("Treatment time")
plt.ylabel("Occurence")

# plt.subplot(122)
# plt.hist(random_from_cdf, bins=bins_range)
# plt.title("Random Treatment time")
# plt.xlabel("Treatment time")
# plt.ylabel("Occurence")
plt.show()