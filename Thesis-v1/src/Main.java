import java.util.ArrayList;
import org.json.JSONException;
import java.io.IOException;
import java.lang.reflect.Array;

import object.Patient;
import object.Record;
import program.Districting;

public class Main {

	public static void main(String[] args) throws IOException, JSONException {
			
		Districting program = new Districting();
		ArrayList<Patient> patients = null;
		
		patients = program.importData();
		program.makeVoronoiDiagram(patients);
		
		program.exportData(patients);
		
		if (patients == null) {
			System.out.println("Initial Reading Error");
			return;
		}
		else {
			Record clusteringResult = program.runInitialClustering("Test-Clustering.txt", patients);
			program.exportResult("Clustering ... ", "Test-Clustering.txt", clusteringResult);
			program.exportPlotting("Clustering_Cluster");
			program.exportBarCharWorkload("Clustering_Workload");
			
			Record metaheuristicResult = program.runMetaheuristic();
			program.exportResult("Metaheuristic ... ", "Test-Metaheuristic.txt", metaheuristicResult);
			program.exportPlotting("Metaheuristic_Cluster");
			program.exportBarCharWorkload("Metaheuristic_Workload");
			
			System.out.println("Finished!");
			
		}
	}

}
