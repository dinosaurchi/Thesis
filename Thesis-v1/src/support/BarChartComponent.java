//BarChartComponent.java - Jimmy Kurian

package support;

import javax.swing.JPanel;

import object.District;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class BarChartComponent extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<District> districts = null;
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (districts == null || districts.size() == 0)
			return;
		
		Graphics2D g2 = (Graphics2D) g;
		BarChart c = new BarChart(getWidth(), getHeight());
		
		g2.setBackground(Color.WHITE);
		
		for (District d : districts) {
			c.add(d.getAverageDailyWorkLoad());
		}

		c.draw(g2);
	}

	public BarChartComponent(ArrayList<District> districts) {
		this.districts = districts;
	}
}