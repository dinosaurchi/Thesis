package support;

import java.util.ArrayList;
import java.util.Arrays;

import object.Key;
import object.Patient;
import org.json.*;

public class SupportFunction {
	public static String googleMapServiceUrlGenerator(Patient originPatient, ArrayList<Patient> destinationPatients, String apiKey) {
		if (destinationPatients.size() > 100) {
			System.out.println("Error : Over limit-rate (more than 100)");
			return null;
		}
		
		String origins = originPatient.getX() + "," + originPatient.getY();
		String destinations = "";
		
		int n = destinationPatients.size();

		for (int i = 0 ; i < destinationPatients.size() ; ++i) {
			destinations += destinationPatients.get(i).getX() + "," + destinationPatients.get(i).getY();
			if (i < n - 1) {
				destinations += "|";
			}
		}
		
		return "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + origins + "&destinations=" + destinations + "&key=" + apiKey;
	}
	
	public static String getFileName(String filePath) {
    	int i = filePath.length() - 1;
    	while (i >= 0 && filePath.charAt(i) != '/') 
    		--i;
    	if (i < 0) 
    		return filePath;
    	++i;
    	
    	return filePath.substring(i, filePath.length() - 1);
    }
	
	
	private static double cross(Patient O, Patient A, Patient B) {
		return (A.getX() - O.getX()) * (B.getY() - O.getY()) - (A.getY() - O.getY()) * (B.getX() - O.getX());
	}
	
	public static Patient[] getConvexHull(Patient[] P) {
		if (P.length > 1) {
			int n = P.length, k = 0;
			Patient[] H = new Patient[2 * n];

			Arrays.sort(P);

			// Build lower hull
			for (int i = 0; i < n; ++i) {
				while (k >= 2 && cross(H[k - 2], H[k - 1], P[i]) <= 0)
					k--;
				H[k++] = P[i];
			}

			// Build upper hull
			for (int i = n - 2, t = k + 1; i >= 0; i--) {
				while (k >= t && cross(H[k - 2], H[k - 1], P[i]) <= 0)
					k--;
				H[k++] = P[i];
			}
			if (k > 1) {
				H = Arrays.copyOfRange(H, 0, k - 1); // remove non-hull vertices after k; remove k - 1 which is a duplicate
			}
			
			return H;
		} else if (P.length <= 1) {
			
			return P;
		} else{
			
			return null;
		}
	}

	public static boolean isInsidePolygon(Patient[] polygon, Patient p) {
		int j = polygon.length - 1 ;
		boolean oddNodes = false ;

		for (int i = 0 ; i < polygon.length ; i++) {
			if ((polygon[i].getY() <  p.getY() && polygon[j].getY() >= p.getY() || 
				 polygon[j].getY() <  p.getY() && polygon[i].getY() >= p.getY()) &&
				(polygon[i].getX() <= p.getX() || polygon[j].getX() <= p.getX())) {
				oddNodes ^= (polygon[i].getX() + (p.getY() - polygon[i].getY()) / (polygon[j].getY() - polygon[i].getY()) * (polygon[j].getX() - polygon[i].getX()) < p.getX()); 
			}
			j = i; 
		}
		return oddNodes;
	}
}
