package support;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Stack;

import javax.swing.JPanel;

import object.District;
import object.Line;
import object.Patient;
import program.Districting;

public class ClusterComponent extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int GRAPH_HEIGHT = 400;
	public static final int GRAPH_WIDTH  = 600;
	public static final int SHIFT_WIDTH = 50;
	public static final int SHIFT_HEIGHT = 50;
	
	private Patient CLSC = null;
	private ArrayList<District> districts = null;
	
	public static class ScalingInfo {
		public double xRange;
		public double yRange;
		public double xLeftMost;
		public double yBottom;
		public double width;
		public double height;
		
		public double getScalingX(double x) {
			return ((x - xLeftMost) / xRange) * width;
		}
		
		public double getScalingY(double y) {
			return ((y - yBottom) / yRange) * height;
		}
	}
	
	public ClusterComponent(ArrayList<District> districts, Patient CLSC){
		this.CLSC = CLSC;
		this.districts = districts;
	}
	
	public void paintComponent(Graphics gh) {
		super.paintComponent(gh);
	    Graphics2D g2d = (Graphics2D) gh;
	    
	    //scalePatientsToSizePatientsList(GRAPH_WIDTH, GRAPH_HEIGHT);
	    // We will scale it after reading all the data (see function 'initializeReadingData')
	    
	    double x, y;
	    
	    ArrayList<Patient> patients = new ArrayList<>();
	    for (District d : districts) {
	    	patients.addAll(d.getPatients());
	    }
	    ScalingInfo info = scalePatientsToSizeDistrictsList(patients, CLSC);
	    
	    for (District d : districts) {
	    	g2d.setColor(d.getColor());
	    	g2d.setStroke(new BasicStroke(4));
	    	
	    	for (Patient p : d.getPatients()) {	    		
	    		x = info.getScalingX(p.getX());
				y = info.getScalingY(p.getY());
	    		
	  	      	g2d.draw(new Line2D.Double(x, y, x, y));
	  	      	//g2d.drawString("" + p.getPid(), (float)x, (float)y);
		    }
	    	
	    	g2d.setStroke(new BasicStroke(1f));
	    	
	    	//ArrayList<Patient> boundingPolygon = sketchSkeletonByDFS(d.getBorderPatients());
	    	ArrayList<Line> boundingEdges = d.getBoundingEdges();
	    	
	    	for (Line edge : boundingEdges) {
	    		// The border list has already been sorted in counter clockwise order
		    	Patient p1 = edge.getFrom();
		    	Patient p2 = edge.getTo();
		    	
	  	      	g2d.draw(new Line2D.Double(info.getScalingX(p1.getX()), info.getScalingY(p1.getY()), info.getScalingX(p2.getX()), info.getScalingY(p2.getY())));
		    }
	    }
	    
	    for (District d : districts) {
	    	g2d.setStroke(new BasicStroke(5));
	    	g2d.setColor(Color.BLACK);
	    	x = info.getScalingX(d.getCentroid().getX());
	    	y = info.getScalingY(d.getCentroid().getY());
	    	g2d.drawString(d.getId() + "", (float) x, (float) y);
	    }
	    
	    // For plotting the CLSC 
	    Random rand = new Random();
		float r = rand .nextFloat();
	    float g = rand.nextFloat();
	    float b = rand.nextFloat();
	    g2d.setColor(new Color(r, g, b));
	    g2d.setStroke(new BasicStroke(12));
	    x = info.getScalingX(CLSC.getX());
		y = info.getScalingY(CLSC.getY());
	    g2d.draw(new Line2D.Double(x, y, x, y));
	}
	
	private ArrayList<District> getNeighboringDistricts(Patient p) {
		District oldDistrict = p.getDistrict();
		ArrayList<District> result = new ArrayList<>();
		for (Patient pp : Patient.adjacentListMap.get(p)) {
			if (pp.getDistrict().getId() != oldDistrict.getId()) {
				result.add(pp.getDistrict());
			}
		}
		return result;
	}
	
//	private ArrayList<Patient> getBoundingPolygon(District d) {
//		ArrayList<Patient> result = new ArrayList<>();
//		
//		// A copy of border list, we do not modify the original one
//		ArrayList<Patient> border = new ArrayList<>();
//		Map<District, ArrayList<Patient>> adjacentMap = new HashMap<>();
//		for (Patient p : d.getBorderPatients()) {
//			border.add(p);
//			ArrayList<District> neighboringDistricts = getNeighboringDistricts(p);
//			for (District neighbor : neighboringDistricts) {
//				if (!adjacentMap.containsKey(neighbor)) {
//					adjacentMap.put(neighbor, new ArrayList<Patient>());
//				}
//				adjacentMap.get(neighbor).add(p);
//			}
//		}
//		
//		for (Map.Entry<District, ArrayList<Patient>> entry : adjacentMap.entrySet()) {
//			ArrayList<Patient> feasiblePatients = entry.getValue();
//			border.removeAll(feasiblePatients);
//			ArrayList<Patient> skeleton = sketchSkeletonByDFS(feasiblePatients);
//			result.addAll(skeleton);
//			
//			Patient seed = null;
//			for (Patient p : border) {
//				boolean flag = false;
//				for (Patient pp : skeleton) {
//					if (Patient.adjacentListMap.get(pp).contains(p)) {
//						flag = true;
//						break;
//					}
//				}
//				if (flag) {
//					seed = p;
//					break;
//				}
//			}
//		}
//        	writer.write(entry.getKey() + " " + entry.getValue() +  " \n");
//		
//		while (0 < border.size()) {
//			ArrayList<Patient> feasiblePatients = new ArrayList<>();
//			for (Patient p :border) {
//				if (p.getDistrict().getId() == seed.getDistrict().getId()) {
//					feasiblePatients.add(p);
//				}
//			}
//			border.removeAll(feasiblePatients);
//			
//		}
//		
//		return result;
//	}

//	private void DFS (ArrayList<Patient> currentTrace, Patient currentNode, ArrayList<Patient> visitted, ArrayList<Patient> feasiblePatients) {
//		currentTrace.add(currentNode);
//		
//		visitted.add(currentNode);
//		ArrayList<Patient> neighbors = Patient.adjacentListMap.get(currentNode);
//		for (Patient p : feasiblePatients) {
//			if (neighbors.contains(p)) {
//				if (!visitted.contains(p)) {
//					DFS(currentTrace, p, visitted, feasiblePatients);
//					currentTrace.add(currentNode);
//				}
//			}
//		}
//		visitted.remove(visitted.size() - 1);
//	}
//	
//	private ArrayList<Patient> sketchSkeletonByDFS(ArrayList<Patient> feasiblePatients) {
//		ArrayList<Patient> result = new ArrayList<>();
//		ArrayList<Patient> visitted = new ArrayList<>();
//		DFS(result, feasiblePatients.get(0), visitted, feasiblePatients);
//		
//		return result;
//	}

	public static ScalingInfo scalePatientsToSizeDistrictsList (ArrayList<Patient> patients, Patient CLSC){
		if (patients == null || patients.size() == 0)
			return null;
		
		double width = GRAPH_WIDTH, height = GRAPH_HEIGHT;
		
		ArrayList<Patient> pList = new ArrayList<>();
		for (Patient p : patients)
			pList.add(p);

		pList.add(CLSC);	
		
		double xRightMost = pList.get(0).getX();
		double xLeftMost = xRightMost;
		
		double yBottom = pList.get(0).getY();
		double yUpper = yBottom;
		
		for (Patient p : pList) {
			double x = p.getX();
			if (x > xRightMost) {
				xRightMost = x;
			}
			
			if (x < xLeftMost) {
				xLeftMost = x;
			}
			
			double y = p.getY();
			if (y < yBottom) {
				yBottom = y;
			}
			
			if (y > yUpper) {
				yUpper = y;
			}
		}
			
		ScalingInfo info = new ClusterComponent.ScalingInfo();
		info.width = width;
		info.height = height;
		info.xRange = xRightMost - xLeftMost;
		info.yRange = yUpper -  yBottom;
		info.xLeftMost = xLeftMost;
		info.yBottom = yBottom;
		
		return info;
		
//		for (Patient p : pList) {
//			p.setX(((p.getX() - xLeftMost) / xRange) * width);
//			p.setY(((p.getY() - yBottom) / yRange) * height);
//		}		
	}
}
