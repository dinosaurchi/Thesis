package support;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import javax.swing.JPanel;

import object.District;
import object.Patient;
import support.ClusterComponent.ScalingInfo;

public class RandomSeedingComponent extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Patient CLSC = null;
	private boolean [] markedIntialSeeding = null;
	private ArrayList<Patient> patients = null;
	private boolean[] markedFollowUpNurse = null;
	
	public RandomSeedingComponent(ArrayList<Patient> patients, Patient CLSC, boolean[] markedIntialSeeding, boolean [] markedFollowUpNurse) {
		this.CLSC = CLSC;
		this.markedIntialSeeding = markedIntialSeeding;
		this.markedFollowUpNurse = markedFollowUpNurse;
		this.patients = patients;
	}
	
	public void paintComponent(Graphics gh) {
		super.paintComponent(gh);
		
		Graphics2D g2d = (Graphics2D) gh;

		// We will scale it after reading all the data (see function 'initializeReadingData')
		if (markedIntialSeeding.length != patients.size() || markedFollowUpNurse.length != patients.size()) {
			System.out.println("Error: Marked array differs from patients array");
			return;
		}

		double x, y;
		
	    ScalingInfo info = ClusterComponent.scalePatientsToSizeDistrictsList(patients, CLSC);

		g2d.setStroke(new BasicStroke(4));
		
		g2d.setColor(Color.BLACK);
		for (int i = 0 ; i < markedIntialSeeding.length ; ++i) {
			if (!markedIntialSeeding[i]) {
				if (!markedFollowUpNurse[i]) {
					Patient p = patients.get(i);
					x = info.getScalingX(p.getX());
					y = info.getScalingY(p.getY());
					g2d.draw(new Line2D.Double(x, y, x, y));
				}
			}
		}
		
		g2d.setColor(Color.GREEN);
		for (int i = 0 ; i < markedIntialSeeding.length ; ++i) {
			if (!markedIntialSeeding[i]) {
				if (markedFollowUpNurse[i]) {
					Patient p = patients.get(i);
					x = info.getScalingX(p.getX());
					y = info.getScalingY(p.getY());
					g2d.draw(new Line2D.Double(x, y, x, y));
				}
			}
		}
		
		g2d.setColor(Color.RED);
		for (int i = 0 ; i < markedIntialSeeding.length ; ++i) {
			if (markedIntialSeeding[i]) {
				Patient p = patients.get(i);
				x = info.getScalingX(p.getX());
				y = info.getScalingY(p.getY());
				g2d.draw(new Line2D.Double(x, y, x, y));
			}
		}

		g2d.setColor(Color.PINK);
		g2d.setStroke(new BasicStroke(12));
		x = info.getScalingX(CLSC.getX());
		y = info.getScalingY(CLSC.getY());
		g2d.draw(new Line2D.Double(x, y, x, y));
		
		
	}
}
