package object;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import support.SupportFunction;

public class District {
	public static final int DAYS_IN_MONTH = 3;
	public static final String MAP_NURSE_ID_FILE = "MapNurseId.txt";
	public static final String INFO_FILE = "DistrictInfo.txt";
	public static final int NON_EXIST_NURSE = -1;
	private static int currentNurseId = 0; // use for mapping
	
	// Mapping info
	private static Map<Integer, String> nurseIdMap = new HashMap<>();
	private static Map<String, Integer> nurseIdReversedMap = new HashMap<>();
	public static Map<Integer, District> districtMap = new HashMap<Integer, District>();
	
	public static void importMappingData(ArrayList<District> districts) throws IOException {
		System.out.print("Importing file '" + SupportFunction.getFileName(MAP_NURSE_ID_FILE) + "' ... ");
		BufferedReader br = new BufferedReader(new FileReader(MAP_NURSE_ID_FILE));
    	try {
    	    String line = br.readLine();
    	    while (line != null) {
    	    	String[] temp = line.split(" ");
        	    int id = Integer.parseInt(temp[0]);
        	    nurseIdMap.put(id, temp[1]);
        	    nurseIdReversedMap.put(temp[1], id);
        	    
    	        line = br.readLine();
    	    }
    	} finally {
    	    br.close();
    	}
    	for (District d : districts) {
    		districtMap.put(d.getId(), d);
    	}
    	System.out.println("Successful!");
	}
	
	public static void exportMappingData(){
    	System.out.print("Exporting file '" + SupportFunction.getFileName(MAP_NURSE_ID_FILE) + "' ... ");
		try {
            FileWriter writer = new FileWriter(MAP_NURSE_ID_FILE);
            
            for (Map.Entry<Integer, String> entry : nurseIdMap.entrySet())
            	writer.write(entry.getKey() + " " + entry.getValue() +  " \n");
            writer.close();
            System.out.println("Successful!");
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
		System.out.println("Error!");
	}
	
	// XML Writing-Parsing
	public void exportXmlData(PrintWriter writer) {
		char a = '"';
		writer.write("<District ");
		writer.write(" id=" + a + id + a);
		writer.write(" color=" + a + color.getRed() + " " + color.getGreen() + " " + color.getBlue() + a);
		writer.write("></District>\n");
	}
		
	// Excel parsing

	public static ArrayList<Integer> parseNursesIntegerIdList(Iterator<Row> rowIterator) {
		// We also expand the nurse map in this process
		
		if (rowIterator == null) { 
			System.out.println("Parse Nurse list error !");
			return null;
		}
		
		Row row = null;
		while (rowIterator.hasNext()) {
			row = rowIterator.next();
			if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC)
				break;
		}
		
		boolean flag = false;
		ArrayList<Integer> nurseIdList = new ArrayList<>();
		while (row != null) {
			Cell cell = row.getCell(2);
			if (cell == null)
				break;
			
			if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				flag = true;	
				Double temp = cell.getNumericCellValue();
				Integer temp2 = temp.intValue();
				
				if (!nurseIdMap.containsValue(temp2.toString())) {
					++currentNurseId;
					nurseIdMap.put(currentNurseId, temp2.toString());
					nurseIdReversedMap.put(temp2.toString(), currentNurseId);
					District d = new District();
					d.setId(currentNurseId);
					districtMap.put(currentNurseId, d);
				}
				nurseIdList.add(nurseIdReversedMap.get(temp2.toString()));
				row = rowIterator.next();
			}
			else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
				flag = true;		
				String temp = cell.getStringCellValue();
				if (!nurseIdMap.containsValue(temp)) {
					++currentNurseId;
					nurseIdMap.put(currentNurseId, temp);
					nurseIdReversedMap.put(temp, currentNurseId);
					District d = new District();
					d.setId(currentNurseId);
					districtMap.put(currentNurseId, d);
				}
				nurseIdList.add(nurseIdReversedMap.get(temp));
				row = rowIterator.next();
			}
			else break;
		}
		
		if (flag) 
			return nurseIdList;
		
		System.out.println("No nurse error!");
		return null;
	}

	// ---------------------------------------------------------------------------------------
	
	// Comparator
	
	private static Comparator<District> workloadComparatorAscending = new Comparator<District>() {
    	public int compare(District d1, District d2) {
    		if (d1.getAverageDailyWorkLoad() > d2.getAverageDailyWorkLoad())
    			return 1;
    		else if (d1.getAverageDailyWorkLoad() < d2.getAverageDailyWorkLoad())
    			return -1;
    		return 0;
    	}
    };
    
    private static Comparator<District> workloadComparatorDescending = new Comparator<District>() {
    	public int compare(District d1, District d2) {
    		if (d1.getAverageDailyWorkLoad() > d2.getAverageDailyWorkLoad())
    			return -1;
    		else if (d1.getAverageDailyWorkLoad() < d2.getAverageDailyWorkLoad())
    			return 1;
    		return 0;
    	}
    };
    
    private static class ComparatorDistanceToDistrict implements Comparator<District> {
    	private District targetDistrict = null;
    	
    	public void setTargetDistrict (District targetDistrict) {
    		this.targetDistrict = targetDistrict;
    	}
    	
		@Override
		public int compare(District d1, District d2) {
			if (d1.distance(targetDistrict) > d2.distance(targetDistrict))
    			return 1;
    		else if (d1.distance(targetDistrict) < d2.distance(targetDistrict))
    			return -1;
    		return 0;
		}
    	
    }
    
    private static ComparatorDistanceToDistrict distanceToDistrictComparatorAscending = new ComparatorDistanceToDistrict();
	
    public static Comparator<District> getWorkloadComparator(boolean type) {
		if (type == true)
			return workloadComparatorAscending;
		else
			return workloadComparatorDescending;
	}

    public static Comparator<District> getDistanceToDistrictComparator(District targetDistrict) {
    	distanceToDistrictComparatorAscending.setTargetDistrict(targetDistrict);
    	return distanceToDistrictComparatorAscending;
    }
    
    // District info
	
	// -----------------------------------------------------------
	
	// District info
    private Color color;
	private int id;
	private boolean isCentroiCalculatedFlag = false;
	private Patient centroid = new Patient();
	private ArrayList<Patient> patients = new ArrayList<>();
	private double workload = 0;
	private boolean isWorkloadCalculatedFlag = false;	
	
	// Convex hull data
	private ArrayList<Patient> border = new ArrayList<>();
	private boolean isBorderCalculated = false;
	
	// -----------------------------------------------------------
	
	public double getAverageDailyTravelingTime(int patientId) {
		double numerator = 0;
		double denominator = 0;
		if (patientId == 0) {
			// In case of considering the average traveling time from CLSC to all patient in this district 
			for (Patient p : patients) {
				Key key = new Key(0, p.getPid());
				if (p.isVisibleForPlotting() && Patient.timeMatrixDistance.containsKey(key)) {
					double f = (p.getNumberOfVisits() / DAYS_IN_MONTH);
					double t = Patient.timeMatrixDistance.get(key);
					numerator +=  f * t;
					denominator += f;
				}
			}
		}
		else {
			if (patientId == Patient.NONE_EXIST_PATIENT)
				return -1;
			
			if (!Patient.getPatient(patientId).isVisibleForPlotting())
				return 0;
			
			for (Patient p : patients) {
				Key key = new Key(patientId, p.getPid());
				if (p.isVisibleForPlotting() && Patient.timeMatrixDistance.containsKey(key)) {
					double f = (p.getNumberOfVisits() / DAYS_IN_MONTH);
					double t = Patient.timeMatrixDistance.get(key);
					numerator +=  f * t;
					denominator += f;
				}
			}
		}
		
		if (denominator != 0)
			return numerator / denominator;
		return 0;
	}
	
	private boolean isPatientExist(int pid) {
		for (Patient p : patients) {
			if (p.getPid() == pid)
				return true;
		}
		return false;
	}
	
	private Patient getPatient(int pid) {
		for (Patient p : patients) {
			if (p.getPid() == pid) {
				return p;
			}
		}
		return null;
	}
	
	public void cleanPatientsWithoutFollowUp() {
		ArrayList<Patient> tempList = new ArrayList<>();
		for (Patient p : patients) {
			if (p.getFollowUpNurseId() != 0)
				continue;
			tempList.add(p);
		}
		for (Patient p : tempList) {
			patients.remove(p);
		}
		isBorderCalculated = false;
		isCentroiCalculatedFlag = false;
		isWorkloadCalculatedFlag = false;
	}
	
	public double getTotalPatient(){		
		return patients.size();
	}

	public void calculateCentroid() {
		double sumX = 0;
		double sumY = 0;
		
		if (patients.size() == 0) {
			centroid.setX(0);
			centroid.setY(0);
			return;
		}
		
		Patient centerPoint = new Patient();
		for (Patient p : patients) {
			sumX += p.getX();
			sumY += p.getY();
		}
		centerPoint.setX(sumX / patients.size());
		centerPoint.setY(sumY / patients.size());
		
		centroid = patients.get(0);
		double minDistance = Double.MAX_VALUE;
		
		for (Patient pb : patients) {
			double distance = pb.getEuclideanDistance(centerPoint);
			if (distance < minDistance) {
				centroid = pb;
				minDistance = distance;
			}
				
		}
		
		isCentroiCalculatedFlag = true;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public Patient getCentroid(){
		return centroid;
	}
	
	public Patient getVirtualCentralGravity(){
		double x = 0;
		double y = 0;
		
		for (Patient p : patients) {
			x += p.getX();
			y += p.getY();
		}
		
		x /= patients.size();
		y /= patients.size();
		
		Patient temp = new Patient();
		temp.setDistrict(this);
		temp.setX(x);
		temp.setY(y);
		
		return temp;
	}
	
	public Patient getCentroidAndCalculating() {
		if (isCentroiCalculatedFlag == false)
			calculateCentroid();
		return centroid;
	}
	
	public ArrayList<Patient> getPatients() {
		return patients;
	}
	
	public Patient removePatient(Patient pb) {
		if (pb == null)
			return null;
		int pid = pb.getPid();
		for (int i = 0; i < patients.size(); ++i) {
			if (pid == patients.get(i).getPid()) {
				isBorderCalculated = false;
				isCentroiCalculatedFlag = false;
				isWorkloadCalculatedFlag = false;
				return patients.remove(i);
			}
		}
		return null;
	}
	
	public double getAverageDailyWorkLoad() {
		if (isWorkloadCalculatedFlag)
			return workload;
		
		if (patients.size() == 0)
			return 0;
		
		double result = 0;
		result += 2 * getAverageDailyTravelingTime(0);
		
		for (Patient p : patients) {
			if (!p.isVisibleForPlotting())
				continue;

			double f = p.getNumberOfVisits() / DAYS_IN_MONTH;
			result += f * p.getAverageTreatmentTime() + getAverageDailyTravelingTime(p.getPid());
		}
		
		workload = result;
		isWorkloadCalculatedFlag = true;
		
		return result;
	}
	
	public void addPatient(Patient p) {
		if (p == null || isPatientExist(p.getPid())) 
			return;
		
		patients.add(p);
				
		isBorderCalculated = false;
		isCentroiCalculatedFlag = false;
		isWorkloadCalculatedFlag = false;
	}

	public void copy(District d) {
		this.centroid = d.centroid;
		this.id = d.id;
		this.isCentroiCalculatedFlag = d.isCentroiCalculatedFlag;
		this.isBorderCalculated = d.isBorderCalculated;
		this.isWorkloadCalculatedFlag = d.isWorkloadCalculatedFlag;
		this.workload = d.workload;
		
		this.patients = new ArrayList<>();
		for (Patient p : d.getPatients()) {
			Patient temp = new Patient();
			temp.copy(p);
			temp.setDistrict(this);
			this.patients.add(temp);
		}
		
		// We get the configuration of border patient from the NEW COPIED PATIENTS LIST
		this.border = new ArrayList<>();
		for (Patient p : d.getBorderPatients()) {
			for (Patient pp : this.patients) {
				Patient originalPatient = Patient.getPatient(pp.getPid());
				if (p.getPid() == originalPatient.getPid()) {
					this.border.add(pp);
					break;
				}
			}
		}
				
		this.color = d.color;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void removeInvisiblePatients() {
		ArrayList<Patient> tempList = new ArrayList<>();
		for (Patient p : patients) {
			if (p.isInvisible())
				tempList.add(p);
		}
		for (Patient p : tempList)
			patients.remove(p);
		
		isBorderCalculated = false;
		isCentroiCalculatedFlag = false;
		isWorkloadCalculatedFlag = false;
	}
	
	public int getTotalInvisiblePatient() {
		int count = 0;
		for (Patient p : patients) {
			if (p.isInvisible())
				++count;
		}
		return count;
	}

	public void addPatientWithRemovalFromOldDistrict(Patient p) {
		if (p == null || isPatientExist(p.getPid())) 
			return;
	
		District d = p.getDistrict();
		if (d != null) {
			if (d.id == id)
				return;
			
			d.removePatient(p);
		}
		
		patients.add(p);
		p.setDistrict(this);
		isCentroiCalculatedFlag = false;
		isBorderCalculated = false;
		isWorkloadCalculatedFlag = false;
	}
	
	public int getTotalNewPatients(District initialDistrict){
		int result = 0;
		for (Patient p : patients) {
			boolean flag = true;
			for (Patient p2 : initialDistrict.getPatients()) {
				if (p.getPid() == p2.getPid()) {
					flag = false;
					break;
				}
			}
			
			if (flag) {
				++result;
			}
		}
		return result;
	}
	
	public int getTotalMovedPatients(District initialDistrict){
		int result = 0;
		for (Patient p : initialDistrict.getPatients()) {
			boolean flag = true;
			for (Patient p2 : patients) {
				if (p.getPid() == p2.getPid()) {
					flag = false;
					break;
				}
			}
			
			if (flag) {
				++result;
			}
		}
		return result;
	}
	
	// Convex Hull - Voronoi
	
	private boolean isBorderPatient(Patient p) {
		// We look for a patient who has neighbor(s) not belonged to the current district
		// => it means this patient has a link to a outer patients => must be on the border
		ArrayList<Patient> neighbors = Patient.adjacentListMap.get(p);
		
//		Patient pp = Patient.getPatient(589);
//		ArrayList<Patient> temp = Patient.adjacentListMap.get(pp);
//		if (neighbors == null)
//			return true;
		
		if (neighbors.size() > 0) {
			for (Patient neighbor : neighbors) {
				if (neighbor.getDistrict().getId() != this.getId())
					return true;
			}
		}
		
		return false;
	}
	
	private void calculateBorder() {
		border = new ArrayList<>();
		for (Patient p : patients) {
			if (isBorderPatient(p)) {
				border.add(p);
				
			}
		}
		
		isBorderCalculated = true;
	}
	
	
	// -----------------------------------------------------------


	
	public static String getNurseStringId(int nid) {
		if (!nurseIdMap.containsKey(nid))
			return null;
		return nurseIdMap.get(nid);
	}
	
	public static District getDistrictFromIntegerId(int id) {
		if (districtMap.containsKey(id))
			return districtMap.get(id);
		return null;
	}
	
	public static int getNurseIntegerId(String nid) {
		if (!nurseIdReversedMap.containsKey(nid))
			return NON_EXIST_NURSE;
		return nurseIdReversedMap.get(nid);
	}
	
	public double distance (District d) {		
		if (!isBorderCalculated)
			calculateBorder();
		
//		if (d.isIntersect(this))
//			return 0;
		
		double minDistance = Double.MAX_VALUE;
		for (Patient p : getBorderPatients()) {
			double distance = d.distance(p);
			if (distance < minDistance) 
				minDistance = distance;
		}
		
		return minDistance;
	}
	
//	public Patient getOutsidePolygonPatient (ArrayList<District> polygonDistricts){
//		if (polygonDistricts.size() == 0)
//			return null;
//		
//		Patient[] polygon = new Patient[polygonDistricts.size()];
//		for (int i = 0 ; i < polygon.length; ++i) {
//			polygon[i] = polygonDistricts.get(i).getCentroidAndCalculating();
//		}
//		
//		for (Patient p : getBorderPatients()) {
//			if (!SupportFunction.isInsidePolygon(polygon, p)) {
//				return p;
//			}
//		}
//		return null;
//	}
//	
//	public void swapBalancingAverageWorkload(District neighboringDistrict) {
//		double minDistance = Double.MAX_VALUE;
//		Patient closestPatient = null;
//		for (Patient p : getBorderPatients()) {
//			double distance = neighboringDistrict.distance(p);
//			if (distance < minDistance) {
//				minDistance = distance;
//				closestPatient = p;
//			}
//		}
//		
//		
//	}
	
	public double distance (Patient p) {
		if (!isBorderCalculated)
			calculateBorder();
		
//		if (patients.size() == 1) 
//			return patients.get(0).getEuclideanDistance(p);
//		
//		if (SupportFunction.isInsidePolygon(getBorderPatients(), p))
//			return 0;
//		
//		ArrayList<Line> edges = new ArrayList<>();
//		int n = border.length;
//		for (int i = 0 ; i < n ; ++i) {
//			edges.add(new Line(border[i % n], border[(i + 1) % n]));		
//		}
//		
		double minDistance = Double.MAX_VALUE;
//		for (Line e : edges) {
//			double distance = e.distance(p);
//			if (distance < minDistance) {
//				minDistance = distance;
//			}
//		}
		
		for (Patient pp : getBorderPatients()) {
			double d = pp.getEuclideanDistance(p);
			if (d < minDistance)
				minDistance = d;
		}
		
		return minDistance;
	}
	
	public ArrayList<Patient> getBorderPatients() {
		if (isBorderCalculated)
			return border;
		
		// Using Voronoi Diagram to calculate the border
		
		calculateBorder();
	
		return border;
	}

	public void cleanPatients() {
		for (Patient p : patients) {
			p.setDistrict(null);
		}
		
		patients = new ArrayList<>();
		border = new ArrayList<>();
		isCentroiCalculatedFlag = false;
		isBorderCalculated = false;
		isWorkloadCalculatedFlag = false;
		
	}

	public double closestDistance(Patient p) {
		double min = Double.MAX_VALUE;
		for (Patient pp : patients) {
			double distance = pp.getEuclideanDistance(p);
			if (distance < min)
				min = distance;
		}
		
		return min;
	}

	public double getSimpleWorkload() {
		double nominator = 0;
		double denominator = 0;
		
		centroid = getCentroid();
		for (Patient p : patients) {
			double f = p.getNumberOfVisits() / DAYS_IN_MONTH;
			double distance = p.getTravelingTimeDistance(centroid);
			
			nominator += f * (p.getAverageTreatmentTime() + distance);
			denominator += f;
		}
		return nominator / denominator;
	}

	public ArrayList<Patient> getAdjacentPatientTo(District target) {
		ArrayList<Patient> result = new ArrayList<>();
		for (Patient p1 : getBorderPatients()) {
			ArrayList<Patient> neighbors = Patient.adjacentListMap.get(p1);
			for (Patient p2 : target.getBorderPatients()) {
				if (neighbors.contains(p2)) {
					result.add(p2);
				}
			}
		}
		return result;
	}
	
	private boolean contains(ArrayList<Line> lines, Line line) {
		for (Line temp : lines) {
			if (temp.equals(line))
				return true;
		}
		return false;
	}
	
	public ArrayList<Line> getBoundingEdges() {
		
		ArrayList<Line> lines = new ArrayList<>();
		ArrayList<Line> notIntersectLines = new ArrayList<>();
		
		synchronized (border) {
			for (Patient p1 : getBorderPatients()) {
				for (Patient p2 : getBorderPatients()) {
					if (p1.getPid() != p2.getPid()) {
						
						if (!contains(lines, new Line(p1, p2)) && !contains(lines, new Line(p2, p1))) {
							Line line = new Line(p1, p2);
							lines.add(line);
							notIntersectLines.add(line);
						}
					}
				}
			}
		}
		
//		for (Line line : lines) {
//			for (Line line2 : lines) {
//				if (!line.equals(line2) && contains(notIntersectLines, line) && contains(notIntersectLines, line2)) {
//					Patient intersection = line.getIntersectVirtualPoint(line2);
//					if (!(intersection.duplicate(line.getFrom()) || intersection.duplicate(line.getTo())) &&
//						!(intersection.duplicate(line2.getFrom()) || intersection.duplicate(line2.getTo())) &&
//						line.contains(intersection)) {
//						notIntersectLines.remove(line);
//						notIntersectLines.remove(line2);
//						break;
//					}
//				}
//			}
//		}
		
		return notIntersectLines;
	}
}

