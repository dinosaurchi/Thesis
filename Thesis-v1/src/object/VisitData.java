package object;

import java.io.PrintWriter;
import java.util.ArrayList;

public class VisitData {
	private ArrayList<Integer> days = new ArrayList<>();
	private int month = 0;
	
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public ArrayList<Integer> getDays() {
		return days;
	}
	public void addDay(int day) {
		days.add(day);
	}
	public void exportXmlData(PrintWriter writer) {
		char a = '"';
		writer.write("   <VisitData month=" + a + month + a + " days=" + a);
		for (Integer d : days) 
			writer.write(d + " ");
		writer.write(a + "></VisitData>\n");
	}
	
}
