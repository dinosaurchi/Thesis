package object;

import java.util.ArrayList;

public class Solution {
	protected class DistrictSolution {
		public int districtId = District.NON_EXIST_NURSE;
		public ArrayList<Integer> patientsId = new ArrayList<>();
	}
	private ArrayList<DistrictSolution> districtSolutions = new ArrayList<>();
	
	public void addDistrictAssignment(District d) {
		DistrictSolution ds = new DistrictSolution();
		ds.districtId = d.getId();
		for (Patient p : d.getPatients()) {
			ds.patientsId.add(p.getPid());
		}
		districtSolutions.add(ds);
	}
	
	public static ArrayList<District> reconstructSolution(Solution solution){
		ArrayList<District> districts = new ArrayList<>();
		for (DistrictSolution ds : solution.districtSolutions) {
			District d = District.getDistrictFromIntegerId(ds.districtId);
			for (Integer pid : ds.patientsId) {
				Patient p = Patient.getPatient(pid);
				d.addPatientWithRemovalFromOldDistrict(p);
			}
			districts.add(d);
		}
		
		return districts;
	}
}
