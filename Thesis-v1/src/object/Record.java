package object;

import java.util.ArrayList;

public class Record {
	public static final int METAHEURISTIC = 1;
	public static final int CLUSTERING = 2;
	
	private Double avgWorkload = new Double(0);
	private Double runtime = new Double(0);
	private Double varation = new Double(0);
	private Double std = new Double(0);
	private int processId = 0;
	private ArrayList<Integer> totalMoved = null;
	private ArrayList<Integer> totalNew = null;
	private ArrayList<Double> changingPercentage = null;
	
	
	public Double getAvgWorkload() {
		return avgWorkload;
	}
	public void setAvgWorkload(double avgWorkload) {
		this.avgWorkload = avgWorkload;
	}
	public Double getRuntime() {
		return runtime;
	}
	public void setRuntime(double runtime) {
		this.runtime = runtime;
	}
	public Double getVaration() {
		return varation;
	}
	public void setVaration(double varation) {
		this.varation = varation;
	}
	public Double getStd() {
		return std;
	}
	public void setStd(Double std) {
		this.std = std;
	}
	public int getProcessId() {
		return processId ;
	}
	public void setProcessId(int processId) {
		this.processId = processId;
	}
	public ArrayList<Integer> getTotalMovedPatients() {
		return totalMoved ;
	}
	public ArrayList<Integer> getTotalNewPatients() {
		return totalNew ;
	}
	public ArrayList<Double> getChangingPercentage() {
		return changingPercentage ;
	}
	public void setTotalMovedPatients(ArrayList<Integer> totalMoved) {
		this.totalMoved = totalMoved;
	}
	public void setTotalNewPatients(ArrayList<Integer> totalNew) {
		this.totalNew = totalNew;	
	}
	public void setChangingPercentage(ArrayList<Double> changingPercentage) {
		this.changingPercentage = changingPercentage;
	}
}
