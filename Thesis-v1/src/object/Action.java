package object;


public class Action {
	public static final String MOVE_ONE = "M1";
	public static final String SWAP_ONE = "S1";
	
	public String action = "";
	public Patient p1 = null;
	public Patient p2 = null;
	public District d = null;
}
