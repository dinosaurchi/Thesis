package object;

public class Line {
	private Patient from = null;
	private Patient to = null;
	
	public Line(Patient from, Patient to) {
		if (from != null && to != null) {
			this.from = from;
			this.to = to;
		}
		else {
			this.from = new Patient();
			this.from.setX(0);
			this.from.setY(0);
			
			this.to = new Patient();
			this.to.setX(0);
			this.to.setY(0);
		}
	}
	
	public Line(Vector2d directionalVector, Patient from) {
		Vector2d normal = directionalVector.getNormalVector();
		
		double a = normal.getDx(), b = normal.getDy();
		double x = from.getX() * 1.1; // plus more 10%
		double y = (a*from.getX() + b*from.getY() - a*x) / b;
		Patient temp = new Patient();
		temp.setX(x);
		temp.setY(y);
		
		this.from = from;
		this.to = temp;
	}
	
	public Patient getTo() {
		return to;
	}
	
	public Patient getFrom() {
		return from;
	}	

	public boolean isConnected(Line line) {
		if (from.duplicate(line.from) || to.duplicate(line.to))
			return true;
		return false;
	}
	
	public boolean contains(Patient p) {
		if (p.duplicate(from) || p.duplicate(to))
			return true;
		
		if (p.getEuclideanDistance(from) + p.getEuclideanDistance(to) > from.getEuclideanDistance(to)) {
			return false;
		}
		
		return true;
	}
	
	public Patient getIntersectVirtualPoint(Line line) {
		Vector2d n1 = getNormalVector();
		Vector2d n2 = line.getNormalVector();
		double a = n1.getDx(), b = n1.getDy();
		double c = n2.getDx(), d = n2.getDy();
		double x1 = from.getX(), y1 = from.getY();
		double x2 = line.getFrom().getX(), y2 = line.getFrom().getY();
		
		Patient p = new Patient();
		double x = (1/(a*d - b*c)) * (d*(a*x1 + b*y1) - b*(c*x2 + d*y2));
		double y = ((a*x1 + b*y1) - a*x) / b;
		p.setX(x);
		p.setY(y);
		
		return p;
	}
	
	public double distance (Patient p) {
		Line temp = new Line(p, from);
		Vector2d n = getNormalVector();
		
		return Math.abs(temp.getDirectionalVector().dotProduct(n)) / n.norm2();
	}
	
	public Line getProjectionOn(Line line) {
		Line orthogonal = new Line(line.getNormalVector(), this.from);
		Patient fromProjection = line.getIntersectVirtualPoint(orthogonal);
		
		orthogonal = new Line(line.getNormalVector(), this.to);
		Patient toProjection = line.getIntersectVirtualPoint(orthogonal);
		
		return new Line(fromProjection, toProjection);
	}
	
	public Vector2d getDirectionalVector() {
		return new Vector2d(getDx(), getDy());
	}
	
	public Vector2d getNormalVector () {
		return new Vector2d(- getDy(), getDx());
	}
	
	public double getDx() {
		return to.getX() - from.getX();
	}
	
	public double getDy() {
		return to.getY() - from.getY();
	}
	
	public double length() {
		return from.getEuclideanDistance(to);
	}
	
	public boolean equals(Line line) {
		if ((from.duplicate(line.from) && to.duplicate(line.to)) ||
			(from.duplicate(line.to) && to.duplicate(line.from))) {
			return true;
		}
		
		return false;
	}
}
