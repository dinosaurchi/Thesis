package object;

import java.util.ArrayList;

public class KeyList {
	public ArrayList<Integer> keysList = new ArrayList<>();

	public KeyList(ArrayList<Integer> keysList) {
		this.keysList = keysList;
	}

	@Override   
	public boolean equals(Object obj) {
		if (!(obj instanceof KeyList))
			return false;
		KeyList ref = (KeyList) obj;
		if (ref.getKeyList().size() != this.keysList.size())
			return false;
		
		for (int i = 0 ; i < this.keysList.size(); ++i) {
			if (ref.getKeyList().get(i) != this.keysList.get(i))
				return false;
		}
		return true;
	}
	
	public ArrayList<Integer> getKeyList() {
		return keysList;
	}

	@Override
	public int hashCode() {
		if (keysList.size() == 0)
			return 0;
		int result = keysList.get(0).hashCode();
		for (Object obj : keysList) {
			result ^= obj.hashCode();
		};	
		return result;
	}
}
