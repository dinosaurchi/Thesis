package object;

public class Vector2d {
	private double dx = 0;
	private double dy = 0;

	public Vector2d (double dx, double dy) {
		this.dx = dx;
		this.dy = dy;
	}
	
	public double dotProduct(Vector2d v) {
		return getDx() * v.getDx() + getDy() * v.getDy();
	}
	
	public Vector2d scalarProduct(double c) {
		return new Vector2d(c * dx, c * dy);
	}
	
	public Vector2d plus (Vector2d v) {
		return new Vector2d(v.dx + dx, v.dy + dy);
	}
	
	public Vector2d minus (Vector2d v) {
		return new Vector2d(v.dx - dx, v.dy - dy);
	}
	
	public Vector2d projectionOnV(Vector2d v) {
		return v.scalarProduct(this.dotProduct(v) / (Math.pow(v.norm2(), 2)));
	}
	
	public Vector2d getNormalVector () {
		return new Vector2d(- dy, dx);
	}
	
	public double norm2() {
		return Math.sqrt(dx * dx + dy * dy);
	}
	
	public double getDy() {
		return dy;
	}
	public void setDy(double dy) {
		this.dy = dy;
	}
	public double getDx() {
		return dx;
	}
	public void setDx(double dx) {
		this.dx = dx;
	}
	
	
}
