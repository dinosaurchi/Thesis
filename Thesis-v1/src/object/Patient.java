package object;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import handler.UserHandler;
import object.District;
import object.Patient;
import support.SupportFunction;

public class Patient implements Comparable<Patient>{

	public static final String REGEX =   "(.*)\\s*"
								  		+ "\\("
								  		+ "(["
								  		+ "\\d\\,\\s\\w"
								  		+ "]+)"
								  		+ "\\).*"
								  		+ "|"
								  		+ "(.*)";
	private static final String MAP_PATIENT_ID_FILE = "MapPatientId.txt";
	public static final String INFO_FILE = "PatientInfo-156.txt";
	private static final String MATRIX_DISTANCE_FILE = "MatrixDistance.txt";
	public static final int CLSC_ID = 0;
	public static final int NONE_EXIST_PATIENT = -1;
	
	// Visit type
	public static final int UNKNOWN_TYPE = 0;
	public static final int NO_FOLLOW_UP_NURSE = 0;
	public static final int TYPE_REGULAR = 1;
	public static final int TYPE_PRISE_SANG = 2;
	private static final String API_KEYS_FILE = "ApiKeys.txt";
	
	// Other
	public static Map<Key, Double> timeMatrixDistance = new HashMap<Key, Double>();
	
	private boolean isVisibleForPlotting = false;

	// Patient info
	private double averageTreatmentTime = 0.0;
	private int numberOfVisits = 0;

	private int pid = NONE_EXIST_PATIENT;
	private VisitData data = new VisitData();
	private int [] monthVisits = new int [12];
	private int followUpNurseId = NO_FOLLOW_UP_NURSE;
	private int typeVisit = UNKNOWN_TYPE;
	private District district = null;
	private String address = "";
	public VisitData getData() {
		return data;
	}

	public void setData(VisitData data) {
		this.data = data;
	}

	public void setAverageTreatmentTime(double averageTreatmentTime) {
		this.averageTreatmentTime = averageTreatmentTime;
	}

	public void setTypeVisit(int typeVisit) {
		this.typeVisit = typeVisit;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	private double x = 0;
	private double y = 0;
	
	// Comparator
	private static Comparator<Patient> xComparator = new Comparator<Patient>() {
    	public int compare(Patient p1, Patient p2) {
    		if (p1.x > p2.x)
    			return 1;
    		else if (p1.x < p2.x)
    			return -1;
    		return 0;
    	}
    };
    private static Comparator<Patient> yComparator = new Comparator<Patient>() {
    	public int compare(Patient p1, Patient p2) {
    		if (p1.y > p2.y)
    			return 1;
    		else if (p1.y < p2.y)
    			return -1;
    		return 0;
    	}
    };	
    
    // Info Map
    private static Map<Integer, String> patientIdMap = new HashMap<Integer, String>();
    private static Map<String, Integer> patientReverseIdMap = new HashMap<String, Integer>();
    private static Map<String, Boolean> isPatientAdded = new HashMap<String, Boolean>();
    private static Map<Integer, Patient> patientIdToPatientMap = new HashMap<Integer, Patient>();
    public static Map<Patient, ArrayList<Patient>> adjacentListMap = new HashMap<>();
    
    public static void importMappingData(ArrayList<Patient> patients) throws IOException {
		System.out.print("Importing file '" + SupportFunction.getFileName(MAP_PATIENT_ID_FILE) + "' ... ");
    	BufferedReader br = new BufferedReader(new FileReader(MAP_PATIENT_ID_FILE));
    	try {
    	    String line = br.readLine();
    	    while (line != null) {
    	    	String[] temp = line.split(" ");
        	    int id = Integer.parseInt(temp[0]);
        	    patientIdMap.put(id, temp[1]);
        	    patientReverseIdMap.put(temp[1], id);
        	    isPatientAdded.put(temp[1], true);
        	    
    	        line = br.readLine();
    	    }
    	} finally {
    	    br.close();
    	}
    	
    	for (Patient p : patients) {
    		patientIdToPatientMap.put(p.getPid(), p);
    	}
    	System.out.println("Successful!");
    }
    
    public static void exportMappingData() {
    	System.out.print("Exporting file '" + SupportFunction.getFileName(MAP_PATIENT_ID_FILE) + "' ... ");
		try {
            FileWriter writer = new FileWriter(MAP_PATIENT_ID_FILE);
//            for (Integer key : patientIdMap.keySet()) 
//            	writer.write(key + " " + patientIdMap.get(key) + " \n");
            
            for (Map.Entry<Integer, String> entry : patientIdMap.entrySet())
            	writer.write(entry.getKey() + " " + entry.getValue() +  " \n");
            writer.close();
            System.out.println("Successful!");
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
		System.out.println("Error!");
	}
    
    // Counting variable for reading data
    private static int currentPatientIntId = 0;	

    
    // XML Writing-Parsing
    
//    public boolean exportXML() {
//    	System.out.print("Exporting to '" + getFileName(filePath) + "' ... ");    
//
//        File file = new File(filePath);
//        if (!file.exists()) {
//        	System.out.println("File does not exist");
//        	return false;
//        }
//
//        return false;
//    }
    
    public static void exportMatrixDistance() {
    	System.out.print("Exporting file '" + SupportFunction.getFileName(MATRIX_DISTANCE_FILE) + "' ... ");
    	try {
            FileWriter writer = new FileWriter(MATRIX_DISTANCE_FILE);
            
            for (Entry<Key, Double> entry : timeMatrixDistance.entrySet()) {
            	int id1 = (Integer) entry.getKey().key1;
            	int id2 = (Integer) entry.getKey().key2;
            	writer.write(id1 + " " + id2 + " " + entry.getValue() +  " \n");
            }
            writer.close();
            System.out.println("Successful!");
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
    	System.out.println("Error!");
    }
    
    public static void importMatrixDistance() throws IOException {
    	String filePath = MATRIX_DISTANCE_FILE;
		System.out.print("Importing file '" + SupportFunction.getFileName(filePath) + "' ... ");
		
		BufferedReader br = new BufferedReader(new FileReader(filePath));
    	try {
    	    String line = br.readLine();
    	    while (line != null) {
    	    	String[] temp = line.split(" ");
        	    int id1 = Integer.parseInt(temp[0]);
        	    int id2 = Integer.parseInt(temp[1]);
        	    double time = Double.parseDouble((temp[2]));
        	    
        	    Key key = new Key(id1, id2);
        	    timeMatrixDistance.put(key, time);
        	    
    	        line = br.readLine();
    	    }
    	} finally {
    	    br.close();
    	}
    	System.out.println("Successful!");
	}
    
// ============================== METHOD ==============================     
    
	public Patient() {
		Arrays.fill(monthVisits, 0);
	}
	
	// Excel file parsing

	private static ArrayList<String> parsePatientList(Row row) {
		Cell cell = null;
		
		ArrayList<String> result = new ArrayList<>();
		int i = 0;
		while (true) {
			cell = row.getCell(i + 5);
			if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK || cell.toString().length() == 0)
				break;
						
			result.add(cell.getStringCellValue());
			++i;
		}
		
		return result;
	}
	
	private static String [] apiKeys = {};
	
	
	public static void getDataFromGoogleMap(ArrayList<Patient> patients) throws InterruptedException {
		int count = 0;
		int k = 0;
		try {
			apiKeys = importApiKeys(API_KEYS_FILE);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.print("Getting matrix distance data from Google Map ... ");
		
		int n = patients.size();
		for (int i = 0 ; i < n ; ++i) {
			++count;
			Patient origin = patients.get(i);
			int id1 = origin.getPid();
			ArrayList<Patient> destinations = new ArrayList<>();
			for (int j = i ; j < n ; ++j) {
				int id2 = patients.get(j).getPid();
				Key key = new Key(id1, id2);
				
				if (id1 == id2) {
					timeMatrixDistance.put(key, 0.0);
					continue;
				}
				
				if (!timeMatrixDistance.containsKey(key)) {
					destinations.add(patients.get(j));
				}
			
				if (destinations.size() == 100) {
					String url = SupportFunction.googleMapServiceUrlGenerator(origin, destinations, apiKeys[k]);
					try {
						TimeUnit.SECONDS.sleep(4);
						
						boolean flag = expandingMatrixDistance(url, origin, destinations);
						while (!flag && k < apiKeys.length){
							url = SupportFunction.googleMapServiceUrlGenerator(origin, destinations, apiKeys[k]);
							TimeUnit.SECONDS.sleep(15);
							flag = expandingMatrixDistance(url, origin, destinations);
							++k;
						}
						if (!flag || k >= apiKeys.length)
							return;
						System.out.println(" (Total matrix elements : " + timeMatrixDistance.size() + ")");
						
					} catch (IOException | JSONException e) {
						e.printStackTrace();
					}
					exportMatrixDistance();
					destinations = new ArrayList<>();
				}
			}
			if (destinations.size() > 0) {
				String url = SupportFunction.googleMapServiceUrlGenerator(origin, destinations, apiKeys[k]);
				try {
					TimeUnit.SECONDS.sleep(4);
					boolean flag = expandingMatrixDistance(url, origin, destinations);
					while (!flag && k < apiKeys.length){
						url = SupportFunction.googleMapServiceUrlGenerator(origin, destinations, apiKeys[k]);
						TimeUnit.SECONDS.sleep(15);
						flag = expandingMatrixDistance(url, origin, destinations);
						++k;
					}
					if (!flag || k >= apiKeys.length)
						return;
					
					System.out.println(" (Total matrix elements : " + timeMatrixDistance.size() + ")");
				} catch (IOException | JSONException e) {
					e.printStackTrace();
				}
				exportMatrixDistance();
				destinations = new ArrayList<>();
			}
			origin.setVisibleForPlotting(true);
			System.out.println(" (Total visible patient : " + getTotalVisiblePatient(patients) + ")");
		}

	}
	
	private static String[] importApiKeys(String apiKeysFile) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(apiKeysFile));
		ArrayList<String> keys = new ArrayList<>();
    	try {
    	    String line = br.readLine();
    	    while (line != null) {
    	    	keys.add(line);
    	    	line = br.readLine();
    	    }
    	} finally {
    	    br.close();
    	}
    	
    	String [] keysArray = new String [keys.size()];
    	for (int i = 0 ; i < keysArray.length ; ++i) 
    		keysArray[i] = keys.get(i);
		return keysArray;
	}

	private static int getTotalVisiblePatient(ArrayList<Patient> patients) {
		int count = 0;
		for (Patient p : patients) {
			if (p.isVisibleForPlotting)
				++count;
		}
		return count;
	}

	private static boolean expandingMatrixDistance(String stringUrl, Patient origin, ArrayList<Patient> destinations) throws IOException, JSONException {
		URL url = new URL(stringUrl);
		 
	    // read from the URL
	    Scanner scan = new Scanner(url.openStream());
	    String str = new String();
	    while (scan.hasNext())
	        str += scan.nextLine();
	    scan.close();
	 
	    // build a JSON object
	    JSONObject obj = new JSONObject(str);
	    if (! obj.getString("status").equals("OK")) {
	    	System.out.println("Error: Rows");
	        return false;
	    }
	 
	    int id1 = origin.getPid();
	    
	    // get the first result
	    JSONArray rows = obj.getJSONArray("rows");
	    for (int i = 0 ; i < rows.length() ; ++i) {
	    	JSONArray elements = rows.getJSONObject(i).getJSONArray("elements");
	    	for (int j = 0 ; j < elements.length() ; ++j) {
	    		JSONObject e = elements.getJSONObject(j);
	    		if (!e.getString("status").equals("OK")) {
	    			System.out.println("Error: element " + j);
	    	        return false;
	    		}
	    		int id2 = destinations.get(j).getPid();
				Key key = new Key(id1, id2);
	    		double time = e.getJSONObject("duration").getInt("value");
	    		timeMatrixDistance.put(key, time / 60.0);
	    	}
	    }
	    
	    System.out.println("Successful!");
	    return true;
	}

	public static boolean expandMatrixTimeDistance(File inputFile) {
		try {
			
			// Get the workbook instance for XLS file
			NPOIFSFileSystem input = new NPOIFSFileSystem(inputFile);
			HSSFWorkbook workbook = new HSSFWorkbook(input.getRoot(), true);
			
			// Get first sheet from the workbook
			HSSFSheet sheet = workbook.getSheetAt(0);
			Cell cell = null;
			Row row = null;

			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();
			
			//Parse list of nurse's IDs
			ArrayList<Integer> nurseIntegerIdList = District.parseNursesIntegerIdList(rowIterator);
			if (nurseIntegerIdList == null) {
				System.out.println("Parsing Nurse Id list Error !");
				input.close();
				return false;
			}
			
			ArrayList<String> patientIdList = null;
			while (rowIterator.hasNext()) {
				//Parse the list of patient codes (horizontally, not vertically)
				row = rowIterator.next();
				
				if (row.getCell(1) != null && row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && row.getCell(1).getStringCellValue().equals("Type visite")) {
					patientIdList = parsePatientList(row);
					break;
				}
			}
			if (patientIdList == null) {
				System.out.println("Parsing Patient Id list Error !");
				input.close();
				return false;
			}
			
			if (rowIterator.hasNext()) {
				row = rowIterator.next();
				if (row.getCell(0).getStringCellValue().equals("CLSC")) {
					Iterator<Cell> cellIterator = row.cellIterator();
					
					//Find the 1st position of matrix distance (top-left corner of the matrix)
					while (cellIterator.hasNext()) {
						cell = cellIterator.next();
						if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
							break;
					}
					
					if (cell == null) { 
						System.out.println("CLSC Cell Null Error !");
						input.close();
						return false;
					}
					
					//Read matrix distance from CLSC to the patients
					for (int i = 0; i < patientIdList.size(); ++i) {
						int id = Patient.getPatientIntegerId(patientIdList.get(i));
						if (id == Patient.NONE_EXIST_PATIENT)
							continue;
						cell = row.getCell(5 + i);
						Key key = new Key(0, id);
						Double value = cell.getNumericCellValue();
						if (!timeMatrixDistance.containsKey(key))
							timeMatrixDistance.put(key, value);
					}
				}	
				
				//Read matrix distance for patients
				for (int i = 0 ; i < patientIdList.size(); ++i) {
					row = rowIterator.next();
					
					int id1 = Patient.getPatientIntegerId(patientIdList.get(i));
					if (id1 == Patient.NONE_EXIST_PATIENT)
						continue;
					
					Patient p = Patient.getPatient(id1);
					if (p == null) {
						System.out.println("Patient null, in Matrix distance Error!");
						input.close();
						return false;
					}
					//Parse the follow up id
					Double temp = row.getCell(2).getNumericCellValue();
					int followUpNurseId = temp.intValue();
					
					if (followUpNurseId != 0) {
						p.setFollowUpNurseId(nurseIntegerIdList.get(followUpNurseId - 1));
					}
					
					p.setTypeVisit(row.getCell(1).getStringCellValue());
					p.addVisitTreatmentTime(row.getCell(4).getNumericCellValue());
					p.setVisibleForPlotting(true);
					
					for (int j = i ; j < patientIdList.size(); ++j){
						int id2 = Patient.getPatientIntegerId(patientIdList.get(j));
						
						if (id2 == Patient.NONE_EXIST_PATIENT)
							continue;
					
						cell = row.getCell(5 + j);
						
						Key key = new Key(id1, id2);
						Double value = cell.getNumericCellValue();
						
						if (!timeMatrixDistance.containsKey(key))
							timeMatrixDistance.put(key, value);
					}
				}
			}
			else { 
				System.out.println("Matrix distance doesnt exist !");
				input.close();
				return false;
			}
			
			input.close();
			return true;
		} 

		catch (FileNotFoundException e) {
			System.err.println("Exception" + e.getMessage());
		}
		catch (IOException e) {
			System.err.println("Exception" + e.getMessage());
		}
		
		return false;
	}
	
	private static int parseMonth(String month) {
		switch (month) {
		case "sept":
			return 9;

		default:
			return 0;
		}
	}
	
	public boolean parseDaysMonthOfData(String daysMonthString){
		String [] temp = daysMonthString.split(" ");
		if (temp.length == 0) 
			return false;
		
		for (int i = 0; i < temp.length - 1; ++i) {
			if (temp[i].length() == 0)
				continue;
			String [] temp2 = temp[i].split(",");
			if (temp2.length == 0) 
				return false;
				
			for (String s : temp2) {
				if (s.length() == 0 || s.charAt(0) < '0' || s.charAt(0) > '9')
					return false;
				data.addDay(Integer.parseInt(s));	
			}
		}
		
		String temp2 = temp[temp.length - 1];
		if (temp2.length() == 0)
			return false;
		
		if (temp2.charAt(0) < 'a' || temp2.charAt(0) > 'z')
			return false;
		
		data.setMonth(parseMonth(temp2));
		if (data.getMonth() == 0)
			return false;
		
		return true;
	}

	// --------------------------------------------------------------------------------
	
	// Plotting
	
	public boolean isVisibleForPlotting() {
		return isVisibleForPlotting;
	}

	public void setVisibleForPlotting(boolean isVisibleForPlotting) {
		this.isVisibleForPlotting = isVisibleForPlotting;
	}
	
	// --------------------------------------------------------------------------------
	
	public VisitData getVisitData() {
		return data;
	}
	
	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}
	
	public void addVisit(int month) {
		++monthVisits[month - 1];
	}

	public int getFollowUpNurseId() {
		return followUpNurseId;
	}

	public void setFollowUpNurseId(int followUpNurseId) {
		this.followUpNurseId = followUpNurseId;
	}
	
	public static void addPatientIdToPatientMap(Integer pid, Patient p) {
		if (patientIdToPatientMap.containsKey(pid))
			return;
		patientIdToPatientMap.put(pid, p);
	}

	public int getTypeVisit() {
		return typeVisit;
	}

	public void setTypeVisit(String typeVisit) {
		switch (typeVisit) {
		case "PriseSang":
			this.typeVisit = TYPE_PRISE_SANG;
			
		case "Reguliere":
			this.typeVisit = TYPE_REGULAR;
	
		}
	}
	
	public void setTypeVisit(Integer typeVisit) {
		switch (typeVisit) {
		case TYPE_PRISE_SANG:
			this.typeVisit = TYPE_PRISE_SANG;
			
		case TYPE_REGULAR:
			this.typeVisit = TYPE_REGULAR;
	
		}
	}
	
	public void addVisitTreatmentTime(double treatmentTime) {
		double temp = averageTreatmentTime * (double) numberOfVisits;
		temp += treatmentTime;
		++numberOfVisits;
		averageTreatmentTime = temp / (double) numberOfVisits;
	}
	
	public double getAverageTreatmentTime() {
		if (numberOfVisits == 0)
			return 0;
		
		return averageTreatmentTime;
	}

	public double getNumberOfVisits() {
		return numberOfVisits;
	}
	
	public int compareTo(Patient pb) {
		if (this.y < pb.y)
            return -1;
        if (this.y > pb.y)
            return +1;
        if (this.x < pb.x)
            return -1;
        if (this.x > pb.x)
            return +1;
        return 0;
	}
	
	public static int getPatientIntegerId(String pid){
		if (patientReverseIdMap.containsKey(pid))
			return patientReverseIdMap.get(pid);
		return NONE_EXIST_PATIENT;
	}
	
	public static Patient getPatient(int pid) {
		if (patientIdToPatientMap.containsKey(pid))
			return patientIdToPatientMap.get(pid);
		return null;
	}
	
	public void setNumberOfVisits(int numberOfVisits) {
		this.numberOfVisits = numberOfVisits;
	}
	
	public boolean parsePatient(Iterator<Cell> cellIterator, Boolean [] stopFlag, Boolean [] duplicatedFlag) {		
		Pattern pattern = Pattern.compile(REGEX);
		String temp;
		
		// Parsing ID
		if (!cellIterator.hasNext()) {
			System.out.println("Parsing ID Error");
			return false;
		}
		
		// Get patient String ID
		temp = cellIterator.next().getStringCellValue();
		if (temp.isEmpty()) {
			stopFlag[0] = true;
			return true;
		}
		
		
		if (isPatientAdded.containsKey(temp)) {
			duplicatedFlag[0] = true;
			return true;
		}
		
		isPatientAdded.put(temp, true);
				
		++currentPatientIntId;
		if (!patientIdMap.containsValue(temp)) {
			patientIdMap.put(currentPatientIntId, temp);
			patientReverseIdMap.put(temp, currentPatientIntId);
		}
		
		// current id is already increased for the next patient
		setPid(currentPatientIntId);
		
		// Parsing address info
		if (!cellIterator.hasNext()) {
			System.out.println("Parsing Address Info Error");
			return false;
		}
		temp = cellIterator.next().getStringCellValue();
		Matcher m = pattern.matcher(temp);
		
		if (!m.find()) {
			System.out.println("Not found pattern Error");
			return false;
		}
		
		if (m.group(1) != null) {
			// Parse address
			this.address = m.group(1).toLowerCase();
			// Parse matrix matrix distance data links
			if (!parseDaysMonthOfData(m.group(2).toLowerCase())) {
				System.out.println("Parsing Days Month Error");
				return false;
			}
		}
		else if (m.group(2) == null) {
			if (m.group(3) != null)
				this.address = m.group(3).toLowerCase();
			else { 
				System.out.println("Parsing Address-Only Error");
				return false;
			}
		}
		else { 
			System.out.println("Parsing Pattern Error");
			return false;
		}
		
		if (!cellIterator.hasNext()) {
			System.out.println("Parsing X-coordinate Error");
			return false;
		}
		this.x = cellIterator.next().getNumericCellValue();
		
		if (!cellIterator.hasNext()) {
			System.out.println("Parsing Y-coordinate Error");
			return false;
		}
		this.y = cellIterator.next().getNumericCellValue();
		
		if (!patientIdToPatientMap.containsKey(currentPatientIntId))
			patientIdToPatientMap.put(currentPatientIntId, this);
		
		Patient.addPatientIdToPatientMap(getPid(), this);
		
		return true;
	}
	
	public double getEuclideanDistance(Patient other) {
		return Math.sqrt(Math.pow(x - other.x, 2) + Math.pow(y - other.y, 2));
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}
	
	public String getAddress() {
		return address;
	}

	public static Comparator<Patient> getxComparator() {
		return xComparator;
	}

	public static Comparator<Patient> getyComparator() {
		return yComparator;
	}
		
	public double getTravelingTimeDistance(Patient p) {
		int id1 = this.getPid();
		int id2 = p.getPid();
		Key key = new Key(id1, id2);
		
		if (timeMatrixDistance.containsKey(key)) {
			return timeMatrixDistance.get(key);
		}
		return Double.MAX_VALUE;
	}
	
	public boolean isInvisible(){
		return !isVisibleForPlotting;
	}
	
	public void copy(Patient p) {
		this.address = p.address;
		this.district = p.district;
		this.x = p.x;
		this.y = p.y;
		this.data = p.data;
		this.followUpNurseId = p.followUpNurseId;
		this.averageTreatmentTime = p.averageTreatmentTime;
		this.isVisibleForPlotting = p.isVisibleForPlotting;
		this.monthVisits = p.monthVisits;
		this.numberOfVisits = p.numberOfVisits;
		this.pid = p.pid;
		this.typeVisit = p.typeVisit;
	}

	// XML writing-parsing
	
	public void exportXmlData(PrintWriter writer) {
		char a = '"';
		writer.write("<Patient>\n");
		writer.write("   <pid v=" + a + pid + a + "></pid>\n");
		writer.write("   <typeVisit v=" + a + typeVisit + a + "></typeVisit>\n");
		writer.write("   <x v=" + a + x + a + "></x>\n");
		writer.write("   <y v=" + a + y + a + "></y>\n");
		writer.write("   <numberOfVisits v=" + a + numberOfVisits + a + "></numberOfVisits>\n");
		writer.write("   <isVisibleForPlotting v=" + a + isVisibleForPlotting + a + "></isVisibleForPlotting>\n");
		writer.write("   <followUpNurseId v=" + a + followUpNurseId + a + "></followUpNurseId>\n");
		writer.write("   <address v=" + a + address + a + "></address>\n");
		writer.write("   <averageTreatmentTime v=" + a + averageTreatmentTime + a + "></averageTreatmentTime>\n");
		writer.write("   <monthVisits v=" + a); 
		for (int i = 0 ; i < monthVisits.length ; ++i) {
			writer.write((monthVisits[i]) + " ");
		}
		writer.write(a + "></monthVisits>\n");
		data.exportXmlData(writer);
		writer.write("</Patient>\n");
	}

	public static String getPatientStringId(int pid) {
		if (patientIdMap.containsKey(pid)) {
			return patientIdMap.get(pid);
		}
		return null;
	}
	
	public static void generateRandomInfo(ArrayList<Patient> patients) {
		Random rand = new Random();
		for (Patient p : patients) {
			p.setAverageTreatmentTime(rand.nextInt(40) + 1);
			p.setNumberOfVisits(rand.nextInt(3) + 1);
		}
	}

	public boolean duplicate(Patient p) {
		if (p.x == x && p.y == y)
			return true;
		return false;
	}
}
