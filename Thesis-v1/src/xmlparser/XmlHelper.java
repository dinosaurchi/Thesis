package xmlparser;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import handler.UserHandler;
import object.Patient;

public class XmlHelper {

	public static ArrayList<Patient> readXmlPatientInfo(String filePath) {
		try {	
			File inputFile = new File(filePath);
			if (!inputFile.exists()) {
				System.out.println("Error: File does not exist!");
				return null;
			}

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			UserHandler userhandler = new UserHandler();
			saxParser.parse(inputFile, userhandler);     
			
			System.out.println("Successfull!");
			
			return userhandler.getPatients();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
