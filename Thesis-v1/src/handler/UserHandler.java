package handler;

import java.awt.Color;
import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import object.District;
import object.Patient;
import object.VisitData;

public class UserHandler extends DefaultHandler {
//
//	boolean bFirstName = false;
//	boolean bLastName = false;
//	boolean bNickName = false;
//	boolean bMarks = false;

	private ArrayList<Patient> patients = new ArrayList<>();
	private Patient p = null;
	
	private ArrayList<District> districts = new ArrayList<>();
	private District d = null;
	
	private boolean flag = true;
	
	@Override
	public void startElement(String uri, 
			String localName, String qName, Attributes attributes)
					throws SAXException 
	{
		if (qName.equalsIgnoreCase("PatientInfo")) {
			flag = true;
		}
		if (qName.equalsIgnoreCase("DistrictInfo")) {
			flag = false;
		}
		
		if (flag) {
			if (qName.equalsIgnoreCase("CLSC")) {
				double x = Double.parseDouble(attributes.getValue("x"));
				double y = Double.parseDouble(attributes.getValue("y"));
				Patient clsc = new Patient();
				clsc.setPid(Patient.CLSC_ID);
				clsc.setX(x);
				clsc.setY(y);
				patients.add(clsc);
	
			} else if (qName.equalsIgnoreCase("Patient")) {
				p = new Patient();
	
			} else if (qName.equalsIgnoreCase("pid")) {
				p.setPid(Integer.parseInt(attributes.getValue("v")));
	
			} else if (qName.equalsIgnoreCase("typeVisit")) {
				p.setTypeVisit(Integer.parseInt(attributes.getValue("v")));
			}
			else if (qName.equalsIgnoreCase("x")) {
				p.setX(Double.parseDouble(attributes.getValue("v")));
			}
			else if (qName.equalsIgnoreCase("y")) {
				p.setY(Double.parseDouble(attributes.getValue("v")));
			}
			else if (qName.equalsIgnoreCase("numberOfVisits")) {
				p.setNumberOfVisits(Integer.parseInt(attributes.getValue("v")));
			}
			else if (qName.equalsIgnoreCase("isVisibleForPlotting")) {
				p.setVisibleForPlotting(Boolean.parseBoolean(attributes.getValue("v")));
			}
			else if (qName.equalsIgnoreCase("followUpNurseId")) {
				p.setFollowUpNurseId(Integer.parseInt(attributes.getValue("v")));
			}
			else if (qName.equalsIgnoreCase("address")) {
				p.setAddress(attributes.getValue("v"));
			}
			else if (qName.equalsIgnoreCase("averageTreatmentTime")) {
				p.setAverageTreatmentTime(Double.parseDouble(attributes.getValue("v")));
			}
			else if (qName.equalsIgnoreCase("monthVisits")) {
				String temp = attributes.getValue("v");
				String [] stringMonths = temp.split(" ");
				for (int i = 0 ; i < stringMonths.length ; ++i) {
					int visitTimes = Integer.parseInt(stringMonths[i]);
					for (int j = 0 ; j < visitTimes ;++j) {
						p.addVisit(i);
					}
				}
			}
			else if (qName.equalsIgnoreCase("VisitData")) {
				VisitData data = new VisitData();
				data.setMonth(Integer.parseInt(attributes.getValue("month")));
				
				String temp = attributes.getValue("days");
				String [] stringDays = temp.split(" ");
				for (String d : stringDays) {
					if (d.length() > 0)
						data.addDay(Integer.parseInt(d));
				}
				p.setData(data);
			}
		}
		else {
			if (qName.equalsIgnoreCase("District")) {
				d = new District();
				d.setId(Integer.parseInt(attributes.getValue("id")));

				int r, g, b;
				String [] temp = attributes.getValue("color").split(" ");
				r = Integer.parseInt(temp[0]);
				g = Integer.parseInt(temp[1]);
				b = Integer.parseInt(temp[2]);

				Color c = new Color(r, g, b);
				d.setColor(c);

				districts.add(d);
			}
		}
	}

	@Override
	public void endElement(String uri, 
			String localName, String qName) throws SAXException {
		if (qName.equalsIgnoreCase("Patient")) {
			if (p != null)
				patients.add(p);
			p = null;
		}
	}

//	@Override
//	public void characters(char ch[], 
//			int start, int length) throws SAXException {
//		if (bFirstName) {
//			System.out.println("First Name: " 
//					+ new String(ch, start, length));
//			bFirstName = false;
//		} else if (bLastName) {
//			System.out.println("Last Name: " 
//					+ new String(ch, start, length));
//			bLastName = false;
//		} else if (bNickName) {
//			System.out.println("Nick Name: " 
//					+ new String(ch, start, length));
//			bNickName = false;
//		} else if (bMarks) {
//			System.out.println("Marks: " 
//					+ new String(ch, start, length));
//			bMarks = false;
//		}
//	}

	public ArrayList<District> getDistricts() {
		return districts;
	}
	
	public ArrayList<Patient> getPatients() {
		return patients;
	}
}
