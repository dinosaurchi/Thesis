package program;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import Voronoi.GraphEdge;
import Voronoi.Voronoi;
import handler.UserHandler;
import object.Action;
import object.District;
import object.Key;
import object.Patient;
import object.Record;
import object.Solution;
import support.BarChartComponent;
import support.ClusterComponent;
import support.RandomSeedingComponent;
import support.SupportFunction;

public class Districting {
	public PrintWriter writer = null;
	
	private Patient CLSC;
	private ArrayList<District> districts = new ArrayList<>();
	private Solution bestSolution = new Solution();
	
	private class TabuList {
		Map<Key, Boolean> tabuMap = new HashMap<>();
		ArrayList<Key> tabuList = new ArrayList<>();
		private int MAX_TABU_LENGTH = 0;
		
		public TabuList(){
			MAX_TABU_LENGTH = 2 * ((int) Math.round(Math.sqrt(getTotalPatient() * districts.size())));
			//MAX_TABU_LENGTH = 70;
		}
		public void addTabu(Patient pb, District d){
			Key k = new Key(pb, d);
			if (!tabuMap.containsKey(k)) {
				if (tabuList.size() == MAX_TABU_LENGTH) {
					Key temp = tabuList.remove(0);
					tabuMap.remove(temp);
				}
				
				tabuList.add(k);
				tabuMap.put(k, true);
			}
		}
		public boolean isTabu(Patient pb, District d) {
			Key k = new Key(pb, d);
			if (tabuMap.containsKey(k))
				return true;
			return false;
		}
		public int size() {
			return tabuList.size();
		}
	}
	
	// -------------------------------------------------------

	private static final double MAX_WORKING_HOURS = 400;
	public static int TOTAL_NURSES = 0;
	public static int MAX_TRIALS = 1;
	public static int MAX_ITERATION = 500;
	
	// ----------------------------------------------------------------------------------------
	
	// Plotting
	
	public static int test_time = 0;

	
	// ----------------------------------------------------------------------------------------
	
// ================================= METHODS ==================================
	
	private Patient getGeneralCenterGravity(ArrayList<Patient> patients) {
		double x = 0, y = 0 ;
		for (Patient p : patients) {
			x += p.getX();
			y += p.getY();
		}
		Patient temp = new Patient();
		temp.setX(x);
		temp.setY(y);
		return temp;
	}
	
	private void randomSeedingInitialization(ArrayList<Patient> patients) {
		if (districts.size() == 0)
			return;
		
		Random r = new Random();
		int n, pidx;
		n = patients.size();
		boolean [] mark = new boolean[n];
		Arrays.fill(mark, false);
		
		boolean [] seed = new boolean[n];
		Arrays.fill(seed, false);
		boolean [] fu = new boolean[n];
		Arrays.fill(fu, false);
		
		for (int i = 0 ; i < n; ++i) {
			if (patients.get(i).getFollowUpNurseId() != 0) {
				mark[i] = true;
				fu[i] = true;
				seed[i] = true;
			}
		}
		
		for (int i = 0 ; i < patients.size() ; ++i) {
			if (mark[i])
				continue;
			
			for (int j = i + 1 ; j < patients.size() ; ++j) {				
				Patient p1 = patients.get(i);
				Patient p2 = patients.get(j);
				if (p1.getX() == p2.getX() && p1.getY() == p2.getY()) {
					mark[i] = true;
					break;
				}
			}
		}
		
		for (District d : districts) {
			if (d.getPatients().size() > 0)
				continue;
			
			do {	
				pidx = r.nextInt(n);
			}
			while (mark[pidx]);
			Patient p = patients.get(pidx);		
			
			d.addPatient(p);
			p.setDistrict(d);
			
			seed[pidx] = true;
			mark[pidx] = true;
		}
		
		exportRandomSeedingPlotting(patients, seed, fu);
	}
	
	private void exportRandomSeedingPlotting(ArrayList<Patient> patients, boolean[] seeding, boolean[] hasFollowUpNurse) {
		String photoFileName = "RandomSeeding_" + test_time;
		
		JFrame frame = new JFrame(photoFileName);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(ClusterComponent.GRAPH_WIDTH + ClusterComponent.SHIFT_WIDTH, ClusterComponent.GRAPH_HEIGHT + ClusterComponent.SHIFT_HEIGHT);

		RandomSeedingComponent component = new RandomSeedingComponent(patients, CLSC, seeding, hasFollowUpNurse);
		frame.add(component);
		
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		component.paintComponent(component.getGraphics());
		
		BufferedImage bImg = new BufferedImage(component.getWidth(), component.getHeight(), BufferedImage.TYPE_INT_RGB);
	    Graphics2D cg = bImg.createGraphics();
	    component.paintAll(cg);
	    try {
	    	if (ImageIO.write(bImg, "png", new File("./" + photoFileName + ".png"))){
	    	}
	    	else {
	    		System.out.println("Saving picture error : " + photoFileName);
	    	}
	    	//frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
	    	frame.setVisible(false);
	    	frame.dispose();
	    	
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	}

	private ArrayList<Patient> getPatients() {
		ArrayList<Patient> patients = new ArrayList<>();
		for (District d : districts) {
			for (Patient p : d.getPatients()) {
				patients.add(p);
			}
		}
		return patients;
	}

	private District getBestDistrict(Patient p) {
		if (districts == null || districts.size() <= 0) {
			System.out.println("Error : No districts");
			return null;
		}
		
		District bestDistrict = p.getDistrict();
		District closestDistrict = bestDistrict;
		District temp;
		
		double minFeasibleDistance;
		double minDistance;
		
		if (bestDistrict != null) {
			temp = new District();
			for (Patient pp : bestDistrict.getPatients())
				temp.addPatient(pp);
			
			//double distance = bestDistrict.closestDistance(p);
			double distance = bestDistrict.getCentroid().getEuclideanDistance(p);
			//double workload = temp.getAverageDailyWorkLoad();
			double workload = temp.getSimpleWorkload();
			
			if (bestDistrict.getId() != p.getFollowUpNurseId()) {
				minFeasibleDistance = (distance + 1) * workload;
				minDistance = distance;
			}
			else {
				minFeasibleDistance = (distance + 1) * workload * (1 - getPenalty(distance, workload));
				minDistance = distance * (1 - getPenalty(distance));
			}
			
//			minFeasibleDistance = (distance + 1) * workload;
//			minDistance = distance;
		}
		else {
			minFeasibleDistance = Double.MAX_VALUE;
			minDistance = minFeasibleDistance;
		}
		
		for (District d : districts) {		
			temp = new District();
			for (Patient ppb : d.getPatients())
				temp.addPatient(ppb);
			
			temp.addPatient(p);
			
			double distance = d.getCentroid().getEuclideanDistance(p);
			if (d.getId() == p.getFollowUpNurseId()) {
				distance *= (1 - getPenalty(distance));
			}
			if (distance < minDistance) {
				closestDistrict = d;
				minDistance = distance;
			}
			distance /= (1 - getPenalty(distance));
			
			//double workload = temp.getAverageDailyWorkLoad();
			double workload = temp.getSimpleWorkload();
			
			if (workload > MAX_WORKING_HOURS)
				continue;
			
			distance = (distance + 1) * workload;
			
			if (d.getId() == p.getFollowUpNurseId()) {
				distance *= (1 - getPenalty(distance, workload));
			}
			
			if (distance < minFeasibleDistance) {
				bestDistrict = d;
				minFeasibleDistance = distance;
			}
		}
		
		if (bestDistrict == null) {
			return closestDistrict;
		}
		
		return closestDistrict;
		//return bestDistrict;
		
	}
	
	private double getPenalty(double distance) {
		return (1/Math.exp(distance * 10));
	}

	private double getPenalty(double distance, double workload) {
		//return (1/Math.exp(distance * 10) + 1/Math.exp(workload / 100000));
		return 2 / (Math.exp(distance * 10) + Math.exp(workload / 100000));
	}

	private void calculateCentroids(){
		for (District c : districts) {
			c.calculateCentroid();
		}
	}
		
	private boolean assignDistrict(ArrayList<Patient> patients) {	
		for (Patient p : patients) {
//			if (p.getFollowUpNurseId() != 0)
//				continue;
			
			District bestDistrict = getBestDistrict(p);
			bestDistrict.addPatientWithRemovalFromOldDistrict(p);
		}
				
		for (Patient p : patients) {
			if (p.getDistrict() == null) {
				return false;
			}
		}
		return true;
	}
	
	private void clearState(){
		for (District c : districts) {
			//c.cleanPatientsWithoutFollowUp();
			c.cleanPatients();
		}
		for (Patient p : getPatients()) {
//			if (p.getFollowUpNurseId() != 0)
//				continue;
			p.setDistrict(null);
		}
	}
	
	private int getTotalPatient (){
		int result = 0;
		for (District d : districts) 
			result += d.getTotalPatient();
		return result;
	}
		
	private boolean clustering(ArrayList<Patient> patients) throws FileNotFoundException, UnsupportedEncodingException {
		if (patients.size() == 0)
			return false;
			
		writer = new PrintWriter("Metaheuristic-log.txt", "UTF-8");
		
		double bestEvaluation = Double.MAX_VALUE;
		
		for (int i = 0 ; i < MAX_TRIALS; ++i) {
			randomSeedingInitialization(patients);
			
			int iteration = MAX_ITERATION;
			
			if (!assignDistrict(patients)) {
				writer.write(": Cannot find out solution \n");
				writer.close();
				return false;
			}
			while (iteration > 0) {
				calculateCentroids();
			
				if (!assignDistrict(patients)) {
					writer.write(": Cannot find out solution \n");
					writer.close();
					return false;
				}
				
				//System.out.println(" Iter : " + iteration);
				--iteration;
			}

			double current = getClusteringEvaluation();
			
			writer.write(" (Evaluation Cost = " + current + ")\n");
			
			if (current < bestEvaluation) {
				bestEvaluation = current;
				copyToBestSolution();
			}
			
			clearState();
		}
		writer.write("\nBest cost  = " + bestEvaluation +"\n");
		
		districts = Solution.reconstructSolution(bestSolution);
		
		writer.close();
		return true;
	}
		
	private double getClusteringEvaluation() {
		double total = 0;
	
		for (District d : districts) {
			total += d.getAverageDailyWorkLoad();
		}

		double std = 0;
		double avg = (total / districts.size()) ;
		for (District d : districts) {
			std += Math.pow(d.getAverageDailyWorkLoad() - avg, 2);
		}
		std = Math.sqrt(std / (districts.size() - 1));
		
		return  std * (total / (double) getTotalPatient()) / 10000;
	}
	
	private void swap(ArrayList<Patient> patients, int i, int j) {
		Patient temp1 = patients.get(i);
		Patient temp2 = patients.get(j);
		
		patients.remove(i);
		patients.add(i, temp2);
				
		patients.remove(j);
		patients.add(j, temp1);
	}
	
	private int partition(Patient target, ArrayList<Patient> patients, int low, int high) {
		double pivot = patients.get(high).getEuclideanDistance(target);
		int i = low;
		
		for (int j = low ; j < high; ++j) {
			double temp = patients.get(j).getEuclideanDistance(target);
			if (temp < pivot) {
				swap(patients, i, j);
				++i;
			}
		}
		swap(patients, i, high);
		return i;
	}
	
	private void quickSort(Patient target, ArrayList<Patient> patients, int low, int high) {
		if (low < high) {
			int i = partition(target, patients, low, high);
			quickSort(target, patients, low, i - 1);
			quickSort(target, patients, i + 1, high);
		}
	}
	
	private void sortByClosetTo(Patient target, ArrayList<Patient> patients) {
		quickSort(target, patients, 0, patients.size() - 1);
	}
	
	public void exportPlotting(String photoFileName) {
		System.out.print("Exporting file " + SupportFunction.getFileName(photoFileName) + " ... ");
		JFrame frame = new JFrame(photoFileName);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ClusterComponent comp = new ClusterComponent(districts, CLSC);
				
		frame.add(comp);
		frame.setSize(ClusterComponent.GRAPH_WIDTH + ClusterComponent.SHIFT_WIDTH, ClusterComponent.GRAPH_HEIGHT + ClusterComponent.SHIFT_HEIGHT);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);		
		
		comp.paintComponent(comp.getGraphics());
		
		BufferedImage bImg = new BufferedImage(comp.getWidth(), comp.getHeight(), BufferedImage.TYPE_INT_RGB);    		
		
		Graphics2D cg = bImg.createGraphics();   
	    comp.paintAll(cg);
	    		
	    try {
	    	if (ImageIO.write(bImg, "png", new File("./" + photoFileName + ".png"))){
				System.out.println("Successful!");
	    	}
	    	else {
	    		System.out.println("Saving picture error : " + photoFileName);
	    	}
	    	//frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
	    	frame.setVisible(false);
	    	frame.dispose();
	    		    	
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	}
	
	public void plotClusters(String title) {
		 JFrame frame = new JFrame(title);
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 ClusterComponent comp = new ClusterComponent(districts, CLSC);
		 frame.add(comp);
		 frame.setSize(ClusterComponent.GRAPH_WIDTH + ClusterComponent.SHIFT_WIDTH, ClusterComponent.GRAPH_HEIGHT + ClusterComponent.SHIFT_HEIGHT);
		 frame.setLocationRelativeTo(null);
		 frame.setVisible(true);
	 }
	
	private void removeInvisiblePatients(ArrayList<Patient> patients) {
//		ArrayList<District> tempList = new ArrayList<>();
//		for (District d : districts) {
//			d.removeInvisiblePatients();
//			if (d.getPatients().size() == 0) {
//				tempList.add(d);
//			}
//		}
//		
//		for (District d : tempList)
//			districts.remove(d);
		
		ArrayList<Patient> tempList = new ArrayList<>();
		for (Patient p : patients) {
			if (p.isInvisible()) {
				tempList.add(p);
			}
		}
		
		for (Patient p : tempList) {
			patients.remove(p);
		}
	}
	
	private District getDistrictByNurseId(int nurseId) {
		for (District d : districts) {
			if (d.getId() == nurseId)
				return d;
		}
		return null;
	}
	
	private void assignPatientToFollowUpNurse(ArrayList<Patient> patients) {
		for (Patient p : patients) {
			// In this time, we assume that p has only one patient
			int nid = p.getFollowUpNurseId();
			if (nid == 0)
				continue;
						
			District d = getDistrictByNurseId(nid);
			if (d == null)
				continue;

			if (p.getDistrict() != null)
				p.getDistrict().removePatient(p);
			p.setDistrict(d);
			d.addPatient(p);
		}		
	}
	
	public Record runInitialClustering(String resultFileName, ArrayList<Patient> patients) {
		long start, end;
		
		clearState();
		assignPatientToFollowUpNurse(patients);
		
		System.out.println("-- Clustering ... ");
		
		// Modified clustering (choose the nearest not overloaded district for each patient block)
		start = System.currentTimeMillis();
		try {
			if (!clustering(patients)) {
				System.out.println("No solution " + resultFileName);
				end = System.currentTimeMillis();
				Record record = new Record();
				record.setProcessId(Record.CLUSTERING);
				record.setRuntime(end - start);
				return record;
			}
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		end = System.currentTimeMillis();
		
		System.out.println("-- Finished Clustering!");
		Record record = new Record();
		record.setProcessId(Record.CLUSTERING);
		record.setRuntime(end - start);
		return record;
	}
	
	private double getHeuristicEvaluation() {
		double avgWorkload = getAverageDailyWorkload();
		double f_workload = 0;
		double f_exc = 0;
		for (District d : districts) {
			double w = d.getAverageDailyWorkLoad();
			f_workload += Math.abs(w - avgWorkload);
			if (w > MAX_WORKING_HOURS)
				f_exc += w - MAX_WORKING_HOURS;
		}
		double f_fu = 0;
		for (District d : districts) {
			for (Patient p : d.getPatients()) {
				if (p.getFollowUpNurseId() != p.getDistrict().getId()) {
					++f_fu;
				}
			}
		}
		
		return 100 * avgWorkload + 50 * f_workload + 5 *f_exc + 15 * f_fu;
	}
	
	private double getAverageDailyWorkload() {
		double total = 0;
		for (District d : districts) {
			total += d.getAverageDailyWorkLoad();
		}
		return total / (double) districts.size();
	}

	private District selectingDistrict(int methods, TabuDistrictList tabuDistrictList) {
		switch (methods) {
		case 1:
			double max = Double.MIN_VALUE;
			District selected = null;
			for (District d : districts) {
				double workload = d.getAverageDailyWorkLoad();
				if (workload > max && !tabuDistrictList.isTabu(d)) {
					max = workload;
					selected = d;
				}
			}
			return selected;
			
		case 2:
			for (District d : districts) {
				if (d.getAverageDailyWorkLoad() > MAX_WORKING_HOURS)
					return d;
			}
			break;
		default:
			break;
		}
		return null;
	}
	
	private District getClosestDistrict(ArrayList<District> districtsList, Patient p) {
		double minDistance = Double.MAX_VALUE;
		District selected = null;
		for (District d : districtsList) {
			double distance = d.distance(p);
			if (distance < minDistance) {
				minDistance = distance;
				selected = d;
			}
		}
		
		return selected;
	}
	
	private ArrayList<District> getPolygonDistricts(ArrayList<District> districtsList) {
		Patient[] polygon = new Patient[districtsList.size()];
		for (int i = 0 ; i < polygon.length; ++i) {
			polygon[i] = districtsList.get(i).getCentroidAndCalculating();
		}
		
		polygon = SupportFunction.getConvexHull(polygon);
		ArrayList<District> result = new ArrayList<>();
		for (Patient p : polygon) {
			result.add(p.getDistrict());
		}
		return result;
	}
	
	private boolean isAdjacent(District d1, District d2) {
		for (Patient p1 : d1.getBorderPatients()) {
			ArrayList<Patient> neighbors = Patient.adjacentListMap.get(p1);
			for (Patient p2 : d2.getBorderPatients()) {
				if (neighbors.contains(p2))
					return true;
			}
		}
		return false;
	}
	
	private ArrayList<District> selectingNeighboringDistricts(District currentDistrict) {
		ArrayList<District> result = new ArrayList<>();
	
		for (District d : districts) {
			if (isAdjacent(currentDistrict, d)) 
				result.add(d);
		}
			
		return result;
	}
	
// =================================== ACCESS ZONE ===================================
	
	public void swapOne(Patient p1, Patient p2) {
		District d1 = p1.getDistrict();
		District d2 = p2.getDistrict();
		
		d1.addPatientWithRemovalFromOldDistrict(p2);
		p2.setDistrict(d1);
		
		d2.addPatientWithRemovalFromOldDistrict(p1);
		p1.setDistrict(d2);
	}

	public void moveOne(Patient p, District targetDistrict) {		
		targetDistrict.addPatientWithRemovalFromOldDistrict(p);
		p.setDistrict(targetDistrict);
	}
	
//	public ArrayList<Patient> getMultiplePatients(District sourceDistrict, District targetDistrict) {
//		// We assume that source district and target district are adjacent to each other
//		
//		ArrayList<Patient> result = new ArrayList<>();
//		double sWorkload = sourceDistrict.getAverageDailyWorkLoad();
//		double tWorkload = targetDistrict.getAverageDailyWorkLoad();
//		
//		double avgWorkload = (sWorkload + tWorkload) / 2;		
//		
//		ArrayList<Patient> adjacentPatients = sourceDistrict.getAdjacentPatientTo(targetDistrict);
//		while (sWorkload > avgWorkload && tWorkload < avgWorkload) {
//			double minDistance = Double.MAX_VALUE;
//			Patient p = null;
//			for (Patient adj : adjacentPatients) {
//				double distance = targetDistrict.distance(adj);
//				if (distance < minDistance) {
//					minDistance = distance;
//					p = adj;
//				}
//			}
//			if (p == null)
//				return result;
//			adjacentPatients.remove(p);
//			result.add(p);
//		}
//	}
	
	public void moveMultiple(ArrayList<Patient> pList, District targetDistrict) {
		for (Patient p : pList) {
			moveOne(p, targetDistrict);
		}
	}
	
	private class TabuDistrictList {
		private Map<District, Boolean> tabuMap = new HashMap<>();
		private ArrayList<District> tabuList = new ArrayList<>();
		
		public TabuDistrictList(){
		}
		public void addTabu(District d){
			if (!tabuMap.containsKey(d)) {
				tabuList.add(d);
				tabuMap.put(d, true);
			}
		}
		public boolean isTabu( District d) {
			if (tabuMap.containsKey(d))
				return true;
			return false;
		}
		public int size() {
			return tabuList.size();
		}
		public void cleanList() {
			tabuList = new ArrayList<>();
			tabuMap = new HashMap<>();
		}
	}
	
	private boolean isConnected(Patient p, District d) {
		for (Patient pp : d.getPatients()) {
			if (Patient.adjacentListMap.containsKey(new Key(p, pp))) {
				return true;
			}
		}
		return false;
	}
	
	public District getPotentialMinimumDistrictAssignment(Patient p, ArrayList<District> potentialDistricts) {
		double minEvaluation = Double.MAX_VALUE;
		
		District oldDistrict = p.getDistrict();
		
		oldDistrict.removePatient(p);
		p.setDistrict(null);
		
		District selected = null;
		for (District d : potentialDistricts) {
			d.addPatient(p);
			p.setDistrict(d);
			
			double evaluation = getHeuristicEvaluation();
			if (evaluation < minEvaluation) {
				selected = d;
				minEvaluation = evaluation;
			}
			
			d.removePatient(p);
			p.setDistrict(null);
		}
		
		oldDistrict.addPatient(p);
		p.setDistrict(oldDistrict);
		
		return selected;
	}
	
	public Action intensification(District selected, TabuList tabuList, TabuDistrictList tabuDistrictList, double bestEvaluation) {
		ArrayList<District> neighboringDistricts = selectingNeighboringDistricts(selected);
		
		Action selectedAction = null;
		double currentBestEvaluation = Double.MAX_VALUE;
						
		for (District nd : neighboringDistricts) {
			
			// --- Phase 1
			Patient closest = null;
			double min = Double.MAX_VALUE;
			for(Patient p : selected.getBorderPatients()) {
				double distance = nd.distance(p);
				if (distance < min) {
					min = distance;
					closest = p;
				}
			}

			moveOne(closest, nd);
			double evaluation = getHeuristicEvaluation();
			
			if (!tabuList.isTabu(closest, nd) || evaluation < bestEvaluation) {
				if (evaluation < currentBestEvaluation) {
					currentBestEvaluation = evaluation;
					selectedAction = new Action();
					selectedAction.action = Action.MOVE_ONE;
					selectedAction.p1 = closest;
					selectedAction.d = nd;
				}
			}
			moveOne(closest, selected);
			
			// --- Phase 2
			double minGap = Double.MAX_VALUE;
			Patient sp = null;
			for (Patient p2 : nd.getBorderPatients()) {
				double distance = p2.getEuclideanDistance(closest);
				if (distance < minGap) {
					minGap = distance;
					sp = p2;
				}
			}
			
			swapOne(closest, sp);
			evaluation = getHeuristicEvaluation();

			if ((!tabuList.isTabu(closest, sp.getDistrict()) && !tabuList.isTabu(sp, closest.getDistrict())) || evaluation < bestEvaluation) { 
				if (evaluation < currentBestEvaluation) {
					currentBestEvaluation = evaluation;
					selectedAction = new Action();
					selectedAction.action = Action.SWAP_ONE;
					selectedAction.p1 = closest;
					selectedAction.p2 = sp;
				}
			}
			swapOne(closest, sp);
		}
		
		return selectedAction;
	}
	
	public Record runMetaheuristic() {
		System.out.println("-- Metaheuristic ... ");
		long start = System.currentTimeMillis();
		TabuList tabuList = new TabuList();
		TabuDistrictList tabuDistrictList = new TabuDistrictList();
		
		int Itry = 5;
		double bestEvaluation = getHeuristicEvaluation();
		
		copyToBestSolution();
		ArrayList<District> initialSolution = getCurrentSolutionCopy();
		
		System.out.println("Clustering evaluation : " + bestEvaluation + " " + getAverageDailyWorkload());
		
		while (Itry > 0) {
			int i = 0;
			
			boolean hasNewBestSolution = false;
			while (i < 300) {	
				District selected = selectingDistrict(1, tabuDistrictList);
				if (selected == null)
					break;
				
				Action selectedAction = intensification(selected, tabuList, tabuDistrictList, bestEvaluation);
				
				if (selectedAction == null) {
					if (tabuDistrictList.size() < districts.size()) {
						tabuDistrictList.addTabu(selected);
						continue;
					}
					else if (tabuDistrictList.size() == districts.size()) {
						break;
					}
				}
				else { 
					tabuDistrictList.cleanList();
				}
				
				if (selectedAction.action.equals(Action.MOVE_ONE)) {
					tabuList.addTabu(selectedAction.p1, selectedAction.p1.getDistrict());
					moveOne(selectedAction.p1, selectedAction.d);					
				}
				else if (selectedAction.action.equals(Action.SWAP_ONE)) {
					tabuList.addTabu(selectedAction.p1, selectedAction.p1.getDistrict());
					tabuList.addTabu(selectedAction.p2, selectedAction.p2.getDistrict());
					swapOne(selectedAction.p1, selectedAction.p2);
				}
				
				double evaluation = getHeuristicEvaluation();
				if (evaluation < bestEvaluation) {
					hasNewBestSolution = true;
					bestEvaluation = evaluation;
					copyToBestSolution();
					i = 0;
				}
				++i;		
				
				System.out.println("Intensification : " + Itry + " - " + i + " - " + bestEvaluation + " - " + evaluation);
				if (selectedAction.action.equals(Action.MOVE_ONE)) {
					System.out.println(selectedAction.action + " - p :" + selectedAction.p1.getPid() + " -- d :" + selectedAction.d.getId());				
				}
				else if (selectedAction.action.equals(Action.SWAP_ONE)) {
					System.out.println(selectedAction.action + " - p1:" + selectedAction.p1.getPid() + " -- p2:" + selectedAction.p2.getPid());				
				}
			}
			
//			// Diversification
						
			double bestDiversificationEvaluation;
			if (hasNewBestSolution) {
				bestDiversificationEvaluation = bestEvaluation;
				districts = Solution.reconstructSolution(bestSolution);
			}
			else {				
				for (Patient p : getPatients()) {
					if (!isConnected(p, p.getDistrict())) {
						ArrayList<District> neighboringDistricts = new ArrayList<>();
						for (Patient neighbor : Patient.adjacentListMap.get(p)) {
							District neighboringDistrict = neighbor.getDistrict();
							neighboringDistricts.add(neighboringDistrict);
						}
												
						District potential = getPotentialMinimumDistrictAssignment(p, neighboringDistricts);
						moveOne(p, potential);
						tabuList.addTabu(p, potential);
						
						// Move back some other patients to get a balance
//						for (District nd : neighboringDistricts) {
//							if (nd.getId() == p.getDistrict().getId()) {
//								nd.moveToBalancing(oldDistrict, p);
//								break;
//							}
//						}
					}
				}
				bestDiversificationEvaluation = getHeuristicEvaluation();
			}
			
			Action selectedAction = null;
			int j = 0;
			
			tabuDistrictList.cleanList();
			while (j < 40) {
				Action currentSelectedAction = null;
				
				for (District d : districts) {
					currentSelectedAction = intensification(d, tabuList, tabuDistrictList, bestEvaluation);

					if (currentSelectedAction == null) {
						if (tabuDistrictList.size() < districts.size()) {
							tabuDistrictList.addTabu(d);
							continue;
						}
						else if (tabuDistrictList.size() == districts.size()) {
							break;
						}
					}
					else { 
						tabuDistrictList.cleanList();
					}
					
					if (currentSelectedAction.action.equals(Action.MOVE_ONE)) {
						District tempD = currentSelectedAction.p1.getDistrict();
						Patient tempP = currentSelectedAction.p1;
						moveOne(tempP, currentSelectedAction.d);

						double evaluation = getHeuristicEvaluation();
						if (evaluation < bestDiversificationEvaluation) {
							bestDiversificationEvaluation = evaluation;
							selectedAction = currentSelectedAction;
						}

						moveOne(tempP, tempD);		
					}
					else if (currentSelectedAction.action.equals(Action.SWAP_ONE)) {
						swapOne(currentSelectedAction.p1, currentSelectedAction.p2);

						double evaluation = getHeuristicEvaluation();
						if (evaluation < bestDiversificationEvaluation) {
							hasNewBestSolution = true;
							bestDiversificationEvaluation = evaluation;
							selectedAction = currentSelectedAction;
						}

						swapOne(currentSelectedAction.p1, currentSelectedAction.p2);
					}
				}
				
				if (selectedAction == null) {
					break;
				}
				
				if (selectedAction.action.equals(Action.MOVE_ONE)) {
					tabuList.addTabu(selectedAction.p1, selectedAction.p1.getDistrict());
					moveOne(selectedAction.p1, selectedAction.d);					
				}
				else if (selectedAction.action.equals(Action.SWAP_ONE)) {
					tabuList.addTabu(selectedAction.p1, selectedAction.p1.getDistrict());
					tabuList.addTabu(selectedAction.p2, selectedAction.p2.getDistrict());
					swapOne(selectedAction.p1, selectedAction.p2);
				}
				
				double evaluation = getHeuristicEvaluation();
				if (evaluation < bestEvaluation) {
					bestEvaluation = evaluation;
					copyToBestSolution();
					j = 0;
				}

				System.out.println("Diversification : " + Itry + " - " + j + " - " + bestEvaluation + " - " + evaluation);
				if (selectedAction.action.equals(Action.MOVE_ONE)) {
					System.out.println(selectedAction.action + " - p :" + selectedAction.p1.getPid() + " -- d :" + selectedAction.d.getId());				
				}
				else if (selectedAction.action.equals(Action.SWAP_ONE)) {
					System.out.println(selectedAction.action + " - p1:" + selectedAction.p1.getPid() + " -- p2:" + selectedAction.p2.getPid());				
				}
				
				++j;
			}
			
			tabuDistrictList.cleanList();
			
			--Itry;
		}

		long end = System.currentTimeMillis();
		System.out.println("-- Finished Metaheuristic! ... ");
		
		districts = Solution.reconstructSolution(bestSolution);
		
		Record record = new Record();
		record.setProcessId(Record.METAHEURISTIC);
		record.setRuntime(end - start);
		
		ArrayList<Integer> totalMoved = new ArrayList<>();
		ArrayList<Integer> totalNew = new ArrayList<>();
		ArrayList<Double> changingPercentage = new ArrayList<>();
		
		for (int i = 0 ; i < districts.size() ; ++i) {
			District initialDistrict = initialSolution.get(i);
			totalNew.add(districts.get(i).getTotalNewPatients(initialDistrict));
			totalMoved.add(districts.get(i).getTotalMovedPatients(initialDistrict));
			
			District currentDistrict = districts.get(i);
			double cp = ((double) (currentDistrict.getPatients().size() - totalNew.get(i)) / (double) initialDistrict.getPatients().size()) * 100;
			cp = (Math.round(cp * 1000) / 1000);
			changingPercentage.add(100 - cp);
		}
		
		record.setTotalMovedPatients(totalMoved);
		record.setTotalNewPatients(totalNew);
		record.setChangingPercentage(changingPercentage);
		
		return record;
	}

	private ArrayList<District> getCurrentSolutionCopy() {
		ArrayList<District> result = new ArrayList<>();
		
		for (District d : districts) {
			District temp = new District();
			temp.copy(d);
			result.add(temp);
		}
		
		return result;
	}

	private void copyToBestSolution() {
		bestSolution = new Solution();
		for (District d : districts) {
			bestSolution.addDistrictAssignment(d);
		}
	}
	
	public void exportDistrictInfo(String filePath) {
		System.out.print("Exporting file '" + SupportFunction.getFileName(filePath) + "' ... ");
		
		try {
			PrintWriter writer = new PrintWriter(filePath, "UTF-8");
			char a = '"';
			writer.write("<?xml version =" + a + "1.0" + a + "?> \n");
			writer.write("<DistrictInfo> \n");
			
			for (District d : districts) {
				d.exportXmlData(writer);
			}
			writer.write("</DistrictInfo> \n");
			writer.close();
			
			System.out.println("Successful!");
			return;
			
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		System.out.println("Error!");
	}
	
	public void exportPatientInfo(String filePath, ArrayList<Patient> patients) {
		System.out.print("Exporting file '" + SupportFunction.getFileName(filePath) + "' ... ");
		
		try {
			PrintWriter writer = new PrintWriter(filePath, "UTF-8");
			char a = '"';
			writer.write("<?xml version =" + a + "1.0" + a + "?> \n");
			writer.write("<PatientInfo> \n");
			writer.write("<CLSC x=" + a + CLSC.getX() + a + " y=" + a + CLSC.getY() + a + "> ");
			writer.write("</CLSC> \n");
			for (Patient p : patients) {
				p.exportXmlData(writer);
			}
			writer.write("</PatientInfo> \n");
			writer.close();
			
			System.out.println("Successful!");
			return;
			
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		System.out.println("Error!");
	}
	
	public ArrayList<Patient> importPatientInfo(String filePath) {
		System.out.print("Importing file '" + SupportFunction.getFileName(filePath) + "' ... ");
		UserHandler userhandler;
		try {	
			File inputFile = new File(filePath);
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			userhandler = new UserHandler();
			saxParser.parse(inputFile, userhandler);    

			System.out.println("Successful!");
			
			Patient.importMappingData(userhandler.getPatients());
			
			
			return userhandler.getPatients();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Error!");
		return null;
	}
		
	public ArrayList<District> importDistrictInfo(String filePath) {
		System.out.print("Importing file '" + SupportFunction.getFileName(filePath) + "' ... ");
		UserHandler userhandler;
		try {	
			File inputFile = new File(filePath);
			
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			userhandler = new UserHandler();
			saxParser.parse(inputFile, userhandler);    

			System.out.println("Successful!");
			
			District.importMappingData(userhandler.getDistricts());
			
			return userhandler.getDistricts();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Error!");
		return null;
	}
	
	public void exportData(ArrayList<Patient> patients) {
		exportDistrictInfo(District.INFO_FILE);
		exportPatientInfo(Patient.INFO_FILE, patients);
		Patient.exportMappingData();
		District.exportMappingData();
		Patient.exportMatrixDistance();
	}
	
	public ArrayList<Patient> importData() {
		districts = importDistrictInfo(District.INFO_FILE);
		ArrayList<Patient> patients = importPatientInfo(Patient.INFO_FILE);
		CLSC = patients.remove(0);
		
		try {
			District.importMappingData(districts);
			Patient.importMappingData(patients);
			Patient.importMatrixDistance();
			
			//ReadExcelFile.fixCoordinateLost(patients, CLSC, "coordonnes_7 juin 2016.xlsx");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//ClusterComponent.scalePatientsToSizeDistrictsList(patients, CLSC);
		
		return patients;
	}

	public void exportResult(String title, String fileName, Record solutionRecord) {
		try {
			System.out.print("Writing " + fileName + " ... ");
			
			writer = new PrintWriter(fileName, "UTF-8");
			
			writer.write(title + " ... \n");
			// Modified clustering (choose the nearest not overloaded district for each patient block)
			
			double totalPatients = 0;
			for (District d : districts) {
				totalPatients += d.getPatients().size();
			}
			
			writer.write("Patient blocks   : " + totalPatients + "\n");
			writer.write("Districts        : " + districts.size() + "\n");
			writer.write("Running time     : " + solutionRecord.getRuntime() + " ms" + "\n");			
			writer.write("Total districts  : " + districts.size() + "\n \n");
			
			double total = 0, highest = Double.MIN_VALUE, lowest = Double.MAX_VALUE;
			
			ArrayList<Integer> totalMoved = null;
			ArrayList<Integer> totalNew = null;
			ArrayList<Double> changingPercentage = null;
			if (solutionRecord.getProcessId() == Record.METAHEURISTIC) {
				totalMoved = solutionRecord.getTotalMovedPatients();
				totalNew = solutionRecord.getTotalNewPatients();
				changingPercentage = solutionRecord.getChangingPercentage();
			}
			
			for (int i = 0 ; i < districts.size(); ++i) {
				writer.write("=========================================================================\n");
				writer.write("District " + (i + 1) + " ---\n");
				
				District d = districts.get(i);
				double w = d.getAverageDailyWorkLoad();
				writer.write("     + Total patients           : " + d.getTotalPatient() + "\n");
				
				if (solutionRecord.getProcessId() == Record.METAHEURISTIC) {
					writer.write("     + Total moved patients     : " + totalMoved.get(i) + " patients\n");
					writer.write("     + Total new patients       : " + totalNew.get(i) + " patients\n");
					writer.write("     + Changing percentage      : " + changingPercentage.get(i) + " % \n\n");
				}
				
				writer.write("     + Workload                 : " + w + " minutes\n");
				
				Patient centroid = d.getCentroid();
				//Patient cg = d.getVirtualCentralGravity();
				double std_cen = 0, mean_cen = 0, furthest_cen = Double.MIN_VALUE;
				//double std_cg = 0,  mean_cg = 0, furthest_cg = Double.MIN_VALUE;
				for (Patient p : d.getPatients()) {
					//double dis_cg = cg.getEuclideanDistance(p);
					double time = Patient.timeMatrixDistance.get(new Key(p.getPid(), centroid.getPid()));
					
//					if (dis_cg > furthest_cg)
//						furthest_cg = dis_cg;
					
					if (time > furthest_cen)
						furthest_cen = time;
					
					mean_cen += time;
					//mean_cg += dis_cg;
							
					//std_cg += Math.pow(dis_cg, 2);
					std_cen += Math.pow(time, 2);
				}
				
				mean_cen /= d.getPatients().size();
				//mean_cg /= d.getPatients().size();
				
				std_cen = Math.sqrt(std_cen / (d.getPatients().size() - 1));
				//std_cg = Math.sqrt(std_cg / (d.getPatients().size() - 1));
				
				writer.write("     + Std.     - centroid      : " + std_cen + " minutes\n");
				writer.write("     + Mean     - centroid      : " + mean_cen + " minutes\n");
				writer.write("     + Furthest - centroid      : " + furthest_cen + " minutes\n\n");
								
				total += w;
				
				if (w > highest)
					highest = w;
				
				if (w < lowest)
					lowest = w;		
				
				double wf = 0;
				double tf = 0;
				for (Patient p : d.getPatients()) {
					if (p.getFollowUpNurseId() != 0) {
						++tf;
						if (p.getFollowUpNurseId() != p.getDistrict().getId()) {
							++wf;
						}
					}
				}
				
				writer.write("     + Wrong follow-up nurse    : " + wf + " / " + tf + "\n\n");
				
				writer.write("     + Patient Info             : \n");				
				for (Patient p : d.getPatients()) {
					double avg = d.getAverageDailyTravelingTime(p.getPid());
					writer.write("         - " + Patient.getPatientStringId(p.getPid()) + " : r = " + p.getAverageTreatmentTime() + " minutes, t_0 = " + Patient.timeMatrixDistance.get(new Key(p.getPid(), 0)) + " minutes, t_avg = " + avg + " minutes\n");
				}
			}
			
			writer.write("=========================================================================\n\n");
			double std = 0;
			double avg = (total / districts.size()) ;
			for (District d : districts) {
				std += Math.pow(d.getAverageDailyWorkLoad() - avg, 2);
			}
			std = Math.sqrt(std / (districts.size() - 1));
			
			int totalFollowUp = 0;
			int wf = 0;
			for (District d : districts) {
				for (Patient p : d.getPatients()) {
					if (p.getFollowUpNurseId() != 0) {
						totalFollowUp += 1;
						if (p.getFollowUpNurseId() != p.getDistrict().getId()) {
							wf += 1;
						}
					}
				}
			}
			
			writer.write("\nTotal workload   : " + total + " minutes\n");
			writer.write("Highest          : " + highest + " minutes\n");
			writer.write("Lowest           : " + lowest + " minutes\n");
			writer.write("Max variation    : " + (highest - lowest) + " minutes\n");
			writer.write("Std. deviation   : " + std + " minutes\n");
			writer.write("Average workload : " + avg + " minutes\n");
			writer.write("Wrong follow-up  : " + wf + "/" + totalFollowUp + " patients");
			
			// Adjust the clustering result so that no district's daily workload exceeds more than 300 minutes
			// adjustClusters();
			
			writer.close();
			System.out.println("Successfully!");
			
		} catch (IOException e) {
			System.out.println("Error!");
			e.printStackTrace();
		}
	}

	public void plotBarCharWorkload(String title) {
		JFrame frame = new JFrame(title);

		final int FRAME_WIDTH = 300;
		final int FRAME_HEIGHT = 400;

		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		BarChartComponent component = new BarChartComponent(districts);
		frame.add(component);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	public void exportBarCharWorkload(String fileName) {
		JFrame frame = new JFrame(fileName);

		final int FRAME_WIDTH = 300;
		final int FRAME_HEIGHT = 400;

		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		BarChartComponent comp = new BarChartComponent(districts);
		frame.add(comp);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		BufferedImage bImg = new BufferedImage(comp.getWidth(), comp.getHeight(), BufferedImage.TYPE_INT_RGB);
	    Graphics2D cg = bImg.createGraphics();
	    comp.paintAll(cg);
	    try {
	    	if (ImageIO.write(bImg, "png", new File("./" + fileName + ".png"))){
	    	}
	    	else {
	    		System.out.println("Saving picture error : " + fileName);
	    	}
	    	frame.setVisible(false);
	    	frame.dispose();
	    	
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	}

	private void addToNeighboringPatientsMap(Patient p1, Patient p2) {	
		if (!Patient.adjacentListMap.containsKey(p1)) 
			Patient.adjacentListMap.put(p1, new ArrayList<Patient>());
		
		if (!Patient.adjacentListMap.containsKey(p2)) 
			Patient.adjacentListMap.put(p2, new ArrayList<Patient>());
		
		ArrayList<Patient> temp;
		temp = Patient.adjacentListMap.get(p1);
		if (!temp.contains(p2))
			temp.add(p2);
		
		temp = Patient.adjacentListMap.get(p2);
		if (!temp.contains(p1))
			temp.add(p1);
		
	}
	
	protected class PatientsBlock {
		public double x;
		public double y;
		public ArrayList<Patient> patients = new ArrayList<>();
	}
	
	public void makeVoronoiDiagram(ArrayList<Patient> patients) {
		Collections.sort(patients, new Comparator<Patient>() {
			@Override
			public int compare(Patient p1, Patient p2) {
				if (p1.getX() > p2.getX())
					return 1;
				if (p1.getX() < p2.getX())
					return -1;
				if (p1.getY() > p2.getY())
					return 1;
				if (p1.getY() < p2.getY())
					return -1;
				return 0;
			}
		});
		
		Voronoi v = new Voronoi(Double.MIN_VALUE);
		    	
    	double previousX = 0;
    	double previousY = 0;
    	
    	ArrayList<PatientsBlock> reduced = new ArrayList<>();
    	
    	for (int i = 0 ; i < patients.size(); ++i) {
    		double x = patients.get(i).getX();
    		double y = patients.get(i).getY();
	    	
	    	if (x == previousX && y == previousY) {
	    		reduced.get(reduced.size() - 1).patients.add(patients.get(i));
	    	}
	    	else {
	    		PatientsBlock newP = new PatientsBlock();
	    		newP.x = x;
	    		newP.y = y;
	    		newP.patients.add(patients.get(i));
	    		reduced.add(newP);
	    	}
	    	
	    	previousX = x;
	    	previousY = y;
    	}
    	
    	// Make all the patients in the same block become neighbors to each others ...
    	// .. and find the max-min positions in term of x and y coordinates
    	int n = reduced.size();
    	double[] xValuesIn = new double [n]; 
    	double[] yValuesIn = new double [n];
    	double minX = 9999999, maxX = -9999999, minY = 9999999, maxY = -99999999;
    	
    	for (int t = 0 ; t < reduced.size() ; ++t) {
    		PatientsBlock pb = reduced.get(t);
    		double x = pb.x;
    		double y = pb.y;
    		
    		for (int i = 0 ; i < pb.patients.size() - 1; ++i) {
    			Patient p1 = pb.patients.get(i);
    			for (int j = i + 1 ; j < pb.patients.size() ; ++j) {
    				Patient p2 = pb.patients.get(j);
    				addToNeighboringPatientsMap(p1, p2);
    			}
    		}
    		
    		xValuesIn[t] = x;
    		yValuesIn[t] = y;
    		
    		if (x > maxX)
	    		maxX = x;
	    	if (x < minX)
	    		minX = x;
	    	
	    	if (y > maxY)
	    		maxY = y;
	    	if (y < minY)
	    		minY = y;
    	}
    	
    	double scalePercentage = 0;
    	double scaleUp = 1 + scalePercentage;
    	double scaleDown = 1 - scalePercentage;
    	
		List<GraphEdge> edges = v.generateVoronoi(xValuesIn, yValuesIn, minX * scaleDown, maxX* scaleUp, minY* scaleDown, maxY* scaleUp);
		for (GraphEdge e : edges) {
			PatientsBlock pb1 = reduced.get(e.site1);
			PatientsBlock pb2 = reduced.get(e.site2);
			
			for (Patient p1 : pb1.patients) {
				for (Patient p2 : pb2.patients) {
					addToNeighboringPatientsMap(p1, p2);
				}
			}
		}
		
		
	}
}
