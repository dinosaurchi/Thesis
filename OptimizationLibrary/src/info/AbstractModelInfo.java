package info;

import state.AbstractState;

public interface AbstractModelInfo <StateType extends AbstractState> {
	
	public AbstractModelInfo<StateType> clone();
	
	@Override
	public boolean equals(Object obj);
	
	public boolean isConsistent(StateType state);
}
