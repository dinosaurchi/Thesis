package info;

import java.util.ArrayList;

import neighborhood.AbstractNeighborhoodMap;
import state.AbstractState;

public abstract class NeighborhoodModelInfo <StateType extends AbstractState> implements AbstractModelInfo<StateType> {
	
	private final AbstractNeighborhoodMap neighborhoodMap;
	
	protected NeighborhoodModelInfo(NeighborhoodModelInfo<StateType> info) {
		if (info == null)
			throw new NullPointerException();
		
		// Copying neighborhood map
		this.neighborhoodMap = info.neighborhoodMap.clone();
	}
	
	protected NeighborhoodModelInfo(AbstractNeighborhoodMap map) {
		if (map == null)
			throw new NullPointerException();
		 
		this.neighborhoodMap = map.clone();
	}
	
	@Override
	abstract public NeighborhoodModelInfo<StateType> clone();
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof NeighborhoodModelInfo))
			return false;
		NeighborhoodModelInfo<StateType> info = (NeighborhoodModelInfo<StateType>) obj;
		if (!this.neighborhoodMap.equals(info.neighborhoodMap))
			return false;
						
		return true;
	}
	
	public AbstractNeighborhoodMap getNeighborhoodMap() {
		return this.neighborhoodMap.clone();
	}
	
	public ArrayList<String> getNeighborDataPointIds(String dataPointId) {
		if (dataPointId == null)
			throw new NullPointerException();
		
		ArrayList<String> dpids = this.neighborhoodMap.getNeighborIds(dataPointId);
		if (dpids == null)
			throw new RuntimeException("Not existing data point id : " + dataPointId);
		return dpids;
	}
	
	public boolean isNeighborDataPoint(String dataPointId1, String dataPointId2) {
		if (dataPointId1 == null)
			throw new NullPointerException();
		if (dataPointId2 == null)
			throw new NullPointerException();
		if (!this.neighborhoodMap.contains(dataPointId1))
			throw new RuntimeException("Not existing data point : " + dataPointId1);
		if (!this.neighborhoodMap.contains(dataPointId2))
			throw new RuntimeException("Not existing data point : " + dataPointId2);
		
		ArrayList<String> dpids = this.neighborhoodMap.getNeighborIds(dataPointId1);
		return dpids.contains(dataPointId2);
	}
}
