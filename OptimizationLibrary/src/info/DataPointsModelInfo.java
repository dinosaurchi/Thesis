package info;

import java.util.ArrayList;

import non_model_entity.DataPoint;
import state.DataPointClassState;

public interface DataPointsModelInfo <StateType extends DataPointClassState> extends AbstractModelInfo<StateType> {
	public ArrayList<DataPoint> getDataPointsList();
	public ArrayList<String> getClassIdsList();
	public DataPoint getDataPoint(String dpid);
	public ArrayList<DataPoint> getDataPoints(ArrayList<String> dataPointIds);
}
