package support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import state.AbstractState;

public class RunningInfo<StateType extends AbstractState> {
	private final StateType currentState;
	private final Map<String, String> valuesMap = new HashMap<>();
	
	public RunningInfo(StateType currentState) {
		if (currentState == null)
			throw new NullPointerException();
		
		this.currentState = currentState;
	}
	
	public StateType getCurrentState() {
		return currentState;
	}
	
	public void addValue(String key, String value) {
		if (key == null)
			throw new NullPointerException();
		if (value == null)
			throw new NullPointerException();
		
		if (this.valuesMap.containsKey(key))
			throw new RuntimeException("Duplicated key : " + key);
		this.valuesMap.put(key, value);
	}

	public ArrayList<String> getKeys() {
		return new ArrayList<>(valuesMap.keySet());
	}
	
	public String getValue(String key) {
		if (key == null)
			throw new NullPointerException();
		if (!this.valuesMap.containsKey(key))
			throw new RuntimeException("Not existing key : " + key);
		return this.valuesMap.get(key);
	}
}
