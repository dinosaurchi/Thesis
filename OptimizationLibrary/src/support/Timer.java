package support;

public class Timer {
	public static long NULL_TIME = -1;
	private long startTime = NULL_TIME;
	private long lastDuration = 0;
	
	public void restart() {
		startTime = System.currentTimeMillis();
	}
	
	public double elapsed() {
		if (startTime == NULL_TIME)
			return lastDuration / 1000.0;
		
		lastDuration = System.currentTimeMillis() - startTime;
		startTime = -1;
		return (double) lastDuration / 1000.0;
	}
}
