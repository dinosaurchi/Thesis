package support;
import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Stack;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import non_model_entity.Coordinate2D;
import non_model_entity.DataPoint;

public class SupportFunction {

	public static void savePlot(JFrame frame, String fileName) {
		if (frame == null)
			throw new NullPointerException();
		if (fileName == null)
			throw new NullPointerException();

		System.out.println("Saving plot to file " + fileName); 
		
		frame.setVisible(true);
        Container content = frame.getContentPane();
        frame.setVisible(false);
        BufferedImage img = new BufferedImage(content.getWidth(), content.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = img.createGraphics();
        content.printAll(g2d);
        try {
			ImageIO.write(img, "png", new File("./" + fileName + ".png"));
			System.out.println("Finished saving plot to file " + fileName); 
			frame.dispose();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	public static String getCurrentTimeStamp() {
	    return new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
	}
	
	public static void saveFile(String content, String fileName) {
		assert(content != null);
		assert(fileName != null);
		
		System.out.println("Saving to file " + fileName); 
		try {
			PrintWriter out = new PrintWriter(fileName);
			out.print(content);
		    out.close();
		    System.out.println("Finished saving to file " + fileName); 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Coordinate2D> convertToCoordinate2DsList(ArrayList<DataPoint> dataPoints) {
		if (dataPoints == null)
			throw new NullPointerException();
		ArrayList<Coordinate2D> coordinate2ds = new ArrayList<>();
		for (DataPoint d : dataPoints) {
			coordinate2ds.add(d.getCoordinate());
		}
		return coordinate2ds;
	}
	
	public static double max(double [] values) {
		if (values == null)
			throw new NullPointerException();
		if (values.length < 1) 
			throw new RuntimeException("Empty values array");
		
		double maxVal = values[0];
		for (int i = 1 ; i < values.length ; ++i) {
			if (maxVal < values[i])
				maxVal = values[i];
		}
		return maxVal;
	}
	
	public static double maxVariation(double [] values) {
		if (values == null)
			throw new NullPointerException();
		if (values.length < 1) 
			throw new RuntimeException("Empty values array");
		
		double maxVariation = -1;
		for (int i = 0; i < values.length ; ++i) {
			for (int j = i + 1 ; j < values.length ; ++j) {
				double variation = Math.abs(values[i] - values[j]);
				if (variation > maxVariation)
					maxVariation = variation;
			}
		}
		return maxVariation;
	}
	
	public static String [] getFilesName(String currentDir, final String type) {
		if (currentDir == null)
			throw new NullPointerException();
		
		File dir = new File(currentDir);
		
		return dir.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (type == null)
					return new File(dir, name).isFile();
				else 
					return name.endsWith("." + type);
			}
		});
	}
	
	public static String [] getSubDirectoriesName(String currentDir) {
		if (currentDir == null)
			throw new NullPointerException();
		
		File dir = new File(currentDir);
		return dir.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return new File(dir, name).isDirectory();
			}
		});
	}
	
	public static double min(double [] values) {
		if (values == null)
			throw new NullPointerException();
		if (values.length < 1) 
			throw new RuntimeException("Empty values array");
		
		double minVal = values[0];
		for (int i = 1 ; i < values.length ; ++i) {
			if (minVal > values[i])
				minVal = values[i];
		}
		return minVal;
	}
	
	public static double mean(double [] values) {
		return sum(values) / (double) values.length;
	}
	
	public static double sum(double [] values) {
		if (values == null)
			throw new NullPointerException();
		if (values.length < 1) 
			throw new RuntimeException("Empty values array");
		
		double total = 0;
		for (int i = 0 ; i < values.length ; ++i) {
			total += values[i];
		}
		return total;
	}
	
	public static double variance(double [] values) {
		if (values == null)
			throw new NullPointerException();
		if (values.length < 1) 
			throw new RuntimeException("Empty values array");
		
		double mean = mean(values);
		double squaredSum = 0;
		for (int i = 0 ; i < values.length ; ++i) {
			squaredSum += Math.pow((values[i] - mean), 2);
		}
		int n = values.length - 1;
		return squaredSum / (double) ((n == 0) ? 1 : n);
	}
	
	public static double std(double [] values) {
		return Math.sqrt(variance(values));
	} 
	
	public static double median(double [] values) {
		if (values == null)
			throw new NullPointerException();
		if (values.length < 1) 
			throw new RuntimeException("Empty values array");
		
		// Copy it to a new array, avoid changing the original array from outside
		values = getCopyArray(values);
		Arrays.sort(values);
		if (values.length % 2 == 0)
		    return ((double) values[values.length / 2] + (double) values[values.length / 2 - 1]) / 2;
		else
		    return (double) values[values.length / 2];
	}
	
	public static double [] getCopyArray(double [] values) {
		if (values == null)
			throw new NullPointerException();
		if (values.length < 1) 
			throw new RuntimeException("Empty values array");
		double [] newArray = new double[values.length];
		for (int i= 0 ; i < values.length ; ++i) {
			newArray[i] = values[i];
		}
		return newArray;
	}
	
	public static <T> boolean contains(ArrayList<T> objs, T obj) {
		if (objs == null)
			throw new NullPointerException();
		if (obj == null)
			throw new NullPointerException();
		
		for (T o : objs) {
			if (o.equals(obj))
				return true;
		}
		return false;
	}
		
	public static ArrayList<String> parseStringArrayFromJsonArray(JSONArray array) {
		if (array == null)
			throw new NullPointerException();
		
		ArrayList<String> result = new ArrayList<>();
		for (int i = 0 ; i < array.length() ; ++i) {
			try {
				result.add(array.getString(i));
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException("JSON String " + array.toString());
			}
		}
		
		return result;
	}
	
	public static String parseStringContentFromFile(String fileName) {
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    return sb.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	public static JSONArray parseJsonArrayFromFile(String fileName) {
		if (fileName == null)
			throw new NullPointerException();
		try {
			return new JSONArray(SupportFunction.parseStringContentFromFile(fileName));
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException("Cannot parse Json file " + fileName);
		}
	}
	
	public static JSONArray parseJsonArrayFromString(String jsonString) {
		if (jsonString == null)
			throw new NullPointerException();
		try {
			return new JSONArray(jsonString);
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException("Cannot parse Json from string " + jsonString);
		}
	}

	public static JSONObject parseJsonObjectFromFilePath(String fileName) {
		if (fileName == null)
			throw new NullPointerException();
				
		try {
			return new JSONObject(SupportFunction.parseStringContentFromFile(fileName));
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException("Cannot parse Json from file " + fileName);
		}
	}

	public static void createDirectory(String dirName) {
		if (dirName == null)
			throw new NullPointerException();
		
		File file = new File(dirName);
		if (!file.exists())
			file.mkdir();
	}

	public static JSONObject parseJsonObjectFromString(String jsonString) {
		if (jsonString == null)
			throw new NullPointerException();
		try {
			return new JSONObject(jsonString);
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException("Cannot parse Json from string " + jsonString);
		}
	}

	public static double[] toDoubleArray(ArrayList<Double> proportions) {
		if (proportions == null)
			throw new NullPointerException();
		
		double [] values = new double[proportions.size()];
		for (int i = 0 ; i < proportions.size() ; ++i) {
			values[i] = proportions.get(i);
		}
		return values;
	}
	
	public static boolean contains(Object [] values, Object value) {
		if (values == null)
			throw new NullPointerException();
		if (value == null)
			throw new NullPointerException();
		
		for (Object v : values) {			
			if (v.equals(value))
				return true;
		}
		return false;
	}
	
	public static String getFileNameFromFilePath(String filePath) {
		if (filePath == null)
			throw new NullPointerException();
		
		String [] fs = filePath.split(File.separator);
    	return fs[fs.length - 1];
    }
	
	private static double cross(Coordinate2D O, Coordinate2D A, Coordinate2D B) {
		if (O == null)
			throw new NullPointerException();
		if (A == null)
			throw new NullPointerException();
		if (B == null)
			throw new NullPointerException();
		
		return (A.getX() - O.getX()) * (B.getY() - O.getY()) - (A.getY() - O.getY()) * (B.getX() - O.getX());
	}
	
	public static Coordinate2D[] getConvexHull(Coordinate2D[] coordinates) {
		if (coordinates == null)
			throw new NullPointerException();
		if (coordinates.length < 1)
			throw new RuntimeException("Need at least one point to process");
		if (coordinates.length == 1)
			return Arrays.copyOf(coordinates, coordinates.length);
		
		int n = coordinates.length;
		int k = 0;
		Coordinate2D [] hull = new Coordinate2D[2 * n];

		Arrays.sort(coordinates);

		// Build lower hull
		for (int i = 0; i < n; ++i) {
			while (k >= 2 && cross(hull[k - 2], hull[k - 1], coordinates[i]) <= 0)
				k--;
			hull[k++] = coordinates[i];
		}

		// Build upper hull
		for (int i = n - 2, t = k + 1; i >= 0; i--) {
			while (k >= t && cross(hull[k - 2], hull[k - 1], coordinates[i]) <= 0)
				k--;
			hull[k++] = coordinates[i];
		}
		if (k > 1) {
			// remove non-hull vertices after k
			// remove k - 1 which is a duplicate
			hull = Arrays.copyOfRange(hull, 0, k - 1); 
		}
		
		return hull;
	}

	public static boolean isInsidePolygon(Coordinate2D[] polygon, Coordinate2D coord) {
		if (polygon == null)
			throw new NullPointerException();
		if (coord == null)
			throw new NullPointerException();
		
		int j = polygon.length - 1 ;
		boolean oddNodes = false ;

		for (int i = 0 ; i < polygon.length ; i++) {
			if ((polygon[i].getY() <  coord.getY() && polygon[j].getY() >= coord.getY() || 
				 polygon[j].getY() <  coord.getY() && polygon[i].getY() >= coord.getY()) &&
				(polygon[i].getX() <= coord.getX() || polygon[j].getX() <= coord.getX())) {
				oddNodes ^= (polygon[i].getX() + (coord.getY() - polygon[i].getY()) / (polygon[j].getY() - polygon[i].getY()) * (polygon[j].getX() - polygon[i].getX()) < coord.getX()); 
			}
			j = i; 
		}
		return oddNodes;
	}
	
	public static ArrayList<Coordinate2D> scalePointsToNewBoundingBox(ArrayList<Coordinate2D> points, 
															 		  Coordinate2D topLeft, Coordinate2D rightBottom) {
		/*
		 * The function guarantees that the returning coordinates will be in the same order with the original coordinates
		 * It means that, the 2nd point in the original array is now becomes the 2nd point point in the new array, but with 
		 * scaled x and y.
		 * */
		
		if (points == null)
			throw new NullPointerException();
		if (points.size() < 1)
			throw new RuntimeException("Need at least one point to scale");
		if (topLeft == null)
			throw new NullPointerException();
		if (rightBottom == null)
			throw new NullPointerException();
		
		// Avoid changing the original points list
		points = new ArrayList<>(points);
		
		// We do that to avoid misunderstanding in reference
		// For instance: some people will say that top is lowest, while with someone else, top is highest
		double lowX = Math.min(topLeft.getX(), rightBottom.getX());
		double highX = Math.max(topLeft.getX(), rightBottom.getX());
		double lowY = Math.min(topLeft.getY(), rightBottom.getY());
		double highY = Math.max(topLeft.getY(), rightBottom.getY());
		
		double [] xVals = Coordinate2D.getXs(points);
		double [] yVals = Coordinate2D.getYs(points);
		
		double minX = min(xVals);
		double minY = min(yVals);
				
		double oldRangeX = max(xVals) - minX;
		double oldRangeY = max(yVals) - minY;
		
		double newRangeX = highX - lowX;
		double newRangeY = highY - lowY;
		
		ArrayList<Coordinate2D> result = new ArrayList<>();
		for(int i = 0 ; i < points.size() ; ++i) {
			Coordinate2D p = points.get(i);
			result.add(new Coordinate2D(((p.getX() - minX) / oldRangeX) * newRangeX + lowX, 
										((p.getY() - minY) / oldRangeY) * newRangeY + lowY));			
		}
		return result;
	}


	public static String getPrefixMatch(String pattern, String[] strings) {
		if (pattern == null)
			throw new NullPointerException();
		if (strings == null)
			throw new NullPointerException();
		
		for (String s : strings) {
			if (s.startsWith(pattern))
				return s;
		}
		return null;
	}
	
	public static <T extends Object> ArrayList<T> concatenate(ArrayList<T> list1, ArrayList<T> list2) {
		ArrayList<T> newList = new ArrayList<>();
		newList.addAll(list1);
		newList.addAll(list2);
		return newList;
	}

	public static String removePrefix(String s, String prefix) {
		return s.replaceFirst("^" + prefix, "");
	}

	public static <T extends Object> boolean isIntersected(ArrayList<T> list1, ArrayList<T> list2) {
		list1 = new ArrayList<>(list1);
		list2 = new ArrayList<>(list2);
		for (T e : list1) {
			if (SupportFunction.contains(list2, e))
				return true;
		}
		
		for (T e : list2) {
			if (SupportFunction.contains(list1, e))
				return true;
		}
		
		return false;
	}
	
	public static void copyFile(String sourceFilePath, String destFilePath) {
		if (sourceFilePath == null)
			throw new NullPointerException();
		if (destFilePath == null)
			throw new NullPointerException();
		
		try {
			Files.copy((new File(sourceFilePath)).toPath(), 
					   (new File(destFilePath)).toPath(), 
					   StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	public static void copyDirectory(String sourceDirPath, String destDirPath) {
		if (sourceDirPath == null)
			throw new NullPointerException();
		if (destDirPath == null)
			throw new NullPointerException();
		
		try {
			FileUtils.copyDirectory(new File(sourceDirPath), 
									new File(destDirPath));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	public static boolean existPath(String path) {
		if (path == null)
			throw new NullPointerException();
		return (new File(path)).exists();
	}

	public static <E extends Object> ArrayList<ArrayList<E>> getCombinations(ArrayList<E> sourceObjects, int r) {
		if (sourceObjects == null)
			throw new NullPointerException();
		if (sourceObjects.size() < r)
			throw new RuntimeException("Invalid objects' size and r : size = " + sourceObjects.size() + ", r = " + r);
		ArrayList<ArrayList<E>> result = new ArrayList<>();
		getCombinationsImpl(sourceObjects, 0, r, new Stack<E>(), result);
		return result;
	}
	
	private static <E extends Object> void getCombinationsImpl(ArrayList<E> source, int start, int r, 
															   Stack<E> trace, ArrayList<ArrayList<E>> result) {	
		for (int i = start ; i < source.size() ; ++i) {
			trace.push(source.get(i));
			if (trace.size() == r) 
				result.add(new ArrayList<>(trace));
			else
				getCombinationsImpl(source, i + 1, r, trace, result);
			trace.pop();
		}
	}

	public static double[] getDoubleDifference(double[] valuesLeft, double[] valuesRight) {
		if (valuesLeft == null)
			throw new NullPointerException();
		if (valuesRight == null)
			throw new NullPointerException();
		if (valuesLeft.length != valuesRight.length)
			throw new RuntimeException("Invalid sizes : size 1 = " + valuesLeft.length + ", size 2 = " + valuesRight.length);
		
		double [] difference = new double[valuesLeft.length];
		for (int i = 0 ; i < difference.length ; ++i)
			difference[i] = valuesLeft[i] - valuesRight[i];
		return difference;
	}
}
