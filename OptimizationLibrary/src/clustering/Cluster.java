package clustering;

import java.util.ArrayList;

import non_model_entity.Coordinate2D;
import non_model_entity.DataPoint;

public class Cluster {
	private final ArrayList<DataPoint> dataPoints = new ArrayList<>();
	private final String id;
	
	public Cluster() {
		id = getNextId();
	}

	public Cluster(String id) {
		if (id == null)
			throw new NullPointerException();
		this.id = id;
	}
	
	public ArrayList<DataPoint> getAssignDataPoints() {
		return new ArrayList<>(dataPoints);
	}

	public boolean hasDataPoint(String dataPointId) {
		if (dataPointId == null)
			throw new NullPointerException();
		for (DataPoint dp : dataPoints) {
			if (dp.getId() == dataPointId)
				return true;
		}
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Cluster))
			return false;
		return this.id.equals(((Cluster) obj).id);
	}
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	public void addDataPoint(DataPoint dataPoint) {
		if (dataPoint == null)
			throw new NullPointerException();
		if (hasDataPoint(dataPoint.getId()))
			throw new RuntimeException("Duplicated data point id : " + dataPoint.getId());
		dataPoints.add(dataPoint);
	}
	
	public void addDataPoints(ArrayList<DataPoint> dataPoints) {
		if (dataPoints == null)
			throw new NullPointerException();
		for (DataPoint dp : dataPoints) {
			addDataPoint(dp);
		}
	}
	
	public String getId() {
		return this.id;
	}
	
	public int size () {
		return dataPoints.size();
	}
	
	public DataPoint getCentroid() {
		if (dataPoints.size() <= 0)
			throw new RuntimeException("Cannot have empty cluster");
		
		double cx = 0, cy = 0;
		for (DataPoint dp : dataPoints) {
			cx += dp.getCoordinate().getX();
			cy += dp.getCoordinate().getY();
		}
		Coordinate2D centroidCoord = new Coordinate2D(cx / (double) dataPoints.size(), cy / (double) dataPoints.size());
		return getClosetDataPoint(centroidCoord);
	}
	
	private DataPoint getClosetDataPoint(Coordinate2D coord) {
		assert coord != null;
		DataPoint closetDataPoint = null;
		double closetDistance = Double.MAX_VALUE;
		for (DataPoint dp : dataPoints) {
			double distance = dp.getCoordinate().euclideanDistance(coord);
			if (distance < closetDistance) {
				closetDistance = distance;
				closetDataPoint = dp;
			}
		}
		assert closetDataPoint != null;
		return closetDataPoint;
	}
	
	private static long currentId = 0;
	private static synchronized String getNextId() {
		return Long.toString(currentId++);
	}

	public Cluster intersect(Cluster rightCluster) {
		if (rightCluster == null)
			throw new NullPointerException();
		
		ArrayList<DataPoint> retainedDataPoints = new ArrayList<>(this.dataPoints);
		retainedDataPoints.retainAll(rightCluster.dataPoints);
		Cluster intersectedCluster = new Cluster();
		intersectedCluster.dataPoints.addAll(retainedDataPoints);
		return intersectedCluster;
	}
	
	@Override
	public String toString() {
		return id + " : " + getDataPointIdsString();
	}

	private String getDataPointIdsString() {
		String s = dataPoints.get(0).getId();
		for (int i = 1 ; i < dataPoints.size() ; ++i)
			s += ", " + dataPoints.get(i).getId();
		return s;
	}
}
