package clustering;

import java.util.ArrayList;

import non_model_entity.DataPoint;

public interface ClusteringAlgorithm {
	public ArrayList<Cluster> clustering(ArrayList<DataPoint> dataPoints, int iterations);
	public ArrayList<Cluster> incrementalClustering(ArrayList<Cluster> currentClusters, int iterations);
}
