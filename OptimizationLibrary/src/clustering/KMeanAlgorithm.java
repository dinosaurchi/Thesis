package clustering;

import java.util.ArrayList;

import non_model_entity.DataPoint;
import non_model_entity.DirectedPair;

public abstract class KMeanAlgorithm implements ClusteringAlgorithm {
	protected final int k;
	
	public KMeanAlgorithm(int k) {
		if (k <= 0)
			throw new RuntimeException("Number of classes needs to be larger than 0");
		this.k = k;
	}
	
	@Override
	public ArrayList<Cluster> incrementalClustering(ArrayList<Cluster> currentClusters, int iterations) {
		if (currentClusters == null)
			throw new NullPointerException();
		if (iterations < 1)
			throw new RuntimeException("Total Iterations needs to be > 0");
		if (currentClusters.size() < 1)
			throw new RuntimeException("Nothing to cluster");
		
		ArrayList<DataPoint> seeds = new ArrayList<>();
		ArrayList<DataPoint> dataPoints = new ArrayList<>();
		ArrayList<DirectedPair<String, DataPoint>> classIdSeedPairsList = new ArrayList<>();
		for (Cluster c : currentClusters) {
			DataPoint seed = c.getCentroid();
			seeds.add(seed);
			dataPoints.addAll(c.getAssignDataPoints());
			classIdSeedPairsList.add(new DirectedPair<String, DataPoint>(c.getId(), seed));
		}
		
		ArrayList<Cluster> clusters = kMeanClustering(seeds, dataPoints, iterations);
		if (clusters.size() != k)
			throw new RuntimeException("Inequality between "
					+ "clustered set size (" + clusters.size() + ") and "
							+ "k (" + k + ")");
		
		ArrayList<Cluster> resultClusters = new ArrayList<>();
		for (DirectedPair<String, DataPoint> classSeedPair : classIdSeedPairsList) {
			Cluster newCluster = new Cluster(classSeedPair.getLeft());
			Cluster currentCluster = getClusterWithSeed(clusters, classSeedPair.getRight());
			newCluster.addDataPoints(currentCluster.getAssignDataPoints());
			resultClusters.add(newCluster);
		}
		
		return resultClusters;
	}
	
	private Cluster getClusterWithSeed(ArrayList<Cluster> clusters, DataPoint seed) {
		assert clusters != null;
		for (Cluster c : clusters) {
			if (c.hasDataPoint(seed.getId()))
				return c;
		}
		throw new RuntimeException("Cannot found seed, "
													+ "cluster size = " + clusters.size() + ", "
													+ "Seed Id = " 		+ seed.getId());
	}

	@Override
	public ArrayList<Cluster> clustering(ArrayList<DataPoint> dataPoints, int iterations) {
		if (dataPoints == null) 
			throw new NullPointerException();		
		if (iterations < 1)
			throw new RuntimeException("Total Iterations needs to be > 0");
				
		ArrayList<DataPoint> seeds = getSeeds(dataPoints);
		ArrayList<Cluster> clusters = kMeanClustering(seeds, dataPoints, iterations);
		if (clusters.size() != k)
			throw new RuntimeException("Inequality between "
					+ "clustered set size (" + clusters.size() + ") and "
							+ "k (" + k + ")");
		return clusters;
	}
	
	abstract protected ArrayList<Cluster> kMeanClustering(ArrayList<DataPoint> seedWithClassIds, ArrayList<DataPoint> dataPoints, int iterations);
	
	abstract protected ArrayList<DataPoint> getSeeds(ArrayList<DataPoint> dataPoints);
}
