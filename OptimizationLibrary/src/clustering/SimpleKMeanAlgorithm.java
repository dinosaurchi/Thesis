package clustering;

import java.util.ArrayList;
import java.util.Collections;

import non_model_entity.DataPoint;
import non_model_entity.DataPointsBlock;
import non_model_entity.DirectedPair;

public class SimpleKMeanAlgorithm extends KMeanAlgorithm {
	public SimpleKMeanAlgorithm(int k) {
		super(k);
	}

	@Override
	protected ArrayList<Cluster> kMeanClustering(ArrayList<DataPoint> seeds, ArrayList<DataPoint> dataPoints, int iterations) {
		if (seeds == null)
			throw new NullPointerException();
		if (dataPoints == null)
			throw new NullPointerException();
		if (seeds.size() != k)
			throw new RuntimeException("Inappropriate seeds");
		if (k <= 0)
			throw new RuntimeException("k needs to be larger than 0");
		if (iterations < 1)
			throw new RuntimeException("Total Iterations needs to be > 0");
		
		ArrayList<Cluster> clusters = new ArrayList<>();
		for (DataPoint dp : seeds) {
			Cluster c = new Cluster();
			c.addDataPoint(dp);
			clusters.add(c);
		}
		
		int iter = 0;
		while (iter++ < iterations) {
			ArrayList<DirectedPair<Cluster, DataPoint>> bestPairs = getBestPairs(dataPoints, clusters);
			// We only change the current state after getting all of the best pairs between patients and cluster
			for (DirectedPair<Cluster, DataPoint> pair : bestPairs) {
				pair.getLeft().addDataPoint(pair.getRight());
			}
 		}
		
		return clusters;
	}
	
	@Override
	protected ArrayList<DataPoint> getSeeds(ArrayList<DataPoint> dataPoints) {
		if (dataPoints == null)
			throw new NullPointerException();
		
		// We generate random seeds
		ArrayList<DataPoint> blocks = DataPointsBlock.getSingleDataPoints(dataPoints);
		if (blocks.size() < k)
			throw new RuntimeException("Invalid total number of blocks : " + blocks.size());
		
		Collections.shuffle(blocks);
		return new ArrayList<>(blocks.subList(0, k));
	}
	
	private ArrayList<DirectedPair<Cluster, DataPoint>> getBestPairs(ArrayList<DataPoint> dataPoints,
																	 ArrayList<Cluster> clusters) {
		assert dataPoints != null;
		assert clusters != null;
		assert dataPoints.size() > 0;
		assert clusters.size() > 0;
		
		ArrayList<DirectedPair<Cluster, DataPoint>> bestPairs = new ArrayList<>(); 
		for (DataPoint dp : dataPoints) {
			// When getting the best cluster, we do not change the current state
			Cluster bestCluster = getBestCluster(dp, clusters);
			if (!bestCluster.hasDataPoint(dp.getId()))
				bestPairs.add(new DirectedPair<Cluster, DataPoint>(bestCluster, dp));
		}
		return bestPairs;
	}

	private Cluster getBestCluster(DataPoint dp, ArrayList<Cluster> clusters) {
		Cluster bestCluster = null;
		double minDistance = Double.MAX_VALUE;
		
		for (Cluster c : clusters) {
			DataPoint centroid = c.getCentroid();
			double distance = centroid.getCoordinate().euclideanDistance(dp.getCoordinate());
			if (distance < minDistance) {
				minDistance = distance;
				bestCluster = c;
			}
		}
		
		return bestCluster;
	}
}
