package model;

import java.util.HashMap;
import java.util.Map;

import info.AbstractModelInfo;
import state.AbstractState;

public abstract class AbstractModel <StateType extends AbstractState,
								     InfoType extends AbstractModelInfo<StateType>> {
/* ---------------DOCUMENTATION----------------------
	
	AbstractModel object is an unmodifiable object, means that it is only defined in constructors. 
	From that assumption, we can apply pass by reference for AbstractModel object, do not need 
	to have a deep copy.	
*/
	
	private final Map<String, Double> weightsMap;
	private final String [] weightKeysList;
	
	protected InfoType info;
	
	@SuppressWarnings("unchecked")
	protected AbstractModel(AbstractModel<StateType, InfoType> model) {
		if (model == null)
			throw new NullPointerException();
		this.info = (InfoType) (model.getModelInfo()).clone();
		this.weightsMap = new HashMap<>(model.weightsMap);
		this.weightKeysList = model.weightKeysList;
	}
	
	@SuppressWarnings("unchecked")
	public AbstractModel(InfoType info, String [] weightKeysList) {
		if (info == null)
			throw new NullPointerException();
		
		this.info = (InfoType) info.clone();
		this.weightsMap = getInitialWeightsMap(weightKeysList);
		this.weightKeysList = weightKeysList;
	}
	
	private static Map<String, Double> getInitialWeightsMap(String [] weightKeysList) {
		if (weightKeysList == null)
			throw new NullPointerException();
		if (weightKeysList.length == 0)
			throw new RuntimeException("Empty input weight keys list");
		
 		Map<String, Double> weightsMap = new HashMap<>();
 		for (String key : weightKeysList) {
 			weightsMap.put(key, 1.0);
 		}
		return weightsMap;
	}
	
	public final double getObjectiveScore(StateType currentState) {
		if (currentState == null)
			throw new NullPointerException();
		
		Map<String, Double> objectiveComponentValuesMap = getObjectiveComponentMap(currentState);
		if (!weightsMap.keySet().equals(objectiveComponentValuesMap.keySet()))
			throw new RuntimeException("Difference between weightsMap and objectiveComponentValuesMap");
		
		double score = 0;
		for (String key : weightsMap.keySet()) {			
			double componentNewValue = objectiveComponentValuesMap.get(key);
			score += getScaledWeight(key) * getScaledComponent(componentNewValue, key);
		}
		
		return score;
	}
	
	private double getScaledComponent(double componentNewValue, String key) {
		return componentNewValue;
	}

	private double getScaledWeight(String key) {
		return weightsMap.get(key) / getSumWeights();
	}
	
	private double getSumWeights() {
		double sum = 0;
		for (Double value : weightsMap.values()) {
			sum += value;
		}
		return sum;
	}

	public void setWeights(Map<String, Double> weights) {
		if (weights == null)
			throw new NullPointerException();
		if (weights.keySet().size() != weightKeysList.length)
			throw new RuntimeException("Inappropriate weights size: " + weights.keySet().size() 
									 + " with keys list size : " + weightKeysList.length);
		
		for (String wKey : weights.keySet()) {
			if (!weights.containsKey(wKey))
				throw new RuntimeException("Inappropriate weight key : " + wKey);
			this.weightsMap.put(wKey, weights.get(wKey));
		}
	}
	
	public Map<String, Double> getWeightsMap() {
		return new HashMap<>(this.weightsMap);
	}
	
	abstract public Map<String, Double> getObjectiveComponentMap(StateType state);
	
	public final InfoType getModelInfo() {
		return this.info;
	}
	
	@SuppressWarnings("unchecked")
	public final void setModelInfo(InfoType info) {
		if (info == null)
			throw new NullPointerException();
		this.info = (InfoType) info.clone();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof AbstractModel<?, ?>))
			return false;
		
		AbstractModel<StateType, InfoType> model = (AbstractModel<StateType, InfoType>) obj;
		if (!this.info.equals(model.info))
			return false;
						
		return true;
	}
	
	abstract public AbstractModel<StateType,InfoType> clone();
	
	abstract public StateType getInitialState();
}
