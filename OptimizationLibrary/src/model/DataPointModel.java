package model;

import java.awt.Color;
import info.DataPointsModelInfo;
import state.DataPointClassState;

public abstract class DataPointModel <StateType extends DataPointClassState,
									  InfoType extends DataPointsModelInfo<StateType>>
					extends AbstractModel<StateType, InfoType> {

	protected DataPointModel(DataPointModel<StateType, InfoType> model) {
		super(model);
	}
	
	protected DataPointModel(InfoType info, String [] weightKeysList) {
		super(info, weightKeysList);
	}
			
	@Override
	abstract public StateType getInitialState();
	
	@Override
	abstract public DataPointModel<StateType, InfoType> clone();
	
	abstract public Color getClassColor(String cid);
}
