package context;

import experiment.StateChange;
import global.CONSTANT;
import model.AbstractModel;
import optimizer.AbstractOptimizer;
import solution_report.SolutionReporter;
import state.AbstractState;
import support.RunningInfo;
import support.Timer;

public abstract class Problem <StateType extends AbstractState, 
							   ModelType extends AbstractModel<StateType, ?>> {
	
	protected final ModelType model;
	private AbstractOptimizer<StateType, ModelType> optimizer = null;
	protected StateType lastState;
	protected StateType bestState;
	protected double lastRunningTime = 0;

	// PUBLIC Methods

	@SuppressWarnings("unchecked")
	public Problem(ModelType model) {
		if (model == null)
			throw new NullPointerException();
		this.model = (ModelType) model.clone();
		this.lastState = this.model.getInitialState();
		this.bestState = (StateType) this.lastState.clone();
	}
	
	@SuppressWarnings("unchecked")
	protected Problem(Problem<StateType, ModelType> problem) {
		if (problem == null)
			throw new NullPointerException();
		this.model = (ModelType) problem.model.clone();
		this.lastState = (StateType) problem.lastState.clone();
		this.bestState = (StateType) problem.bestState.clone();
		this.optimizer = problem.optimizer;
		this.lastRunningTime = problem.lastRunningTime;
	}

	public StateType getLastState() {
		return lastState;
	}
	
	public StateType getBestState() {
		return bestState;
	}

	public void setOptimizer(AbstractOptimizer<StateType, ModelType> optimizer) {		
		if (optimizer == null) 
			throw new NullPointerException();
		this.optimizer = optimizer;
	}

	public void solve() {
		if (optimizer == null) 
			throw new RuntimeException("No optimizer");
		lastRunningTime = Timer.NULL_TIME;
		lastState = optimizer.optimize(model, getLastState(), getBestState());
		if (this.model.getObjectiveScore(lastState) < this.model.getObjectiveScore(bestState)) {
			bestState = lastState;
		}
		
		assert lastState != null : "Null result";
		lastRunningTime = optimizer.getRunningTime();
	}
	
	public RunningInfo<StateType> getLastRunningInfo() {
		RunningInfo<StateType> runningInfo = new RunningInfo<>(this.getLastState());
		runningInfo.addValue(CONSTANT.RUNNING_TIME, Double.toString(this.getLastRunningTime()));
		runningInfo.addValue(CONSTANT.TOTAL_ITERATIONS, Double.toString(this.getLastTotalIterations()));
		runningInfo.addValue(CONSTANT.BEST_OBJECTIVE_SCORE, Double.toString(this.getBestObjectiveScore()));
		runningInfo.addValue(CONSTANT.LAST_OBJECTIVE_SCORE, Double.toString(this.model.getObjectiveScore(getLastState())));
		runningInfo.addValue(CONSTANT.OPTIMIZER, this.optimizer.getClass().getName());
		return runningInfo;
	}
	
	abstract public SolutionReporter<StateType, ModelType> getReporter();

	abstract protected SolutionReporter<StateType, ModelType> getReporter(ModelType model);
	
	private double getBestObjectiveScore() {
		if (optimizer == null)
			return this.model.getObjectiveScore(getBestState());
		return optimizer.getBestObjectiveScore();
	}

	public double getLastRunningTime() {
		return this.lastRunningTime;
	}
	
	@SuppressWarnings("unchecked")
	public ModelType getModel() {
		return (ModelType) this.model.clone();
	}
	
	public AbstractOptimizer<StateType, ModelType> getOptimizer() {
		return this.optimizer;
	}
	
	public int getLastTotalIterations() {
		if (this.optimizer == null)
			return 0;
		return this.optimizer.getTotalIterations();
	}
	
	@Override
	public abstract Problem<StateType, ModelType> clone();

	@SuppressWarnings("unchecked")
	public Problem<StateType, ModelType> refreshState() {
		Problem<StateType, ModelType> problem = clone();
		problem.bestState = (StateType) this.lastState.clone();
		return problem;
	}

	abstract public Problem<StateType, ModelType> changeProblem(StateChange<StateType, ModelType> change);
}
