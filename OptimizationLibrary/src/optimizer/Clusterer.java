package optimizer;

import java.util.ArrayList;

import model.DataPointModel;
import move.Move;
import move_generator.ClustererMoveGenerator;
import state.DataPointClassState;

public class Clusterer <StateType extends DataPointClassState, 
						ModelType extends DataPointModel<StateType, ?>> 
						
						extends AbstractOptimizer<StateType, 
												  ModelType> {
	/*
	 * 	DOCUMENTATION
	 * 		
	 * 
	 */
	protected final int MAX_ITERATIONS;

	public Clusterer(ClustererMoveGenerator<StateType, ModelType> moveGenerator, int maxIterations) {
		super(moveGenerator);
		if (maxIterations < 1)
			throw new RuntimeException("Total iterations needs to be > 0");
		
		this.MAX_ITERATIONS = maxIterations;
	}

	@Override
	protected void optimizeModel() {
		int iter = 0;
		while (iter++ < MAX_ITERATIONS) {
			ArrayList<Move<StateType>> movesList = generateMovesFromCurrentState().getMoves();
			StateType tempState = this.getCurrentDataPointClassState();
			
			// We assign all data point to an appropriate class, based on the clustering result
			for (Move<StateType> pairClassAndDataPoint : movesList) {
				tempState = pairClassAndDataPoint.execute(tempState);
			}
			
			this.setCurrentState(tempState);
		}
	}

	private StateType getCurrentDataPointClassState() {
		return this.getCurrentState();
	}

	@Override
	public StateType getReturnState() {
		return this.getCurrentState();
	}
}
