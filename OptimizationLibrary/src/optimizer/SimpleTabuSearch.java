package optimizer;

import java.util.ArrayList;

import model.AbstractModel;
import move.Move;
import move.UnitMove;
import move_generator.AbstractMoveGenerator;
import move_generator.MovesList;
import state.AbstractState;

public class SimpleTabuSearch  <StateType extends AbstractState, 
								ModelType extends AbstractModel<StateType, ?>>
						extends TabuSearch <StateType, ModelType> {
	public SimpleTabuSearch(AbstractMoveGenerator<StateType, ModelType> moveGenerator, int maxIterations, 
							ArrayList<UnitMove<StateType>> initialTabuMoves) {
		super(moveGenerator, maxIterations, 150, initialTabuMoves);
	}

	@Override
	protected void optimizeModel() {
		StateType bestState = this.getBestState();
		double minObjective = this.getCurrentModel().getObjectiveScore(bestState);
		
		int iter = 0;
		while (iter++ < MAX_ITERATIONS) {
			MovesList<StateType> moves = generateMovesFromCurrentState();
			Move<StateType> potentialMove = null;
			double minPotentialObjective = Double.MAX_VALUE;
			ArrayList<Move<StateType>> movesList = moves.getMoves();
			for (Move<StateType> move : movesList) {
				double score = this.getCurrentModel().getObjectiveScore(move.execute(this.getCurrentState()));
				if ((!isTabuMove(move) && score < minPotentialObjective) /* || (score < minObjective)*/) {
					minPotentialObjective = score;
					potentialMove = move;
				}
			}
			
			if (potentialMove == null)
				break;
			
			Move<StateType> reverseMove = potentialMove.reverseMove(this.getCurrentState());
			StateType currentState = this.getCurrentState();
			StateType newState = potentialMove.execute(currentState);
			
			assert !currentState.equals(newState);
			
			this.setCurrentState(newState);
			double score = this.getCurrentModel().getObjectiveScore(this.getCurrentState());
			if (score < minObjective) {
				minObjective = score;
				bestState = this.getCurrentState();
				iter = 0;
			}
			
			addToTabuList(potentialMove);
			addToTabuList(reverseMove);
		}				
	}
}
