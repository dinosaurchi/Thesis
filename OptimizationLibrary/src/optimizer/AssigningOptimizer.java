package optimizer;

import java.util.ArrayList;

import model.DataPointModel;
import move.Move;
import move.UnitMove;
import move_generator.AssigningMoveGenerator;
import move_generator.MovesList;
import state.DataPointClassState;

public class AssigningOptimizer <StateType extends DataPointClassState,
								 ModelType extends DataPointModel<StateType, ?>> 
					extends AbstractOptimizer<StateType, ModelType> {
	
	private final ArrayList<UnitMove<StateType>> lastMoves;
	public AssigningOptimizer(AssigningMoveGenerator<StateType, ModelType> moveGenerator) {
		super(moveGenerator);
		lastMoves = new ArrayList<>();
	}

	@Override
	protected void optimizeModel() {
		MovesList<StateType> movesList = generateMovesFromCurrentState();
		for (Move<StateType> move : movesList.getMoves()) {
			StateType state = move.execute(getCurrentState());
			setCurrentState(state);
			lastMoves.addAll(move.toUnitMoves());
		}
	}

	@Override
	public StateType getReturnState() {
		return getCurrentState();
	}
	
	public ArrayList<UnitMove<StateType>> getLastMoves() {
		return new ArrayList<>(this.lastMoves);
	}
}
