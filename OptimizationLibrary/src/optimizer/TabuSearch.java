package optimizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import model.AbstractModel;
import move.Move;
import move.UnitMove;
import move_generator.AbstractMoveGenerator;
import state.AbstractState;

public abstract class TabuSearch <StateType extends AbstractState, 
								  ModelType extends AbstractModel<StateType, ?>> 
					extends AbstractOptimizer<StateType,
											  ModelType> {

	/*
	 * 	DOCUMENTATION
	 * 		
	 * 
	 */
	
	private static class TabuList<A> {
		private final int MAX_ITERATION_LIFE;
		private final Map<A, ArrayList<A>> existingMap;
		private final ArrayList<ArrayList<A>> chronicleList;
		
		public TabuList(int maxLength) {
			if (maxLength < 1)
				throw new RuntimeException("Invalid max length : " + maxLength);
			MAX_ITERATION_LIFE = maxLength;
			existingMap = new HashMap<>();
			
			chronicleList = new ArrayList<>();
			chronicleList.add(new ArrayList<A>());
		}
		
		public void push(A obj) {
			if (obj == null)
				throw new NullPointerException();
			ArrayList<A> mostRecentList = chronicleList.get(chronicleList.size() - 1);
			if (mostRecentList.contains(obj))
				return;
			mostRecentList.add(obj);
			
			if (existingMap.containsKey(obj)) {
				existingMap.get(obj).remove(obj);
			}
			existingMap.put(obj, mostRecentList);
		}
		
		public boolean contains(A obj) {
			if (obj == null) 
				throw new NullPointerException();
			for (A a : existingMap.keySet()) {
				if (a.equals(obj))
					return true;
			}
			return false;
		}
		
		private void update() {
			if (chronicleList.size() > MAX_ITERATION_LIFE) {
				ArrayList<A> removed = chronicleList.remove(0);
				for (A a : removed)
					existingMap.remove(a);
			}
		}
	}
	
	protected final int MAX_ITERATIONS;
	
	private final TabuList<UnitMove<StateType>> tabuList;
	
	public TabuSearch(AbstractMoveGenerator<StateType, ModelType> moveGenerator, int maxIterations, int tabuListLength,
					  ArrayList<UnitMove<StateType>> initialTabuMoves) {
		super(moveGenerator);
		if (maxIterations < 1)
			throw new RuntimeException("Invalid max iterations : " + maxIterations);
		if (tabuListLength < 1)
			throw new RuntimeException("Invalid tabu list length : " + tabuListLength);
		if (initialTabuMoves == null)
			throw new NullPointerException();
		
		this.MAX_ITERATIONS = maxIterations;
		this.tabuList = new TabuList<UnitMove<StateType>>(tabuListLength);
		
		for (UnitMove<StateType> move : initialTabuMoves) {
			this.tabuList.push(move);
		}
	}
	
	@Override
	protected void setCurrentState(StateType state) {
		super.setCurrentState(state);
		this.tabuList.update();
	}
	
	@Override
	public StateType getReturnState() {
		return getBestState();
	}
		
	public boolean isTabuMove(Move<StateType> move) {
		if (move == null)
			throw new NullPointerException();
		for (UnitMove<StateType> umove : move.toUnitMoves()) {
			if (!this.tabuList.contains(umove))
				return false;
		}
		return true;
	}
	
	public void addToTabuList(Move<StateType> move) {
		if (move == null)
			throw new NullPointerException();
		for (UnitMove<StateType> umove : move.toUnitMoves())
			this.tabuList.push(umove);
	}
	
	public ArrayList<UnitMove<StateType>> getLastTabuMoves() {
		return new ArrayList<>(this.tabuList.existingMap.keySet());
	}
}
