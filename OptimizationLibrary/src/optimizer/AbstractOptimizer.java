package optimizer;

import model.AbstractModel;
import move_generator.AbstractMoveGenerator;
import move_generator.MovesList;
import state.AbstractState;
import support.Timer;

public abstract class AbstractOptimizer <StateType extends AbstractState,
										 ModelType extends AbstractModel<StateType, ?>> {
	
	private static final int PRINT_PERIOD = 1;
	private final Timer timer = new Timer();
	private final AbstractMoveGenerator<StateType, ModelType> moveGenerator;
	
	private StateType bestState = null;
	private StateType curState = null;
	
	private ModelType curModel = null;
	
	private int currentIteration = 0;
	private double curMinScore = Double.MAX_VALUE;
	
	public AbstractOptimizer(AbstractMoveGenerator<StateType, ModelType> moveGenerator) {
		if (moveGenerator == null)
			throw new NullPointerException();
		this.moveGenerator = moveGenerator;
	}
	
	public int getTotalIterations() {
		return currentIteration;
	}
	
	public double getBestObjectiveScore() {
		return this.curMinScore;
	}
	
	public double getRunningTime() {
		return this.timer.elapsed();
	}
	
	public StateType optimize(ModelType model, StateType currentState, StateType bestState) {
		if (model == null)
			throw new NullPointerException();
		
		if (currentState == null)
			throw new NullPointerException();
		
		if (bestState == null)
			throw new NullPointerException();
		
		System.out.println(getLogPrefix() + " Start Optimizing");
		preProcessing(model, currentState, bestState);
		optimizeModel();
		postProcessing();
		System.out.println(getLogPrefix() + " Finished Optimizing : " + timer.elapsed() + " seconds");
				
		return getReturnState();
	}
	
	abstract public StateType getReturnState();
	
	public StateType optimize(ModelType model, StateType currentState) {
		return optimize(model, currentState, currentState);
	}
	
	private void postProcessing() {
		timer.elapsed();
		
		// We log here for the last state
		log(true);
	}
	
	private void preProcessing(ModelType model, StateType currentState, StateType bestState) {
		if (!model.getModelInfo().isConsistent(currentState))
			throw new RuntimeException("Inconsistent input state");
		
		if (!model.getModelInfo().isConsistent(bestState))
			throw new RuntimeException("Inconsistent input state");
		
		setCurrentModel(model);
		if (model.getObjectiveScore(currentState) < model.getObjectiveScore(bestState))
			throw new RuntimeException("Best state must have the lowest objective score : " 
										+ model.getObjectiveScore(currentState) + ", "
										+ model.getObjectiveScore(bestState));
		
		setBestState(bestState);
		setCurrentState(currentState);
		
		currentIteration = 0;
		curMinScore = model.getObjectiveScore(bestState);
		timer.restart();
	}
	
	abstract protected void optimizeModel ();
	
	protected MovesList<StateType> generateMovesFromCurrentState() {
		log(false);
		++currentIteration;
		return this.moveGenerator.generate(this.getCurrentState());
	}
	
	protected StateType getBestState() {
		if (this.bestState == null)
			throw new NullPointerException();
		return this.bestState;
	}
	
	protected StateType getCurrentState() {
		if (this.curState == null)
			throw new NullPointerException();
		return this.curState;
	}
	
	protected void setCurrentState(StateType state) {
		if (state == null)
			throw new NullPointerException();
		this.curState = state;
	}
	
	private void setBestState(StateType state) {
		if (state == null)
			throw new NullPointerException();
		this.bestState = state;
	}
	
	@SuppressWarnings("unchecked")
	private void setCurrentModel(ModelType model) {
		if (model == null)
			throw new NullPointerException();
		this.curModel = (ModelType) model.clone();
	}
	
	protected ModelType getCurrentModel() {
		return this.curModel;
	}
	
	private void log(boolean isLast) {
		double score = getCurrentModel().getObjectiveScore(this.getCurrentState());
		if (score < curMinScore) {
			curMinScore = score;
			setBestState(this.getCurrentState());
		}
		
		if (currentIteration % PRINT_PERIOD == 0 || isLast) {
			System.out.println(getLogPrefix() + " Current score = " + score 
												+ ", Best score = " + curMinScore 
												+ ", Iterations = " + currentIteration);
		}
	}
	
	private String getLogPrefix() {
		return "[" + toString() + "]";
	}
	
	@Override
	public String toString() {
		return getClass().getName() + "-" + moveGenerator.getClass().getName();
	}
}
