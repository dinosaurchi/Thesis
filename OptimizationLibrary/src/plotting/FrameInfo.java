package plotting;

import non_model_entity.Coordinate2D;

public class FrameInfo {
	private final Coordinate2D topLeft;
	private final Coordinate2D rightBottom;
	
	public FrameInfo(double top, double left, double bottom, double right) {
		if (!isValid(top))
			throw new RuntimeException("Invalid top : " + top);
		if (!isValid(left))
			throw new RuntimeException("Invalid left : " + left);
		if (!isValid(bottom))
			throw new RuntimeException("Invalid bottom : " + bottom);
		if (!isValid(right))
			throw new RuntimeException("Invalid right : " + right);
		
		this.topLeft = new Coordinate2D(left, top);
		this.rightBottom = new Coordinate2D(right, bottom);
	}

	private static boolean isValid(double value) {
		return !Double.isInfinite(value) && !Double.isNaN(value);
	}

	public Coordinate2D getRightBottom() {
		return rightBottom;
	}

	public Coordinate2D getTopLeft() {
		return topLeft;
	}
}
