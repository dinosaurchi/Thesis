package plotting;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.JComponent;
import javax.swing.JFrame;

import clustering.Cluster;
import info.DataPointsModelInfo;
import model.DataPointModel;
import non_model_entity.Coordinate2D;
import non_model_entity.DataPoint;
import non_model_entity.DirectedPair;
import state.DataPointClassState;

public class Plotter {
	public static final int FRAME_WIDTH = 300;
	
	public static JFrame plotClusters(ArrayList<Cluster> clusters, FrameInfo frameInfo, boolean isDisplay) {
		if (clusters == null)
			throw new NullPointerException();
		if (frameInfo == null)
			throw new NullPointerException();
		
		ArrayList<DirectedPair<Coordinate2D, Color>> coloredPoints = Plotter.getColoredPoints(clusters);
			
		PointComponent component = new PointComponent(coloredPoints, frameInfo, Plotter.FRAME_WIDTH);
		return Plotter.plotWithComponent(component, 
								"Districting graph - clustering with k = " + clusters.size(), 
								FRAME_WIDTH, component.getHeight(),
								isDisplay);
	}

	private static ArrayList<DirectedPair<Coordinate2D, Color>> getColoredPoints(ArrayList<Cluster> clusters) {
		if (clusters == null)
			throw new NullPointerException();
		ArrayList<DirectedPair<Coordinate2D, Color>> result = new ArrayList<>();
		Random rand = new Random();
		for (Cluster c : clusters) {
			Color color = new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
			for (DataPoint dp : c.getAssignDataPoints()) {
				result.add(new DirectedPair<>(dp.getCoordinate(), color));
			}
		}
		return result;
	}

	public static JFrame plotHighlightSelectedPoints(ArrayList<DirectedPair<Coordinate2D, Boolean>> points, 
													 FrameInfo frameInfo, 
													 boolean isDisplay) {
		if (points == null)
			throw new NullPointerException();
		if (points.size() < 1)
			throw new RuntimeException("Nothing to plot");
		if (frameInfo == null)
			throw new NullPointerException();
		
		ArrayList<DirectedPair<Coordinate2D, Color>> coloredPoints = getColoredSelectedPoints(points);
				
		PointComponent component = new PointComponent(coloredPoints, frameInfo, FRAME_WIDTH);
		return Plotter.plotWithComponent(component, 
								"Assigned graph - " + getTotalSelected(points) + "/" + points.size(), 
								FRAME_WIDTH, component.getHeight(),
								isDisplay);
	}

	private static ArrayList<DirectedPair<Coordinate2D, Color>> getColoredSelectedPoints(
			ArrayList<DirectedPair<Coordinate2D, Boolean>> points) {
		assert points != null;
		ArrayList<DirectedPair<Coordinate2D, Color>> result = new ArrayList<>();
		
		// We add the not selected ones first
		for (DirectedPair<Coordinate2D, Boolean> p : points) {
			if (!p.getRight())
				result.add(new DirectedPair<Coordinate2D, Color>(p.getLeft(), Color.GRAY));
		}
		
		// Then we add the selected ones. The reason is to avoid drawing the non-selected point overlapping the 
		// ... selected ones.
		for (DirectedPair<Coordinate2D, Boolean> p : points) {
			if (p.getRight())
				result.add(new DirectedPair<Coordinate2D, Color>(p.getLeft(), Color.RED));
		}
		return result;
	}

	private static int getTotalSelected(ArrayList<DirectedPair<Coordinate2D, Boolean>> points) {
		assert points != null;
		int total = 0;
		for (int i = 0 ; i < points.size() ; ++i) {
			if (points.get(i).getRight())
				++total;
		}
		return total;
	}

	public static <StateType extends DataPointClassState, 
				   InfoType extends DataPointsModelInfo<StateType>,
				   ModelType extends DataPointModel<StateType, InfoType>> 
						JFrame plotSolution(ModelType model, StateType state, FrameInfo frameInfo, boolean isDisplay) {
		if (model == null)
			throw new NullPointerException();
		if (state == null)
			throw new NullPointerException();
		if (frameInfo == null)
			throw new NullPointerException();
		
		ArrayList<DirectedPair<Coordinate2D, Color>> coloredPoints = Plotter.getColoredPoints(model, state);
				
		PointComponent component = new PointComponent(coloredPoints, frameInfo, FRAME_WIDTH);
		return Plotter.plotWithComponent(component, 
								"Districting graph - " + model.getClass(), 
								FRAME_WIDTH, component.getHeight(),
								isDisplay);
	}

	public static JFrame plot2DPointsWithDense(ArrayList<Coordinate2D> points, FrameInfo frameInfo, boolean isDisplay) {
		if (points == null)
			throw new NullPointerException();
		if (frameInfo == null)
			throw new NullPointerException();
		if (points.size() == 0)
			throw new RuntimeException("Need at least one point to plot");
		
		ArrayList<DirectedPair<Coordinate2D, Color>> coloredPoints = Plotter.getDensePoints(points);
				
		PointComponent component = new PointComponent(coloredPoints, frameInfo, FRAME_WIDTH);
		return Plotter.plotWithComponent(component, 
								"2D Position graph of " + points.size() + " data points", 
								FRAME_WIDTH, component.getHeight(),
								isDisplay);
	}

	public static JFrame plotWithComponent(JComponent component, String title, int width, int height, boolean isDisplay) {
		if (component == null)
			throw new NullPointerException();
		if (title == null)
			throw new NullPointerException();
		if (!isValidDouble(width) || width < 1)
			throw new RuntimeException("Invalid width : " + width);
		if (!isValidDouble(height) || height < 1)
			throw new RuntimeException("Invalid height : " + height);
		
		JFrame frame = new JFrame(title);
		
		/*
		 * Set the Maximum and Minimum sizes to avoid rendering only half of the graph
		 * 
		 * */
		frame.setMaximumSize(new Dimension(width, height));
		frame.setMinimumSize(new Dimension(width, height));
		
		frame.add(component);
		frame.setSize(new Dimension(width, height));
		frame.setLocationRelativeTo(null);
		frame.setVisible(isDisplay);
		frame.validate();
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		return frame;
	}

	private static boolean isValidDouble(int value) {
		return !Double.isInfinite(value) && !Double.isNaN(value);
	}

	private static <StateType extends DataPointClassState, 
	   				InfoType extends DataPointsModelInfo<StateType>,
	   				ModelType extends DataPointModel<StateType, InfoType>> 
						ArrayList<DirectedPair<Coordinate2D, Color>> 
				getColoredPoints(ModelType model, StateType state) {
		
		ArrayList<DirectedPair<Coordinate2D, Color>> result = new ArrayList<>();
	    for (String cid : state.getClassIds()) {	    	
	    	ArrayList<String> dpids = state.getAssignedDataPointIdsList(cid);
	    	for (String dpid : dpids) {
	    		DataPoint dp = model.getModelInfo().getDataPoint(dpid);
	    		Coordinate2D coord = new Coordinate2D(dp.getCoordinate().getX(), dp.getCoordinate().getY());
				result.add(new DirectedPair<Coordinate2D, Color>(coord, model.getClassColor(cid)));
	    	}	    	
	    }
		
		return result;
	}

	private static ArrayList<DirectedPair<Coordinate2D, Color>> getDensePoints(ArrayList<Coordinate2D> points) {
		if (points == null)
			throw new NullPointerException();
		if (points.size() < 1)
			return new ArrayList<>();
		
		Map<Coordinate2D, Integer> coordCountMap = new HashMap<>();
		float maxCount = 0;
		for (Coordinate2D p : points) {
			if (coordCountMap.containsKey(p)) {
				int count = coordCountMap.get(p) + 1;
				maxCount = Math.max(maxCount, count);
				coordCountMap.put(p, count);
			}
			else {
				coordCountMap.put(p, 1);
				maxCount = Math.max(maxCount, 1);
			}
		}
		
		assert maxCount != 0;
		
		ArrayList<DirectedPair<Coordinate2D, Color>> coloredCoords = new ArrayList<>();
		final int BASE = 30;
		for (Coordinate2D coord : coordCountMap.keySet()) {
			float r = coordCountMap.get(coord);
			r = 255 - ((r / maxCount) * (255 - BASE) + BASE);
			Color color = new Color(Math.round(r), Math.round(r), Math.round(r));
			coloredCoords.add(new DirectedPair<Coordinate2D, Color>(coord, color));
		}
		return coloredCoords;
	}

	public static ArrayList<DirectedPair<Coordinate2D, Boolean>> highlighSelectedDataPoints(ArrayList<DataPoint> selectedDataPoints,
																							ArrayList<DataPoint> dataPoints) {
		if (dataPoints == null)
			throw new NullPointerException();
		if (selectedDataPoints == null)
			throw new NullPointerException();
		if (!dataPoints.containsAll(selectedDataPoints))
			throw new RuntimeException("Invalid selected data points");
		
		ArrayList<DirectedPair<Coordinate2D, Boolean>> result = new ArrayList<>();
		for (DataPoint dp : dataPoints) {
			boolean isSelected = false;
			if (selectedDataPoints.contains(dp))
				isSelected = true;
			result.add(new DirectedPair<>(dp.getCoordinate(), isSelected));
		}
		return result;
	}
}
