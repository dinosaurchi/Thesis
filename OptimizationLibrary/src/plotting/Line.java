package plotting;

import non_model_entity.Coordinate2D;

public class Line {
	private Coordinate2D from = null;
	private Coordinate2D to = null;
	
	public Line(Coordinate2D from, Coordinate2D to) {
		if (from == null) 
			throw new NullPointerException();
		if (to == null)
			throw new NullPointerException();
		
		this.from = from;
		this.to = to;
	}
	
	public Line(Vector2D directionalVector, Coordinate2D from) {
		Vector2D normal = directionalVector.getNormalVector();
		
		double a = normal.getDx(), b = normal.getDy();
		double x = from.getX() * 1.1; // plus more 10%
		double y = (a*from.getX() + b*from.getY() - a*x) / b;
				
		this.from = from;
		this.to = new Coordinate2D(x, y);
	}
	
	public Coordinate2D getTo() {
		return to;
	}
	
	public Coordinate2D getFrom() {
		return from;
	}	

	public boolean isConnected(Line line) {
		if (from.duplicate(line.from) || to.duplicate(line.to))
			return true;
		return false;
	}
	
	public boolean contains(Coordinate2D coord) {
		if (coord.duplicate(from) || coord.duplicate(to))
			return true;
		
		if (coord.euclideanDistance(from) + coord.euclideanDistance(to) > from.euclideanDistance(to)) {
			return false;
		}
		
		return true;
	}
	
	public Coordinate2D getIntersectVirtualPoint(Line line) {
		Vector2D n1 = getNormalVector();
		Vector2D n2 = line.getNormalVector();
		double a = n1.getDx(), b = n1.getDy();
		double c = n2.getDx(), d = n2.getDy();
		double x1 = from.getX(), y1 = from.getY();
		double x2 = line.getFrom().getX(), y2 = line.getFrom().getY();
		
		double x = (1/(a*d - b*c)) * (d*(a*x1 + b*y1) - b*(c*x2 + d*y2));
		double y = ((a*x1 + b*y1) - a*x) / b;
		Coordinate2D p = new Coordinate2D(x, y);
		
		return p;
	}
	
	public double distance (Coordinate2D p) {
		Line temp = new Line(p, from);
		Vector2D n = getNormalVector();
		
		return Math.abs(temp.getDirectionalVector().dotProduct(n)) / n.norm2();
	}
	
	public Line getProjectionOn(Line line) {
		Line orthogonal = new Line(line.getNormalVector(), this.from);
		Coordinate2D fromProjection = line.getIntersectVirtualPoint(orthogonal);
		
		orthogonal = new Line(line.getNormalVector(), this.to);
		Coordinate2D toProjection = line.getIntersectVirtualPoint(orthogonal);
		
		return new Line(fromProjection, toProjection);
	}
	
	public Vector2D getDirectionalVector() {
		return new Vector2D(getDx(), getDy());
	}
	
	public Vector2D getNormalVector () {
		return new Vector2D(- getDy(), getDx());
	}
	
	public double getDx() {
		return to.getX() - from.getX();
	}
	
	public double getDy() {
		return to.getY() - from.getY();
	}
	
	public double length() {
		return from.euclideanDistance(to);
	}
	
	public boolean equals(Line line) {
		if ((from.duplicate(line.from) && to.duplicate(line.to)) ||
			(from.duplicate(line.to) && to.duplicate(line.from))) {
			return true;
		}
		
		return false;
	}
}
