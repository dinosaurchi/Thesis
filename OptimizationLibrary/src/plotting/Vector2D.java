package plotting;

public class Vector2D {
	private double dx = 0;
	private double dy = 0;

	public Vector2D (double dx, double dy) {
		this.dx = dx;
		this.dy = dy;
	}
	
	public double dotProduct(Vector2D v) {
		return getDx() * v.getDx() + getDy() * v.getDy();
	}
	
	public Vector2D scalarProduct(double c) {
		return new Vector2D(c * dx, c * dy);
	}
	
	public Vector2D plus (Vector2D v) {
		return new Vector2D(v.dx + dx, v.dy + dy);
	}
	
	public Vector2D minus (Vector2D v) {
		return new Vector2D(v.dx - dx, v.dy - dy);
	}
	
	public Vector2D projectionOnV(Vector2D v) {
		return v.scalarProduct(this.dotProduct(v) / (Math.pow(v.norm2(), 2)));
	}
	
	public Vector2D getNormalVector () {
		return new Vector2D(- dy, dx);
	}
	
	public double norm2() {
		return Math.sqrt(dx * dx + dy * dy);
	}
	
	public double getDy() {
		return dy;
	}
	public void setDy(double dy) {
		this.dy = dy;
	}
	public double getDx() {
		return dx;
	}
	public void setDx(double dx) {
		this.dx = dx;
	}
	
	
}
