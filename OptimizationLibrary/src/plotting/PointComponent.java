package plotting;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import javax.swing.JComponent;

import non_model_entity.Coordinate2D;
import non_model_entity.DirectedPair;
import support.SupportFunction;

public class PointComponent extends JComponent {
	private static final long serialVersionUID = 5838520268714853819L;
	
	protected static final Color BACKGROUND_COLOR = Color.WHITE;
	
	protected final int TRUE_HEIGHT;
	protected final int TRUE_WIDTH;
	protected final int GRAPH_HEIGHT;
	protected final int GRAPH_WIDTH;
	protected final int PADDING_WIDTH;
	protected final int PADDING_HEIGHT;
	
	protected final ArrayList<DirectedPair<Coordinate2D, Color>> coloredPoints;
	protected final ScalingInfo scalingInfo;
	
	public PointComponent(ArrayList<DirectedPair<Coordinate2D, Color>> coloredPoints, FrameInfo frameInfo, int width){
		if (coloredPoints == null)
			throw new NullPointerException();
		if (frameInfo == null)
			throw new NullPointerException();
		if (width < 1)
			throw new RuntimeException("Illegal width : " + width);
		
		this.setBackground(BACKGROUND_COLOR);
		this.coloredPoints = getBoundedColoredPoints(coloredPoints, frameInfo);
		this.TRUE_WIDTH = width;
		this.TRUE_HEIGHT = (int) (this.TRUE_WIDTH * getRatio(this.coloredPoints));
		
		/*
		 * Set the Maximum, Minimum and Preferred sizes to avoid rendering only half of the graph
		 * 
		 * */
		
		Dimension dim = new Dimension(TRUE_WIDTH, TRUE_HEIGHT);
		this.setPreferredSize(dim);
		this.setMaximumSize(dim);
		this.setMinimumSize(dim);
		this.setSize(dim);
						
		final double PADDING_PERCENTAGE = 0.05;
		this.PADDING_HEIGHT = (int) Math.round(this.TRUE_HEIGHT * PADDING_PERCENTAGE);
		this.PADDING_WIDTH = (int) Math.round(width * PADDING_PERCENTAGE);
		this.GRAPH_WIDTH = width - (this.PADDING_WIDTH * 2);
		this.GRAPH_HEIGHT = this.TRUE_HEIGHT - (this.PADDING_HEIGHT * 2);		
		this.scalingInfo = new ScalingInfo(getCoordinates(this.coloredPoints), GRAPH_WIDTH, GRAPH_HEIGHT);
	}
	
	private static double getRatio(ArrayList<DirectedPair<Coordinate2D, Color>> coloredPoints) {
		coloredPoints = new ArrayList<>(coloredPoints);
		ArrayList<Coordinate2D> coords = getCoordinates(coloredPoints);
		double [] xs = Coordinate2D.getXs(coords);
		double [] ys = Coordinate2D.getYs(coords);
		return (SupportFunction.max(ys) - SupportFunction.min(ys)) / (SupportFunction.max(xs) - SupportFunction.min(xs));
	}

	private static ArrayList<DirectedPair<Coordinate2D, Color>> getBoundedColoredPoints(ArrayList<DirectedPair<Coordinate2D, Color>> points, 
																						FrameInfo frameInfo) {
		points = new ArrayList<>(points);
		points.add(new DirectedPair<>(frameInfo.getTopLeft(), BACKGROUND_COLOR));
		points.add(new DirectedPair<>(frameInfo.getRightBottom(), BACKGROUND_COLOR));
		return points;
	}

	private static ArrayList<Coordinate2D> getCoordinates(ArrayList<DirectedPair<Coordinate2D, Color>> points) {
		ArrayList<Coordinate2D> coords = new ArrayList<>();
		for (DirectedPair<Coordinate2D, Color> p : points) {
			coords.add(p.getLeft());
		}
		return coords;
	}
	
	@Override
	public void paintComponent(Graphics gh) {
		super.paintComponent(gh);
		Graphics2D g2d = (Graphics2D) gh;
		g2d.setColor(BACKGROUND_COLOR);
		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
		g2d.setColor(getForeground());
		
		for (DirectedPair<Coordinate2D, Color> coord : coloredPoints) {
			g2d.setColor(coord.getRight());
	    	g2d.setStroke(new BasicStroke(4));
	    	
	    	double x = scalingInfo.getScalingX(coord.getLeft().getX()) + PADDING_WIDTH;
	    	double y = scalingInfo.getScalingY(coord.getLeft().getY()) + PADDING_HEIGHT;
			g2d.draw(new Line2D.Double(x, y, x, y));	    	
		}
	}	
}
