package plotting;

import java.util.ArrayList;

import non_model_entity.Coordinate2D;

public class ScalingInfo {
	private double xRange;
	private double yRange;
	private double xLeftMost;
	private double yBottom;
	private double width;
	private double height;
	
	public double getScalingX(double x) {
		return ((x - this.xLeftMost) / this.xRange) * this.width;
	}
	
	public double getScalingY(double y) {
		return ((y - this.yBottom) / this.yRange) * this.height;
	}
	
	public ScalingInfo (ArrayList<Coordinate2D> coordinates, double width, double height){
		if (coordinates == null)
			throw new NullPointerException();
		
		if (width < 1)
			throw new RuntimeException("Illegal width : " + width);
		
		if (height < 1)
			throw new RuntimeException("Illegal height : " + height);
		
		// Copy input data to another array
		coordinates = new ArrayList<>(coordinates);
		
		double xRightMost = coordinates.get(0).getX();
		double xLeftMost = xRightMost;
		
		double yBottom = coordinates.get(0).getY();
		double yUpper = yBottom;
		
		for (Coordinate2D p : coordinates) {
			double x = p.getX();
			if (x > xRightMost) {
				xRightMost = x;
			}
			
			if (x < xLeftMost) {
				xLeftMost = x;
			}
			
			double y = p.getY();
			if (y < yBottom) {
				yBottom = y;
			}
			
			if (y > yUpper) {
				yUpper = y;
			}
		}
			
		this.width = width;
		this.height = height;
		this.xRange = xRightMost - xLeftMost;
		this.yRange = yUpper -  yBottom;
		this.xLeftMost = xLeftMost;
		this.yBottom = yBottom;		
	}
}