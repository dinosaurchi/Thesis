package distribution_simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import non_model_entity.DirectedPair;

public class DistributionSimulator <T extends Object> {
	private final ArrayList<DirectedPair<T, Integer>> cumulativeObjectCounts;
	private final Random rand = new Random();
	private final int MAX;
	
	public DistributionSimulator(ArrayList<T> objects) {
		if (objects == null)
			throw new NullPointerException();
		if (objects.size() == 0)
			throw new RuntimeException("Nothing to simulate");
		
		MAX = objects.size();
		this.cumulativeObjectCounts = getCumulativeObjectCounts(objects);
	}
	
	private ArrayList<DirectedPair<T, Integer>> getCumulativeObjectCounts(ArrayList<T> objects) {
		Map<T, Integer> countMap = new HashMap<>();
		for (T obj : objects) {
			if (!countMap.containsKey(obj))
				countMap.put(obj, 1);
			else
				countMap.put(obj, countMap.get(obj) + 1);
		}
		
		ArrayList<DirectedPair<T, Integer>> result = new ArrayList<>();
		int previousCount = 0;
		for (T obj : countMap.keySet()) {
			previousCount += countMap.get(obj);
			result.add(new DirectedPair<T, Integer>(obj, previousCount));
		}
		return result;
	}
	
	public T getRandomObject() {
		int num = rand.nextInt(MAX);
		T obj = null;
		for (int i = cumulativeObjectCounts.size() - 1 ; i >= 0 ; --i) {
			DirectedPair<T, Integer> pair = cumulativeObjectCounts.get(i);
			if (num < pair.getRight()) {
				obj = pair.getLeft();
			}
			else
				break;
		}
		
		if (obj == null)
			throw new RuntimeException("Inconsistent random object with num : " + num);
		return obj;
	}
}
