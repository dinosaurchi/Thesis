package solution_report;

import model.AbstractModel;
import state.AbstractState;
import support.RunningInfo;
import support.SupportFunction;

public abstract class SolutionReporter <StateType extends AbstractState,
										ModelType extends AbstractModel<StateType, ?>> {
	
	private String currentDirectory;
	protected final ModelType model;
	
	@SuppressWarnings("unchecked")
	public SolutionReporter(ModelType model) {
		if (model == null)
			throw new NullPointerException();
		this.model = (ModelType) model.clone();
	}
	
	public final void exportReport(String reportDirectoryName, RunningInfo<StateType> info) {
		if (reportDirectoryName == null)
			throw new NullPointerException();
		if (info == null)
			throw new NullPointerException();
		
		SupportFunction.createDirectory(reportDirectoryName);
		setCurrentDirectory(reportDirectoryName);
		
		exportReport(info);
	}
	
	abstract protected void exportReport(RunningInfo<StateType> info);

	protected String getReportDirectory() {
		return this.currentDirectory;
	}
	
	private void setCurrentDirectory(String reportDirectoryName) {
		this.currentDirectory = reportDirectoryName;
	}
}
