package model_entity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class ModelEntity {
	private final String id;
	
	protected ModelEntity(ModelEntity entity) {
		if (entity == null)
			throw new NullPointerException();
		
		this.id = entity.id;
	}
	
	protected ModelEntity(String id) {
		if (id == null)
			throw new NullPointerException();
		
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ModelEntity))
			return false;
		
		return id.equals(((ModelEntity) obj).id);
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	
	public String getId() {
		return id;
	}
	
	abstract public JSONObject generateJsonObject();

	public static <E extends ModelEntity> JSONArray generateModelEntitiesJsonArray(ArrayList<E> entities) {
		if (entities == null)
			throw new NullPointerException();
		JSONArray array = new JSONArray();
		for (E e : entities) {
			array.put(e.generateJsonObject());
		}
		return array;
	}

	public static <E extends ModelEntity> ArrayList<String> getIds(ArrayList<E> entities) {
		if (entities == null)
			throw new NullPointerException();
		ArrayList<String> ids = new ArrayList<>();
		for (E e : entities) 
			ids.add(e.getId());
		return ids;
	}
}
