package global;

public class CONSTANT {
	
	// Running info keys
	public static final String OPTIMIZER = "optimizer";
	public static final String LAST_OBJECTIVE_SCORE = "last_objective_score";
	public static final String BEST_OBJECTIVE_SCORE = "best_objective_score";
	public static final String TOTAL_ITERATIONS = "total_iterations";
	public static final String RUNNING_TIME = "running_time";
	public static final String BEST_OBJECTIVE_COMPONENT_SCORES = "best_objective_component_scores";
}
