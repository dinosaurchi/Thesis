package move_generator;

import java.util.ArrayList;

import clustering.Cluster;
import clustering.ClusteringAlgorithm;
import model.DataPointModel;
import move.OneMove;
import non_model_entity.DataPoint;
import state.DataPointClassState;

public class ClustererMoveGenerator <StateType extends DataPointClassState, 
									 ModelType extends DataPointModel<StateType, ?>> 
					extends AbstractMoveGenerator<StateType, 
												  ModelType> 
{
	private final ClusteringAlgorithm algorithm;
	
	public ClustererMoveGenerator(ModelType model, ClusteringAlgorithm algorithm) {
		super(model);
		if (algorithm == null)
			throw new NullPointerException();
		this.algorithm = algorithm;
	}

	protected ClustererMoveGenerator(ClustererMoveGenerator<StateType, ModelType> clustererMoveGenerator) {
		super(clustererMoveGenerator);
		this.algorithm = clustererMoveGenerator.algorithm;
	}

	@Override
	public ClustererMoveGenerator<StateType, ModelType> clone() {
		return new ClustererMoveGenerator<StateType, ModelType>(this);
	}
	
	
	@Override
	public MovesList<StateType> generate(StateType state) {
		if (state == null)
			throw new NullPointerException();
		if (!model.getModelInfo().isConsistent(state))
			throw new RuntimeException("Inconsistent state");
		
		// We do not need to check whether 's' is an instance of DataPointClassState, 
		// ... because the above consistency check already did it
		ArrayList<Cluster> currentClusters = getCurrentClusters(state);
		ArrayList<Cluster> newClusters = algorithm.incrementalClustering(currentClusters, 1);
		
		return getMovesList(currentClusters, newClusters);
	}
	
	private MovesList<StateType> getMovesList(ArrayList<Cluster> oldClusters, ArrayList<Cluster> newClusters) {
		// Because we have already added a specific class id for each cluster, thus we can get it back to 
		// ... infer the moves
		assert oldClusters != null;
		assert newClusters != null;
		
		// The 2 clusters lists have to have the same set of cluster IDs
		if (!oldClusters.containsAll(newClusters))
			throw new RuntimeException("Inconsistent clustering process");
		
		MovesList<StateType> movesList = new MovesList<StateType>();
		for (Cluster newCluster : newClusters) {
			for (DataPoint dp : newCluster.getAssignDataPoints()) {
				Cluster oldCluster = getAssignedCluster(dp, oldClusters);
				if (!oldCluster.getId().equals(newCluster.getId())) {
					movesList.addMove(new OneMove<StateType>(newCluster.getId(), dp.getId()));
				}
			}
			// We do not need to care about the opposite move, mean the removal from the old cluster,
			// ... because the removal from one cluster is equivalent to an assignment for another cluster,
			// ... everything is preserved.
		}
		
		return movesList;
	}

	
	private static Cluster getAssignedCluster(DataPoint dataPoint, ArrayList<Cluster> oldClusters) {
		assert dataPoint != null;
		assert oldClusters != null;
		assert oldClusters.size() != 0;
		
		for (Cluster c : oldClusters) {
			if (c.getAssignDataPoints().contains(dataPoint))
				return c;
		}
		
		throw new RuntimeException("Not existing data point id : " + dataPoint.getId());
	}

	private ArrayList<Cluster> getCurrentClusters(StateType state) {
		assert state != null;
		ArrayList<Cluster> clusters = new ArrayList<>();
		ArrayList<String> cids = state.getClassIds();
		for (String cid : cids) {
			Cluster c = new Cluster(cid);
			ArrayList<String> assignedDataPointIds = state.getAssignedDataPointIdsList(cid);
			for (String dpid : assignedDataPointIds) {
				c.addDataPoint(model.getModelInfo().getDataPoint(dpid));
			}
			clusters.add(c);
		}
		
		assert clusters.size() == cids.size();
		return clusters;
	}
}
