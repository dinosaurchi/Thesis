package move_generator;

import java.util.ArrayList;

import move.Move;
import state.AbstractState;

public class MovesList <StateType extends AbstractState> {
	private final ArrayList<Move<StateType>> moves = new ArrayList<>();
	
	public void addMove(Move<StateType> move) {
		if (move == null)
			throw new NullPointerException();
		moves.add(move);
	}
	
	public ArrayList<Move<StateType>> getMoves() {
		return new ArrayList<>(moves);
	}
	
	public boolean contains(Move<StateType> move) {
		if (move == null)
			throw new NullPointerException();
		return moves.contains(move);
	}

	public void addMoves(MovesList<StateType> movesList) {
		if (movesList == null)
			throw new NullPointerException();
		moves.addAll(movesList.moves);
	}
}
