package move_generator;

import model.DataPointModel;
import move.OneMove;
import state.DataPointClassState;

public abstract class AssigningMoveGenerator <StateType extends DataPointClassState,
									 		  ModelType extends DataPointModel<StateType, ?>>  
				extends AbstractMoveGenerator<StateType, ModelType> {

	private final MovesList<StateType> movesList = new MovesList<>();
	
	public AssigningMoveGenerator(ModelType model) {
		super(model);
	}
	
	protected AssigningMoveGenerator(AssigningMoveGenerator<StateType, ModelType> moveGenerator) {
		super(moveGenerator);
	}
	
	@Override
	abstract public AssigningMoveGenerator<StateType, ModelType> clone();
	
	@Override
	public MovesList<StateType> generate(StateType state) {
		if (state == null)
			throw new NullPointerException();
		assigning(state);
		return this.movesList;
	}

	abstract protected void assigning(StateType state);
	
	protected void assign(String classId, String dataPointId) {
		if (classId == null)
			throw new NullPointerException();
		if (dataPointId == null)
			throw new NullPointerException();
		
		OneMove<StateType> move = new OneMove<>(classId, dataPointId);
		if (movesList.contains(move))
			throw new RuntimeException("Duplicated move : " + move.toString());
		movesList.addMove(move);
	}
}
