package move_generator;

import model.AbstractModel;
import state.AbstractState;

public abstract class AbstractMoveGenerator<StateType extends AbstractState,
											ModelType extends AbstractModel<StateType, ?>> {
	protected final ModelType model;

	protected AbstractMoveGenerator(AbstractMoveGenerator<StateType, ModelType> generator) {
		if (generator == null)
			throw new NullPointerException();
		this.model = generator.getModel();
	}
		
	@SuppressWarnings("unchecked")
	public AbstractMoveGenerator(ModelType model) {
		if (model == null)
			throw new NullPointerException();
		
		this.model = (ModelType) model.clone();
	}
	
	protected ModelType getModel() {
		return this.model;
	}
	
	abstract public AbstractMoveGenerator<StateType, ModelType> clone();
	
	abstract public MovesList<StateType> generate(StateType state);
}
