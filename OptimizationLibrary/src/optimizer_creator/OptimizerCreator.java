package optimizer_creator;

import clustering.SimpleKMeanAlgorithm;
import model.DataPointModel;
import move_generator.ClustererMoveGenerator;
import optimizer.Clusterer;
import state.DataPointClassState;

public class OptimizerCreator <StateType extends DataPointClassState, 
							   ModelType extends DataPointModel<StateType, ?>> {
	
	public Clusterer<StateType, ModelType> createSimpleKMean(ModelType model, int iterations, int k) {
		ClustererMoveGenerator<StateType, 
								ModelType> cluseringMoveGenerator = 
										new ClustererMoveGenerator<>(model, new SimpleKMeanAlgorithm(k));
		
		return new Clusterer<StateType, 
				   			 ModelType> (cluseringMoveGenerator, iterations);
	}
}
