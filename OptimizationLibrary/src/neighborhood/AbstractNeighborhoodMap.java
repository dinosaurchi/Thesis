package neighborhood;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import non_model_entity.DataPoint;

abstract public class AbstractNeighborhoodMap {
	
	/*
	DOCUMENTATION

	AbstractNeighborhoodMap object is an unmodifiable object, means that it is only defined in constructors. 
	From that assumption, we can apply pass by reference for AbstractNeighborhoodMap object, do not need 
	to have a deep copy.
	
	*/
	
	private final Map<String, ArrayList<String>> neighborhoodMap;
	
	public AbstractNeighborhoodMap(ArrayList<DataPoint> coordinates) {
		if (coordinates == null)
			throw new RuntimeException("Null input");
			
		neighborhoodMap = generateInitialMap(coordinates);
		constructNeighborhoodMap(coordinates);
		
		assert (neighborhoodMap != null) : "Null adjacent map";
	}	
	
	protected AbstractNeighborhoodMap(AbstractNeighborhoodMap map) {
		neighborhoodMap = new HashMap<>();
		neighborhoodMap.putAll(map.neighborhoodMap);
		assert neighborhoodMap.equals(map.neighborhoodMap); 
	}
	
	public ArrayList<String> getNeighborIds(String id) {
		if (id == null)
			throw new RuntimeException("Null input");
		
		if (!neighborhoodMap.containsKey(id))
			return null;
		
		return new ArrayList<String>(neighborhoodMap.get(id));
	}
	
	public boolean contains(String id) {
		if (id == null)
			throw new NullPointerException();
		
		return this.neighborhoodMap.containsKey(id);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof AbstractNeighborhoodMap))
			return false;
		
		AbstractNeighborhoodMap adjMap = (AbstractNeighborhoodMap) obj;
		
		return this.neighborhoodMap.equals(adjMap.neighborhoodMap);
	}
	
	abstract public AbstractNeighborhoodMap clone();
	
	
	//--------------------------------------------------------------------------------
	// Protected methods
	abstract protected void constructNeighborhoodMap(ArrayList<DataPoint> coordinates);

	protected void addNeighboringPair(String id1, String id2) {
		if (id1 == null)
			throw new RuntimeException("Null input");
		if (id2 == null)
			throw new RuntimeException("Null input");
		
		if (!neighborhoodMap.containsKey(id1))
			throw new RuntimeException("Not-existing id: " + id1);
		if (!neighborhoodMap.containsKey(id2))
			throw new RuntimeException("Not-existing id: " + id2);
		
		if (!neighborhoodMap.get(id1).contains(id2) && !neighborhoodMap.get(id2).contains(id1)) {
			neighborhoodMap.get(id1).add(id2);
			
			if (!id1.equals(id2))
				neighborhoodMap.get(id2).add(id1);
		}
		else if (!(neighborhoodMap.get(id1).contains(id2) && neighborhoodMap.get(id2).contains(id1))) {
			throw new RuntimeException("Inconsistent adjacency: " + id1 + ", " + id2);
		}
	}
	
	
	//--------------------------------------------------------------------------------
	// Private methods
	private static Map<String, ArrayList<String>> generateInitialMap(ArrayList<DataPoint> dataPoints) {
		assert dataPoints !=null;
		assert dataPoints.size() > 0;
		
		Map<String, ArrayList<String>> map = new HashMap<>();
		for (DataPoint data : dataPoints) {
			String id = data.getId();
			if (!map.containsKey(id)) {
				map.put(id, new ArrayList<String>());
			}
			else {
				throw new RuntimeException("Duplicated id: " + id);
			}
		}
		
		assert map.size() == map.keySet().size();
		return map;
	}
}
