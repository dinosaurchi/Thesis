package neighborhood;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import non_model_entity.DataPoint;
import non_model_entity.DataPointsBlock;
import support.SupportFunction;
import voronoi.GraphEdge;
import voronoi.Voronoi;

public class VoronoiMap extends AbstractNeighborhoodMap {

	public VoronoiMap(ArrayList<DataPoint> coordinates) {
		super(coordinates);
	}

	protected VoronoiMap(VoronoiMap voronoiMap) {
		super(voronoiMap);
	}

	@Override
	public VoronoiMap clone() {
		return new VoronoiMap(this);
	}
	
	// -----------------------------------------
		// Protected methods
	
	@Override
	protected void constructNeighborhoodMap(ArrayList<DataPoint> coordinates) {
		if  (coordinates == null)
			throw new RuntimeException("Null input");
		constructVoronoiDiagramBasedAdjacentMap(coordinates);
	}
	
	// -----------------------------------------
	// Private methods
	
	private void constructVoronoiDiagramBasedAdjacentMap(ArrayList<DataPoint> dataPoints) {
		if (dataPoints == null)
			throw new RuntimeException("Null input");
		if (dataPoints.size() <= 0)  
			throw new RuntimeException("Empty coordinates list");
				
		// Because the Voronoi constructor needs to sort the input points before running the algorithm,
		// ..., thus, in order to be consistent with the output edge result (because e.site1 and e.site2 are based
		// on the index reference of the sorted points), we need to sort it as the same way the Voronoi constructor will
		// sort it. 
		
		Collections.sort(dataPoints, new Comparator<DataPoint>() {
			@Override
			public int compare(DataPoint d1, DataPoint d2) {
				if (d1.getCoordinate().getX() > d2.getCoordinate().getX())
					return 1;
				if (d1.getCoordinate().getX() < d2.getCoordinate().getX())
					return -1;
				if (d1.getCoordinate().getY() > d2.getCoordinate().getY())
					return 1;
				if (d1.getCoordinate().getY() < d2.getCoordinate().getY())
					return -1;
				return 0;
			}
		});

    	ArrayList<DataPointsBlock> blocks = DataPointsBlock.getCommonBlockIds(dataPoints);
    	
    	// Make all the patients in the same block become neighbors to each others ...
    	// .. and find the max-min positions in term of x and y coordinates
    	
    	double[] xs = getXcoordinateValues(blocks), ys = getYcoordinateValues(blocks); 
    	assert ys.length == xs.length : "Different length between x values and y values";
    	
    	double minX = SupportFunction.min(xs), maxX = SupportFunction.max(xs);
    	double minY = SupportFunction.min(ys), maxY = SupportFunction.max(ys);
    	
    	final double scalePercentage = 0.5;
    	final double scaleUp = 1 + scalePercentage;
    	final double scaleDown = 1 - scalePercentage;
    	
    	Voronoi v = new Voronoi(Double.MIN_VALUE);
		List<GraphEdge> edges = v.generateVoronoi(xs, ys, minX * scaleDown, maxX* scaleUp, minY* scaleDown, maxY* scaleUp);
		
		// Create adjacent links among Id in 2 adjacent block based on the generated Voronoi diagram for blocks
		for (GraphEdge e : edges) {
			if (e.length() > 0)
				createFullyConnectedAdjacentMap(blocks.get(e.site1), blocks.get(e.site2));
		}
		
		// Create adjacent links among Id in block
		for (DataPointsBlock block : blocks) {
    		createFullyConnectedAdjacentMap(block, block);
    	}
	}
	
	private void createFullyConnectedAdjacentMap(DataPointsBlock block1, DataPointsBlock block2) {
		for (int i = 0; i < block1.size() ; ++i) {
			for (int j = 0 ; j < block2.size() ; ++j) {
				super.addNeighboringPair(block1.getId(i), block2.getId(j));
			}
		}
	}
	
	private static double[] getXcoordinateValues(ArrayList<DataPointsBlock> blocks) {
		if (blocks == null)
			throw new RuntimeException("Null input");
		
		final int n = blocks.size();
		double[] xValuesIn = new double [n]; 
    	for (int t = 0 ; t < n ; ++t) {
    		xValuesIn[t] = blocks.get(t).getX();
    	}
    	assert xValuesIn != null && xValuesIn.length == n;
    	return xValuesIn;
	}
	
	private static double[] getYcoordinateValues(ArrayList<DataPointsBlock> blocks) {
		if (blocks == null)
			throw new RuntimeException("Null input");
		
		final int n = blocks.size();
		double[] yValuesIn = new double [n]; 
    	for (int t = 0 ; t < n ; ++t) {
    		yValuesIn[t] = blocks.get(t).getY();
    	}
    	assert yValuesIn != null && yValuesIn.length == n;
    	return yValuesIn;
	}
	
	

	
}
