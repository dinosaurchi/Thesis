package move;

import java.util.ArrayList;

import state.AbstractState;

public abstract class Move <StateType extends AbstractState> {
	
	@Override
	abstract public boolean equals(Object obj);
	
	@Override
	abstract public int hashCode();
	
	abstract public StateType execute(StateType currentState);
	
	@Override
	abstract public String toString();
	
	abstract public Move<StateType> reverseMove(StateType currentState);
	
	abstract public ArrayList<UnitMove<StateType>> toUnitMoves();
}
