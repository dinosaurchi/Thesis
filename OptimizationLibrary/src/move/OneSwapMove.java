package move;

import java.util.ArrayList;

import non_model_entity.IndirectedPair;
import state.DataPointClassState;

public class OneSwapMove <StateType extends DataPointClassState> extends Move<StateType>{
	private final IndirectedPair<OneMove<StateType>, OneMove<StateType>> moves;
	
	public OneSwapMove(OneMove<StateType> move1, OneMove<StateType> move2) {
		if (move1 == null)
			throw new NullPointerException();
		if (move2 == null)
			throw new NullPointerException();
		if (move1.equals(move2))
			throw new RuntimeException("Duplicated move : " + this.toString());
		
		this.moves = new IndirectedPair<OneMove<StateType>, 
										OneMove<StateType>>(move1, move2);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof OneSwapMove))
			return false;
		
		return ((OneSwapMove<StateType>) obj).moves.equals(this.moves);
	}

	@Override
	public int hashCode() {
		return moves.hashCode();
	}

	@Override
	public StateType execute(StateType currentState) {
		if (currentState == null)
			throw new NullPointerException();
		
		String currentNid = currentState.getAssignedClassId(this.moves.getLeft().getConsideringDataPointId());
		if (!currentNid.equals(this.moves.getRight().getTargetClassId()))
			throw new RuntimeException("Inconsistent state");
		
		currentNid = currentState.getAssignedClassId(this.moves.getRight().getConsideringDataPointId());
		if (!currentNid.equals(this.moves.getLeft().getTargetClassId()))
			throw new RuntimeException("Inconsistent state");
		
		StateType newState = moves.getLeft().execute(currentState);
		newState = moves.getRight().execute(newState);
		
		return newState;
	}

	@Override
	public String toString() {
		return moves.getLeft().toString() + ", " + moves.getRight().toString();
	}

	@Override
	public OneSwapMove<StateType> reverseMove(StateType currentState) {
		if (currentState == null)
			throw new NullPointerException();
		
		String currentCid = currentState.getAssignedClassId(this.moves.getLeft().getConsideringDataPointId());
		if (!currentCid.equals(this.moves.getRight().getTargetClassId()))
			throw new RuntimeException("Inconsistent state");
		
		currentCid = currentState.getAssignedClassId(this.moves.getRight().getConsideringDataPointId());
		if (!currentCid.equals(this.moves.getLeft().getTargetClassId()))
			throw new RuntimeException("Inconsistent state");
		
		return new OneSwapMove<>(new OneMove<StateType>(this.moves.getLeft().getTargetClassId(), this.moves.getRight().getConsideringDataPointId()), 
								 new OneMove<StateType>(this.moves.getRight().getTargetClassId(), this.moves.getLeft().getConsideringDataPointId()));
	}

	@Override
	public ArrayList<UnitMove<StateType>> toUnitMoves() {
		ArrayList<UnitMove<StateType>> moves = new ArrayList<>();
		moves.add(this.moves.getLeft());
		moves.add(this.moves.getRight());
		return moves;
	}
	
}
