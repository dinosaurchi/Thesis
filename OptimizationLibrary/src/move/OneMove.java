package move;

import java.util.ArrayList;

import non_model_entity.DirectedPair;
import state.DataPointClassState;

/*
 * OneMove is a concrete class of UnitMove for DataPointClassState state
 * 
 * */

public class OneMove<StateType extends DataPointClassState> extends UnitMove<StateType> {
	private final DirectedPair<String, String> movePair;
	
	public OneMove(String targetClassId, String consideringDataPointId) {
		if (targetClassId == null)
			throw new NullPointerException();
		if (consideringDataPointId == null)
			throw new NullPointerException();
		
		this.movePair = new DirectedPair<String, String>(targetClassId, consideringDataPointId);
	}
	
	public String getTargetClassId() {
		return movePair.getLeft();
	}
	
	public String getConsideringDataPointId() {
		return movePair.getRight();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof OneMove))
			return false;
		
		OneMove<StateType> move = (OneMove<StateType>) obj;
		if (!this.movePair.equals(move.movePair))
			return false;
		
		return true;
	}

	@Override
	public int hashCode() {
		return movePair.hashCode();
	}

	@SuppressWarnings("unchecked")
	@Override
	public StateType execute(StateType currentState) {
		if (currentState == null)
			throw new NullPointerException();
		
		return (StateType) currentState.assignDataPointToClass(getConsideringDataPointId(), getTargetClassId());
	}

	@Override
	public String toString() {
		return "[" + movePair.getLeft() + " <- " + movePair.getRight() + "]";
	}

	@Override
	public OneMove<StateType> reverseMove(StateType currentState) {
		if (currentState == null)
			throw new NullPointerException();
		
		String previousCid = currentState.getAssignedClassId(this.movePair.getRight());
		return new OneMove<>(previousCid, this.movePair.getRight());
	}

	@Override
	public ArrayList<UnitMove<StateType>> toUnitMoves() {
		ArrayList<UnitMove<StateType>> result = new ArrayList<>();
		result.add(this);
		return result;
	}
}
