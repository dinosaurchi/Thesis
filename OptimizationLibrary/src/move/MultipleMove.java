package move;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import state.DataPointClassState;

public class MultipleMove <StateType extends DataPointClassState> extends Move <StateType> {
	private final Set<UnitMove<StateType>> movesList;
	
	public MultipleMove(ArrayList<UnitMove<StateType>> moves) {
		if (moves == null)
			throw new NullPointerException();
		
		this.movesList = new HashSet<>();
		for (UnitMove<StateType> move : moves) {
			if (this.movesList.contains(move))
				throw new RuntimeException("Duplicated move : " + move.toString());
			this.movesList.add(move);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof MultipleMove))
			throw new RuntimeException();
		
		MultipleMove<StateType> move = (MultipleMove<StateType>) obj;
		if (!move.movesList.equals(this.movesList))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		for (UnitMove<StateType> move : this.movesList)
			hash += move.hashCode();
		return hash;
	}

	@Override
	public StateType execute(StateType currentState) {
		if (currentState == null)
			throw new NullPointerException();
		for (UnitMove<StateType> move : this.movesList)
			currentState = move.execute(currentState);
		return currentState;
	}

	@Override
	public String toString() {
		String s = "";
		for (UnitMove<StateType> move : this.movesList)
			s += move.toString() + ", ";
		return s;
	}

	@Override
	public MultipleMove<StateType> reverseMove(StateType currentState) {
		if (currentState == null)
			throw new NullPointerException();
		
		ArrayList<UnitMove<StateType>> reverseMoves = new ArrayList<>();
		for (UnitMove<StateType> move : this.movesList) {
			reverseMoves.add(move.reverseMove(currentState));
		}
		return new MultipleMove<>(reverseMoves);
	}

	@Override
	public ArrayList<UnitMove<StateType>> toUnitMoves() {
		return new ArrayList<>(this.movesList);
	}
}
