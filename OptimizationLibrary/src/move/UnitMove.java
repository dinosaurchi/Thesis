package move;

import state.AbstractState;

public abstract class UnitMove <StateType extends AbstractState> extends Move<StateType> {
	abstract public UnitMove<StateType> reverseMove(StateType currentState);
}
