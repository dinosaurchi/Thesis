package non_model_entity;

public abstract class Pair<A, B> extends Object {
	protected final A v1;
	protected final B v2;
	
	public Pair(A v1, B v2) {
		this.v1 = v1;
		this.v2 = v2;
	}
	
	public A getLeft() {
		return this.v1;
	}
	
	public B getRight() {
		return this.v2;
	}
	
	@Override
	abstract public boolean equals(Object obj);
	
	@Override
	abstract public int hashCode();
	
	protected int getOriginalHashCode() {
		return super.hashCode();
	}
}
