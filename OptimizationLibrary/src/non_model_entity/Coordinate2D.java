package non_model_entity;

import java.util.ArrayList;

public class Coordinate2D {
	/*
	DOCUMENTATION

	Coordinate2D object is an unmodifiable object, means that it is only defined in constructors. 
	From that assumption, we can apply pass by reference for Coordinate2D object, do not need 
	to have a deep copy.

 */
	
	private final Double locX;
	private final Double locY;
	
	public Coordinate2D(Double locX, Double locY) {
		if (locX == null)
			throw new NullPointerException();
		if (locY == null)
			throw new NullPointerException();
		
		this.locX = locX;
		this.locY = locY;
	}
	
	public double getX() {
		return locX;
	}

	public Double getY() {
		return locY;
	}
	
	public double euclideanDistance (Coordinate2D c) {
		if (c == null)
			throw new NullPointerException();
		return Math.sqrt(Math.pow(this.locX - c.locX, 2) + Math.pow(this.locY - c.locY, 2));
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Coordinate2D))
			throw new RuntimeException();
		
		Coordinate2D coor = (Coordinate2D) obj;
		return this.locX.equals(coor.locX) && this.locY.equals(coor.locY);
	}

	@Override
	public int hashCode() {
		return this.locX.hashCode() + this.locY.hashCode() * this.locY.hashCode();
	}
	
	public boolean duplicate(Coordinate2D coord) {
		if (coord == null)
			throw new NullPointerException();
		
		return this.locX.equals(coord.locX) && this.locY.equals(coord.locY);
	}
	
	public static double [] getYs(ArrayList<Coordinate2D> coords) {
		if (coords == null)
			throw new NullPointerException();
		
		double [] result = new double[coords.size()];
		for (int i = 0 ; i < coords.size() ; ++i) {
			result[i] = coords.get(i).getY();
		}
		return result;
	}
	
	public static double [] getXs(ArrayList<Coordinate2D> coords) {
		if (coords == null)
			throw new NullPointerException();
		
		double [] result = new double[coords.size()];
		for (int i = 0 ; i < coords.size() ; ++i) {
			result[i] = coords.get(i).getX();
		}
		return result;
	}
}
