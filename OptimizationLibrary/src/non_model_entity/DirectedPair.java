package non_model_entity;

public class DirectedPair<A, B> extends Pair<A, B> {
	
	public DirectedPair(A v1, B v2) {
		super(v1, v2);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof DirectedPair))
			throw new RuntimeException("Invalid class : " + obj.getClass());
		
		@SuppressWarnings("unchecked")
		DirectedPair<A, B> p = (DirectedPair<A, B>) obj;
		
		return (p.v1.equals(v1) && p.v2.equals(v2));
	}
	
	@Override
	public int hashCode() {
		final int prime = 17;
		return (v1.hashCode() * prime + v2.hashCode()) * prime;
	}
}
