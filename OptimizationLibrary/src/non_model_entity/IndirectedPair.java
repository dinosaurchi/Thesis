package non_model_entity;

public class IndirectedPair<A, B> extends Pair<A, B>{
	public IndirectedPair(A v1, B v2) {
		super(v1, v2);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof IndirectedPair))
			return false;
		
		@SuppressWarnings("unchecked")
		IndirectedPair<A, B> p = (IndirectedPair<A, B>) obj;
		
		return (p.v1.equals(v1) && p.v2.equals(v2)) || 
				(p.v1.equals(v2) && p.v2.equals(v1));
	}
	
	@Override
	public int hashCode() {
		return v1.hashCode() * v2.hashCode();
	}
}
