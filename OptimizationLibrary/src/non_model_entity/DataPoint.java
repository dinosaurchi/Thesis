package non_model_entity;

public class DataPoint {
	private final DirectedPair<String, Coordinate2D> data;
	
	public DataPoint(String id, Coordinate2D coord) {
		if (id == null) 
			throw new NullPointerException();
		
		if (coord == null) 
			throw new NullPointerException();
		
		data = new DirectedPair<String, Coordinate2D>(id, coord);
	}
	
	public String getId() {
		return this.data.getLeft();
	}
	
	public Coordinate2D getCoordinate() {
		return this.data.getRight();
	}
	
	@Override
	public int hashCode() {
		return data.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof DataPoint))
			throw new RuntimeException("Invalid class : " + obj.getClass().getName());
		DataPoint dp = (DataPoint) obj;
		return this.data.equals(dp.data);
	}
}
