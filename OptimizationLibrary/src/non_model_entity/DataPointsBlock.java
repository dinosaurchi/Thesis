package non_model_entity;

import java.util.ArrayList;


public class DataPointsBlock {
	private final Double x;
	private final Double y;
	private final ArrayList<String> ids = new ArrayList<>();
	
	public DataPointsBlock (String id, Double x, Double y) {
		assert id != null;
		assert x != null;
		assert y != null;
		
		this.x = x;
		this.y = y;
		
		assert ids != null;
		ids.add(id);
	}
	
	public Double getX() {
		return x;
	}
	
	public Double getY() {
		return y;
	}
	
	public void addId(String id) {
		assert id != null;
		if (ids.contains(id))
			throw new RuntimeException("Duplicated patients in block");
			
		ids.add(id);
	}
	
	public String getId(int idx) {
		if (idx < 0 || idx >= ids.size())
			return null;
		return ids.get(idx);
	}
	
	public int size() {
		return ids.size();
	}
	
	public static ArrayList<DataPointsBlock> getCommonBlockIds(ArrayList<DataPoint> dataPoints) {
		if (dataPoints == null)
			throw new NullPointerException();
		
		ArrayList<DataPointsBlock> blocks = new ArrayList<>();
		Double previousX = new Double(0);
		Double previousY = new Double(0);
    	for (int i = 0 ; i < dataPoints.size(); ++i) {
    		Double x = dataPoints.get(i).getCoordinate().getX();
    		Double y = dataPoints.get(i).getCoordinate().getY();
	    	
	    	if (x.equals(previousX) && y.equals(previousY)) {
	    		blocks.get(blocks.size() - 1).addId(dataPoints.get(i).getId());
	    	}
	    	else {
	    		blocks.add(new DataPointsBlock(dataPoints.get(i).getId(), x, y));
	    	}
	    	
	    	previousX = x;
	    	previousY = y;
    	}
    	return blocks;
	}
	
	public static ArrayList<DataPoint> getSingleDataPoints(ArrayList<DataPoint> dataPoints) {
		if (dataPoints == null)
			throw new NullPointerException();
		
		// It returns a list of Data point which is not duplicated, mean for each duplicated ids block, it remains only one
		// .. id
		ArrayList<DataPointsBlock> blocks = getCommonBlockIds(dataPoints);
		ArrayList<DataPoint> result = new ArrayList<>();
		for (DataPointsBlock block : blocks) {
			String id = block.getId(0);
			for (DataPoint dp : dataPoints) {
				if (dp.getId().equals(id)) {
					result.add(dp);
					break;
				}
			}
		}
		return result;
	}
	
	// ------------------------------------------------------------------------------------------
	// PRIVATE methods
	
}
