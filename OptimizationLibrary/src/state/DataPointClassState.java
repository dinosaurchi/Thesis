package state;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import support.SupportFunction;

public abstract class DataPointClassState extends AbstractState {
	public static final String EMPTY_CLASS = "EMPTY_CLASS";
	
	// HomecareAssignmentState info
	private final Map<String, ArrayList<String>> classToDataPointMap = new HashMap<>();
	private final Map<String, String> dataPointToClassMap = new HashMap<>();
	
	protected DataPointClassState(DataPointClassState state) {
		super(state);

		// Copy constructor
		for (String nid : state.classToDataPointMap.keySet()) {
			ArrayList<String> newDataPointsIdsList = state.getAssignedDataPointIdsList(nid);

			// Because we cannot change a String object, thus we do not care about unexpected modification, 
			// then just simple pass its pointer
			this.classToDataPointMap.put(nid, newDataPointsIdsList);
			for (int i = 0 ; i < newDataPointsIdsList.size() ; ++i) {
				String dpid = newDataPointsIdsList.get(i);
				if (this.dataPointToClassMap.containsKey(dpid))
					throw new RuntimeException("Duplicated data point id");
				this.dataPointToClassMap.put(dpid, nid);
			}
		}

		assert (this.dataPointToClassMap.size() == state.dataPointToClassMap.size()) : "Different copied size";
		assert (this.classToDataPointMap.size() == state.classToDataPointMap.size()) : "Different copied size";
	}
	
	@Override
	abstract protected DataPointClassState makeCopy();
	
	protected DataPointClassState(ArrayList<String> dataPoints, ArrayList<String> classes) {
		super(null);
		// Generate random assignment based on the input data points list and nurses list
		for (String cid : classes) {
			if (!classToDataPointMap.containsKey(cid)) {
				classToDataPointMap.put(cid, new ArrayList<String>());
			}
			else {
				throw new RuntimeException("Duplicated class");
			}
		}
		
		// We generate a random solution 
		
		for (String pid : dataPoints) {
			if (!dataPointToClassMap.containsKey(pid)) {		
				// Initialize the assignment from a data point to a class with an empty Class id
				// We need this because we need to remove the old Class before assigning to the new one
				// ... in the assignDataPointToClass, if the data point has not been assigned to any class, then it will
				// ... return null for the patientToNurseMap.get(pid), and logically we do not have any patient
				// ... has no assignment to a class, thus in order to avoid this situation, we use EMPTY_CLASS
				// ... to initialize the assignment of a new data point. 
				// Note that EMPTY_CLASS wont be used in later assignment, it is just used as a trick avoid null map
				dataPointToClassMap.put(pid, EMPTY_CLASS);
				
				// Assign to a random class, it is acceptable for class has no assignment, but
				// ... a data point has to have assignment to a class.
				String nid = getRandomClassId(classes);
				assign(pid, nid);
			}
			else {
				throw new RuntimeException("Duplicated data point");
			}
		}				
	}
	
	protected DataPointClassState (String assignmentJsonFileName) {
		super(null);
		if (assignmentJsonFileName == null)
			throw new NullPointerException();
		
		JSONArray array = SupportFunction.parseJsonArrayFromFile(assignmentJsonFileName);	
		if (array == null)
			throw new NullPointerException();
		
		try {
			reconstructDataPointClassState(array);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}		
	}
	
	public DataPointClassState (JSONArray array) {
		super(null);
		if (array == null)
			throw new NullPointerException();
		try {
			this.reconstructDataPointClassState(array);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	public String getAssignedClassId(String dataPointId) {
		if (dataPointId == null)
			throw new NullPointerException();
		
		// It returns the current assigned Class if the data point p is in the loaded information
		//		-> If p is not assigned yet, it returns EMPTY_CLASSES
		// Otherwise, it returns null
		
		String cid = dataPointToClassMap.get(dataPointId);
		if (cid != null)
			return cid;
		else {
			throw new RuntimeException("Inconsistent Data point Id : " + dataPointId);
		}
	}
	
	public ArrayList<String> getAssignedDataPointIdsList(String cid) {
		if (cid == null)
			throw new NullPointerException();
		
		ArrayList<String> dataPointsList = classToDataPointMap.get(cid);
		if (dataPointsList != null) {
			return new ArrayList<>(dataPointsList);
		}
		else {
			throw new RuntimeException("Non-existing map Class - patients : " + cid);
		}
	}

	@Override
	public JSONArray generateStateJsonArray() {
		JSONArray result = new JSONArray();
		
		for (String cid : classToDataPointMap.keySet()) {
			ArrayList<String> temp = classToDataPointMap.get(cid);
			
			// double check before generating the assignment file
			for (String dpid : temp) {
				if (!dataPointToClassMap.containsKey(dpid)) {
					throw new RuntimeException("Class was mapped to a non-existing Data point");
				}
				else {
					if (!dataPointToClassMap.get(dpid).equals(cid)) {
						throw new RuntimeException("Inconsistent map was found");
					}
				}
			}
			
			JSONArray patientIdsJson = new JSONArray(temp);
			JSONObject pair = new JSONObject();
			try {
				pair.put(cid, patientIdsJson);
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
			result.put(pair);
		}
		
		return result;
	}

	public ArrayList<String> getDataPointIds() {
		return new ArrayList<>(dataPointToClassMap.keySet());
	}
	
	public ArrayList<String> getClassIds() {
		return new ArrayList<>(classToDataPointMap.keySet());
	}
	
	protected static boolean isEmptyClass (String cid) {
		if (cid == null)
			throw new NullPointerException();
		return cid.equals(EMPTY_CLASS);
	}
	
	@Override
	abstract public DataPointClassState clone();
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof DataPointClassState))
			return false;
		
		DataPointClassState a = (DataPointClassState) obj;
										
		if (!this.dataPointToClassMap.keySet().equals(a.dataPointToClassMap.keySet()))
			return false;
									
		if (!this.classToDataPointMap.keySet().equals(a.classToDataPointMap.keySet()))
			return false;
										
		for (String dpid : this.dataPointToClassMap.keySet()) {
			if (!this.getAssignedClassId(dpid).equals(a.getAssignedClassId(dpid)))
				return false;
		}
										
		for (String cid : this.classToDataPointMap.keySet()) {
			if (!this.getAssignedDataPointIdsList(cid).containsAll(a.getAssignedDataPointIdsList(cid)))
				return false;
		}
										
		return true;
	}

	public DataPointClassState assignDataPointsToClass(ArrayList<String> dataPointIds, String cid) {
		if (dataPointIds == null)
			throw new NullPointerException();
		if (cid == null)
			throw new NullPointerException();
		if (dataPointIds.size() < 1)
			throw new RuntimeException("Nothing to assign");
		DataPointClassState state = this.makeCopy();
		for (String dpid : dataPointIds) {
			state.assign(dpid, cid);
		}
		return state;
	}
	
	public DataPointClassState assignDataPointToClass(String dataPointId, String cid) {
		if (dataPointId == null)
			throw new NullPointerException();
		if (cid == null)
			throw new NullPointerException();
		
		DataPointClassState state = this.makeCopy();
		state.assign(dataPointId, cid);
		return state;
	}

	// -----------------------------------------------------------------------------------
	// Private methods
	
	private void assign(String dataPointId, String cid) {
		assert (dataPointId != null);
		assert (cid != null);
		
		removeDataPointFromCurrentClass(dataPointId);
		
		ArrayList<String> currentDataPointIdsList = classToDataPointMap.get(cid);
		if (currentDataPointIdsList == null)
			throw new RuntimeException("Not existing cid : " + cid);
		
		// Note that ArrayList<String> treats the 2 different strings have the same value as equal
		if (currentDataPointIdsList.contains(dataPointId)) {
			throw new RuntimeException("Inconsistent assignment : " + dataPointId + ", " + cid);
		}
		else {
			dataPointToClassMap.put(dataPointId, cid);
			currentDataPointIdsList.add(dataPointId);
		}		
	}
	
	private int curClassIdx = 0;
	private String getRandomClassId(ArrayList<String> cids) {
		if (cids.size() < 1) {
			throw new RuntimeException("No class to choose");
		}

		curClassIdx %= cids.size();
		return cids.get(curClassIdx++);
	}

	private void removeDataPointFromCurrentClass(String dpid) {
		if (!dataPointToClassMap.containsKey(dpid)) {
			throw new RuntimeException("Invalid Data point Id : " + dpid);
		}

		String currentClassId = dataPointToClassMap.get(dpid);
		// If there exists a mapping to a current class, just remove this mapping
		if (currentClassId != EMPTY_CLASS) {
			removeDataPoint(dpid, currentClassId);
		}
	}

	private void removeDataPoint(String dpid, String targetClassId) {
		// Remove data point is a private method because it is not a complete action, means it just simply detach a data point
		// from a target class. It leaves the job of assigning the data point to a new class for another method (Because 
		// ... we cannot have any data point with no assignments)

		ArrayList<String> dataPointIdsList = classToDataPointMap.get(targetClassId);

		// The logic here is, if the system work properly, we will not have to remove any data point from an empty class
		if (!dataPointIdsList.isEmpty()) {
			dataPointIdsList.remove(dpid);

			// Remove the reverse map from this data point to the current class, by setting the map from p to EMPTY_CLASS
			dataPointToClassMap.put(dpid, EMPTY_CLASS);
		}
		else {
			throw new RuntimeException("Inconsistent map");
		}
	}
	
	private void reconstructDataPointClassState(JSONArray array) throws IOException {
		if (array == null)
			throw new NullPointerException();
		
		classToDataPointMap.clear();
		dataPointToClassMap.clear();
		for (int i = 0 ; i < array.length() ; ++i) {
			JSONObject classToDataPoints;
			try {
				classToDataPoints = array.getJSONObject(i);
			
				ArrayList<String> temp = new ArrayList<>(getKeysSet(classToDataPoints.keys()));
				if (temp.size() != 1) {
					throw new RuntimeException("Incorrect assginment file, cannot have more than one pair or empty pair in 1 classToDataPoints map");
				}
				String cid = temp.get(0);
				
				if (classToDataPointMap.keySet().contains(cid)) {
					throw new RuntimeException("Duplicated class Id");
				}
				
				temp = SupportFunction.parseStringArrayFromJsonArray(classToDataPoints.getJSONArray(cid));
				
				ArrayList<String> pids = new ArrayList<>();
				for (String pid : temp) {
					if (dataPointToClassMap.keySet().contains(pid)) {
						throw new RuntimeException("Duplicated data point Id");
					}
					dataPointToClassMap.put(pid, cid);
					pids.add(pid);
				}
				classToDataPointMap.put(cid, pids);
			
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}				
	}

	private Set<String> getKeysSet(Iterator<?> keys) {
		Set<String> keysSet = new HashSet<>();
		while (keys.hasNext()) {
			keysSet.add(keys.next().toString());
		}
		return keysSet;
	}
}
