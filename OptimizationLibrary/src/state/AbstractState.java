package state;

import org.json.JSONArray;

public abstract class AbstractState {
	public final long GENERATED_ID;
	public static final long EMPTY_GENERATED_ID = Long.MIN_VALUE;
	
	protected AbstractState(AbstractState state) {
		if (state == null)
			this.GENERATED_ID = getNextStateId();
		else 
			this.GENERATED_ID = state.GENERATED_ID;
	}
	
	@Override
	abstract public AbstractState clone();
	
	@Override
	abstract public boolean equals(Object obj);
	
	abstract public JSONArray generateStateJsonArray();
	
	private static long currentAssignmentId = EMPTY_GENERATED_ID;
	private static synchronized long getNextStateId() {
		// Create a unique id for each assignment
		return ++currentAssignmentId;
	}	
	
	abstract protected AbstractState makeCopy();
}
