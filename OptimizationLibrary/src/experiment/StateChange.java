package experiment;

import model.AbstractModel;
import state.AbstractState;

public interface StateChange <StateType extends AbstractState, 
						 ModelType extends AbstractModel<StateType, ?>> {	
}
