package experiment;

import java.util.ArrayList;


import org.json.JSONException;
import org.json.JSONObject;

import context.Problem;
import global.CONSTANT;
import model.AbstractModel;
import non_model_entity.DirectedPair;
import optimizer.AbstractOptimizer;
import solution_report.SolutionReporter;
import state.AbstractState;
import support.RunningInfo;
import support.SupportFunction;

public class Experimenter <StateType extends AbstractState,
							ModelType extends AbstractModel<StateType, ?>> {
	
	protected final Problem<StateType, ModelType> problem;
	protected final ArrayList<DirectedPair<String, RunningInfo<StateType>>> runningInfosWithOptimizer;
	protected final StateType initialState;
	
	public Experimenter(Problem<StateType, ModelType> problem, ArrayList<DirectedPair<String, RunningInfo<StateType>>> priorRunningInfo) {
		if (problem == null)
			throw new NullPointerException();
		if (priorRunningInfo == null)
			throw new NullPointerException();
		this.problem = problem;
		this.initialState = problem.getLastState();
		this.runningInfosWithOptimizer = new ArrayList<>(priorRunningInfo);
	}
	
	public Experimenter(Problem<StateType, ModelType> problem) {
		if (problem == null)
			throw new NullPointerException();
		this.problem = problem;
		this.initialState = problem.getLastState();
		this.runningInfosWithOptimizer = new ArrayList<>();
	}
	
	public ArrayList<DirectedPair<String, RunningInfo<StateType>>> getRunningInfos() {
		return new ArrayList<>(this.runningInfosWithOptimizer);
	}
	
	public final void perform(AbstractOptimizer<StateType, ModelType> optimizer) {
		if (optimizer == null)
			throw new NullPointerException();
		
		problem.setOptimizer(optimizer);
		problem.solve();
		RunningInfo<StateType> info = problem.getLastRunningInfo();
		
		if (info == null)
			throw new RuntimeException("Null running info from experiment");
		this.runningInfosWithOptimizer
					.add(new DirectedPair<String, RunningInfo<StateType>>
							(optimizer.getClass().getName(), info));
	}
	
	public void exportExperimentalReports(String directory) {
		if (runningInfosWithOptimizer == null)
			throw new NullPointerException();
		if (runningInfosWithOptimizer.size() == 0)
			return;
		if (directory == null)
			throw new NullPointerException();
		
		SupportFunction.createDirectory(directory);
		
		try {
			SupportFunction.saveFile(initialState.generateStateJsonArray().toString(3), 
					directory + "/input-state.json");
			SupportFunction.saveFile(getSummaryJson().toString(3), 
					directory + "/summary.json");
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
				
		SolutionReporter<StateType, ModelType> reporter = problem.getReporter();
		for (int i = 0 ; i < runningInfosWithOptimizer.size() ; ++i) {
			String optimizerClass = runningInfosWithOptimizer.get(i).getLeft();
			String subDir = directory + "/" + i + " - " + optimizerClass;
			
			RunningInfo<StateType> info = runningInfosWithOptimizer.get(i).getRight();
			
			SupportFunction.createDirectory(subDir);
			try {
				SupportFunction.saveFile(info.getCurrentState().generateStateJsonArray().toString(3), 
										 subDir + "/output-state.json");
				reporter.exportReport(subDir, info);
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		runningInfosWithOptimizer.clear();
	}

	private JSONObject getSummaryJson() {
		JSONObject summary = new JSONObject();
		
		double total = 0;
		for (int i = 0 ; i < runningInfosWithOptimizer.size() ; ++i) {
			total += Double.parseDouble(runningInfosWithOptimizer.get(i).getRight().getValue(CONSTANT.RUNNING_TIME));
		}
		
		try {
			summary.put(CONSTANT.RUNNING_TIME, total);
			summary.put(CONSTANT.BEST_OBJECTIVE_SCORE, problem.getModel().getObjectiveScore(problem.getBestState()));
			summary.put(CONSTANT.BEST_OBJECTIVE_COMPONENT_SCORES, problem.getModel().getObjectiveComponentMap(problem.getBestState()));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return summary;
	}
	
	public Problem<StateType, ModelType> getProblem(){
		return this.problem.clone();
	}
}
