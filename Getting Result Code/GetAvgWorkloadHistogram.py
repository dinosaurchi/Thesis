import json
import os
import matplotlib.pyplot as plt
import numpy as np
from SupportFunction import *

advanceTabuReport = "../report-20170721-020642 (real data)"
simpleTabuReport = "../report-20170721-032625 (real data - simple tabu)"

def getAverageWorkload(mapName, allReportsPath, changeRange):
	avgWorkloads = []
	for freport in os.listdir(allReportsPath):
		reportPath = allReportsPath + "/" + freport;
		if os.path.isdir(reportPath) is False:
			continue

		for fcase in os.listdir(reportPath):
			if fcase.startswith("report-" + mapName):
				casePath = reportPath + "/" + fcase
				for i in changeRange:
					changePath = casePath + "/" + "report-change-" + str(i)
					lastOptPath = changePath + "/" + getLastOptimzerPath(changePath)
					avgWorkload, stdWorkload, maxVariance = getWorkloadInfo(lastOptPath)
					avgWorkloads.append(float(avgWorkload))
	return avgWorkloads

def runRealStatistic(figDir, testType="initial"):
	figureSize = (8, 5)
	changeRange = range(0, 1)
	stdMaxX = 4
	maxVarMaxX = 8

	stdMaxY = 60
	stdBinWidth = 0.1
	maxVarMaxY = 60
	maxVarBinWidth = 0.5

	if testType is "simulating":
		changeRange = range(1, 7)
		stdMaxY = 160
		stdBinWidth = 0.1
		maxVarMaxY = 100
		maxVarBinWidth = 0.22

	avgWorkloads1 = getAverageWorkload("", advanceTabuReport, changeRange)
	avgWorkloads2 = getAverageWorkload("", simpleTabuReport, changeRange)

	figPath = figDir + '/avgWorkload-' + testType + '-' + "real" + '.png';
	plotFig(avgWorkloads1, avgWorkloads2, figPath, minX, maxX, maxY, binWidth, figureSize)


def runStatistic(figDir, testType="initial"):
	figureSize = (8, 5)
	changeRange = range(0, 1)
	minX = 190
	maxX = 300
	maxY = 30
	binWidth = 5

	reportDir = "/Users/macpro/Google Drive/APCS/Subjects/THESIS/Project/Report"
	if testType is "simulating":
		changeRange = range(1, 7)
		minX = 190
		maxX = 300
		maxY = 70
		binWidth = 5

	for mapType in ['A', 'B', 'C']:
		avgWorkloads1 = getAverageWorkload(mapType, reportDir + "/report-20170709-230936 (complete)", changeRange)
		avgWorkloads2 = getAverageWorkload(mapType, reportDir + "/report-20170720-024749 (simple tabu)", changeRange)

		figPath = figDir + '/avgWorkload-' + testType + '-' + mapType + '.png';
		plotFig(avgWorkloads1, avgWorkloads2, figPath, minX, maxX, maxY, binWidth, figureSize)


#runStatistic("./testFig", "initial")
#runStatistic("./testFig", "simulating")
runRealStatistic("./realTestFig", "initial")
runRealStatistic("./realTestFig", "simulating")


