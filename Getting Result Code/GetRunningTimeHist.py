import json
import os
import matplotlib.pyplot as plt
import numpy as np
from SupportFunction import *

def getRunningTime(mapName, allReportsPath):
	runningTimes = []
	for freport in os.listdir(allReportsPath):
		reportPath = allReportsPath + "/" + freport;
		if os.path.isdir(reportPath) is False:
			continue

		for fcase in os.listdir(reportPath):
			if fcase.startswith("report-" + mapName):
				casePath = reportPath + "/" + fcase
				for i in range(1, 7):
					changePath = casePath + "/" + "report-change-" + str(i)
					runningTimes.append(getRunning(changePath))

	return runningTimes

def runStatistic(figDir):
	figureSize = (8, 5)
	minX = 0
	maxX = 800
	maxY = 800
	binWidth = 10

	reportDir = "/Users/macpro/Google Drive/APCS/Subjects/THESIS/Project/Report"
	for mapType in ['']:
		runningTimes1 = getRunningTime(mapType, reportDir + "/report-20170709-230936 (complete)")
		runningTimes2 = getRunningTime(mapType, reportDir + "/report-20170720-024749 (simple tabu)")

		continue
		figPath = figDir + '/runningTime-' + mapType + '.png';
		plotFig(runningTimes1, runningTimes2, figPath, minX, maxX, maxY, binWidth, figureSize)

runStatistic("./testFig")

