import numpy as np
import os
import json
import matplotlib.pyplot as plt

def subPlot(subPlotId, title, data, bins_range, maxY):
	plt.subplot(subPlotId)
	plt.gca().set_ylim([0, maxY])
	plt.hist(data, bins=bins_range, color='blue')
	plt.axvline(np.average(data), color='r', linestyle='dashed', linewidth=2)
	plt.axvline(np.median(data), color='r', linestyle='dotted', linewidth=2)
	plt.title(title)
	plt.xlabel("proportion (%)")
	plt.ylabel("counts")

def plotFig(data1, data2, figPath, minX, maxX, maxY, binWidth, figureSize):
	bins_range = np.arange(minX, maxX, binWidth)
	plt.figure(figsize=figureSize)

	subPlot(211, "Proposed Tabu search", data1, bins_range, maxY)
	subPlot(212, "Simple Tabu search", data2, bins_range, maxY)
	plt.tight_layout()

	plt.savefig(figPath, dpi=100)

	plt.clf()
	plt.cla()

def getExceed(values, t):
	res = []
	for v in values:
		if v > t:
			res.append(v)
	return res

def getExceedAtLeastOne(values1, t1, values2, t2):
	if len(values1) != len(values2):
		raise "Error"
	res = []
	for i in range(0, len(values1)):
		if values1[i] > t1 or values2[i] > t2:
			res.append((values1[i], values2[i]))
	return res

def getDistribution(data, bins_range):
	hist, bin_edges = np.histogram(data, bins=bins_range, density=True)
	#print bin_edges
	#print [hist[i] / sum(hist) for i in range(0, len(hist))]

	bin_midpoints = bin_edges[:-1] + np.diff(bin_edges)/2
	#print (bin_edges)
	cdf = np.cumsum(hist)
	values = np.random.rand(len(data))
	value_bins = np.searchsorted(cdf, values)
	random_from_cdf = bin_midpoints[value_bins]
	return random_from_cdf

def getOrder(optimizerName):
	return int(optimizerName.split(" - ")[0])

def getLastOptimzerPath(changePath):
	lastOpt = None
	highest = 0
	for optimizerName in os.listdir(changePath):
		if "optimizer." not in optimizerName:
			continue

		order = getOrder(optimizerName)
		if order > highest:
			highest = order
			lastOpt = optimizerName
	return lastOpt 

def getWorkloadInfo(optimizerPath):
	contextInfo = json.load(open(optimizerPath + '/context_info.json'))
	workload = contextInfo["workload"]
	return workload["average"], workload["std"], workload["max_variance"]

def getCorrectProportion(followUpCase):
	return float(followUpCase["total_assigned_to_previous_nurse"]) / float(followUpCase["total_cases"])

def getRunning(changePath):
	summary = json.load(open(changePath + '/summary.json'))
	return summary['running_time']

def getFollowUpInfo(optimizerPath):
	contextInfo = json.load(open(optimizerPath + '/context_info.json'))
	followUpInfo = contextInfo["follow_up_info"]
	followUpImportantProp = getCorrectProportion(followUpInfo["follow_up_important"])
	notFollowUpImportantProp = getCorrectProportion(followUpInfo["follow_up_not_important"])
	return followUpImportantProp, notFollowUpImportantProp
