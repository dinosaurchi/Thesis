import json
import os
import matplotlib.pyplot as plt
import numpy as np
from SupportFunction import *

stdConstraint = 4
maxVarConstraint = 8
followUpConstraint = 90
notFollowUpConstraint = 75
constraints = (stdConstraint, maxVarConstraint, followUpConstraint, notFollowUpConstraint)

def getFeasibleProportion(mapName, allReportsPath, changeRange, isInitial):
	global constraints

	results = []
	for freport in os.listdir(allReportsPath):
		reportPath = allReportsPath + "/" + freport;
		if os.path.isdir(reportPath) is False:
			continue

		for fcase in os.listdir(reportPath):
			if fcase.startswith("report-" + mapName):
				casePath = reportPath + "/" + fcase
				for i in changeRange:
					changePath = casePath + "/" + "report-change-" + str(i)
					lastOptPath = changePath + "/" + getLastOptimzerPath(changePath)
					
					avgWorkload, stdWorkload, maxVariance = getWorkloadInfo(lastOptPath)
					stdProp = float(stdWorkload) / float(avgWorkload) * 100.0
					maxVarProp = float(maxVariance) / float(avgWorkload) * 100.0
					
					followUpProp, notFollowUpProp = getFollowUpInfo(lastOptPath)
					followUpProp *= 100.0
					notFollowUpProp *= 100.0

					results.append((stdProp, maxVarProp, followUpProp, notFollowUpProp));

	exceedAtLeastOne = getExceedAtLeastOneForTuple(results, constraints)
	temps = allReportsPath.split("/")
	print temps[len(temps) - 1] + ' - ' + mapName
	print "   Feasible : " + str(100 - float(len(exceedAtLeastOne)) / float(len(stdProportions)) * 100) + " %"

def runStatistic(isInitial):
	if isInitial:
		changeRange = range(0, 1)
	else:
		changeRange = range(1, 7)

	reportDir = "/Users/macpro/Google Drive/APCS/Subjects/THESIS/Project/Report/official"
	for mapType in ['A', 'B', 'C']:
		p1 = getFeasibleProportion(mapType, reportDir + "/report-20170709-230936 (complete)", changeRange, isInitial)
		p2 = getFeasibleProportion(mapType, reportDir + "/report-20170720-024749 (simple tabu)", changeRange, isInitial)

runStatistic("initial")




