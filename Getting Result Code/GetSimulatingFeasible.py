import json
import os
import matplotlib.pyplot as plt
import numpy as np
from SupportFunction import *

stdConstraint = 4
maxVarConstraint = 8
followUpConstraint = 90
notFollowUpConstraint = 75

def getFeasibleProportion(mapName, allReportsPath):
	global stdConstraint, maxVarConstraint, followUpConstraint, notFollowUpConstraint

	followUpProps = []
	notFollowUpProps = []
	stdProportions = []
	maxVarProportions = []

	for freport in os.listdir(allReportsPath):
		reportPath = allReportsPath + "/" + freport;
		if os.path.isdir(reportPath) is False:
			continue

		for fcase in os.listdir(reportPath):
			if fcase.startswith("report-" + mapName):
				casePath = reportPath + "/" + fcase
				for i in range(1, 7):
					changePath = casePath + "/" + "report-change-" + str(i)
					lastOptPath = changePath + "/" + getLastOptimzerPath(changePath)
					
					followUpProp, notFollowUpProp = getFollowUpInfo(lastOptPath)
					followUpProps.append(followUpProp * 100.0)
					notFollowUpProps.append(notFollowUpProp * 100.0)

					avgWorkload, stdWorkload, maxVariance = getWorkloadInfo(lastOptPath)
					stdProportions.append(float(stdWorkload) / float(avgWorkload) * 100.0)
					maxVarProportions.append(float(maxVariance) / float(avgWorkload) * 100.0)

	exceedFollowUp = getExceed(followUpProps, followUpConstraint)
	exceedNotFollowUp = getExceed(notFollowUpProps, notFollowUpConstraint)

	print allReportsPath + ' - ' + mapName
	print "Feasible Follow 		: " + str(float(len(exceedFollowUp)) / float(len(followUpProps)) * 100) + " %"
	print "Feasible Not Follow 	: " + str(float(len(exceedNotFollowUp)) / float(len(notFollowUpProps)) * 100) + " %"
	
	return exceedFollowUp, exceedNotFollowUp

def runStatistic(figDir, testType):
	figureSize = (8, 5)
	followUpImportantMinX = 90
	followUpNotImportantMinX = 75
	followUpImportantMaxY = 80
	followUpNotImportantMaxY = 30
	followUpImportantBinWidth = 1
	followUpNotImportantBinWidth = 1

	reportDir = "/Users/macpro/Google Drive/APCS/Subjects/THESIS/Project/Report"
	for mapType in ['A', 'B', 'C']:
		followUpProps1, notFollowUpProps1 = getFeasibleFollowUpProportion(mapType, reportDir + "/report-20170709-230936 (complete)")
		followUpProps2, notFollowUpProps2 = getFeasibleFollowUpProportion(mapType, reportDir + "/report-20170720-024749 (simple tabu)")

		continue
		figPath = figDir + '/followUpImportantProp-' + testType + '-' + mapType + '.png';
		plotFig(followUpProps1, followUpProps2, figPath, followUpImportantMinX, 101, followUpImportantMaxY, followUpImportantBinWidth, figureSize)

		figPath = figDir + '/followUpNotImportantProp-' + testType + '-' + mapType + '.png';
		plotFig(notFollowUpProps1, notFollowUpProps2, figPath, followUpNotImportantMinX, 101, followUpNotImportantMaxY, followUpNotImportantBinWidth, figureSize)


runStatistic("./realTestFig", "simulating")

