import json
import os
import matplotlib.pyplot as plt
import numpy as np
from SupportFunction import *

stdConstraint = 4
maxVarConstraint = 8

def getFeasibleStdMaxVarProportion(mapName, allReportsPath, changeRange):
	global stdConstraint, maxVarConstraint

	stdProportions = []
	maxVarProportions = []
	for freport in os.listdir(allReportsPath):
		reportPath = allReportsPath + "/" + freport;
		if os.path.isdir(reportPath) is False:
			continue

		for fcase in os.listdir(reportPath):
			if fcase.startswith("report-" + mapName):
				casePath = reportPath + "/" + fcase
				for i in changeRange:
					changePath = casePath + "/" + "report-change-" + str(i)
					lastOptPath = changePath + "/" + getLastOptimzerPath(changePath)
					avgWorkload, stdWorkload, maxVariance = getWorkloadInfo(lastOptPath)
					
					stdProportions.append(float(stdWorkload) / float(avgWorkload) * 100.0)
					maxVarProportions.append(float(maxVariance) / float(avgWorkload) * 100.0)

	exceedStd = getExceed(stdProportions, stdConstraint)
	exceedMaxVar = getExceed(maxVarProportions, maxVarConstraint)
	exceedAtLeastOne = getExceedAtLeastOne(stdProportions, stdConstraint, maxVarProportions, maxVarConstraint)

	print allReportsPath + ' - ' + mapName
	print "   Feasible Std 		: " + str(100 - float(len(exceedStd)) / float(len(stdProportions)) * 100) + " %"
	print "   Feasible Max Var 		: " + str(100 - float(len(exceedMaxVar)) / float(len(maxVarProportions)) * 100) + " %"
	print "   Feasible All 		: " + str(100 - float(len(exceedAtLeastOne)) / float(len(stdProportions)) * 100) + " %"
	
	feasibleStd = [x for x in stdProportions if x not in exceedStd]
	feasibleMaxVar = [x for x in maxVarProportions if x not in exceedMaxVar]
	return feasibleStd, feasibleMaxVar

def runStatistic(figDir, testType="initial"):
	figureSize = (8, 5)
	changeRange = range(0, 1)
	stdMaxX = stdConstraint
	maxVarMaxX = maxVarConstraint

	stdMaxY = 60
	stdBinWidth = 0.1
	maxVarMaxY = 60
	maxVarBinWidth = 0.5

	reportDir = "/Users/macpro/Google Drive/APCS/Subjects/THESIS/Project/Report/official"
	if testType is "simulating":
		changeRange = range(1, 7)
		stdMaxY = 160
		stdBinWidth = 0.1
		maxVarMaxY = 100
		maxVarBinWidth = 0.22

	for mapType in ['A', 'B', 'C']:
		stdProp1, maxVarProp1 = getFeasibleStdMaxVarProportion(mapType, reportDir + "/report-20170709-230936 (complete)", changeRange)
		stdProp2, maxVarProp2 = getFeasibleStdMaxVarProportion(mapType, reportDir + "/report-20170720-024749 (simple tabu)", changeRange)

		continue
		figPath = figDir + '/stdProp-' + testType + '-' + mapType + '.png';
		plotFig(stdProp1, stdProp2, figPath, 0, stdMaxX, stdMaxY, stdBinWidth, figureSize)

		figPath = figDir + '/maxVarProp-' + testType + '-' + mapType + '.png';
		plotFig(maxVarProp1, maxVarProp2, figPath, 0, maxVarMaxX, maxVarMaxY, maxVarBinWidth, figureSize)

#runStatistic("./testFig", "initial")
runStatistic("./testFig", "simulating")




