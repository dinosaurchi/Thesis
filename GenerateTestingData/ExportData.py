import json
import os

def getData(dataType, jsonFilePath, isFloat):
	result = []
	for f in os.listdir(jsonFilePath):
		if f.endswith('.json'):
			with open(jsonFilePath + '/' + f) as file:
				data = json.load(file)
				for i in range(0, len(data)):
					t = data[i][dataType]
					if isFloat is True:
						t = float(data[i][dataType])
					result.append(t)
	return result

treatmentTimes = getData('serviceTime', './info-temp/visit-info', True)
with open('treatment-times.json', 'w') as outfile:
	json.dump(treatmentTimes, outfile, indent=3)

visitTypes = getData('visitType', './info-temp/visit-info', False)
with open('visit-types.json', 'w') as outfile:
	json.dump(visitTypes, outfile, indent=3)

