import json
import os
import matplotlib.pyplot as plt
import numpy as np

def getDistribution(data, bins_range):
	hist, bin_edges = np.histogram(data, bins=bins_range, density=True)
	#print bin_edges
	#print [hist[i] / sum(hist) for i in range(0, len(hist))]

	bin_midpoints = bin_edges[:-1] + np.diff(bin_edges)/2
	#print (bin_edges)
	cdf = np.cumsum(hist)
	values = np.random.rand(len(data))
	value_bins = np.searchsorted(cdf, values)
	random_from_cdf = bin_midpoints[value_bins]
	return random_from_cdf

info_dir = "./info-temp"

patients = json.load(open(info_dir + '/patient-info-153.json'))
isFollowUpPatients = {}
for p in patients:
	if p['isFollowUpImportant'] == 'true':
		isFollowUpPatients[p['pid']] = True
	else:
		isFollowUpPatients[p['pid']] = False

visitDirPath = info_dir + '/' + 'visit-info'
timesAll = []
timesFollowUp = []
timeNotFollowUp = []
timePatientAll = {}
timePatientFollowUp = {}
timePatientNotFollowUp = {}
for f in os.listdir(visitDirPath):
	if f.endswith('.json'):
		with open(visitDirPath + '/' + f) as file:
			data = json.load(file)
			for i in range(0, len(data)):
				t = float(data[i]['serviceTime'])
				timesAll.append(t)
				pid = data[i]['pid']
				timePatientAll[pid] = t
				if isFollowUpPatients[pid] == True:
					timesFollowUp.append(t)
					timePatientFollowUp[pid] = t
				else:
					timeNotFollowUp.append(t)
					timePatientNotFollowUp[pid] = t

bins_range = np.arange(10.0, 70.1, 5.0)
#random_from_cdf = getDistribution(timesAll, bins_range)

plt.subplot(111)
plt.hist(timePatientAll.values(), bins=bins_range)
plt.title("All treatment times")
plt.xlabel("Treatment time")
plt.ylabel("occurrence")

# plt.subplot(121)
# plt.hist(timePatientFollowUp.values(), bins=bins_range)
# plt.title("Long-term")
# plt.xlabel("Treatment time")
# plt.ylabel("occurrence")

# plt.subplot(122)
# plt.hist(timePatientNotFollowUp.values(), bins=bins_range)
# plt.title("Not long-term")
# plt.xlabel("Treatment time")
# plt.ylabel("occurrence")

# plt.subplot(122)
# plt.hist(random_from_cdf, bins=bins_range)
# plt.title("Random Treatment time")
# plt.xlabel("Treatment time")
# plt.ylabel("Occurence")
plt.show()