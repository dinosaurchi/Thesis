package main_generator;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;

import model_entity.CLSC;
import model_entity.Patient;
import non_model_entity.DataPoint;
import puller.Puller;
import support.SupportFunction;

public class MainBuildDistanceMatrix {
	public static void main(String[] args) {
		Puller puller = new Puller("./ApiKeys.txt");
		String [] dirPaths = {"./generated-info-C-20170705-010827"};
		
		for (String curDirPath : dirPaths) {
			try {
				while (!pullDistanceMatrix(puller, curDirPath)) {
					TimeUnit.SECONDS.sleep(60);
				}
				TimeUnit.SECONDS.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
	}
	
	private static boolean pullDistanceMatrix(Puller puller, String curDirPath) {
		JSONArray array = SupportFunction.parseJsonArrayFromFile(curDirPath + "/" + getPatientInfoFileName(curDirPath));
		ArrayList<Patient> patients = Patient.parsePatientsListFromJsonArray(array);
		array = SupportFunction.parseJsonArrayFromFile("./raw-info/clsc-info-1.json");
		ArrayList<CLSC> clscs = CLSC.parseClscsListFromJsonArray(array);
		return puller.updateDistanceMatrix(getDataPoints(patients, clscs), curDirPath);
	}
	
	private static String getPatientInfoFileName(String curDirPath) {
		if (curDirPath == null)
			throw new NullPointerException();
		String [] fileNames = SupportFunction.getFilesName(curDirPath, "json");
		return SupportFunction.getPrefixMatch("patient-info", fileNames);
	}

	private static ArrayList<DataPoint> getDataPoints(ArrayList<Patient> patients, ArrayList<CLSC> clscs) {
		ArrayList<DataPoint> points = new ArrayList<>();
		for (CLSC clsc : clscs)
			points.add(new DataPoint(clsc.getId(), clsc.getCoordinate()));
		for (Patient p : patients)
			points.add(new DataPoint(p.getId(), p.getCoordinate()));
		
		return points;
	}
}
