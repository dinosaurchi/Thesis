package main_generator;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;

import generator.CoordinateGenerator;
import generator.EntityInfoGenerator;
import global.CHI_CONSTANT;
import non_model_entity.Coordinate2D;
import plotting.Plotter;
import support.SupportFunction;

public class MainGenerateEntityInfo {
	
	public static void main(String[] args) {
		EntityInfoGenerator entityInfoGenerator = new EntityInfoGenerator();
		CoordinateGenerator coorGenerator = new CoordinateGenerator();
		
		String [] types = {"A", "B", "C"};
		Random rand = new Random();
		
		for (String type : types) {
			int totalPatients = rand.nextInt(20) + 165;
			ArrayList<Coordinate2D> patientCoordinates = coorGenerator.generate(totalPatients, type, 
																				CHI_CONSTANT.FRAME_INFO.getTopLeft(), 
																				CHI_CONSTANT.FRAME_INFO.getRightBottom());
			
			String outDirPath = "./generated-info-" + type + "-" + SupportFunction.getCurrentTimeStamp();
			entityInfoGenerator.generateFromCoordinates(outDirPath, 
										 patientCoordinates,
										 SupportFunction.parseJsonArrayFromFile("./raw-info/nurse-info-12.json"),
										 SupportFunction.parseJsonArrayFromFile("./raw-info/clsc-info-1.json"),
										 SupportFunction.parseJsonArrayFromFile("./raw-info/treatment-times.json"), 
										 SupportFunction.parseJsonArrayFromFile("./raw-info/visit-types.json"));
			
			JFrame frame = Plotter.plot2DPointsWithDense(patientCoordinates, CHI_CONSTANT.FRAME_INFO, false);
			SupportFunction.savePlot(frame, outDirPath + "/location-graph");
		}
	}
}
