package main_generator;
import java.util.ArrayList;

import org.json.JSONArray;

import generator.EntityInfoGenerator;
import model_entity.Patient;
import non_model_entity.DataPoint;
import support.SupportFunction;

public class MainGenerateVisitInfo {
	public static void main(String[] args) {
		String oldGenerateInfoDirPath = "./generated-data (12 nurses)";
		for (String oldDataDirName : SupportFunction.getSubDirectoriesName(oldGenerateInfoDirPath)) {
			String oldDataDirPath = oldGenerateInfoDirPath + "/" + oldDataDirName;
			JSONArray array = SupportFunction.parseJsonArrayFromFile(getPatientInfoFilePath(oldDataDirPath));
			ArrayList<Patient> patients = Patient.parsePatientsListFromJsonArray(array);
			
			EntityInfoGenerator entityInfoGenerator = new EntityInfoGenerator();
			String type = getType(oldDataDirName);
			String outputDirPath = "./generated-info-" + type + "-" + SupportFunction.getCurrentTimeStamp();
			entityInfoGenerator.generate(outputDirPath, 
										 getDataPoints(patients), 
										 SupportFunction.parseJsonArrayFromFile("./raw-info/nurse-info-16.json"),
										 SupportFunction.parseJsonArrayFromFile("./raw-info/clsc-info-1.json"),
										 SupportFunction.parseJsonArrayFromFile("./raw-info/treatment-times.json"), 
										 SupportFunction.parseJsonArrayFromFile("./raw-info/visit-types.json"));
			copyInfoExisting("distance-matrix.json", oldDataDirPath, outputDirPath);
			copyInfoExisting("location-graph.png", oldDataDirPath, outputDirPath);
		}
	}
	
	private static void copyInfoExisting(String fileName, String oldDataDirPath, String outputDirPath) {
		if (fileName == null)
			throw new NullPointerException();
		if (oldDataDirPath == null)
			throw new NullPointerException();
		if (outputDirPath == null)
			throw new NullPointerException();
		
		if (SupportFunction.existPath(oldDataDirPath + "/" + fileName)) {
			SupportFunction.copyFile(oldDataDirPath + "/" + fileName,
									 outputDirPath + "/" + fileName);
		}
	}

	private static String getType(String dataDirName) {
		if (dataDirName == null)
			throw new NullPointerException();
		return dataDirName.split("-")[2];
	}

	private static String getPatientInfoFilePath(String curDirPath) {
		if (curDirPath == null)
			throw new NullPointerException();
		String [] infoDirs = SupportFunction.getSubDirectoriesName(curDirPath);
		curDirPath = curDirPath + "/" + infoDirs[0];
		String [] fileNames = SupportFunction.getFilesName(curDirPath, "json");
		return curDirPath + "/" + SupportFunction.getPrefixMatch("patient-info", fileNames);
	}
	
	private static ArrayList<DataPoint> getDataPoints(ArrayList<Patient> patients) {
		ArrayList<DataPoint> points = new ArrayList<>();
		for (Patient p : patients) {
			points.add(new DataPoint(p.getId(), p.getCoordinate()));
		}
		return points;
	}
}
