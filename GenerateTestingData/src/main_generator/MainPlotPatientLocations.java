package main_generator;
import java.util.ArrayList;

import org.json.JSONArray;

import global.CHI_CONSTANT;
import model_entity.Patient;
import non_model_entity.DataPoint;
import plotting.Plotter;
import support.SupportFunction;

public class MainPlotPatientLocations {
	public static void main(String[] args) {
		JSONArray array = SupportFunction.parseJsonArrayFromFile("./generated-info-A-20170629-163413/patient-info-171.json");
		ArrayList<Patient> patients = Patient.parsePatientsListFromJsonArray(array);
		Plotter.plot2DPointsWithDense(SupportFunction.convertToCoordinate2DsList(getDataPoints(patients)), CHI_CONSTANT.FRAME_INFO, true);
	}

	private static ArrayList<DataPoint> getDataPoints(ArrayList<Patient> patients) {
		ArrayList<DataPoint> points = new ArrayList<>();
		for (Patient p : patients) {
			points.add(new DataPoint(p.getId(), p.getCoordinate()));
		}
		return points;
	}
}
