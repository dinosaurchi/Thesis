package main_generator;
import java.util.HashMap;
import java.util.Map;

import experiment.UnexpectedChangeParameter;
import generator.TestCaseGenerator;
import support.SupportFunction;

public class MainGenerateTestCase {

	public static void main(String[] args) {
		Map<Integer, Double> subProportionMap = new HashMap<>();
		subProportionMap.put(1, 0.6);
		subProportionMap.put(2, 0.3);
		subProportionMap.put(3, 0.1);
		UnexpectedChangeParameter parameter = new UnexpectedChangeParameter(0.3, subProportionMap);
		
		String curDir = "generated-info-A-20170630-205459";
		TestCaseGenerator generator = new TestCaseGenerator();
		generator.generateTestCase("./test-cases-" + SupportFunction.getCurrentTimeStamp(), 
								   curDir + "/distance-matrix.json", 
								   curDir + "/info-0.3-[0.1_0.3_0.3_0.29999995]", 
								   parameter);	
	}

}
