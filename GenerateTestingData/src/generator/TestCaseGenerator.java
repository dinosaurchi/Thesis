package generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import experiment.NurseDistrictingChange;
import experiment.UnexpectedChangeParameter;
import model_entity.ModelEntity;
import model_entity.Nurse;
import model_entity.Patient;
import model_entity.Visit;
import model_info.EntityInfo;
import model_info.VisitInfo;
import support.SupportFunction;

public class TestCaseGenerator {
	public void generateTestCase(String testCaseOutputDirPath, String distanceMatrixFilePath, String entityInfoDirPath,
								 UnexpectedChangeParameter parameter) {
		if (testCaseOutputDirPath == null)
			throw new NullPointerException();
		if (distanceMatrixFilePath == null)
			throw new NullPointerException();
		if (entityInfoDirPath == null)
			throw new NullPointerException();
		if (parameter == null)
			throw new NullPointerException();
		
		SupportFunction.createDirectory(testCaseOutputDirPath);
		ArrayList<NurseDistrictingChange> changes = generateChanges(entityInfoDirPath, distanceMatrixFilePath, parameter);
		generateChangeInfo(testCaseOutputDirPath + "/change-info", changes);
		generateProblemInfo(testCaseOutputDirPath + "/problem-info", distanceMatrixFilePath, entityInfoDirPath);
		
		Map<String, Integer> iterationsMap = new HashMap<>();
		iterationsMap.put("simple-tabu", 16);
		iterationsMap.put("greedy-tabu", 2);
		
		Map<String, Integer> weightsMap = new HashMap<>();
		weightsMap.put("w_balanced_workload", 100);
		weightsMap.put("w_follow_up", 1000);
		weightsMap.put("w_exceeding", 50);
		weightsMap.put("w_avg_workload", 0);
		weightsMap.put("w_moving", 100);
		
		try {
			SupportFunction.saveFile((new JSONObject(iterationsMap)).toString(3), 
									 testCaseOutputDirPath + "/iteration-info.json");
			SupportFunction.saveFile((new JSONObject(weightsMap)).toString(3),
									 testCaseOutputDirPath + "/weight-info.json");
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	private static void generateProblemInfo(String outputDirPath, String distanceMatrixFilePath, String entityInfoDirPath) {
		SupportFunction.createDirectory(outputDirPath);
		SupportFunction.copyFile(distanceMatrixFilePath, outputDirPath + "/distance-matrix.json");
		SupportFunction.copyDirectory(entityInfoDirPath, outputDirPath + "/info");
	}

	private static ArrayList<NurseDistrictingChange> generateChanges(String entityInfoDirPath, String distanceMatrixFilePath, 
															  UnexpectedChangeParameter parameter) {
		EntityInfo entityInfo = new EntityInfo(entityInfoDirPath);
		ArrayList<Patient> currentInsidePatients = entityInfo.getPatients();
		Collections.shuffle(currentInsidePatients);		
		ArrayList<Patient> currentOutsidePatients = new ArrayList<>();
		
		ArrayList<NurseDistrictingChange> changes = new ArrayList<>();
		for (int i = 0 ; i < 6 ; ++i) {
			ArrayList<Patient> newPatients = pickupPatients(currentOutsidePatients, 5);
			currentOutsidePatients.removeAll(newPatients);
			
			ArrayList<Patient> removingPatients = pickupPatients(currentInsidePatients, 5);
			currentInsidePatients.removeAll(removingPatients);
			
			ArrayList<Patient> oldPatientsHasNewVisits = pickupPatients(currentInsidePatients, 8);
			
			ArrayList<Patient> patientHasNewVisits = SupportFunction.concatenate(oldPatientsHasNewVisits, 
																				 newPatients);
			
			if (SupportFunction.isIntersected(newPatients, removingPatients))
				throw new RuntimeException();
			
			/*
			 * We only update the the Inside and Outside lists when we finished all the picking process related to
			 *  them
			 * */
			currentOutsidePatients.addAll(removingPatients);
			currentInsidePatients.addAll(newPatients);
			
			ArrayList<Visit> newVisits = SupportFunction.concatenate(getVisits(entityInfo, newPatients),
																	 getAdditionalVistis(entityInfo, patientHasNewVisits, 5));
						
			changes.add(new NurseDistrictingChange(patientHasNewVisits, 
												   ModelEntity.getIds(removingPatients), 
												   getNewNurses(entityInfo, patientHasNewVisits), 
												   getRemovedNids(entityInfo, removingPatients), 
												   newVisits, entityInfo.getTotalDataDays(), 
												   parameter));
		}
		return changes;
	}

	private static ArrayList<Visit> getAdditionalVistis(EntityInfo entityInfo, ArrayList<Patient> patients, int maxVisits) {
		ArrayList<Visit> visits = new ArrayList<>();
		VisitInfo visitInfo = entityInfo.getVisitInfo();
		Random rand = new Random();
		for (Patient p : patients) {
			int numVisits = rand.nextInt(maxVisits) + 1;
			ArrayList<Visit> curVisits = visitInfo.getVisits(p.getId());
			while (numVisits-- > 0) {
				visits.add(curVisits.get(rand.nextInt(curVisits.size())));
			}
		}
		return visits;
	}

	private static ArrayList<Visit> getVisits(EntityInfo entityInfo, ArrayList<Patient> patients) {
		ArrayList<Visit> visits = new ArrayList<>();
		for (Patient p : patients) {
			visits.addAll(entityInfo.getVisitInfo().getVisits(p.getId()));
		}
		return visits;
	}

	private static ArrayList<String> getRemovedNids(EntityInfo entityInfo, ArrayList<Patient> removingPatients) {
		/*
		 * At the moment, our assumption is do not remove any nurse, thus we just return an empty list
		 * */
		return new ArrayList<>();
	}

	private static ArrayList<Nurse> getNewNurses(EntityInfo entityInfo, ArrayList<Patient> patientHasNewVisits) {
		/*
		 *  The new nurse here is just the nurse who executes the visit to 
		 *   the new patient, not necessary a totally new nurse.
	 	 * */
		
		ArrayList<Nurse> nurses = new ArrayList<>();
		for (Patient p : patientHasNewVisits) {
			ArrayList<String> nids = entityInfo.getVisitInfo().getNursesVisited(p.getId());
			for (String nid : nids) {
				nurses.add(entityInfo.getNurse(nid));
			}
		}
		return nurses;
	}

	private static ArrayList<Patient> pickupPatients(ArrayList<Patient> patients, int maxRange) {
		patients = new ArrayList<>(patients);
		
		Collections.shuffle(patients);
		Random rand = new Random();
		maxRange = Math.min(patients.size(), maxRange);
		if (maxRange == 0)
			return new ArrayList<>();
		return new ArrayList<>(patients.subList(0, rand.nextInt(maxRange) + 1));
	}

	private static void generateChangeInfo(String changeInfoDirPath, ArrayList<NurseDistrictingChange> changes) {
		SupportFunction.createDirectory(changeInfoDirPath);
		for (int i = 0 ; i < changes.size() ; ++i) {
			try {
				SupportFunction.saveFile(changes.get(i).generateJsonObject().toString(3), changeInfoDirPath + "/change-" + (i + 1) + ".json");
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			} ;
		}
	}
}
