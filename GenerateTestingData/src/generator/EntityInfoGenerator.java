package generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import distribution_simulator.DistributionSimulator;
import model_entity.ModelEntity;
import model_entity.Nurse;
import model_entity.Patient;
import model_entity.Visit;
import model_info.VisitDayInfo;
import model_info.VisitInfo;
import non_model_entity.Coordinate2D;
import non_model_entity.DataPoint;
import support.SupportFunction;

public class EntityInfoGenerator {
	protected class VisitFrequencyProportion {
		public final float m4, m8, m12, m20;
		public VisitFrequencyProportion(float m4, float m8, float m12) {
			if (m4 < 0 || m8 < 0 || m12 < 0)
				throw new RuntimeException("Invalid input : " + m4 + ", " + m8 + ", " + m12);
			if (m4 + m8 + m12 > 1)
				throw new RuntimeException("Invalid input : " + m4 + ", " + m8 + ", " + m12);
			this.m4 = m4;
			this.m8 = m8;
			this.m12 = m12;
			this.m20 = 1 - (m4 + m8 + m12);
		}
		
		@Override
		public String toString() {
			return m4 + "_" + m8 + "_" + m12 + "_" + m20;
		}
	}
	
	public void generateFromCoordinates(String outputDirPath, ArrayList<Coordinate2D> patientCoordinates, 
						 JSONArray nursesJsonArray, JSONArray clscsJsonArray,
						 JSONArray treatmentTimesJsonArray, JSONArray visitTypesJsonArray) {
		generate(outputDirPath, generateIdsForCoordinate(patientCoordinates),
				 nursesJsonArray, clscsJsonArray,
				 treatmentTimesJsonArray, visitTypesJsonArray);
	}

	public void generate(String outputDirPath, ArrayList<DataPoint> patientDataPoints, 
						 JSONArray nursesJsonArray, JSONArray clscsJsonArray,
						 JSONArray treatmentTimesJsonArray, JSONArray visitTypesJsonArray) {
		if (outputDirPath == null)
			throw new NullPointerException();
		if (patientDataPoints == null)
			throw new NullPointerException();
		if (nursesJsonArray == null)
			throw new NullPointerException();
		if (clscsJsonArray == null)
			throw new NullPointerException();
		if (treatmentTimesJsonArray == null)
			throw new NullPointerException();
		if (visitTypesJsonArray == null)
			throw new NullPointerException();

		SupportFunction.createDirectory(outputDirPath);
		ArrayList<Nurse> nurses = Nurse.parseNursesListFromJsonArray(nursesJsonArray);

		float [] ps = {0.3f, 0.5f, 0.7f};
		for (float followupProportion : ps){
			ArrayList<Patient> patients = generatePatientsList(patientDataPoints, followupProportion);
			JSONArray pArray = ModelEntity.generateModelEntitiesJsonArray(patients);;

			ArrayList<VisitFrequencyProportion> visitFreqPropsList = new ArrayList<>();
			visitFreqPropsList.add(new VisitFrequencyProportion(0.25f, 0.25f, 0.25f));
			visitFreqPropsList.add(new VisitFrequencyProportion(0.3f, 0.3f, 0.3f));
			visitFreqPropsList.add(new VisitFrequencyProportion(0.1f, 0.3f, 0.3f));

			for (VisitFrequencyProportion visitFreqProp : visitFreqPropsList) {
				String config = followupProportion + "-" + "[" + visitFreqProp.toString() + "]";
				String curOutputDirPath = outputDirPath + "/info-" + config;
				SupportFunction.createDirectory(curOutputDirPath);

				try {
					SupportFunction.saveFile(pArray.toString(3), curOutputDirPath + "/patient-info-" + pArray.length() + ".json");
					SupportFunction.saveFile(nursesJsonArray.toString(3), curOutputDirPath + "/nurse-info-" + nursesJsonArray.length() + ".json");
					SupportFunction.saveFile(clscsJsonArray.toString(3), curOutputDirPath + "/clsc-info-" + clscsJsonArray.length() + ".json");
				} catch (JSONException e) {
					e.printStackTrace();
					throw new RuntimeException();
				}

				ArrayList<VisitDayInfo> dayInfos = generateDayInfos(patients, nurses, 30, visitFreqProp,
						new DistributionSimulator<>(
								getTreatmentTimes(treatmentTimesJsonArray)),
						new DistributionSimulator<>(
								getVisitTypes(visitTypesJsonArray)));
				VisitInfo visitInfo = new VisitInfo(dayInfos);
				visitInfo.generateJsonsInfoDirectory(curOutputDirPath + "/visit-info");
			}
		}
	}

	private ArrayList<DataPoint> generateIdsForCoordinate(ArrayList<Coordinate2D> patientCoordinates) {
		if (patientCoordinates == null)
			throw new NullPointerException();
		ArrayList<DataPoint> result = new ArrayList<>();
		for (int i = 0 ; i < patientCoordinates.size() ; ++i)
			result.add(new DataPoint(String.format("P%06d", i), patientCoordinates.get(i)));
		
		return result;
	}

	private ArrayList<String> getVisitTypes(JSONArray visitTypesJsonArray) {
		ArrayList<String> result = new ArrayList<>();
		for (int i = 0 ; i < visitTypesJsonArray.length() ; ++i) {
			try {
				result.add(visitTypesJsonArray.getString(i));
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		return result;
	}

	private ArrayList<Double> getTreatmentTimes(JSONArray treatmentTimesJsonArray) {
		ArrayList<Double> result = new ArrayList<>();
		for (int i = 0 ; i < treatmentTimesJsonArray.length() ; ++i) {
			try {
				result.add(treatmentTimesJsonArray.getDouble(i));
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		return result;
	}

	private ArrayList<VisitDayInfo> generateDayInfos(ArrayList<Patient> patients, ArrayList<Nurse> nurses, 
													 int totalDays, VisitFrequencyProportion visitFreqProportion,
													 DistributionSimulator<Double> treatmentTimeDistribution,
													 DistributionSimulator<String> visitTypeDistibution) {
		if (patients == null)
			throw new NullPointerException();
		if (nurses == null)
			throw new NullPointerException();
		if (totalDays < 1)
			throw new RuntimeException("Invalid total-days : " + totalDays);
		if (visitFreqProportion == null)
			throw new NullPointerException();
	
		ArrayList<Visit> visits = generateVisitFromVisitFrequency(patients, nurses, visitFreqProportion,
																  treatmentTimeDistribution,
																  visitTypeDistibution);
		return generateDailyInfosFromVistis(visits, totalDays);
	}

	private ArrayList<VisitDayInfo> generateDailyInfosFromVistis(ArrayList<Visit> visits, int totalDays) {
		if (visits == null)
			throw new NullPointerException();
		if (totalDays < 1)
			throw new RuntimeException("Invalid total days : " + totalDays);
		if (visits.size() < totalDays)
			throw new RuntimeException("Total visits must be more than total days : " + visits.size() + " - " + totalDays);
		
		// To avoid changing the original visits list
		visits = new ArrayList<>(visits);
		Collections.shuffle(visits);
		
		ArrayList<ArrayList<Visit>> days = new ArrayList<>();
		for (int i = 0 ; i < totalDays ; ++i)
			days.add(new ArrayList<Visit>());
		
		int i = 0;
		while (visits.size() > 0) {
			days.get(i).add(visits.remove(0));
			i = (i + 1) % totalDays;
		}
		
		ArrayList<VisitDayInfo> dailyVisits = new ArrayList<>();
		for (ArrayList<Visit> day : days) {
			dailyVisits.add(new VisitDayInfo(day));
		}
		
		return dailyVisits;
	}

	private static ArrayList<Visit> generateVisitFromVisitFrequency(ArrayList<Patient> patients, ArrayList<Nurse> nurses,
																	VisitFrequencyProportion visitFreqProportion,
																	DistributionSimulator<Double> treatmentTimeDistribution,
																	DistributionSimulator<String> visitTypeDistibution) {
		Collections.shuffle(patients);
		ArrayList<Visit> visits = new ArrayList<>();
		int n = patients.size();
		
		// Return to the closest proportion
		int p1 = Math.round(n * visitFreqProportion.m4);
		visits.addAll(generateVisitWithFrequency(patients.subList(0, p1), nurses, 4,
												 treatmentTimeDistribution,
												 visitTypeDistibution));
		
		int p2 = Math.round(n * (visitFreqProportion.m4 + visitFreqProportion.m8));
		visits.addAll(generateVisitWithFrequency(patients.subList(p1, p2), nurses, 8,
												 treatmentTimeDistribution,
												 visitTypeDistibution));
		
		int p3 = Math.round(n * (visitFreqProportion.m4 + visitFreqProportion.m8 + visitFreqProportion.m12));
		visits.addAll(generateVisitWithFrequency(patients.subList(p2, p3), nurses, 12,
												 treatmentTimeDistribution,
												 visitTypeDistibution));
		
		visits.addAll(generateVisitWithFrequency(patients.subList(p3, n), nurses, 20,
												 treatmentTimeDistribution,
												 visitTypeDistibution));		
		return visits;
	}

	private static ArrayList<Visit> generateVisitWithFrequency(List<Patient> patients, ArrayList<Nurse> nurses, 
															   int numOfVisitInMonth,
															   DistributionSimulator<Double> treatmentTimeDistribution,
															   DistributionSimulator<String> visitTypeDistibution) {
		ArrayList<Visit> visits = new ArrayList<>();
		int curNurseIdx = 0;
		int n = nurses.size();
		
		for (Patient p : patients) {
			int i = numOfVisitInMonth;
			Nurse nurse = nurses.get(curNurseIdx);
			curNurseIdx = (curNurseIdx + 1) % n;
			double serviceTime = treatmentTimeDistribution.getRandomObject();
			String visitType = visitTypeDistibution.getRandomObject();
			while (i-- > 0) {
				visits.add(new Visit(p.getId(), nurse.getId(), visitType, serviceTime));
			}
		}
		return visits;
	}

	private static ArrayList<Patient> generatePatientsList(ArrayList<DataPoint> dataPoints, float followupProportion) {
		if (dataPoints == null)
			throw new NullPointerException();
		dataPoints = new ArrayList<>(dataPoints);
		Collections.shuffle(dataPoints);
		
		ArrayList<Patient> result = new ArrayList<>();
		int n = dataPoints.size();
		for (int i = 0 ; i < n ; ++i) {
			double curProportion = (float) (i + 1) / (float) n;
			boolean isFollowUpImportant = curProportion > followupProportion ? false : true;			
			result.add(new Patient(
					   dataPoints.get(i).getId(), 
					   dataPoints.get(i).getCoordinate().getX(), 
					   dataPoints.get(i).getCoordinate().getY(), 
					   isFollowUpImportant));
		}
		return result;
	}
}
