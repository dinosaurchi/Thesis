package generator;
import java.util.ArrayList;
import java.util.Random;

import non_model_entity.Coordinate2D;
import support.SupportFunction;

public class CoordinateGenerator {
	/*
	 * Both of the LOW and HIGH are applied for x and y coordinate, which are is lowest and highest values respectively
	 * */
	private static final double LOW = 0;
	private static final double HIGH = 999999;
	
	public ArrayList<Coordinate2D> generate(int totalPoints, String type, Coordinate2D topLeft, Coordinate2D rightBottom) {
		if (totalPoints < 1)
			throw new RuntimeException("Invalid total points : " + totalPoints);
		if (type == null)
			throw new NullPointerException();
		if (topLeft == null)
			throw new NullPointerException();
		if (rightBottom == null)
			throw new NullPointerException();
		
		ArrayList<Coordinate2D> generatedPoints = getGeneratedPoints(totalPoints, type);
		
		addVirtualBoundingBoxPoint(generatedPoints);
		
		generatedPoints = SupportFunction.scalePointsToNewBoundingBox(generatedPoints, topLeft, rightBottom);
		
		removeVirtualBoundingBoxPoint(generatedPoints);		
		
		return generatedPoints;
	}
	
	private void removeVirtualBoundingBoxPoint(ArrayList<Coordinate2D> generatedPoints) {
		/*
		 * In theory, we have to remove the points in the 4 corners of the bounding box. 
		 * However, we have a trick that the SupportFunction.scalePointsToNewBoundingBox returns the same 
		 * order with the original points. Hence, we can just remove the points from the same order when 
		 * we added them.
		 * */
		generatedPoints.remove(0);
		generatedPoints.remove(0);
		generatedPoints.remove(0);
		generatedPoints.remove(0);
	}

	private static void addVirtualBoundingBoxPoint(ArrayList<Coordinate2D> generatedPoints) {
		/* We do that to add a maximum rectangle bounding for the data, then 
	 		when we change the scale, the distribution does not change
		*/
		generatedPoints.add(0, new Coordinate2D(LOW, HIGH));
		generatedPoints.add(0, new Coordinate2D(HIGH, HIGH));
		generatedPoints.add(0, new Coordinate2D(HIGH, LOW));
		generatedPoints.add(0, new Coordinate2D(LOW, LOW));
	}

	private ArrayList<Coordinate2D> getGeneratedPoints(int totalPoints, String type) {
		switch (type) {
		case "A":
			return generate_typeA(totalPoints, LOW, HIGH);
		case "B":
			return generate_typeB(totalPoints, LOW, HIGH);
		case "C":
			return generate_typeC(totalPoints, LOW, HIGH);
		default:
			throw new RuntimeException("Invalid type : " + type);
		}
	}

	private static ArrayList<Coordinate2D> generate_typeA(int totalPoints, double low, double high) {
		ArrayList<Coordinate2D> result = new ArrayList<>();
		
		int numBlocks = 3;
		int blockSize = ((int) (high - low)) / numBlocks;
		int numPointsPerBlock = totalPoints / (numBlocks * numBlocks);
		
		int total = 0;
		Random random = new Random();
		
		while (total < totalPoints) {
			for(int i = 0; i < high; i+= blockSize) {
				for(int j = 0; j < high; j+= blockSize) {
					for(int count = 0 ; count < numPointsPerBlock ; ++count) {
						double randX = random.nextInt(blockSize) + i;
						double randY = random.nextInt(blockSize) + j;
						result.add(new Coordinate2D(randX, randY));
						++total;
						if (total >= totalPoints)
							return result;
					}
				}
			}
		}
		return result;
	}
	
	private static ArrayList<Coordinate2D> generate_typeB(int totalPoints, double low, double high) {
		ArrayList<Coordinate2D> result = new ArrayList<>();
		
		double stdX = (high - low) * 0.22; 
		double stdY = (high - low) * 0.13; 
		double mean = (high + low) / 2;
		Random random = new Random();
		
		for(int i = 0 ; i < totalPoints ; ++i) {
			double randX = Math.round(random.nextGaussian() * stdX + mean); 
			double randY = Math.round(random.nextGaussian() * stdY + mean); 
			result.add(new Coordinate2D(randX, randY));
		}
		return result;
	}
	
	private static ArrayList<Coordinate2D> generate_typeC(int totalPoints, double low, double high) {
		ArrayList<Coordinate2D> result = new ArrayList<>();
				
		double seedX = (low + (high - low) * 0.30); 
		double seedY = (low + (high - low) * 0.75);
		double stdX = (high - low) * 0.23; 
		double stdY = (high - low) * 0.22; 
		Random random = new Random();
		
		for(int i = 0 ; i < totalPoints ; ++i) {
			double randX = Math.round(random.nextGaussian() * stdX + seedX); 
			while (randX > high || randX < 0)
				randX = Math.round(random.nextGaussian() * stdX + seedX); 
			
			double randY = Math.round(random.nextGaussian() * stdY + seedY); 
			while (randY > high || randY < 0)
				randY = Math.round(random.nextGaussian() * stdY + seedY); 
			
			result.add(new Coordinate2D(randX, randY));
		}
		return result;
	}
}
