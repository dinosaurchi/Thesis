import java.util.HashMap;
import java.util.Map;

import experiment.UnexpectedChangeParameter;
import generator.TestCaseGenerator;
import support.SupportFunction;

public class MainGenerateTests {
	public static void main(String[] args) {	
		String dataDirPath = "./generated-data (16 nurses)";
		String outputDirPath = "all-test-cases-" + SupportFunction.getCurrentTimeStamp();
		
		export(dataDirPath, outputDirPath, 0.3, 0.6, 0.3, 0.1);
		export(dataDirPath, outputDirPath, 0.3, 0.5, 0.3, 0.2);
		
		export(dataDirPath, outputDirPath, 0.4, 0.6, 0.3, 0.1);
		export(dataDirPath, outputDirPath, 0.4, 0.5, 0.3, 0.2);
	}
	
	private static void export(String rawDataDirPath, String outputDirPath, double pOverall, double p1, double p2, double p3) {
		Map<Integer, Double> subProportionMap = new HashMap<>();
		subProportionMap.put(1, p1);
		subProportionMap.put(2, p2);
		subProportionMap.put(3, p3);
		exportTestCaseWithUnexpectedChangeParams(rawDataDirPath, outputDirPath,
											     new UnexpectedChangeParameter(pOverall, subProportionMap));
	}
	
	private static void exportTestCaseWithUnexpectedChangeParams(String generatedRawDataDirPath, 
																 String outputTestCasesDirPath,
																 UnexpectedChangeParameter parameter) {
		SupportFunction.createDirectory(outputTestCasesDirPath);
		
		TestCaseGenerator generator = new TestCaseGenerator();
		String outputTestCasesWithParamsDirPath = outputTestCasesDirPath + "/test-cases-" + parameter.toString();
		SupportFunction.createDirectory(outputTestCasesWithParamsDirPath);
		
		for (String dataName : SupportFunction.getSubDirectoriesName(generatedRawDataDirPath)) {
			String curDataDirPath = generatedRawDataDirPath + "/" + dataName;
			String caseName = SupportFunction.removePrefix(dataName, "generated-info-");
			
			for (String infoDirName : SupportFunction.getSubDirectoriesName(curDataDirPath)) {
				String outputDirPath = outputTestCasesWithParamsDirPath + "/" + caseName + "-" + SupportFunction.removePrefix(infoDirName, "info-");
				generator.generateTestCase(outputDirPath, 
										   curDataDirPath + "/distance-matrix.json", 
										   curDataDirPath + "/" + infoDirName, 
										   parameter);	
			}
		}
	}
}
