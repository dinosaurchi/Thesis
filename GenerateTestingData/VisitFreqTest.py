import json
import os
import numpy as np
from collections import defaultdict

info_dir = "./generated-info-A-20170630-001739"
visitDirPath = info_dir + '/' + 'visit-info-0.7-[0.1_0.3_0.3_0.29999995]'

visitsMap = defaultdict(lambda: 0)
for f in os.listdir(visitDirPath):
	if f.endswith('.json'):
		with open(visitDirPath + '/' + f) as file:
			data = json.load(file)
			for i in range(0, len(data)):
				visitsMap[data[i]['pid']] += 1

countMap = defaultdict(lambda: 0)
total = 0
for key, value in visitsMap.iteritems():
	countMap[value] += 1
	total += 1

for key, value in countMap.iteritems():
	print str(key) + " : " + str(float(countMap[key]) / float(total))