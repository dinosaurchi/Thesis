package model_entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import global.KEY_MAP;
import non_model_entity.Coordinate2D;
import support.SupportFunction;


public class Patient extends ModelEntity {
	/*
		DOCUMENTATION

	Patient object is an unmodifiable object, means that it is only defined in constructors. 
	From that assumption, we can apply pass by reference for Patient object, do not need 
		to have a deep copy.

	 */
	
	private final Coordinate2D coordinate;
	private final Boolean isFollowUpImportant;
		
	public Patient (String id, double locX, double locY, boolean isFollowUpImportant) {
		super(id);
		if (Double.isNaN(locX))
			throw new RuntimeException("Invalid pid info : " + id);
		if (Double.isInfinite(locX))
			throw new RuntimeException("Invalid pid info : " + id);
		if (Double.isNaN(locY))
			throw new RuntimeException("Invalid pid info : " + id);
		if (Double.isInfinite(locY))
			throw new RuntimeException("Invalid pid info : " + id);
		
		this.coordinate = new Coordinate2D(locX, locY);
		this.isFollowUpImportant = isFollowUpImportant;
	}
	
	public Patient (JSONObject object) throws NumberFormatException, JSONException {	
		this (object.get(KEY_MAP.PATIENT_ID).toString(),
			  Double.parseDouble(object.getString(KEY_MAP.COORDINATE_X)),
			  Double.parseDouble(object.getString(KEY_MAP.COORDINATE_Y)),
			  Boolean.parseBoolean(object.getString(KEY_MAP.IS_FOLLOW_UP_IMPORTANT)));
	}
	
	protected Patient (Patient p) {
		super(p);
		assert p != null;
		this.coordinate = p.coordinate;
		this.isFollowUpImportant = p.isFollowUpImportant;
	}
	
	public Coordinate2D getCoordinate() {
		return coordinate;
	}
	
	public Boolean isFollowUpImportant() {
		return isFollowUpImportant;
	}

	public static ArrayList<Patient> parsePatientsListFromJsonFile(String patientJsonInfoFilePath) {
		if (patientJsonInfoFilePath == null)
			throw new NullPointerException();
				
		JSONArray array = SupportFunction.parseJsonArrayFromFile(patientJsonInfoFilePath);
		return parsePatientsListFromJsonArray(array);
	}
	
	public static ArrayList<Patient> parsePatientsListFromJsonArray(JSONArray array) {
		if (array == null)
			throw new NullPointerException();
		
		ArrayList<Patient> patients = new ArrayList<>();
		for (int i = 0 ; i < array.length() ; ++i) {
			JSONObject obj;
			try {
				obj = array.getJSONObject(i);
				Patient p = new Patient(obj);
				if (p != null)
					patients.add(p);
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		return patients;
	}

	public static Map<String, String> getPatientValueMap(String patientJsonInfoFilePath, String key) {
		if (patientJsonInfoFilePath == null)
			throw new NullPointerException();
		
		JSONArray array = SupportFunction.parseJsonArrayFromFile(patientJsonInfoFilePath);
		Map<String, String> map = new HashMap<>();
		for (int i = 0 ; i < array.length() ; ++i) {
			try {
				JSONObject obj = array.getJSONObject(i);
				if (obj.has(key)) {
					String id = obj.getString(KEY_MAP.PATIENT_ID);
					String v = obj.getString(key);
					map.put(id, v);
				}
				else 
					throw new RuntimeException("Not-existing key");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		assert (array.length() == map.size());
		
		return map;
	}

	@Override
	public JSONObject generateJsonObject() {
		JSONObject obj = new JSONObject();
		try {
			obj.put(KEY_MAP.PATIENT_ID, getId());
			obj.put(KEY_MAP.COORDINATE_X, Double.toString(this.coordinate.getX()));
			obj.put(KEY_MAP.COORDINATE_Y, Double.toString(this.coordinate.getY()));
			obj.put(KEY_MAP.IS_FOLLOW_UP_IMPORTANT, Boolean.toString(this.isFollowUpImportant));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj;
	}
}
