package model_entity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import global.KEY_MAP;
import non_model_entity.Coordinate2D;

public class CLSC extends ModelEntity {
	/*
	DOCUMENTATION

	CLSC object is an unmodifiable object, means that it is only defined in constructors. 
	From that assumption, we can apply pass by reference for CLSC object, do not need 
		to have a deep copy.

	 */

	public static final String CLSC_ID = "CLSC";

	private final Coordinate2D coordinate;

	public CLSC (String id, double locX, double locY) {
		super(id);
		if (Double.isNaN(locX))
			throw new RuntimeException();
		if (Double.isInfinite(locX))
			throw new RuntimeException();
		if (Double.isNaN(locY))
			throw new RuntimeException();
		if (Double.isInfinite(locY))
			throw new RuntimeException();
		
		this.coordinate = new Coordinate2D(locX, locY);
	}

	public CLSC (JSONObject object) throws NumberFormatException, JSONException {
		this (object.get(KEY_MAP.CLSC_ID).toString(), 
			  Double.parseDouble(object.getString(KEY_MAP.COORDINATE_X)),
			  Double.parseDouble(object.getString(KEY_MAP.COORDINATE_Y)));
	}
	
	protected CLSC (CLSC clsc) {
		super(clsc);
		assert clsc != null;

		this.coordinate = clsc.coordinate;
	}

	public Coordinate2D getCoordinate() {
		return coordinate;
	}

	public static ArrayList<CLSC> parseClscsListFromJsonArray(JSONArray array) {
		if (array == null)
			throw new NullPointerException();

		ArrayList<CLSC> clscs = new ArrayList<>();
		for (int i = 0 ; i < array.length() ; ++i) {
			try {
				JSONObject obj = array.getJSONObject(i);
				CLSC clsc = new CLSC(obj);
				if (clsc != null)
					clscs.add(clsc);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return clscs;
	}

	@Override
	public JSONObject generateJsonObject() {
		JSONObject obj = new JSONObject();
		
		try {
			obj.put(KEY_MAP.CLSC_ID, getId());
			obj.put(KEY_MAP.COORDINATE_X, Double.toString(this.coordinate.getX()));
			obj.put(KEY_MAP.COORDINATE_Y, Double.toString(this.coordinate.getY()));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return obj;
	}
}
