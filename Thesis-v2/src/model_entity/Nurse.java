package model_entity;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import global.KEY_MAP;


public class Nurse extends ModelEntity {
	/*
		DOCUMENTATION

	Nurse object is an unmodifiable object, means that it is only defined in constructors. 
	From that assumption, we can apply pass by reference for Nurse object, do not need 
	to have a deep copy.

	*/
	
	public static final String EMPTY_NURSE = "N0";
	
	private final Color color;
	
	public Nurse(String id) {
		super(id);
		Random rand = new Random();
		this.color = new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
	}
	
	public Nurse (JSONObject object) throws NumberFormatException, JSONException {	
		this ((String) object.get(KEY_MAP.NURSE_ID), 
			  new Color(Integer.parseInt((String) object.get(KEY_MAP.NURSE_COLOR))));
	}
	
	public Nurse(String id, Color color) {
		super(id);
		
		if (color == null)
			throw new NullPointerException();
		
		// Because we cannot change a Color/String object, thus we do not care about unexpected modification, 
		// then just simple pass its pointer		
		this.color = color;
	}
	
	protected Nurse(Nurse n) {
		super(n);		
		this.color = n.color;
	}
	
	public Color getColor() {
		return color;
	}

	public static ArrayList<Nurse> parseNursesListFromJsonArray(JSONArray array) {
		if (array == null)
			throw new NullPointerException();
		
		ArrayList<Nurse> nurses = new ArrayList<>();		
		for (int i = 0 ; i < array.length() ; ++i) {
			try {
				Nurse p = new Nurse(array.getJSONObject(i));
				if (p != null)
					nurses.add(p);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return nurses;
	}

	@Override
	public JSONObject generateJsonObject() {
		JSONObject obj = new JSONObject();
		
		try {
			obj.put(KEY_MAP.NURSE_ID, getId());
			obj.put(KEY_MAP.NURSE_COLOR, Integer.toString(this.color.getRGB()));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return obj;
	}
}
