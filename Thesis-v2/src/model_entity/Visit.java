package model_entity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import global.KEY_MAP;

public class Visit extends ModelEntity {
	private final String visitType;
	private final Double serviceTime;
	private final String executingNurseId;
	private final String visittedPatientId;
	
	protected Visit(Visit entity) {
		super(entity);
		this.visitType = entity.visitType;
		this.serviceTime = entity.serviceTime;
		this.executingNurseId = entity.executingNurseId;
		this.visittedPatientId = entity.visittedPatientId;
	}
	
	public Visit (JSONObject object) throws JSONException {
		this (object.getString(KEY_MAP.VISIT_ID),
			  object.getString(KEY_MAP.PATIENT_ID), 
			  object.getString(KEY_MAP.NURSE_ID), 
			  object.getString(KEY_MAP.VISIT_TYPE),
			  Double.parseDouble(object.getString(KEY_MAP.SERVICE_TIME)));
	}
	
	private Visit(String vid, String visittedPatientId, String executingNurseId, String visitType, double serviceTime) {
		super(vid);
		if (executingNurseId == null)
			throw new NullPointerException();
		if (visittedPatientId == null)
			throw new NullPointerException();
		if (visitType == null)
			throw new NullPointerException();
		if (Double.isNaN(serviceTime))
			throw new RuntimeException("Invalid vid info : " + vid);
		if (Double.isInfinite(serviceTime))
			throw new RuntimeException("Invalid vid info : " + vid);
		if (serviceTime <= 0)
			throw new RuntimeException("Invalid service time : " + serviceTime + ", from vid : " + vid);
		
		this.executingNurseId = executingNurseId;
		this.visitType = visitType;
		this.serviceTime = serviceTime;
		this.visittedPatientId = visittedPatientId;
	}
	
	public Visit(String visittedPatientId, String executingNurseId, String visitType, double serviceTime) {
		this (Long.toString(getNextId()),
				visittedPatientId,
				executingNurseId,
				visitType,
				serviceTime);
	}
	
	@Override
	public boolean equals(Object obj) {
		if ((obj instanceof Visit))
			return false;
		
		// We do not involve the visit ID in equality check
		Visit visit = (Visit) obj;
		return (this.visitType.equals(visit.visitType)) && 
				(this.serviceTime.equals(visit.visitType) &&
				(this.executingNurseId.equals(visit.executingNurseId)) &&
				(this.visittedPatientId.equals(visit.visittedPatientId)));
	}
	
	@Override
	public int hashCode() {
		return (this.visitType 
				+ this.executingNurseId 
				+ this.visittedPatientId 
				+ this.serviceTime).hashCode();
	}
	
	private static long currentId = Long.MIN_VALUE;
	private static synchronized long getNextId() {
		return currentId++;
	}

	@Override
	public JSONObject generateJsonObject() {
		JSONObject obj = new JSONObject();
		try {
			obj.put(KEY_MAP.VISIT_ID, this.getId());
			obj.put(KEY_MAP.VISIT_TYPE, this.visitType);
			obj.put(KEY_MAP.SERVICE_TIME, Double.toString(this.serviceTime));
			obj.put(KEY_MAP.NURSE_ID, this.executingNurseId);
			obj.put(KEY_MAP.PATIENT_ID, this.visittedPatientId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj;
	}

	public String getVisitType() {
		return visitType;
	}

	public Double getServiceTime() {
		return serviceTime;
	}

	public String getNid() {
		return executingNurseId;
	}

	public String getPid() {
		return visittedPatientId;
	}
	
	public static ArrayList<Visit> parseVisitsListFromJsonArray(JSONArray array) {
		if (array == null)
			throw new NullPointerException();
		
		ArrayList<Visit> visits = new ArrayList<>();		
		for (int i = 0 ; i < array.length() ; ++i) {
			try {
				JSONObject obj = array.getJSONObject(i);
				Visit visit = new Visit(obj);
				if (visit != null)
					visits.add(visit);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return visits;
	}
}
