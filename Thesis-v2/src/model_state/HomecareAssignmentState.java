package model_state;

import java.util.ArrayList;

import org.json.JSONArray;

import state.DataPointClassState;

public class HomecareAssignmentState extends DataPointClassState{
	/*
	 DOCUMENTATION
	 
	 
	HomecareAssignmentState object is an unmodifiable object (except for patientIdToFollowUpNurseId), means that it is only defined in constructors. 
	From that assumption, we can apply pass by reference for HomecareAssignmentState object, do not need 
	to have a deep copy.
	
	If a Nurse has no assigned patient, 
		-> it has a map to an ArrayList<String> with length = 0 in "nurseToPatientsMap"
	If a Patient has no map to any nurse, 
		-> it has a map to EMPTY_NURSE in "patientToNurseMap"
		
	Info of patients and nurses are all mapped into patientIdToPatientMap and nurseIdToNurseMap respectively, 
	  with their corresponding ID (pid or nid)
		-> The reason for this use instead of using object pointers as the key for mapping is because 
		 	the fact that using a String reference will be more flexible when you want to replace the information of 
		 	patient P0001 with another information, but want to keep all the current assignment, and guarantee the 
		 	unmodifiable characteristic of Patient/Nurse object. Moreover, it can facilitate debugging: you can simple
		 	put a string ID to get the reference to the actual Nurse/Patient object, instead of using the real memory
		 	address if you use address as the reference key. 	
		
		
	 */
	
	// PUBLIC Methods
	
	protected HomecareAssignmentState(HomecareAssignmentState a) {
		super(a);
	}
	
	public HomecareAssignmentState(ArrayList<String> patients, ArrayList<String> nurses) {
		super(patients, nurses);			
	}
	
	public HomecareAssignmentState (String assignmentJsonFileName) {
		super(assignmentJsonFileName);	
	}
	
	public HomecareAssignmentState (JSONArray array) {
		super(array);
	}
	
	public String getAssignedNurseId(String pid) {
		return super.getAssignedClassId(pid);
	}
	
	public ArrayList<String> getAssignedPatientIdsList(String nid) {
		return super.getAssignedDataPointIdsList(nid);
	}
	
	public HomecareAssignmentState assignPatientToNurse(String pid, String nid) {
		if (pid == null)
			throw new NullPointerException();
		if (nid == null)
			throw new NullPointerException();
		
		return (HomecareAssignmentState) super.assignDataPointToClass(pid, nid);	
	}
	
	public HomecareAssignmentState assignPatientsToNurse(ArrayList<String> pids, String nid) {
		if (pids == null)
			throw new NullPointerException();
		if (nid == null)
			throw new NullPointerException();
		
		return (HomecareAssignmentState) super.assignDataPointsToClass(pids, nid);	
	}
	
	public ArrayList<String> getPatientIds() {
		return super.getDataPointIds();
	}
	
	public ArrayList<String> getNurseIds() {
		return super.getClassIds();
	}
	
	@Override
	public HomecareAssignmentState clone() {
		return this;
	}

	@Override
	protected HomecareAssignmentState makeCopy() {
		// TODO Auto-generated method stub
		return new HomecareAssignmentState(this);
	}
}
