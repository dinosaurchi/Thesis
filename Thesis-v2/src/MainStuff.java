import java.util.ArrayList;

import distance_matrix.IndirectedDistanceMatrix;
import model.VivianeModel;
import model_info.EntityInfo;
import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import move.UnitMove;
import nurse_districting_problem.NurseDistrictingProblem;
import optimizer_creator.ExtendedOptimizerCreator;
import solution_reporter.VivianeModelSolutionReporter;
import support.SupportFunction;

public class MainStuff {
	
	public static void main(String[] args) {
		VivianeModelInfo info = new VivianeModelInfo(new EntityInfo("./info"), 
													new IndirectedDistanceMatrix("distance-matrix-156-156.json"), 
													null);
		VivianeModel model = new VivianeModel(info);
		model.setWeights(30, 100, 50, 80, 10);
		
		VivianeModelSolutionReporter reporter = new VivianeModelSolutionReporter(model);
		
		NurseDistrictingProblem program = new NurseDistrictingProblem(model);
				
		String timeStamp = SupportFunction.getCurrentTimeStamp();
		
		ExtendedOptimizerCreator creator = new ExtendedOptimizerCreator();
		
		program.setOptimizer(creator.createSimpleKMean(model, 500, 16));
		program.solve();
		reporter.exportReport("kmean-" + timeStamp, program.getLastRunningInfo());
		
		HomecareAssignmentState lastState = program.getLastState();
		
		program.setOptimizer(creator.createSimpleTabuSearch(model, 5000, new ArrayList<UnitMove<HomecareAssignmentState>>()));
		program.solve();
		
		if (program.getLastState().equals(lastState))
			throw new NullPointerException();
		
		reporter.exportReport("tabu-" + timeStamp, program.getLastRunningInfo());
		
		SupportFunction.saveFile(program.getLastState().generateStateJsonArray().toString(), "assignment-m2.json");
	}
}
