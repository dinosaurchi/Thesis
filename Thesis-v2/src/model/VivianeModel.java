package model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import non_model_entity.DirectedPair;

public class VivianeModel extends DataPointModel<HomecareAssignmentState, VivianeModelInfo> {	
		
	// PUBLIC Methods
	public static final String W_AVERAGE_WORKLOAD 	= "w_avg_workload";
	public static final String W_BALANCE_WORKLOAD 	= "w_balanced_workload";
	public static final String W_EXCEEDING 			= "w_exceeding";
	public static final String W_FOLLOW_UP 			= "w_follow_up";
	public static final String W_MOVING 			= "w_moving";
		
 	protected VivianeModel(VivianeModel model) {
 		super(model); 	
	}
	
 	public VivianeModel(VivianeModelInfo info) {
		super(info, new String [] {
			W_AVERAGE_WORKLOAD,
			W_BALANCE_WORKLOAD,
			W_EXCEEDING,
			W_FOLLOW_UP,
			W_MOVING
		});
	}

	public void setWeights(double wAvgWorkload, double wBalanceWorkload, double wExceeding, double wFollowUp, double wMoving) {
		Map<String, Double> weightsMap = new HashMap<>();
		weightsMap.put(W_AVERAGE_WORKLOAD, wAvgWorkload);
 		weightsMap.put(W_BALANCE_WORKLOAD, wBalanceWorkload);
 		weightsMap.put(W_EXCEEDING, wExceeding);
 		weightsMap.put(W_FOLLOW_UP, wFollowUp);
 		weightsMap.put(W_MOVING, wMoving);
 		this.setWeights(weightsMap);
 	}
 	
	@Override
	public Map<String, Double> getObjectiveComponentMap(HomecareAssignmentState currentState) {
		if (currentState == null)
			throw new NullPointerException();
		
		HomecareAssignmentState currentMonthAssignment = currentState.clone();
		
		if (!info.isConsistent(currentMonthAssignment))
			throw new RuntimeException("Inconsistent assignment");
		
		double fExceeding = 0;
		double fFollowUp = 0;
		double totalWorkload = 0;
		double fMoving = 0;
		
		ArrayList<String> nids = info.getNurseIds();
		int n = nids.size();
		double [] workloads = new double [n];
		for (int i = 0 ; i < n ; ++i) {
			String nid = nids.get(i);
			List<String> pids = Collections.unmodifiableList(currentMonthAssignment.getAssignedPatientIdsList(nid));
			workloads[i] = this.getModelInfo().getNurseAverageDailyWorkLoad(pids);
			totalWorkload += workloads[i];
			if (workloads[i] > VivianeModelInfo.MAX_WORKING_MINUTES)
				fExceeding += workloads[i] - VivianeModelInfo.MAX_WORKING_MINUTES;
			
			for (String pid : pids) {
				double frequency = this.getModelInfo().getVisitFrequency(pid);
				if (this.getModelInfo().isFollowUpImportant(pid)) {
					fFollowUp += (1 - this.getModelInfo().getLastMonthVisitProportion(pid, nid)) * frequency;
				}
				String previousNid = this.getModelInfo()
										 .getEntityInfo()
										 .getVisitInfo()
										 .getHighestVisitingProportionNurseId(pid);
				if (!previousNid.equals(nid)) {
					fMoving += frequency;
				}
			}
		}
		
		double avgWorkload = totalWorkload / (double) n;
		double fWorkload = 0;
		for (int i = 0 ; i < workloads.length ; ++i) {
			fWorkload += Math.abs(workloads[i] - avgWorkload);
		}
				
		assert fWorkload != 0;
		assert fExceeding != 0;
		assert fFollowUp != 0;
		
		Map<String, Double> componentValuesMap = new HashMap<>();
		componentValuesMap.put(W_AVERAGE_WORKLOAD, avgWorkload);
		componentValuesMap.put(W_BALANCE_WORKLOAD, fWorkload);
		componentValuesMap.put(W_EXCEEDING, 	   fExceeding); 
		componentValuesMap.put(W_FOLLOW_UP, 	   fFollowUp);
		componentValuesMap.put(W_MOVING, 	   	   fMoving);
		
		return componentValuesMap;
	}

	@Override
	public VivianeModel clone() {
		return new VivianeModel(this);
	}
	
	@Override
	public Color getClassColor(String cid) {
		if (cid == null)
			throw new NullPointerException();
		
		return this.info.getEntityInfo().getNurse(cid).getColor();
	}
	
	public ArrayList<DirectedPair<String, Double>> getAverageDailyWorkloadForEachNurse(HomecareAssignmentState currentAssignment) {
		if (currentAssignment == null)
			throw new NullPointerException();
		
		if (!this.info.isConsistent(currentAssignment))
			throw new RuntimeException("Inconsistent assignment");
		
		ArrayList<DirectedPair<String, Double>> result = new ArrayList<>();
		ArrayList<String> nids = this.info.getNurseIds();
		for (String nid : nids) {
			List<String> pids = Collections.unmodifiableList(currentAssignment.getAssignedPatientIdsList(nid));
			double workload = this.getModelInfo().getNurseAverageDailyWorkLoad(pids);
			result.add(new DirectedPair<String, Double>(nid, workload));
		}
		
		assert result.size() == nids.size();
		
		return result;
	}
	
	public double getNurseAverageDailyWorkload (HomecareAssignmentState currentAssignment, String nid) {
		if (nid == null)
			throw new NullPointerException();
		if (currentAssignment == null)
			throw new NullPointerException();
		if (!this.info.isConsistent(currentAssignment))
			throw new RuntimeException("Inconsistent assignment");
		if (!this.info.hasNurse(nid))
			throw new RuntimeException("Not existing nurse id : " + nid);
		
		List<String> pids = Collections.unmodifiableList(currentAssignment.getAssignedPatientIdsList(nid));
		return this.getModelInfo().getNurseAverageDailyWorkLoad(pids);
	}

	public ArrayList<String> getNeighborNurses(HomecareAssignmentState currentAssignment, String nid) {
		if (nid == null)
			throw new NullPointerException();
		if (currentAssignment == null)
			throw new NullPointerException();
		if (!this.info.isConsistent(currentAssignment))
			throw new RuntimeException("Inconsistent state");
		if (!this.info.hasNurse(nid))
			throw new RuntimeException("Not existing nurse id : " + nid);
		
		ArrayList<String> pids = currentAssignment.getAssignedPatientIdsList(nid);
		
		ArrayList<String> nids = this.info.getNurseIds();
		ArrayList<String> result = new ArrayList<>();
		for (String id : nids) {
			if (!id.equals(nid)) {
				if (hasNeighborPatients(pids, currentAssignment.getAssignedPatientIdsList(id))) {
					result.add(id);
				}
			}
		}
		return result;
	}
	
	public ArrayList<String> getBorderPatients(HomecareAssignmentState currentAssignment, String nid) {
		if (nid == null)
			throw new NullPointerException();
		if (currentAssignment == null)
			throw new NullPointerException();
		if (!this.info.isConsistent(currentAssignment))
			throw new RuntimeException("Inconsistent state");
		if (!this.info.hasNurse(nid))
			throw new RuntimeException("Not existing nurse id : " + nid);
		
		ArrayList<String> pids = currentAssignment.getAssignedPatientIdsList(nid);
		ArrayList<String> result = new ArrayList<>();
		for (String pid : pids) {
			if (isBorderPatient(currentAssignment, pid, nid))
				result.add(pid);
		}
		
		return result;
	}
	
	public ArrayList<Set<String>> getDisjointSets(String nid, HomecareAssignmentState state) {
		ArrayList<String> pids = state.getAssignedPatientIdsList(nid);
		ArrayList<Set<String>> result = new ArrayList<>();
		while (!pids.isEmpty()) {
			String seedPid = pids.get(0);
			ArrayList<String> disjointSet = getPropagatedNeighboringPids(seedPid, nid, state);
			result.add(new HashSet<>(disjointSet));
			pids.removeAll(disjointSet);
		}
		return result;
	}
	
	// ----------------------------------------------------------------------------------------------------
	// Private methods
	
	private ArrayList<String> getPropagatedNeighboringPids(String seedPid, String nid, HomecareAssignmentState state) {
		ArrayList<String> visitedPids = new ArrayList<>();
		Stack<String> stack = new Stack<>();
		stack.push(seedPid);

		VivianeModelInfo modelInfo = this.getModelInfo();
		while (!stack.isEmpty()) {
			String pid = stack.pop();
			visitedPids.add(pid);
			for (String neighborPid : modelInfo.getNeighborPatientIds(pid)) {
				if (!stack.contains(neighborPid) && !visitedPids.contains(neighborPid) && state.getAssignedNurseId(neighborPid).equals(nid))
					stack.push(neighborPid);				
			}
		}
		
		return visitedPids;
	}
	
	private boolean isBorderPatient(HomecareAssignmentState currentAssignment, String pid, String currentNid) {
		assert pid != null;
		assert currentNid != null;
		assert currentAssignment != null;
		
		ArrayList<String> neighborPatientIds = this.info.getNeighborPatientIds(pid);
		for (String neighborId : neighborPatientIds) {
			if (!currentAssignment.getAssignedNurseId(neighborId).equals(currentNid)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean hasNeighborPatients(ArrayList<String> pids1, ArrayList<String> pids2) {
		assert pids1 != null;
		assert pids2 != null;
		if (pids1.size() < 1) 
			throw new RuntimeException("IDs list size needs to be > 0");
		if (pids2.size() < 1) 
			throw new RuntimeException("IDs list size needs to be > 0");
		
		for (String pid1 : pids1) {
			for (String pid2 : pids2) {
				if (this.info.isNeighborDataPoint(pid1, pid2)) 
					return true;
			}
		}
		return false;
	}

	@Override
	public HomecareAssignmentState getInitialState() {
		return info.getEntityInfo().getVisitInfo().getCurrentAssignment();
	}
}
