package model_info;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;

import distance_matrix.AbstractDistanceMatrix;
import experiment.NurseDistrictingChange;
import experiment.UnexpectedChangeParameter;
import model_entity.CLSC;
import model_entity.ModelEntity;
import model_entity.Nurse;
import model_entity.Patient;
import model_state.HomecareAssignmentState;
import non_model_entity.DataPoint;
import support.SupportFunction;

public class EntityInfo {
	/*
	DOCUMENTATION

	EntityInfo object is an unmodifiable object (except for patientIdToFollowUpNurseId), means that it is only defined in constructors. 
	From that assumption, we can apply pass by reference for EntityInfo object, do not need 
	to have a deep copy.
	
	*/
	
	private static final String PATIENT_INFO = "patient-info";
	private static final String VISIT_INFO = "visit-info";
	private static final String CLSC_INFO = "clsc-info";
	private static final String NURSE_INFO = "nurse-info";
	// Entities info
	private final Map<String, Patient> patientIdToPatientMap;
	private final Map<String, Nurse> nurseIdToNurseMap;
	private final Map<String, CLSC> clscIdToClscMap;
	private final VisitInfo visitInfo;
	
	private final Set<Long> checkedCurrentAssignmentIds;
	
	// Public methods
	
	private EntityInfo(Map<String, Patient> patientIdToPatientMap, 
					   Map<String, Nurse> nurseIdToNurseMap, 
					   Map<String, CLSC> clscIdToClscMap,
					   VisitInfo visitInfo) {
		this.patientIdToPatientMap = new HashMap<>(patientIdToPatientMap);
		this.nurseIdToNurseMap = new HashMap<>(nurseIdToNurseMap);
		this.clscIdToClscMap = new HashMap<>(clscIdToClscMap);
		this.visitInfo = new VisitInfo(visitInfo); 
		this.checkedCurrentAssignmentIds = new HashSet<>();
	}
	
	public EntityInfo (EntityInfo info, NurseDistrictingChange changeInfo) {
		this(getChangedMap(info.patientIdToPatientMap, changeInfo.getRemovedPatientIds(), changeInfo.getNewPatients()), 
			 getChangedMap(info.nurseIdToNurseMap, changeInfo.getRemovedNurseIds(), changeInfo.getNewNurses()), 
			 info.clscIdToClscMap, 
			 getChangedInfo(info.visitInfo, changeInfo));
		if (!this.patientIdToPatientMap.keySet().equals(new HashSet<>(this.visitInfo.getPatientIds())))
			throw new RuntimeException("Inconsistent change");
		if (!this.nurseIdToNurseMap.keySet().equals(new HashSet<>(this.visitInfo.getNurseIds())))
			throw new RuntimeException("Inconsistent change");
	}

	protected EntityInfo(EntityInfo info) {
		if (info == null) 
			throw new RuntimeException("Null input");
		
		// Copying info 
		this.patientIdToPatientMap = new HashMap<>();
		this.patientIdToPatientMap.putAll(info.patientIdToPatientMap);
		assert (patientIdToPatientMap.size() == info.patientIdToPatientMap.size());
		
		this.nurseIdToNurseMap = new HashMap<>();
		this.nurseIdToNurseMap.putAll(info.nurseIdToNurseMap);
		assert (nurseIdToNurseMap.size() == info.nurseIdToNurseMap.size());
		
		this.clscIdToClscMap = new HashMap<>();
		this.clscIdToClscMap.putAll(info.clscIdToClscMap);
		assert (clscIdToClscMap.size() == info.clscIdToClscMap.size());
		
		this.visitInfo = info.visitInfo;
		
		this.checkedCurrentAssignmentIds = new HashSet<>();
		this.checkedCurrentAssignmentIds.addAll(info.checkedCurrentAssignmentIds);
		assert (checkedCurrentAssignmentIds.size() == info.checkedCurrentAssignmentIds.size());
	}
	
	public EntityInfo(String entityInfoDirectoryPath) {
		if (entityInfoDirectoryPath == null) 
			throw new NullPointerException();
		String [] fileNames = SupportFunction.getFilesName(entityInfoDirectoryPath, "json");
		
		String fName = SupportFunction.getPrefixMatch(PATIENT_INFO, fileNames);
		if (fName == null)
			throw new NullPointerException();
		JSONArray array = SupportFunction.parseJsonArrayFromFile(entityInfoDirectoryPath + "/" + fName);
		ArrayList<Patient> patients = Patient.parsePatientsListFromJsonArray(array);
		
		fName = SupportFunction.getPrefixMatch(NURSE_INFO, fileNames);
		if (fName == null)
			throw new NullPointerException();
		array = SupportFunction.parseJsonArrayFromFile(entityInfoDirectoryPath + "/" + fName);
		ArrayList<Nurse> nurses = Nurse.parseNursesListFromJsonArray(array);
		
		fName = SupportFunction.getPrefixMatch(CLSC_INFO, fileNames);
		if (fName == null)
			throw new NullPointerException();
		array = SupportFunction.parseJsonArrayFromFile(entityInfoDirectoryPath + "/" + fName);
		ArrayList<CLSC> clscs = CLSC.parseClscsListFromJsonArray(array);
		
		this.patientIdToPatientMap = getPatientIdToPatientMap(patients);
		this.nurseIdToNurseMap = getNurseIdToNurseMap(nurses);
		this.clscIdToClscMap = getClscIdToClscMap(clscs);
		
		this.visitInfo = new VisitInfo(entityInfoDirectoryPath + "/" + VISIT_INFO);
		if (!this.visitInfo.isConsistent(new ArrayList<>(this.patientIdToPatientMap.keySet()),
										 new ArrayList<>(this.nurseIdToNurseMap.keySet())))
			throw new RuntimeException("Inconsitent visit info");
				
		this.checkedCurrentAssignmentIds = new HashSet<>();
	}

	public EntityInfo(EntityInfo info, HomecareAssignmentState currentAssignment) {
		if (info == null)
			throw new NullPointerException();
		if (currentAssignment == null)
			throw new NullPointerException();

		// Copying info 
		this.patientIdToPatientMap = new HashMap<>();
		this.patientIdToPatientMap.putAll(info.patientIdToPatientMap);
		assert (patientIdToPatientMap.size() == info.patientIdToPatientMap.size());

		this.nurseIdToNurseMap = new HashMap<>();
		this.nurseIdToNurseMap.putAll(info.nurseIdToNurseMap);
		assert (nurseIdToNurseMap.size() == info.nurseIdToNurseMap.size());

		this.clscIdToClscMap = new HashMap<>();
		this.clscIdToClscMap.putAll(info.clscIdToClscMap);
		assert (clscIdToClscMap.size() == info.clscIdToClscMap.size());
		
		this.visitInfo = new VisitInfo(info.visitInfo, currentAssignment);

		this.checkedCurrentAssignmentIds = new HashSet<>();
	}

	public long getPatientsListSize() {
		return patientIdToPatientMap.keySet().size();
	}
	
	public long getNursesListSize() {
		return nurseIdToNurseMap.keySet().size();
	}
	
	public long getClscListSize() {
		return clscIdToClscMap.keySet().size();
	}
	
	public ArrayList<String> getPatientIds() {
		return new ArrayList<>(this.patientIdToPatientMap.keySet());
	}
	
	public ArrayList<String> getNurseIds() {
		return new ArrayList<>(this.nurseIdToNurseMap.keySet());
	}
	
	public ArrayList<String> getClscIds() {
		return new ArrayList<>(this.clscIdToClscMap.keySet());
	}
	
	public EntityInfo clone() {
		return this;
	}
	
	public ArrayList<DataPoint> getPatientDataPoints() {
		ArrayList<DataPoint> result = new ArrayList<>();
		for (String pid : this.patientIdToPatientMap.keySet()) {
			Patient p = this.patientIdToPatientMap.get(pid);
			result.add(new DataPoint(pid, p.getCoordinate()));
		}
		return result;
	}
	
	public DataPoint getPatientCoordinate(String dpid) {
		if (dpid == null)
			throw new NullPointerException();
		
		if (!this.patientIdToPatientMap.containsKey(dpid))
			throw new RuntimeException("Not existing data point id : " + dpid);
		
		return new DataPoint(dpid, this.patientIdToPatientMap.get(dpid).getCoordinate());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof EntityInfo))
			return false;

		EntityInfo info = (EntityInfo) obj;
		
		// Validate entities info 
		if (!patientIdToPatientMap.keySet().equals(info.patientIdToPatientMap.keySet()))
			return false;

		if (!nurseIdToNurseMap.keySet().equals(info.nurseIdToNurseMap.keySet()))
			return false;
		
		return true;
	}
	
	public VisitInfo getVisitInfo(){
		return this.visitInfo;
	}
	
	public boolean hasNurse(String nid) {
		if (nid == null)
			throw new NullPointerException();
		
		return nurseIdToNurseMap.containsKey(nid);
	}
	
	public boolean hasPatient(String pid) {
		if (pid == null)
			throw new NullPointerException();
		
		return patientIdToPatientMap.containsKey(pid);
	}
	
	public boolean hasClsc(String clscId) {
		if (clscId == null)
			throw new NullPointerException();
		
		return clscIdToClscMap.containsKey(clscId);
	}
	
	public boolean isConsistent(HomecareAssignmentState a) {
		if (a == null) {
			throw new NullPointerException();
		}
		
		if (a.GENERATED_ID != HomecareAssignmentState.EMPTY_GENERATED_ID && 
				checkedCurrentAssignmentIds.contains(a.GENERATED_ID))
			return true;
		
		// No matter how the consistency check will results, this HomecareAssignmentState a will be checked
		// ... thus, we just simply add it to the checked IDs list before checking it.
		this.checkedCurrentAssignmentIds.add(a.GENERATED_ID);
		
		// Checking before performing reconstruction
		ArrayList<String> pids = a.getPatientIds();
		ArrayList<String> nids = a.getNurseIds();
		
		if (pids.size() != patientIdToPatientMap.keySet().size() ||
				nids.size() != nurseIdToNurseMap.keySet().size())
			return false;
		
		// Because these 2 collections have the same size as checked before, thus
		// ... if a set contains all of the elements of the remaining one, 
		// ... then these 2 collections are equal to each other.
		if (!pids.containsAll(patientIdToPatientMap.keySet()))
			return false;
		
		if (!nids.containsAll(nurseIdToNurseMap.keySet()))
			return false;
		
		return true;
	}	
	
	public boolean isFollowUpImportant(String pid) {
		return this.patientIdToPatientMap.get(pid).isFollowUpImportant();
	}
	
	public Patient getPatient(String pid) {
		if (pid == null)
			throw new NullPointerException();
		
		if (!patientIdToPatientMap.containsKey(pid))
			throw new RuntimeException("Not existing patient id : " + pid);
		
		return patientIdToPatientMap.get(pid);
	}
	
	public CLSC getCLSC(String clscId) {
		if (clscId == null)
			throw new NullPointerException();
		if (!this.clscIdToClscMap.containsKey(clscId))
			throw new RuntimeException("Not existing CLSC Id : " + clscId);
		
		return this.clscIdToClscMap.get(clscId);
	}
	
	public ArrayList<CLSC> getCLSCs() {
		return new ArrayList<>(this.clscIdToClscMap.values());
	}
	
	public ArrayList<Nurse> getNurses() {
		return new ArrayList<>(this.nurseIdToNurseMap.values());
	}
	
	public ArrayList<Patient> getPatients() {
		return new ArrayList<>(this.patientIdToPatientMap.values());
	}
	
	public Nurse getNurse(String nid) {
		if (nid == null)
			throw new NullPointerException();
		
		if (!nurseIdToNurseMap.containsKey(nid))
			throw new RuntimeException("Not existing nurse id : " + nid);
		
		return nurseIdToNurseMap.get(nid);
	}
	
	public void generateJsonsInfoDirectory(String destDirectoryPath) {
		if (destDirectoryPath == null)
			throw new NullPointerException();
		SupportFunction.createDirectory(destDirectoryPath);
		
		saveToFile(this.patientIdToPatientMap.values(), destDirectoryPath 
														+ "/" + PATIENT_INFO + "-" 
														+  this.patientIdToPatientMap.values().size()
														+ ".json");
		saveToFile(this.nurseIdToNurseMap.values(), destDirectoryPath 
														+ "/" + NURSE_INFO + "-" 
														+  this.nurseIdToNurseMap.values().size()
														+ ".json");
		saveToFile(this.clscIdToClscMap.values(), destDirectoryPath 
														+ "/" + CLSC_INFO + "-" 
														+  this.clscIdToClscMap.values().size()
														+ ".json");
		visitInfo.generateJsonsInfoDirectory(destDirectoryPath + "/" + VISIT_INFO);
	}
	
	private static <E extends ModelEntity> void saveToFile(Collection<E> values, String filePath) {
		JSONArray array = ModelEntity.generateModelEntitiesJsonArray(new ArrayList<>(values));
		try {
			SupportFunction.saveFile(array.toString(3), filePath);
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	// ------------------------------------------------------------------------------------------------------
		// PRIVATE Methods

	private static Map<String, CLSC> getClscIdToClscMap(ArrayList<CLSC> clscs) {
		if (clscs == null)
			throw new NullPointerException();
		if (clscs.size() == 0)
			throw new RuntimeException("Nothing to generate");
		
		Map<String, CLSC> clscIdToClscMap = new HashMap<>();
		
		for (CLSC clsc : clscs) {
			String id = clsc.getId();
			if (!clscIdToClscMap.containsKey(id)) {
				// Because we cannot change a CLSC object, thus we do not care about unexpected modification, 
				// then just simple pass its pointer
				clscIdToClscMap.put(id, clsc);
			}
			else {
				throw new RuntimeException("Duplicated CLSC : " + id);
			}
		}
		
		return clscIdToClscMap;
	}

	private static Map<String, Nurse> getNurseIdToNurseMap(ArrayList<Nurse> nurses) {
		if (nurses == null)
			throw new NullPointerException();
		if (nurses.size() == 0)
			throw new RuntimeException("Nothing to generate");
		
		Map<String, Nurse> nurseIdToNurseMap = new HashMap<>();
		for (Nurse nurse : nurses) {
			String nid = nurse.getId();
			if (!nurseIdToNurseMap.containsKey(nid)) {
				// Because we cannot change a Nurse object, thus we do not care about unexpected modification, 
				// then just simple pass its pointer
				nurseIdToNurseMap.put(nid, nurse);
			}
			else {
				throw new RuntimeException("Duplicated nurse : " + nid);
			}
		}
		
		return nurseIdToNurseMap;
	}

	private static Map<String, Patient> getPatientIdToPatientMap(ArrayList<Patient> patients) {
		if (patients == null)
			throw new NullPointerException();
		if (patients.size() == 0)
			throw new RuntimeException("Nothing to generate");
		
		Map<String, Patient> patientIdToPatientMap = new HashMap<>();
		
		for (Patient p : patients) {
			String pid = p.getId();			
			if (!patientIdToPatientMap.containsKey(pid)) {
				// Because we cannot change a Patient object, thus we do not care about unexpected modification, 
				// then just simple pass its pointer
				patientIdToPatientMap.put(pid, p);
			}
			else {
				throw new RuntimeException("Duplicated patient : " + pid);
			}
		}	
		
		return patientIdToPatientMap;
	}

	public int getTotalDataDays() {
		return this.visitInfo.getTotalDataDays();
	}
	
	private static VisitInfo getChangedInfo(VisitInfo visitInfo, NurseDistrictingChange changeInfo) {
		if (visitInfo == null)
			throw new NullPointerException();
		if (changeInfo == null)
			throw new NullPointerException();
		
		return new VisitInfo(visitInfo, changeInfo.getNewDailyVisits(), changeInfo.getRemovedPatientIds());
	}

	private static <T extends ModelEntity> Map<String, T> getChangedMap(Map<String, T> map, ArrayList<String> removedEntityIds, ArrayList<T> newEntities) {
		if (map == null)
			throw new NullPointerException();
		if (removedEntityIds == null)
			throw new NullPointerException();
		if (newEntities == null)
			throw new NullPointerException();
		
		Map<String, T> newMap = getRemovingChangedMap(map, removedEntityIds);		
		return getAddingNewChangedMap(newMap, newEntities);
	}
	
	private static <T extends ModelEntity> Map<String, T> getAddingNewChangedMap(Map<String, T> map, ArrayList<T> newEntities) {
		Map<String, T> newMap = new HashMap<>(map);
		for (T e : newEntities) {
			if (!newMap.containsKey(e.getId()))
				newMap.put(e.getId(), e);
		}
		return newMap;
	}

	private static <T extends ModelEntity> Map<String, T> getRemovingChangedMap (Map<String, T> map, ArrayList<String> removedEntityIds) {
		Map<String, T> newMap = new HashMap<>(map);
		for (String id : removedEntityIds) {
			if (!newMap.containsKey(id))
				throw new RuntimeException("Not existing entity ID : " + id);
			newMap.remove(id);
		}
		return newMap;
	}

	public boolean isConsistent(AbstractDistanceMatrix distanceMatrix) {
		if (distanceMatrix == null)
			throw new NullPointerException();
		
		ArrayList<String> ids = new ArrayList<>(this.patientIdToPatientMap.keySet());
		ids.addAll(this.clscIdToClscMap.keySet());
				
		return distanceMatrix.getIdsList().containsAll(ids);
	}

	public EntityInfo generateInfoWithUnexpectedChange(UnexpectedChangeParameter parameter, Map<String, ArrayList<String>> closestNursesMap) {
		return new EntityInfo(this.patientIdToPatientMap,
							  this.nurseIdToNurseMap,
							  this.clscIdToClscMap,
							  this.visitInfo.generateInfoWithUnexpectedVisits(parameter, closestNursesMap));
	}
}
