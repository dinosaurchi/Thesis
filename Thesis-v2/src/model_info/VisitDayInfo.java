package model_info;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;

import experiment.UnexpectedChangeParameter;
import model_entity.Visit;
import support.SupportFunction;

public class VisitDayInfo {
	/* The reason for the patient to multiple nurses is because even though we assume
			that there will be only one nurse assigned to a patient, we will have the cases
			(in practical simulations) when a patient will be visited by another nurse instead of 
			the assigned one for some visits.  
	*/
	private final Map<String, Map<String, ArrayList<Visit>>> patientIdToNurseVisitMap;
	
	public VisitDayInfo(JSONArray array) {
		if (array == null)
			throw new NullPointerException();		
		
		ArrayList<Visit> visits = Visit.parseVisitsListFromJsonArray(array);
		checkDuplicatedVisit(visits);
		patientIdToNurseVisitMap = generatePatientIdToVisitMap(visits);
	}
	
	private static void checkDuplicatedVisit(ArrayList<Visit> visits) {
		ArrayList<String> vids = new ArrayList<>();
		for (Visit visit : visits) {
			if (vids.contains(visit.getId()))
				throw new RuntimeException("Duplicated vid : " + visit.getId());
			vids.add(visit.getId());
		}
	}

	protected VisitDayInfo(VisitDayInfo info) {
		if (info == null)
			throw new NullPointerException();
		
		this.patientIdToNurseVisitMap = getCopiedMap(info.patientIdToNurseVisitMap);
		assert (patientIdToNurseVisitMap.size() == info.patientIdToNurseVisitMap.size());
	}
	
	private static Map<String, Map<String, ArrayList<Visit>>> getCopiedMap(Map<String, Map<String, ArrayList<Visit>>> patientIdToNurseVisitMap) {
		Map<String, Map<String, ArrayList<Visit>>> newMap = new HashMap<>();
		for (String pid : patientIdToNurseVisitMap.keySet()) {
			Map<String, ArrayList<Visit>> copiedMap = new HashMap<String, ArrayList<Visit>>();
			Map<String, ArrayList<Visit>> nurseToVisitMap = patientIdToNurseVisitMap.get(pid);
			for (String nid : nurseToVisitMap.keySet()) {
				copiedMap.put(nid, new ArrayList<>(nurseToVisitMap.get(nid)));
			}
			newMap.put(pid, copiedMap);
		}
		return newMap;
	}
	
	public VisitDayInfo(VisitDayInfo info, 
						ArrayList<Visit> newVisits,
						ArrayList<String> removedPatientIds) {
		if (info == null)
			throw new NullPointerException();
		if (newVisits == null)
			throw new NullPointerException();
		if (removedPatientIds == null)
			throw new NullPointerException();
		
		this.patientIdToNurseVisitMap = getCopiedMap(info.patientIdToNurseVisitMap);
		
		// Remove all the visits to the removed patients if there are visits to them in this visit day
		for (String pid : removedPatientIds) {
			if (this.patientIdToNurseVisitMap.containsKey(pid))
				this.patientIdToNurseVisitMap.remove(pid);
		}
		
		extendVisitMap(this.patientIdToNurseVisitMap, newVisits);
	}
	
	public VisitDayInfo(ArrayList<Visit> visits) {
		if (visits == null)
			throw new NullPointerException();
		
		this.patientIdToNurseVisitMap = new HashMap<>();
		extendVisitMap(this.patientIdToNurseVisitMap, visits);
	}
	
	private static void extendVisitMap(Map<String, Map<String, ArrayList<Visit>>> destPatientIdToNurseVisitMap, ArrayList<Visit> newVisits) {
		if (destPatientIdToNurseVisitMap == null)
			throw new NullPointerException();
		if (newVisits == null)
			throw new NullPointerException();
		
		for (Visit visit : newVisits) {
			String pid = visit.getPid();
			if (!destPatientIdToNurseVisitMap.containsKey(pid))
				destPatientIdToNurseVisitMap.put(pid, new HashMap<String, ArrayList<Visit>>());
			
			String nid = visit.getNid();
			Map<String, ArrayList<Visit>> nurseToVisit = destPatientIdToNurseVisitMap.get(pid);
			if (!nurseToVisit.containsKey(nid))
				nurseToVisit.put(nid, new ArrayList<Visit>());
			
			ArrayList<Visit> visitsList = nurseToVisit.get(nid);
			visitsList.add(visit);
		}
	}

	public JSONArray generateJsonArray() {
		JSONArray array = new JSONArray();
		for (Map<String, ArrayList<Visit>> nurseToVisits : patientIdToNurseVisitMap.values()) {
			for (ArrayList<Visit> visits : nurseToVisits.values()) {
				for (Visit v : visits)
					array.put(v.generateJsonObject());
			}
		}
		return array;
	}
	
	public ArrayList<String> getNursesVisited(String pid) {
		if (pid == null)
			throw new NullPointerException();
		if (!this.patientIdToNurseVisitMap.containsKey(pid))
			return new ArrayList<>();
		
		return new ArrayList<>(this.patientIdToNurseVisitMap.get(pid).keySet());
	}
	
	// We do not copy these Map thru copy constructor
	private Map<String, Integer> totalVisitsToPatientMap = new HashMap<>();
	public int getTotalVisitsToPatient(String pid) {
		if (pid == null)
			throw new NullPointerException();
		if (!this.patientIdToNurseVisitMap.containsKey(pid))
			return 0;
		
		if (!totalVisitsToPatientMap.containsKey(pid)) {
			int total = 0;
			for (String nid : this.patientIdToNurseVisitMap.get(pid).keySet()) {
				total += this.patientIdToNurseVisitMap.get(pid).get(nid).size();
			}
			totalVisitsToPatientMap.put(pid, total);
		}
		return totalVisitsToPatientMap.get(pid);
	}
	
	// Do not need to map this function result, because it already is a map
	public ArrayList<Visit> getVisits(String pid, String nid) {
		if (pid == null)
			throw new NullPointerException();
		if (nid == null)
			throw new NullPointerException();
		if (!patientIdToNurseVisitMap.containsKey(pid))
			return new ArrayList<>();
		if (!patientIdToNurseVisitMap.get(pid).containsKey(nid))
			return new ArrayList<>();
			
		return new ArrayList<>(patientIdToNurseVisitMap.get(pid).get(nid));
	}
	
	// Do not need to map this function result, because it already is a map
	public double getTotalVisitsToPatientByNurse(String pid, String nid) {
		if (nid == null)
			throw new NullPointerException();
		if (pid == null)
			throw new NullPointerException();
		if (!this.patientIdToNurseVisitMap.containsKey(pid))
			return 0;
		if (!this.patientIdToNurseVisitMap.get(pid).containsKey(nid))
			return 0;
		
		return this.patientIdToNurseVisitMap.get(pid).get(nid).size();
	}
	
	public ArrayList<String> getPatientIds() {
		return new ArrayList<>(this.patientIdToNurseVisitMap.keySet());
	}

	// We do not copy these Map thru copy constructor
	private boolean isComputedNurseIdsArray = false;
	private ArrayList<String> nurseIdsArray = null;
	public ArrayList<String> getNurseIds() {
		if (!isComputedNurseIdsArray) {
			// We remove all duplicated IDs by using HashSet
			Set<String> nids = new HashSet<>();
			for (Map<String, ArrayList<Visit>> visitsMap : patientIdToNurseVisitMap.values()) {
				nids.addAll(visitsMap.keySet());
			}
			nurseIdsArray = new ArrayList<>(nids);
			isComputedNurseIdsArray = true;
		}
		if (nurseIdsArray == null)
			throw new RuntimeException();
		
		return new ArrayList<>(nurseIdsArray);
	}
	
	private static Map<String, Map<String, ArrayList<Visit>>> generatePatientIdToVisitMap(ArrayList<Visit> visits) {
		if (visits == null)
			throw new NullPointerException();
		if (visits.size() == 0)
			throw new RuntimeException("Nothing to generate");
		
		Map<String, Map<String, ArrayList<Visit>>> patientIdToVisitMap = new HashMap<>();
		for (Visit visit : visits) {
			String pid = visit.getPid();
			String nid = visit.getNid();
			
			if (!patientIdToVisitMap.containsKey(pid)) {
				Map<String, ArrayList<Visit>> nurseToVisitMap = new HashMap<>();
				ArrayList<Visit> visitsList = new ArrayList<>();
				visitsList.add(visit);
				nurseToVisitMap.put(nid, visitsList);
				patientIdToVisitMap.put(pid, nurseToVisitMap);
			}
			else {
				Map<String, ArrayList<Visit>> nurseToVisitMap = patientIdToVisitMap.get(pid);
				if (!nurseToVisitMap.containsKey(nid)) {
					ArrayList<Visit> visitsList = new ArrayList<>();
					visitsList.add(visit);
					nurseToVisitMap.put(nid, visitsList);
				}
				else {
					nurseToVisitMap.get(nid).add(visit);
				}
			}
		}
		
		return patientIdToVisitMap;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof VisitDayInfo))
			throw new RuntimeException("Wrong type : " + obj.getClass());
		VisitDayInfo info = (VisitDayInfo) obj;
		if (!this.patientIdToNurseVisitMap.keySet().equals(info.patientIdToNurseVisitMap.keySet()))
			return false;
		
		for (String pid : patientIdToNurseVisitMap.keySet()) {
			if (!SupportFunction.contains(new ArrayList<>(info.patientIdToNurseVisitMap.values()),
									 	 patientIdToNurseVisitMap.get(pid))) {
				return false;
			}
		}
		for (String pid : info.patientIdToNurseVisitMap.keySet()) {
			if (!SupportFunction.contains(new ArrayList<>(patientIdToNurseVisitMap.values()),
										  info.patientIdToNurseVisitMap.get(pid))) {
				return false;
			}
		}
		return true;
	}
	
	// We do not copy these Map thru copy constructor
	private boolean isComputedallVisitsArray = false;
	private ArrayList<Visit> allVisitsArray = null;
	public ArrayList<Visit> getAllVisits() {
		if (!isComputedallVisitsArray) {
			allVisitsArray = new ArrayList<>();
			for (Map<String, ArrayList<Visit>> m : patientIdToNurseVisitMap.values()) {
				for (ArrayList<Visit> v : m.values())
					allVisitsArray.addAll(v);
			}
			isComputedallVisitsArray = true;
		}
		return new ArrayList<>(allVisitsArray);
	}
	
	private Map<String, ArrayList<Visit>> visitsFromPidMap = new HashMap<>();
	public ArrayList<Visit> getVisits(String pid) {
		if (pid == null)
			throw new NullPointerException();
		if (!this.patientIdToNurseVisitMap.containsKey(pid))
			return new ArrayList<>();
		
		if (!visitsFromPidMap.containsKey(pid)) {
			ArrayList<Visit> visits = new ArrayList<>();
			for (ArrayList<Visit> vs : this.patientIdToNurseVisitMap.get(pid).values()) {
				visits.addAll(vs);
			}
			visitsFromPidMap.put(pid, visits);
		}
		return new ArrayList<>(visitsFromPidMap.get(pid));
	}
	
	private Map<String, Set<String>> visitedPatientIdsMap = new HashMap<>();
	public ArrayList<String> getVisitedPatientIds(String nid) {
		if (nid == null)
			throw new NullPointerException();
		
		if (!visitedPatientIdsMap.containsKey(nid)) {
			Set<String> result = new HashSet<>();
			for (String pid : this.patientIdToNurseVisitMap.keySet()) {
				if (this.patientIdToNurseVisitMap.get(pid).containsKey(nid)) 
					result.add(pid);
			}
			visitedPatientIdsMap.put(nid, result);
		}
		return new ArrayList<>(visitedPatientIdsMap.get(nid));
	}

	public VisitDayInfo generateInfoWithUnexpectedVisits(UnexpectedChangeParameter parameter, 
														 Map<String, ArrayList<String>> closestNursesMap) {
		if (parameter == null)
			throw new NullPointerException();
		if (closestNursesMap == null)
			throw new NullPointerException();
		
		ArrayList<String> initialPids = new ArrayList<>(this.patientIdToNurseVisitMap.keySet());
		
		ArrayList<String> tabuPids = new ArrayList<>();
		ArrayList<String> pidsAtLeastThree = extract(initialPids, tabuPids, parameter, 3);
		tabuPids.addAll(pidsAtLeastThree);
		
		ArrayList<String> pidsTwice = extract(initialPids, tabuPids, parameter, 2);	
		tabuPids.addAll(pidsTwice);
		
		ArrayList<String> pidsOnce = extract(initialPids, tabuPids, parameter, 1);
		tabuPids.addAll(pidsOnce);
		
		ArrayList<Visit> randomChangedAtLeastThree = getRandomChangedAtLeast(pidsAtLeastThree, 3, closestNursesMap);
		ArrayList<Visit> randomChangedTwice = getRandomChanged(pidsTwice, 2, closestNursesMap);
		ArrayList<Visit> randomChangedOnce = getRandomChanged(pidsOnce, 1, closestNursesMap);
		
		initialPids.removeAll(tabuPids);
		ArrayList<Visit> remainingVisits = new ArrayList<>();
		for (String pid : initialPids) {
			remainingVisits.addAll(this.getVisits(pid));
		}
		
		ArrayList<Visit> newVisits = new ArrayList<>();
		newVisits.addAll(randomChangedOnce);
		newVisits.addAll(randomChangedTwice);
		newVisits.addAll(randomChangedAtLeastThree);
		newVisits.addAll(remainingVisits);
		
		if (newVisits.size() != this.getAllVisits().size())
			throw new RuntimeException("Inconsistent new visits list size : " + newVisits.size() 
											+ " and " + this.getAllVisits().size());
		
		return new VisitDayInfo(this);
	}
	
	private ArrayList<Visit> getRandomChangedAtLeast(ArrayList<String> pids, int minNumberOfChangesEachPatient,
													 Map<String, ArrayList<String>> closestNursesMap) {
		ArrayList<Visit> result = new ArrayList<>();
		
		pids = new ArrayList<>(pids);
		int maxChanges = Math.max(minNumberOfChangesEachPatient, pids.size());
		double proportion = 0.5;
		ArrayList<String> tabuPids = new ArrayList<>();
		
		int i = minNumberOfChangesEachPatient;
		while (i <= maxChanges) {
			ArrayList<String> subPids = extractFrom(pids, tabuPids, proportion, i);
			tabuPids.addAll(subPids);
			proportion /= 2.0;
			result.addAll(getRandomChanged(subPids, i, closestNursesMap));
			if (subPids.size() == 0) {
				pids.removeAll(tabuPids);
				result.addAll(getRandomChanged(pids, i, closestNursesMap));
				break;
			}
			++i;
		}
		pids.removeAll(tabuPids);
		result.addAll(getRandomChanged(pids, minNumberOfChangesEachPatient, closestNursesMap));
		
		return result;
	}

	private ArrayList<Visit> getRandomChanged(ArrayList<String> pids, int numberOfChangesEachPatient, Map<String, ArrayList<String>> closestNursesMap) {
		ArrayList<Visit> newVisits = new ArrayList<>();
		for (String pid : pids) {
			int n = numberOfChangesEachPatient;
			for (Visit v : this.getVisits(pid)) {
				String nid = v.getNid();
				if (n > 0) {
					for (String nextNid : closestNursesMap.get(pid)) {
						if (!nextNid.equals(v.getNid())) {
							nid = nextNid;
							break;
						}
					}
					--n;
				}
				newVisits.add(new Visit(v.getPid(), nid, v.getVisitType(), v.getServiceTime()));
			}
		}
		return newVisits;
	}

	private ArrayList<String> extract(ArrayList<String> pids, ArrayList<String> tabuPids, UnexpectedChangeParameter parameter, int numOfChanges) {
		double overallChangeProportion = parameter.getOverallPatientsVisitedByAnotherNurseProportion();
		double subProportion = parameter.getPatientsVisitedByAnotherNurseSubProportion(numOfChanges);
		return extractFrom(pids, tabuPids, overallChangeProportion * subProportion, numOfChanges);
	}

	private ArrayList<String> extractFrom(ArrayList<String> pids, ArrayList<String> tabuPids, double proportion, int numberOfChanges) {
		ArrayList<String> newPids = new ArrayList<>(pids);
		Collections.shuffle(newPids);
		ArrayList<String> selected = new ArrayList<>();	
		int totalVisits = Math.round((float) (newPids.size() * proportion));
		while (selected.size() < totalVisits && numberOfChanges > 0) {
			for (String pid : newPids) {
				if (getTotalVisitsToPatient(pid) >= numberOfChanges && !tabuPids.contains(pid))
					selected.add(pid);
				if (selected.size() >= totalVisits)
					break;
			}
			newPids.removeAll(selected);
			--numberOfChanges;
		}
		return selected;
	}

	private Map<Integer, Integer> totalPatientsWithFrequencyMap = new HashMap<>();
	public int getTotalPatientsWithFrequency(int visitFrequency) {
		if (visitFrequency < 1)
			throw new RuntimeException("Invalid visit frequency : " + visitFrequency);
		if (!totalPatientsWithFrequencyMap.containsKey(visitFrequency)) {
			int total = 0;
			for (String pid : getPatientIds()) {
				if (this.getTotalVisitsToPatient(pid) == visitFrequency)
					total += 1;
			}
			totalPatientsWithFrequencyMap.put(visitFrequency, total);
		}
		return totalPatientsWithFrequencyMap.get(visitFrequency);
	}
	
}
