package model_info;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import distance_matrix.AbstractDistanceMatrix;
import experiment.NurseDistrictingChange;
import global.CHI_CONSTANT;
import info.DataPointsModelInfo;
import info.NeighborhoodModelInfo;
import model_entity.CLSC;
import model_entity.Patient;
import model_state.HomecareAssignmentState;
import neighborhood.AbstractNeighborhoodMap;
import neighborhood.VoronoiMap;
import non_model_entity.Coordinate2D;
import non_model_entity.DataPoint;

public class VivianeModelInfo extends NeighborhoodModelInfo<HomecareAssignmentState> implements DataPointsModelInfo<HomecareAssignmentState> {
/* ---------------DOCUMENTATION----------------------
	
	VivianeModelInfo object is an unmodifiable object (except for the previousMonthAssignment), means that it is only defined in constructors. 
	From that assumption, we can apply pass by reference for VivianeModelInfo object, do not need 
	to have a deep copy.	
*/
	
	public static final double MAX_WORKING_MINUTES = 300.0;
	
	private final EntityInfo entityInfo;
	private final AbstractDistanceMatrix distanceMatrix;
	
	protected VivianeModelInfo(VivianeModelInfo info) {
		super(info);
		
		// Copying info
		entityInfo = info.entityInfo.clone();
		
		// Copying distance matrix
		distanceMatrix = info.distanceMatrix.clone();		
	}
	
 	public VivianeModelInfo(EntityInfo info, AbstractDistanceMatrix distanceMatrix, AbstractNeighborhoodMap map) {
 		super(getNeighborhoodMapSelection(map, info));
 		if (info == null)
			throw new NullPointerException();
		if (distanceMatrix == null)
			throw new NullPointerException();
		
		if (!info.isConsistent(distanceMatrix))
			throw new RuntimeException("Inconsistent distance matrix");
		
		this.entityInfo = info.clone();
		this.distanceMatrix = distanceMatrix.clone();
	}

	public VivianeModelInfo(VivianeModelInfo modelInfo, NurseDistrictingChange change, HomecareAssignmentState currentState) {
		this(getChangedEntityInfo(modelInfo.getEntityInfo(currentState), 
								  modelInfo.getDistanceMatrix(), 
								  modelInfo.getNeighborhoodMap(), 
								  change), 
			 modelInfo.distanceMatrix, 
			 null);
	}
	
	private static EntityInfo getChangedEntityInfo(EntityInfo original, 
												   AbstractDistanceMatrix originalDistanceMatrix,
												   AbstractNeighborhoodMap originalNeighborhoodMap,
												   NurseDistrictingChange change) {
		EntityInfo newInfo = new EntityInfo(original, change);
		Map<String, ArrayList<String>> closestNursesMap = generateClosestNurseMap(newInfo, 
																				  originalDistanceMatrix, 
																				  originalNeighborhoodMap,
																				  2);
		return newInfo.generateInfoWithUnexpectedChange(change.getParameter(), closestNursesMap);
	}
	
	private static Map<String, ArrayList<String>> generateClosestNurseMap(EntityInfo info,
																		  AbstractDistanceMatrix distanceMatrix, 
																		  AbstractNeighborhoodMap neighborhoodMap, 
																		  int maxRank) {
		if (info == null)
			throw new NullPointerException();
		if (distanceMatrix == null)
			throw new NullPointerException();
		if (neighborhoodMap == null)
			throw new NullPointerException();
		if (maxRank < 1)
			throw new RuntimeException("Invalid max rank : " + maxRank);
		
		Map<String, ArrayList<String>> nurseMap = new HashMap<>();
		for (String pid : info.getPatientIds()) {
			nurseMap.put(pid, getClosestNurses(pid, info, distanceMatrix, neighborhoodMap, maxRank));
		}
		return nurseMap;
	}

	private static ArrayList<String> getClosestNurses(String pid, 
													  EntityInfo info,
													  AbstractDistanceMatrix distanceMatrix, 
													  AbstractNeighborhoodMap neighborhoodMap, 
													  int maxRank) {
		ArrayList<String> nids = getRankedNidsByDistanceToPid(pid, info, distanceMatrix, neighborhoodMap);		
		return new ArrayList<>(nids.subList(0, Math.min(maxRank, nids.size())));
	}

	private static ArrayList<String> getRankedNidsByDistanceToPid(final String pid, 
																  final EntityInfo info,
																  final AbstractDistanceMatrix distanceMatrix,
																  final AbstractNeighborhoodMap neighborhoodMap) {
		ArrayList<String> nids = info.getNurseIds();
		final VisitInfo visitInfo = info.getVisitInfo();
		Collections.sort(nids, new Comparator<String>() {
			public int compare(String nid1, String nid2) {
				double workload1 = getTemporaryTravelload(nid1);
				double workload2 = getTemporaryTravelload(nid2);
				
				/* We can use workload to make decision among nurses is because we are adding
					unexpected change (noise) to the visits list, thus it is just a simulation. The selection
					of nurse for the noise in fact should lead to an optimal solution at the end of month,
					and we are now supposing that we know the info of the whole month, and we can choose nurse 
					such that it leads to an optimal assignment for the whole month.
				*/
				
				if (workload1 > workload2)
					return 1;
				else if (workload1 < workload2)
					return -1;
				return 0;
			}
			
			private double getTemporaryTravelload(String nid) {
				ArrayList<String> assignedPids = visitInfo.getVisitedPatientIds(nid);
				if (!assignedPids.contains(pid))
					assignedPids.add(pid);
				
				double total = 0;
				for (String pid : assignedPids) {
					total += getAverageDailyTravelingTimeImplementation(pid, assignedPids, info, distanceMatrix, neighborhoodMap);
				}
				return total;				
			}
		});
		
		return nids;
	}
	
	public double getNurseAverageDailyWorkLoad(List<String> assignedPids) {	
		/*
	       The reason for the separation between an interface and an Implementation method is because 
		   ... I want to utilize the code of of computing the workload for another purpose, where the info
		   ... is not necessary belonged to "this" object. For instance, you can look at the constructor
		   ... which has argument Change.
		 */
		
		return getNurseAverageDailyWorkLoadImplementation(
					assignedPids,
					this.entityInfo,
					this.distanceMatrix,
					this.getNeighborhoodMap()
				);		
	}
	
	private static double getNurseAverageDailyWorkLoadImplementation(
						List<String> assignedPids,
						EntityInfo info,
						AbstractDistanceMatrix distanceMatrix,
						AbstractNeighborhoodMap neighborhoodMap) {
		if (assignedPids == null)
			throw new NullPointerException();
		if (info == null)
			throw new NullPointerException();
		if (distanceMatrix == null)
			throw new NullPointerException();
		if (neighborhoodMap == null)
			throw new NullPointerException();
		
		double result = 0;
		result += 2 * getAverageDailyTravelingTimeImplementation(CLSC.CLSC_ID, assignedPids, info, distanceMatrix, neighborhoodMap);
	
		for (String pid : assignedPids) {
			double temp1 = getAverageDailyTravelingTimeImplementation(pid, assignedPids, info, distanceMatrix, neighborhoodMap);
			double temp2 = getAverageDailyTreatmentTimeImplementation(pid, info);
			result +=  temp1 + temp2;
		}
		return result;
	}
	
	public double getAverageDailyTravelingTime(List<String> consideringPids) {
		if (consideringPids == null)
			throw new NullPointerException();
		
		double total = 0;
		for (String pid : consideringPids) {
			total += getAverageDailyTravelingTimeImplementation(pid, consideringPids, this.entityInfo, this.distanceMatrix, this.getNeighborhoodMap());
		}
		return total;
	}
	
	public double getAverageDailyTravelingTime(String currentPid, List<String> consideringPids) {
		return getAverageDailyTravelingTimeImplementation(currentPid, consideringPids, 
														  this.entityInfo, this.distanceMatrix, this.getNeighborhoodMap());
	}
	
	public double getAverageDailyTreatmentTime(String pid) {
		return getAverageDailyTreatmentTimeImplementation(pid, this.entityInfo);
	}
	
	private static double getAverageDailyTreatmentTimeImplementation(String pid, EntityInfo info) {
		if (pid == null)
			throw new NullPointerException();
		if (info == null)
			throw new NullPointerException();
		return info.getVisitInfo().getVisitFrequency(pid) * getAverageServiceTimeImplementation(pid, info);
	}

	private static double getAverageDailyTravelingTimeImplementation(String pid, List<String> consideringPids,
																	 EntityInfo info, AbstractDistanceMatrix distanceMatrix,
																	 AbstractNeighborhoodMap neighborhoodMap ) {
		// Return the average daily traveling time from a patient to all of the other patients assigned to the same nurse 
		if (pid == null)
			throw new NullPointerException();
		if (consideringPids == null)
			throw new NullPointerException();
		if (info == null)
			throw new NullPointerException();
		if (distanceMatrix == null)
			throw new NullPointerException();
		if (neighborhoodMap == null)
			throw new NullPointerException();
		
		double numerator = 0;
		double denominator = 0;
		
		if (pid.equals(CHI_CONSTANT.CLSC_ID) || info.hasPatient(pid)) {
			for (String otherPid : consideringPids) {
				if (!otherPid.equals(pid)) {
					double freqeuency = info.getVisitInfo().getVisitFrequency(otherPid);		
					double time = distanceMatrix.getDistance(pid, otherPid);
					
					numerator +=  freqeuency * time;
					denominator += freqeuency;
				}
			}
		}
		else {
			throw new RuntimeException("Not-existing patient id : " + pid);
		}
		
		if (denominator != 0)
			return numerator / denominator;
		return 0;
	}

	private static AbstractNeighborhoodMap getNeighborhoodMapSelection(AbstractNeighborhoodMap map, EntityInfo info) {
		if (map == null)
			return new VoronoiMap(info.getPatientDataPoints());
		return map.clone();
	}

	@Override
 	public VivianeModelInfo clone() {
 		return new VivianeModelInfo(this);
 	}
 	
 	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof VivianeModelInfo))
			throw new RuntimeException();
		
		VivianeModelInfo info = (VivianeModelInfo) obj;
	
		if (!this.entityInfo.equals(info.entityInfo))
			return false;
		
		if (!this.distanceMatrix.equals(info.distanceMatrix))
			return false;
						
		return super.equals(obj);
	}
 	
	public long getTotalNurses() {
		return this.entityInfo.getNursesListSize();
	}
	
	public long getTotalPatients() {
		return this.entityInfo.getPatientsListSize();
	}
	
	public EntityInfo getEntityInfo() {
		return this.entityInfo;
	}
	
	public EntityInfo getEntityInfo(HomecareAssignmentState currentAssignment) {
		return new EntityInfo(this.entityInfo, currentAssignment);
	}
	
	public double getTimeDistance(String pid1, String pid2) {
		if (pid1 == null)
			throw new NullPointerException();
		if (pid2 == null)
			throw new NullPointerException();
	
		return this.distanceMatrix.getDistance(pid1, pid2);
	}
	
	public String getClosetTimeDistancePatientId(String targetPid, ArrayList<String> consideringPids) {
		if (targetPid == null)
			throw new NullPointerException();
		if (consideringPids == null)
			throw new NullPointerException();
		if (consideringPids.size() < 1)
			throw new RuntimeException("Considering list size needs to be > 0");
		
		double minTime = Double.MAX_VALUE;
		String closetPid = null;
		for (String pid : consideringPids) {
			double time = this.distanceMatrix.getDistance(targetPid, pid);
			if (time < minTime) {
				minTime = time;
				closetPid = pid;
			}
		}
		assert closetPid != null;
		return closetPid;
	}
	
	public double getEuclideanDistance(String pid1, String pid2) {
		if (pid1 == null)
			throw new NullPointerException();
		if (pid2 == null)
			throw new NullPointerException();
		
		Patient p1 = entityInfo.getPatient(pid1);
		Patient p2 = entityInfo.getPatient(pid2);
		return p1.getCoordinate().euclideanDistance(p2.getCoordinate());
	}
	
	public String getClosetEuclideanDistancePatientId(Coordinate2D coord, ArrayList<String> consideringPids) {
		if (coord == null)
			throw new NullPointerException();
		if (consideringPids == null)
			throw new NullPointerException();
		if (consideringPids.size() < 1)
			throw new RuntimeException("Considering list size needs to be > 0");
		
		double minDistance = Double.MAX_VALUE;
		String closestPid = null;
		for (String pid : consideringPids) {
			Patient p = entityInfo.getPatient(pid);
			double distance = p.getCoordinate().euclideanDistance(coord);
			if (distance < minDistance) {
				minDistance = distance;
				closestPid = pid;
			}
		}
		assert closestPid != null;
		return closestPid;
	}

	public ArrayList<String> getNurseIds() {
		return this.entityInfo.getNurseIds();
	}

	public Patient getPatient(String pid) {
		return this.entityInfo.getPatient(pid);
	}

	public boolean hasPatient(String pid) {
		return this.entityInfo.hasPatient(pid);
	}
	
	public boolean isFollowUpImportant(String pid) {
		return this.entityInfo.isFollowUpImportant(pid);
	}

	public double getLastMonthVisitProportion(String pid, String nid) {
		return this.entityInfo.getVisitInfo().getVisitProportion(pid, nid);
	}

	public double getVisitFrequency(String pid) {
		return this.entityInfo.getVisitInfo().getVisitFrequency(pid);
	}
	
	public double getAverageServiceTime(String pid) {
		return getAverageServiceTimeImplementation(pid, this.entityInfo);
	}
	
	private static double getAverageServiceTimeImplementation(String pid, EntityInfo info) {
		if (pid == null)
			throw new NullPointerException();
		if (info == null)
			throw new NullPointerException();
		
		double totalTime = 0;
		ArrayList<String> nids = info.getVisitInfo().getNursesVisited(pid);
		if (nids.size() == 0)
			throw new RuntimeException("Invalid pid : " + pid);
		
		for (String nid : nids) {
			totalTime += info.getVisitInfo().getAverageVisitServiceTime(pid, nid);
		}
		
		return totalTime / nids.size();
	}
	
	public ArrayList<String> getPatientIds() {
		return this.entityInfo.getPatientIds();
	}

	@Override
	public ArrayList<DataPoint> getDataPointsList() {
		return this.entityInfo.getPatientDataPoints();
	}
	
	@Override
	public ArrayList<String> getClassIdsList() {
		return this.getNurseIds();
	}

	@Override
	public boolean isConsistent(HomecareAssignmentState state) {
		if (state == null)
			throw new NullPointerException();
		
		return this.entityInfo.isConsistent(state);
	}

	@Override
	public DataPoint getDataPoint(String dpid) {
		return this.entityInfo.getPatientCoordinate(dpid);
	}
	
	public boolean hasNurse(String nid) {
		return this.entityInfo.hasNurse(nid);
	}
	
	public ArrayList<String> getNeighborPatientIds(String pid) {
		return getNeighborDataPointIds(pid);
	}
	
	@Override
	public ArrayList<DataPoint> getDataPoints(ArrayList<String> dataPointIds) {
		if (dataPointIds == null)
			throw new NullPointerException();
		
		// It is possible to have empty data points set
		
		ArrayList<DataPoint> dataPoints = new ArrayList<>();
		for (String dpid : dataPointIds) {
			dataPoints.add(this.getDataPoint(dpid));
		}
		return dataPoints;
	}
	
	public CLSC getCLSC(String clscId) {
		return this.entityInfo.getCLSC(clscId);
	}

	public int getTotalDataDays() {
		return this.entityInfo.getTotalDataDays();
	}

	public String getHighestProportionNurse(String pid) {
		if (pid == null)
			throw new NullPointerException();
		
		String highestNid = null;
		double maxVal = -9999999999.9;
		for (String nid : this.entityInfo.getVisitInfo().getNursesVisited(pid)) {
			double val = this.entityInfo.getVisitInfo().getVisitProportion(pid, nid);
			if (maxVal < val) {
				maxVal = val;
				highestNid = nid;
			}
		}
		
		if (highestNid == null)
			throw new RuntimeException();
		
		return highestNid;
	}

	public AbstractDistanceMatrix getDistanceMatrix() {
		return distanceMatrix.clone();
	}

	public HomecareAssignmentState getPreviousMonthAssignment() {
		return this.getEntityInfo().getVisitInfo().getCurrentAssignment();
	}
}
