package model_info;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;

import experiment.UnexpectedChangeParameter;
import model_entity.ModelEntity;
import model_entity.Visit;
import model_state.HomecareAssignmentState;
import non_model_entity.DirectedPair;
import support.SupportFunction;

public class VisitInfo {
	private final ArrayList<VisitDayInfo> dailyVisits;
	private final HomecareAssignmentState currentAssignment;
	
	protected VisitInfo(VisitInfo info) {
		if (info == null)
			throw new NullPointerException();
		
		this.dailyVisits = new ArrayList<>(info.dailyVisits);
		this.currentAssignment = info.currentAssignment.clone();
	}
	
	public VisitInfo(String visitDaysDirectoryPath) {
		if (visitDaysDirectoryPath == null)
			throw new NullPointerException();
		
		String [] days  = SupportFunction.getFilesName(visitDaysDirectoryPath, "json");
		dailyVisits = new ArrayList<>();
		
		for (int i = 1 ; i <= days.length ; ++i) {
			String fName = SupportFunction.getPrefixMatch("day-" + i, days);
			JSONArray array = SupportFunction.parseJsonArrayFromFile(visitDaysDirectoryPath + "/" + fName);
			VisitDayInfo dayInfo = new VisitDayInfo(array);
			dailyVisits.add(dayInfo);
		}
		
		checkDailyVisitData(dailyVisits);
		
		// The assign here, not above the for loop
		currentAssignment = computeCurrentAssignment();
	}
	
	private static void checkDailyVisitData(ArrayList<VisitDayInfo> dailyVisits) {
		ArrayList<String> vids = new ArrayList<>();
		for (VisitDayInfo day : dailyVisits) {
			for (Visit visit : day.getAllVisits()) {
				if (vids.contains(visit.getId()))
					throw new RuntimeException("Duplicated vid : " + visit.getId());
				vids.add(visit.getId());
			}
		}
	}

	public VisitInfo(VisitInfo visitInfo, ArrayList<ArrayList<Visit>> newVisitsDayList, ArrayList<String> removedPatientIds) {
		if (visitInfo == null)
			throw new NullPointerException();
		
		this.dailyVisits = getChangedDailyVisits(visitInfo.dailyVisits, newVisitsDayList, removedPatientIds);
		
		// The assign here, not above the for loop
		currentAssignment = computeCurrentAssignment();
	}

	public VisitInfo(ArrayList<VisitDayInfo> dayInfos) {
		if (dayInfos == null)
			throw new NullPointerException();
		this.dailyVisits = new ArrayList<>(dayInfos);
		
		// The assign here, not above the for loop
		currentAssignment = computeCurrentAssignment();
	}

	public VisitInfo(VisitInfo visitInfo, HomecareAssignmentState assignment) {
		if (visitInfo == null)
			throw new NullPointerException();
		if (assignment == null)
			throw new NullPointerException();
		
		ArrayList<Visit> visits = new ArrayList<>();
		for (String pid : visitInfo.getPatientIds()) {
			String nid = assignment.getAssignedNurseId(pid);
			String oldNid = visitInfo.getHighestVisitingProportionNurseId(pid);
			int totalVisits = (int) visitInfo.getTotalVisitsToPatient(pid);
			while (totalVisits-- > 0) {
				visits.add(new Visit(pid, nid, 
									 visitInfo.getVisits(pid).get(0).getVisitType(), 
									 visitInfo.getAverageVisitServiceTime(pid, oldNid)));
			}
		}
		
		Collections.shuffle(visits);
		
		int n = visitInfo.getTotalDataDays();
		ArrayList<ArrayList<Visit>> visitSlots = new ArrayList<>();
		for (int i = 0 ; i < n ; ++i)
			visitSlots.add(new ArrayList<Visit>());
		
		for (int i = 0 ; i < visits.size() ; ++ i) {
			visitSlots.get(i % n).add(visits.get(i));
		}
		
		this.dailyVisits = new ArrayList<>();
		for (ArrayList<Visit> vs : visitSlots) {
			this.dailyVisits.add(new VisitDayInfo(vs));
		}
		
		// The assign here, not above the for loop
		currentAssignment = computeCurrentAssignment();
	}
	
	private Map<String, ArrayList<Visit>> patientToVisitsMap = new HashMap<>();
	public ArrayList<Visit> getVisits(String pid) {
		if (pid == null)
			throw new NullPointerException();
		
		if (!patientToVisitsMap.containsKey(pid)) {
			ArrayList<Visit> visits = new ArrayList<>();
			for (VisitDayInfo day : this.dailyVisits) {
				visits.addAll(day.getVisits(pid));
			}
			this.patientToVisitsMap.put(pid, visits);
		}
		return patientToVisitsMap.get(pid);
	}

	private static ArrayList<VisitDayInfo> getChangedDailyVisits(ArrayList<VisitDayInfo> dailyVisits,
																 ArrayList<ArrayList<Visit>> newVisitsDayList,
																 ArrayList<String> removedPatientIds) {
		if (dailyVisits == null)
			throw new NullPointerException();
		if (newVisitsDayList == null)
			throw new NullPointerException();
		if (removedPatientIds == null)
			throw new NullPointerException();
		if (dailyVisits.size() != newVisitsDayList.size())
			throw new RuntimeException("Inconsistent size: " + dailyVisits.size() + " - " + newVisitsDayList.size());
		
		ArrayList<VisitDayInfo> newDailyInfos = new ArrayList<>();
		for (int i = 0 ; i < dailyVisits.size() ; ++i) {
			newDailyInfos.add(new VisitDayInfo(dailyVisits.get(i), newVisitsDayList.get(i), removedPatientIds));
		}
		
		return newDailyInfos;
	}
	
	private Map<String, Set<String>> nursesVisitedMap = new HashMap<>();
	public ArrayList<String> getNursesVisited(String pid) {
		if (pid == null)
			throw new NullPointerException();
		
		if (!nursesVisitedMap.containsKey(pid)) {
			Set<String> nids = new HashSet<>();
			for (VisitDayInfo day : dailyVisits) {
				nids.addAll(day.getNursesVisited(pid));
			}
			if (nids.size() < 1)
				throw new RuntimeException("Not exisitng pid = " + pid);
			
			nursesVisitedMap.put(pid, nids);
		}
		return new ArrayList<>(nursesVisitedMap.get(pid));
	}
		
	public int getTotalDataDays() {
		return dailyVisits.size();
	}
	
	public double getVisitFrequency(String pid) {
		if (pid == null)
			throw new NullPointerException();
		
		return getTotalVisitsToPatient(pid) / (double) getTotalDataDays(); 
	}
	
	public double getVisitProportion(String pid, String nid) {
		return getTotalVisitsToPatientByNurse(pid, nid) / getTotalVisitsToPatient(pid);
	}
	
	// We do not copy these Map thru copy constructor
	private Map<DirectedPair<String, String>, Double> totalVisitsToPatientByNurseMap = new HashMap<>();
	public double getTotalVisitsToPatientByNurse(String pid, String nid) {
		if (nid == null)
			throw new NullPointerException();
		if (pid == null)
			throw new NullPointerException();
		
		DirectedPair<String, String> key = new DirectedPair<>(pid, nid);
		if (!totalVisitsToPatientByNurseMap.containsKey(key)) {
			double total = 0;
			for (VisitDayInfo day : dailyVisits)
				total += day.getTotalVisitsToPatientByNurse(pid, nid);
			totalVisitsToPatientByNurseMap.put(key, total);
		}
		return totalVisitsToPatientByNurseMap.get(key);
	}
	
	// We do not copy these Map thru copy constructor
	private Map<String, Double> totalVisitsToPatientMap = new HashMap<>();
	public double getTotalVisitsToPatient(String pid) {
		if (pid == null)
			throw new NullPointerException();
		if (!totalVisitsToPatientMap.containsKey(pid)) {
			double total = 0;
			for (VisitDayInfo day : dailyVisits)
				total += day.getTotalVisitsToPatient(pid);
			totalVisitsToPatientMap.put(pid, total);
		}
		return totalVisitsToPatientMap.get(pid);
	}
	
	public boolean isConsistent(ArrayList<String> pids, ArrayList<String> nids) {
		if (pids == null)
			throw new NullPointerException();
		if (nids == null)
			throw new NullPointerException();
		if (pids.size() == 0)
			throw new RuntimeException();
		if (nids.size() == 0)
			throw new RuntimeException();
		
		ArrayList<String> currentNids = getNurseIds();
		if (!nids.containsAll(currentNids))
			return false;
		if (!currentNids.containsAll(nids))
			return false;
		
		return true;
	}
	
	// We do not copy these Map thru copy constructor
	private boolean isComputedPatientIdsResult = false;
	private ArrayList<String> patientIdsResult = null;
	public ArrayList<String> getPatientIds() {
		if (!isComputedPatientIdsResult) {
			Set<String> pids = new HashSet<>();
			for (VisitDayInfo day : dailyVisits) {
				pids.addAll(day.getPatientIds());
			}
			patientIdsResult = new ArrayList<>(pids);
			isComputedPatientIdsResult = true;
		}
		if (patientIdsResult == null)
			throw new RuntimeException();
		
 		return new ArrayList<>(patientIdsResult);
	}
	
	// We do not copy these Map thru copy constructor
	private boolean isComputedNurseIdsResult = false;
	private ArrayList<String> nurseIdsResult = null;
	public ArrayList<String> getNurseIds() {
		if (!isComputedNurseIdsResult) {
			Set<String> nids = new HashSet<>();
			for (VisitDayInfo day : dailyVisits) {
				nids.addAll(day.getNurseIds());
			}
			nurseIdsResult = new ArrayList<>(nids);
			isComputedNurseIdsResult = true;
		}
		if (nurseIdsResult == null)
			throw new RuntimeException();
		
 		return new ArrayList<>(nurseIdsResult);
	}
	
	// We do not copy these Map thru copy constructor
	private Map<DirectedPair<String, String>, Double> averageVisitServiceTimeMap = new HashMap<>();
	public double getAverageVisitServiceTime(String pid, String nid) {
		if (pid == null)
			throw new NullPointerException();
		if (nid == null)
			throw new NullPointerException();
		
		DirectedPair<String, String> key = new DirectedPair<>(pid, nid);
		if (!averageVisitServiceTimeMap.containsKey(key)) {
			ArrayList<Visit> visits = new ArrayList<>();
			for (VisitDayInfo day : dailyVisits) {
				visits.addAll(day.getVisits(pid, nid));
			}
			
			if (visits.size() == 0)
				throw new RuntimeException("No visit between " + pid + " and " + nid);
			
			double total = 0;
			for (Visit visit : visits) {
				total += visit.getServiceTime();
			}
			averageVisitServiceTimeMap.put(key, total / (double) visits.size());
		}
		return averageVisitServiceTimeMap.get(key);
	}
	
	public void generateJsonsInfoDirectory(String destDirectoryPath) {
		if (destDirectoryPath == null)
			throw new NullPointerException();
		
		SupportFunction.createDirectory(destDirectoryPath);
		for (VisitDayInfo day : dailyVisits) {
			ArrayList<Visit> visits = day.getAllVisits();
			JSONArray array = ModelEntity.generateModelEntitiesJsonArray(new ArrayList<>(visits));
			try {
				SupportFunction.saveFile(array.toString(3), destDirectoryPath + "/day-" 
															+ (dailyVisits.indexOf(day) + 1) 
															+ "-" 
															+ array.length()
															+ ".json");
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
	}
	
	public HomecareAssignmentState getCurrentAssignment() {
		return currentAssignment;
	}

	private HomecareAssignmentState computeCurrentAssignment() {
		ArrayList<String> pids = getPatientIds();
		HomecareAssignmentState state = new HomecareAssignmentState(pids, getNurseIds());
		for (String pid : pids) {
			String nid = getHighestVisitingProportionNurseId(pid);
			state = state.assignPatientToNurse(pid, nid);
		}
		return state;
	}
	
	// We do not copy these Map thru copy constructor
	private Map<String, Set<String>> visitedPatientIdsMap = new HashMap<>();
	public ArrayList<String> getVisitedPatientIds(String nid) {
		if (nid == null)
			throw new NullPointerException();
		
		if (!visitedPatientIdsMap.containsKey(nid)) {
			Set<String> pids = new HashSet<>();
			for (VisitDayInfo day : dailyVisits) {
				pids.addAll(day.getVisitedPatientIds(nid));
			}
			visitedPatientIdsMap.put(nid, pids);
		}
		return new ArrayList<>(visitedPatientIdsMap.get(nid));
	}

	// We do not copy these Map thru copy constructor
	private Map<String, String> highestVisitingProportionNurseIdMap = new HashMap<>();
	public String getHighestVisitingProportionNurseId(String pid) {
		if (pid == null)
			throw new NullPointerException();

		if (!highestVisitingProportionNurseIdMap.containsKey(pid)) {
			double highest = -1;
			String maxNid = null;
			for (String nid : this.getNurseIds()) {
				double proportion = this.getVisitProportion(pid, nid);
				if (proportion > highest) {
					highest = proportion;
					maxNid = nid;
				}
			}
			if (maxNid == null)
				throw new RuntimeException();
			highestVisitingProportionNurseIdMap.put(pid, maxNid);
		}
		return highestVisitingProportionNurseIdMap.get(pid);
	}

	public VisitInfo generateInfoWithUnexpectedVisits(UnexpectedChangeParameter parameter, Map<String, ArrayList<String>> closestNursesMap) {
		if (parameter == null)
			throw new NullPointerException();
		if (closestNursesMap == null)
			throw new NullPointerException();
		if (closestNursesMap.size() == 0)
			throw new RuntimeException();
		
		ArrayList<VisitDayInfo> newDailyInfo = new ArrayList<>();
		for (VisitDayInfo day : this.dailyVisits) {
			newDailyInfo.add(day.generateInfoWithUnexpectedVisits(parameter, closestNursesMap));
		}
		return new VisitInfo(newDailyInfo);
	}

	private Map<Integer, Double> visitingFrequencyProportionMap = new HashMap<>();
	public double getVisitingFrequencyProportion(int visitFrequency) {
		if (visitFrequency < 1)
			throw new RuntimeException("Invalid visit frequency : " + visitFrequency);
		
		if (!visitingFrequencyProportionMap.containsKey(visitFrequency)) {
			int totalPatientsWithFreq = 0;
			for (VisitDayInfo day : dailyVisits) {
				totalPatientsWithFreq += day.getTotalPatientsWithFrequency(visitFrequency);
			}
			double proportion = (double) totalPatientsWithFreq / (double) this.getPatientIds().size(); 
			visitingFrequencyProportionMap.put(visitFrequency, proportion);
		}
		return visitingFrequencyProportionMap.get(visitFrequency);
	}

	private boolean isComputedTotalVisits = false;
	private int totalVisits = 0;
	public int getTotalVisits() {		
		if (!isComputedTotalVisits) {
			for (VisitDayInfo day : dailyVisits) 
				totalVisits += day.getAllVisits().size();
			isComputedTotalVisits = true;
		}
		return totalVisits;
	}
}
