package move_generator;

import java.util.ArrayList;

import clustering.Cluster;
import model.VivianeModel;
import model_entity.Patient;
import model_state.HomecareAssignmentState;
import move.OneMove;
import non_model_entity.DataPoint;
import non_model_entity.DirectedPair;

public class SimpleMoveGenerator extends AbstractMoveGenerator<HomecareAssignmentState, VivianeModel> {
	
	public SimpleMoveGenerator(VivianeModel model) {
		super(model);		
	}

	protected SimpleMoveGenerator(SimpleMoveGenerator moveGenerator) {
		super(moveGenerator);
	}

	@Override
	public SimpleMoveGenerator clone() {
		return new SimpleMoveGenerator(this);
	}

	@Override
	public MovesList<HomecareAssignmentState> generate(HomecareAssignmentState state) {
		if (state == null)
			throw new NullPointerException();
		
		VivianeModel model = this.model;
		if (!model.getModelInfo().isConsistent(state))
			throw new RuntimeException("Inconsistent state");
		
		MovesList<HomecareAssignmentState> movesList = new MovesList<HomecareAssignmentState>();
		
		ArrayList<DirectedPair<String, Double>> workloads = model.getAverageDailyWorkloadForEachNurse(state);
		String highestNurse = getHighestWorkloadNurse(workloads);
		ArrayList<String> nids = model.getNeighborNurses(state, highestNurse);
		ArrayList<String> consideringPids = state.getAssignedPatientIdsList(highestNurse);
		
		for (String nid : nids) {
			if (nid.equals(highestNurse))
				throw new RuntimeException("Inconsistent neighborhood");
			
			Cluster cluster = new Cluster();
			cluster.addDataPoints(getAssignedDataPoints(nid, state));
			String closetPid = model.getModelInfo()
									.getClosetTimeDistancePatientId(cluster.getCentroid().getId(), consideringPids);
			movesList.addMove(new OneMove<HomecareAssignmentState>(nid, closetPid));
		}
		
		return movesList;
	}
	
	protected ArrayList<DataPoint> getAssignedDataPoints(String nid, HomecareAssignmentState state) {
		assert nid != null;
		assert state != null;
		
		ArrayList<DataPoint> dps = new ArrayList<>();
		ArrayList<String> pids = state.getAssignedPatientIdsList(nid);
		for (String pid: pids) {
			Patient p = this.model.getModelInfo().getPatient(pid);
			dps.add(new DataPoint(pid, p.getCoordinate()));
		}
		return dps;
	}
	
	protected String getHighestWorkloadNurse(ArrayList<DirectedPair<String, Double>> workloads) {
		assert workloads != null;
		assert workloads.size() > 0;
		
		String nid = workloads.get(0).getLeft();
		double maxWorkload = workloads.get(0).getRight();
		for (int i = 1 ; i < workloads.size() ; ++i) {
			double workload = workloads.get(i).getRight();
			if (workload > maxWorkload) {
				maxWorkload = workload;
				nid = workloads.get(i).getLeft();
			}
		}
		return nid;
	}	
}
