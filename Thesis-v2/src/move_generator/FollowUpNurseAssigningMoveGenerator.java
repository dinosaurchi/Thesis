package move_generator;

import java.util.ArrayList;

import model.VivianeModel;
import model_state.HomecareAssignmentState;

public class FollowUpNurseAssigningMoveGenerator extends AssigningMoveGenerator<HomecareAssignmentState, VivianeModel> {
	public FollowUpNurseAssigningMoveGenerator(VivianeModel model) {
		super(model);
	}

	public FollowUpNurseAssigningMoveGenerator(FollowUpNurseAssigningMoveGenerator moveGenerator) {
		super(moveGenerator);
	}
	
	@Override
	public FollowUpNurseAssigningMoveGenerator clone() {
		return new FollowUpNurseAssigningMoveGenerator(this);
	}

	@Override
	protected void assigning(HomecareAssignmentState state) {
		if (state == null)
			throw new NullPointerException();
		
		ArrayList<String> pids = model.getModelInfo().getPatientIds();
		for (String pid : pids) {
			if (model.getModelInfo().isFollowUpImportant(pid)) {
				String fnid = model.getModelInfo().getHighestProportionNurse(pid);
				this.assign(fnid, pid);
			}
		}
	}
}
