package move_generator;

import java.util.ArrayList;

import model.VivianeModel;
import model_state.HomecareAssignmentState;
import move.OneMove;
import move.OneSwapMove;
import non_model_entity.DirectedPair;

public class ChiMoveGenerator extends SimpleMoveGenerator {
	
	protected ChiMoveGenerator(ChiMoveGenerator chiMoveGenerator) {
		super(chiMoveGenerator);
	}
	
	public ChiMoveGenerator(VivianeModel model) {
		super(model);
	}
	
	@Override
	public ChiMoveGenerator clone() {
		return new ChiMoveGenerator(this);
	}
	
	@Override
	public MovesList<HomecareAssignmentState> generate(HomecareAssignmentState state) {
		if (state == null)
			throw new NullPointerException();
		
		VivianeModel model = this.model;
		if (!model.getModelInfo().isConsistent(state))
			throw new RuntimeException("Inconsistent state");
		
		ArrayList<DirectedPair<String, Double>> workloads = model.getAverageDailyWorkloadForEachNurse(state);
		String highestNid = getHighestWorkloadNurse(workloads);
		
		return getMovesToNeighbors(state, highestNid);
	}
	
	protected MovesList<HomecareAssignmentState> getMovesToNeighbors(HomecareAssignmentState state, String targetNid) {
		if (state == null)
			throw new NullPointerException();
		if (targetNid == null)
			throw new NullPointerException();
		
		MovesList<HomecareAssignmentState> movesList = new MovesList<>();
		
		ArrayList<String> currentBorderPids = model.getBorderPatients(state, targetNid);
		for (String nid : model.getNeighborNurses(state, targetNid)) {
			if (nid.equals(targetNid))
				throw new RuntimeException("Inconsistent neighborhood");
			
			// Phase 1
			
			ArrayList<String> neighborBorderPids = model.getBorderPatients(state, nid);
			String closestPid = getClosestPid(currentBorderPids, neighborBorderPids);
			movesList.addMove(new OneMove<HomecareAssignmentState>(nid, closestPid));
			
			// Phase 2
			
			String swapBorderPid = model.getModelInfo().getClosetTimeDistancePatientId(closestPid, neighborBorderPids);
			OneSwapMove<HomecareAssignmentState> swapMove = 
					new OneSwapMove<>(new OneMove<HomecareAssignmentState>(targetNid, swapBorderPid), 
									  new OneMove<HomecareAssignmentState>(nid, closestPid));
			movesList.addMove(swapMove);
		}
		
		return movesList;
	}
	
	protected String getClosestPid(ArrayList<String> sourcePids, ArrayList<String> destPids) {
		if (sourcePids == null)
			throw new NullPointerException();
		if (destPids == null)
			throw new NullPointerException();
		
		double minDistance = Double.MAX_VALUE;
		String closestPid = null;
		for (String pid : sourcePids) {
			double distance = getDistance(pid, destPids);
			if (distance < minDistance) {
				minDistance = distance;
				closestPid = pid;
			}
		}
		return closestPid;
	}
	
	protected double getDistance(String pid, ArrayList<String> consideringPids) {
		if (pid == null)
			throw new NullPointerException();
		if (consideringPids == null)
			throw new NullPointerException();
		
		double minDistance = Double.MAX_VALUE;
		for (String cPid : consideringPids) {
			double distance = model.getModelInfo().getTimeDistance(cPid, pid);
			if (distance < minDistance)
				minDistance = distance;
		}
		return minDistance;
	}
}
