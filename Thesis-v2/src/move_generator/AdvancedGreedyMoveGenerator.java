package move_generator;

import model.VivianeModel;
import model_state.HomecareAssignmentState;

public class AdvancedGreedyMoveGenerator extends AdvancedChiMoveGenerator {
	public AdvancedGreedyMoveGenerator(VivianeModel model) {
		super(model);
	}

	@Override
	public MovesList<HomecareAssignmentState> generate(HomecareAssignmentState state) {
		if (state == null)
			throw new NullPointerException();
		
		VivianeModel model = this.model;
		if (!model.getModelInfo().isConsistent(state))
			throw new RuntimeException("Inconsistent state");
		
		MovesList<HomecareAssignmentState> moves = new MovesList<>();
		for (String nid : state.getNurseIds()) {
			moves.addMoves(getMovesToNeighbors(state, nid));
		}
		
		return moves;
	}
}
