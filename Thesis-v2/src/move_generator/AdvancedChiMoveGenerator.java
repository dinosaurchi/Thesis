package move_generator;

import java.util.ArrayList;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.cplex.IloCplex;
import model.VivianeModel;
import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import move.Move;
import move.MultipleMove;
import move.OneMove;
import move.UnitMove;
import support.SupportFunction;

public class AdvancedChiMoveGenerator extends ChiMoveGenerator{	
	private static final int CONSECUTIVE_MOVES_DEPTH = 1;

	public AdvancedChiMoveGenerator(VivianeModel model) {
		super(model);
	}
	
	@Override
	protected MovesList<HomecareAssignmentState> getMovesToNeighbors(HomecareAssignmentState state, String targetNid) {
		return getMovesToNeighbors(state, targetNid, new ArrayList<String>());
	}
	
	protected MovesList<HomecareAssignmentState> getMovesToNeighbors(HomecareAssignmentState state, String targetNid, ArrayList<String> ignoredNids) {
		if (state == null)
			throw new NullPointerException();
		if (targetNid == null)
			throw new NullPointerException();
		if (ignoredNids == null)
			throw new NullPointerException();
		if (ignoredNids.contains(targetNid))
			throw new RuntimeException("Target Nid cannot be ignored");
		
		ignoredNids.add(targetNid);
		
		MovesList<HomecareAssignmentState> movesList = new MovesList<>();
		
		ArrayList<String> currentBorderPids = model.getBorderPatients(state, targetNid);
		for (String neighboringNid : model.getNeighborNurses(state, targetNid)) {
			if (ignoredNids.contains(neighboringNid))
				continue;
			
			if (neighboringNid.equals(targetNid))
				throw new RuntimeException("Inconsistent neighborhood");
			
			ArrayList<String> neighborBorderPids = model.getBorderPatients(state, neighboringNid);
			ArrayList<String> nClosestPids = getNClosestPids(currentBorderPids, neighborBorderPids, 3);
			nClosestPids = getHasNeighborhoodPids(nClosestPids, neighborBorderPids);
			
			// We do not want to removed all of the patient of a nurse.
			int n = Math.min(3, nClosestPids.size() - 1);
			for (int r = 1 ; r <= n ; ++r) {
				for (ArrayList<String> pids : SupportFunction.getCombinations(nClosestPids, r)) {
					// Phase 1
					MultipleMove<HomecareAssignmentState> multipleSingleOneMoves = getMultipleSingleOneMoves(neighboringNid, pids);
					movesList.addMove(multipleSingleOneMoves);
					movesList.addMoves(getDoubleMoves(multipleSingleOneMoves, state, neighboringNid, ignoredNids));
					
					// Phase 2
					MultipleMove<HomecareAssignmentState> multipleSingleSwapMoves = getMultipleSingleSwapMoves(targetNid, neighboringNid, pids, state);
					movesList.addMove(multipleSingleSwapMoves);
					movesList.addMoves(getDoubleMoves(multipleSingleSwapMoves, state, neighboringNid, ignoredNids));
				}							
			}
		}
		
		ignoredNids.remove(targetNid);
		return movesList;
	}
	
	protected MovesList<HomecareAssignmentState> getDoubleMoves(MultipleMove<HomecareAssignmentState> previousMove,
															  HomecareAssignmentState currentState, String targetNid, 
															  ArrayList<String> ignoredNids) {
		// Avoid too long recursion
		if (ignoredNids.size() > CONSECUTIVE_MOVES_DEPTH)
			return new MovesList<>();
		
		MovesList<HomecareAssignmentState> movesList = new MovesList<>();
		HomecareAssignmentState newState = previousMove.execute(currentState);
		MovesList<HomecareAssignmentState> nextMoveLists = getMovesToNeighbors(newState, targetNid, ignoredNids);
		
		ArrayList<UnitMove<HomecareAssignmentState>> umoves = previousMove.toUnitMoves();
		for (Move<HomecareAssignmentState> nextMove : nextMoveLists.getMoves()) {
			movesList.addMove(new MultipleMove<>(reduceTriangleMoves(umoves, nextMove.toUnitMoves())));
		}
		return movesList;
	}

	protected ArrayList<UnitMove<HomecareAssignmentState>> reduceTriangleMoves(ArrayList<UnitMove<HomecareAssignmentState>> oldMoves,
																			 ArrayList<UnitMove<HomecareAssignmentState>> newMoves) {
		// If triangle move exists, we only keep the last destination (nurse)
		
		oldMoves = new ArrayList<>(oldMoves);
		ArrayList<UnitMove<HomecareAssignmentState>> result = new ArrayList<>();
		for (UnitMove<HomecareAssignmentState> move : newMoves) {
			UnitMove<HomecareAssignmentState> samePidMove = getSamePidMove(move, oldMoves);
			if (samePidMove != null)
				oldMoves.remove(samePidMove);
		}
		result.addAll(oldMoves);
		result.addAll(newMoves);
		return result;
	}

	protected UnitMove<HomecareAssignmentState> getSamePidMove(UnitMove<HomecareAssignmentState> currentMove,
															 ArrayList<UnitMove<HomecareAssignmentState>> moves) {
		// Because for each proper move, there is only one move from an PID to an NID, thus we return immediately if we find one
		String pid = ((OneMove<HomecareAssignmentState>) currentMove).getConsideringDataPointId();
		for (UnitMove<HomecareAssignmentState> move : moves) {
			if (pid.equals(((OneMove<HomecareAssignmentState>) move).getConsideringDataPointId()))
				return move;
		}
		return null;
	}

	protected MultipleMove<HomecareAssignmentState> getMultipleSingleSwapMoves(String currentNid, String swapNid,
															   ArrayList<String> consideringSwapPids, 
															   HomecareAssignmentState currentState) {
		// Each swap move has 2 phases : giving away some patients and getting some patients
		
		MultipleMove<HomecareAssignmentState> givingAwayMoves = getMultipleSingleOneMoves(swapNid, consideringSwapPids);
		MultipleMove<HomecareAssignmentState> gettingBackMoves = getGettingBackMoves(currentNid, swapNid,
																					 consideringSwapPids,
																					 currentState);
		
		ArrayList<UnitMove<HomecareAssignmentState>> allMoves = givingAwayMoves.toUnitMoves();
		allMoves.addAll(gettingBackMoves.toUnitMoves());
		return new MultipleMove<>(allMoves);
	}

	protected MultipleMove<HomecareAssignmentState> getGettingBackMoves(String targetNid, String currentNid,
																	  ArrayList<String> consideredToBeMovedToCurrentNursePids, 
																	  HomecareAssignmentState currentState) {
		// Now, we get the state when all the considering patients from the target nurse (old nurse) to the intent nurse
		HomecareAssignmentState newState = currentState.assignPatientsToNurse(consideredToBeMovedToCurrentNursePids, currentNid);
		
		// Get all the patient of the target nurse after moving all the considered to be moved patients
		ArrayList<String> targetNurseNewAssignedPids = newState.getAssignedPatientIdsList(targetNid);
		
		// We do not consider to patients who have just been moved from the old nurse (target nurse), thus we use the current state
		// .... which does not involve them in the assignment of current nurse (not moved yet)
		ArrayList<String> currentNurseCurrentAssignedPids = currentState.getAssignedPatientIdsList(currentNid);
		
		// We only keep the patients who have neighborhood to at least one of the patients of the target nurse, of course
		// ... in the new assignment because we are going to find a way to move some patients of current nurse to the 
		// ... target nurse, thus we do not want to get back the patients who have just been moved to the current nurse
		// ... from the target nurse
		ArrayList<String> consideringPids = getHasNeighborhoodPids(currentNurseCurrentAssignedPids, targetNurseNewAssignedPids);
		
		// Preparing data
		double[] oldAverageTravelingTime = getPatientTravelingTime(consideringPids, currentNurseCurrentAssignedPids);
		double[] newAverageTravelingTime = getPatientTravelingTime(consideringPids, newState.getAssignedPatientIdsList(currentNid));
		double[] differenceOldNew = SupportFunction.getDoubleDifference(oldAverageTravelingTime, newAverageTravelingTime);
		double[] dailyAverageTreatmentTime = getDailyAverageTreatmentTime(consideringPids);
		double totalTreatmentTimeOfMovedPids = SupportFunction.sum(getDailyAverageTreatmentTime(consideredToBeMovedToCurrentNursePids));
		if (newAverageTravelingTime.length != consideringPids.size())
			throw new RuntimeException("Invalid sizes : " + newAverageTravelingTime.length + " and " + consideringPids.size());
		if (newAverageTravelingTime.length != dailyAverageTreatmentTime.length)
			throw new RuntimeException("Invalid sizes : " + newAverageTravelingTime.length + " and " + dailyAverageTreatmentTime.length);
		
		double [] selectedResult = getKnapsackResults(differenceOldNew, dailyAverageTreatmentTime, totalTreatmentTimeOfMovedPids);
		if (selectedResult.length != consideringPids.size())
			throw new RuntimeException("Invalid sizes : " + selectedResult.length + " and " + consideringPids.size());
		
		ArrayList<UnitMove<HomecareAssignmentState>> moves = new ArrayList<>();
		for (int i = 0 ; i < selectedResult.length ; ++i) {
			Double isChosen = selectedResult[i];
			if (isChosen.equals(1.0)) {
				moves.add(new OneMove<HomecareAssignmentState>(targetNid, consideringPids.get(i)));
			}
		}
		return new MultipleMove<>(moves);
	}

	protected double[] getDailyAverageTreatmentTime(ArrayList<String> consideringPids) {
		double[] result = new double[consideringPids.size()];
		VivianeModelInfo modelInfo = model.getModelInfo();
		for (int i = 0 ; i < result.length ; ++i)
			result[i] = modelInfo.getAverageDailyTreatmentTime(consideringPids.get(i));
		return result;
	}

	protected double[] getPatientTravelingTime(ArrayList<String> candidatePids, ArrayList<String> consideringPids) {
		double[] result = new double[candidatePids.size()];
		VivianeModelInfo modelInfo = model.getModelInfo();
		for (int i = 0 ; i < result.length ; ++i)
			result[i] = modelInfo.getAverageDailyTravelingTime(candidatePids.get(i), consideringPids);
		return result;
	}

	protected double [] getKnapsackResults(double[] differenceOldNew, double[] dailyAverageTreatmentTime, double totalTreatmentTimeOfMovedPids) {
		if (differenceOldNew.length != dailyAverageTreatmentTime.length)
			throw new RuntimeException("Invalid sizes : " + differenceOldNew.length + " and " + dailyAverageTreatmentTime.length);
		if (Double.isInfinite(totalTreatmentTimeOfMovedPids) || Double.isNaN(totalTreatmentTimeOfMovedPids))
			throw new RuntimeException("Invalid value : " + totalTreatmentTimeOfMovedPids);

		double minDiff = SupportFunction.min(differenceOldNew);
		int totalPids = differenceOldNew.length;

		try {
			// Solve the knapsack problem
			IloCplex cplex = new IloCplex();

			// We solve 0-1 goal programming
			IloIntVar[] xs = cplex.boolVarArray(totalPids);

			// Defining Objective function
			IloLinearNumExpr objective = cplex.linearNumExpr();
			IloNumExpr[] treatmentTime = new IloNumExpr[totalPids];
			for (int i = 0 ; i < totalPids ; ++i) {
				// Add 1 to avoid getting 0, which will lead to 0 with any value multiplied with it
				objective.addTerm(differenceOldNew[i] - minDiff + 1, xs[i]);

				treatmentTime[i] = cplex.prod(dailyAverageTreatmentTime[i], xs[i]);
			}

			// Adding objective
			cplex.addMaximize(objective);

			// Adding constraints
			cplex.addLe(cplex.sum(treatmentTime), totalTreatmentTimeOfMovedPids);

			// Solve
			cplex.setOut(null);
			if (!cplex.solve())
				throw new RuntimeException("Cannot solve linear programming problem");
			double [] result = cplex.getValues(xs);
			cplex.end();	

			return result;
		} catch (IloException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	protected MultipleMove<HomecareAssignmentState> getMultipleSingleOneMoves(String nid, ArrayList<String> pids) {
		ArrayList<UnitMove<HomecareAssignmentState>> moves = new ArrayList<>();
		for (String pid : pids) {
			moves.add(new OneMove<HomecareAssignmentState>(nid, pid));
		}
		return new MultipleMove<>(moves);
	}

	protected ArrayList<String> getHasNeighborhoodPids(ArrayList<String> candidatePids, ArrayList<String> consideringPids) {
		ArrayList<String> result = new ArrayList<>();
		for (String pid : candidatePids) {
			if (hasNeighborhood(pid, consideringPids))
				result.add(pid);
		}
		return result;
	}

	protected boolean hasNeighborhood(String nPid, ArrayList<String> consideringPids) {
		VivianeModelInfo modelInfo = this.model.getModelInfo();
		for (String pid : consideringPids)
			if (modelInfo.isNeighborDataPoint(nPid, pid))
				return true;
		return false;
	}

	protected ArrayList<String> getNClosestPids(ArrayList<String> sourcePids, ArrayList<String> destPids, int n) {
		if (sourcePids == null)
			throw new NullPointerException();
		if (destPids == null)
			throw new NullPointerException();
		if (n < 1)
			throw new RuntimeException("Invalid n : " + n);
		
		sourcePids = new ArrayList<>(sourcePids);
		ArrayList<String> nClosestPids = new ArrayList<>();
		while (n-- > 0 && sourcePids.size() > 0) {
			String pid = getClosestPid(sourcePids, destPids);
			sourcePids.remove(pid);
			nClosestPids.add(pid);
		}
		return nClosestPids;
	}
}
