package move_generator;

import java.util.ArrayList;
import java.util.Random;

import model.VivianeModel;
import model_state.HomecareAssignmentState;
import move.MultipleMove;
import support.SupportFunction;

public class AdvancedChiRandomSingleMoveGenerator extends AdvancedChiMoveGenerator {

	public AdvancedChiRandomSingleMoveGenerator(VivianeModel model) {
		super(model);
	}
	
	@Override
	protected MovesList<HomecareAssignmentState> getMovesToNeighbors(HomecareAssignmentState state, String targetNid, ArrayList<String> ignoredNids) {
		if (state == null)
			throw new NullPointerException();
		if (targetNid == null)
			throw new NullPointerException();
		if (ignoredNids == null)
			throw new NullPointerException();
		if (ignoredNids.contains(targetNid))
			throw new RuntimeException("Target Nid cannot be ignored");
		
		ignoredNids.add(targetNid);
		
		Random rand = new Random();
		
		MovesList<HomecareAssignmentState> movesList = new MovesList<>();
		ArrayList<String> currentBorderPids = model.getBorderPatients(state, targetNid);
		
		ArrayList<String> neighboringNurses = model.getNeighborNurses(state, targetNid);
		neighboringNurses.removeAll(ignoredNids);
		
		if (neighboringNurses.size() < 1) {
			ignoredNids.remove(targetNid);
			return movesList;
		}
		
		String neighboringNid = null;
		ArrayList<String> nClosestPids = null;
		int n = 0;
		while (!neighboringNurses.isEmpty()) {
			neighboringNid = neighboringNurses.remove(rand.nextInt(neighboringNurses.size()));
			
			if (neighboringNid.equals(targetNid))
				throw new RuntimeException("Inconsistent neighborhood");
	
			ArrayList<String> neighborBorderPids = model.getBorderPatients(state, neighboringNid);
			nClosestPids = getNClosestPids(currentBorderPids, neighborBorderPids, 3);
			nClosestPids = getHasNeighborhoodPids(nClosestPids, neighborBorderPids);
	
			// We do not want to removed all of the patient of a nurse.
			n = Math.min(3, nClosestPids.size() - 1);
			if (n < 1)
				continue;
			else 
				break;
		}
		
		if (n < 1) {
			ignoredNids.remove(targetNid);
			return movesList;
		}
		
		int r = rand.nextInt(n) + 1;
		
		ArrayList<ArrayList<String>> combinations = SupportFunction.getCombinations(nClosestPids, r);
		if (combinations.size() < 1) {
			ignoredNids.remove(targetNid);
			return movesList;
		}
		
		ArrayList<String> pids = combinations.get(rand.nextInt(combinations.size()));
		
		ArrayList<Integer> idxs = new ArrayList<>();
		idxs.add(0);
		idxs.add(1);
		while (!idxs.isEmpty()) {
			int choice = idxs.remove(rand.nextInt(idxs.size()));
			switch (choice) {
			case 0:
				// Phase 1
				MultipleMove<HomecareAssignmentState> multipleSingleOneMoves = getMultipleSingleOneMoves(neighboringNid, pids);
				movesList.addMove(multipleSingleOneMoves);
				movesList.addMoves(getDoubleMoves(multipleSingleOneMoves, state, neighboringNid, ignoredNids));
				break;
			case 1:
				// Phase 2
				MultipleMove<HomecareAssignmentState> multipleSingleSwapMoves = getMultipleSingleSwapMoves(targetNid, neighboringNid, pids, state);
				movesList.addMove(multipleSingleSwapMoves);
				movesList.addMoves(getDoubleMoves(multipleSingleSwapMoves, state, neighboringNid, ignoredNids));
				break;
			default:
				break;
			}
		}
		
		ignoredNids.remove(targetNid);
		return movesList;
	}
}
