package nurse_districting_problem;
import java.util.Map;

import context.Problem;
import experiment.NurseDistrictingChange;
import experiment.StateChange;
import model.VivianeModel;
import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import solution_reporter.VivianeModelSolutionReporter;

public class NurseDistrictingProblem extends Problem <HomecareAssignmentState, VivianeModel> {	
	
	// PUBLIC Methods
	
	public NurseDistrictingProblem(VivianeModel model) {
		super(model);
	}
	
	protected NurseDistrictingProblem(NurseDistrictingProblem problem) {
		super(problem);
	}
	
	@Override
	protected VivianeModelSolutionReporter getReporter(VivianeModel model) {
		return new VivianeModelSolutionReporter(model);
	}

	@Override
	public VivianeModelSolutionReporter getReporter() {
		return new VivianeModelSolutionReporter(model);
	}

	public NurseDistrictingProblem changeProblem(StateChange<HomecareAssignmentState, VivianeModel> change) {
		if (!(change instanceof NurseDistrictingChange))
			throw new RuntimeException("Invalid class : " + change.getClass());
		NurseDistrictingChange castChange = (NurseDistrictingChange) change;
		VivianeModelInfo info = new VivianeModelInfo(this.model.getModelInfo(), castChange, this.getBestState());
		return new NurseDistrictingProblem(new VivianeModel(info));		
	}

	public void setModelWeights(Map<String, Double> weights) {
		this.model.setWeights(weights);
	}

	public NurseDistrictingProblem refreshState() {
		return (NurseDistrictingProblem) super.refreshState();
	}

	@Override
	public NurseDistrictingProblem clone() {
		return new NurseDistrictingProblem(this);
	}
	
	// ----------------------------------------------------------------------------------------------------
	// PRIVATE Methods
}
