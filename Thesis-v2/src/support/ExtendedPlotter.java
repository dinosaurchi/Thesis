package support;

import javax.swing.JFrame;

import model.VivianeModel;
import model_state.HomecareAssignmentState;
import plotting.Plotter;

public class ExtendedPlotter extends Plotter {
	public static JFrame plotBarCharWorkload(VivianeModel model, HomecareAssignmentState state, boolean isDisplay) {
		if (model == null)
			throw new NullPointerException();
		if (state == null)
			throw new NullPointerException();
		
		final int FRAME_WIDTH = 300;
		final int FRAME_HEIGHT = 400;
		
		BarChartComponent component = new BarChartComponent(model, state);
		return Plotter.plotWithComponent(component, 
				"Workload distribution - " + model.getClass(), 
				FRAME_WIDTH, FRAME_HEIGHT,
				isDisplay);
	}
}
