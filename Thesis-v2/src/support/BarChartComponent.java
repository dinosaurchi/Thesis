package support;

import javax.swing.JPanel;

import model.VivianeModel;
import model_state.HomecareAssignmentState;
import non_model_entity.DirectedPair;
import plotting.BarChart;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class BarChartComponent extends JPanel {
	private static final long serialVersionUID = 1L;
	private final VivianeModel model;
	private final HomecareAssignmentState state;
	
	public BarChartComponent(VivianeModel model, HomecareAssignmentState state) {
		if (model == null)
			throw new NullPointerException();
		if (state == null)
			throw new NullPointerException();
		
		this.model = model.clone();
		this.state = state;
		this.setSize(300, 400);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D) g;
		BarChart c = new BarChart(getWidth(), getHeight());
		
		g2.setBackground(Color.WHITE);
		
		ArrayList<DirectedPair<String, Double>> workloads = model.getAverageDailyWorkloadForEachNurse(state);
		
		if (workloads.size() != state.getClassIds().size())
			throw new RuntimeException("Inconsistency");
		
		for (int i = 0 ; i < workloads.size() ; ++i) {
			c.add(workloads.get(i).getRight());
		}
		
		c.draw(g2);
	}
}