package experiment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class UnexpectedChangeParameter {
	private static final String SUB_PROPORTION = "sub-proportion";
	private static final String CHANGE_PROPORTION = "change-proportion";
	private final double overallPatientsVisitedByAnotherNurseProportion;
	private final Map<Integer, Double> patientsVisitedByAnotherNurseSubProportionMap;
	
	public UnexpectedChangeParameter(JSONObject obj) throws JSONException {
		this (obj.getDouble(CHANGE_PROPORTION), getSubProportionMap(obj.getJSONObject(SUB_PROPORTION)));
	}
	
	private static Map<Integer, Double> getSubProportionMap(JSONObject subProportionJsonObject) throws JSONException {
		if (subProportionJsonObject == null)
			throw new NullPointerException();
		Map<Integer, Double> map = new HashMap<>();
		Iterator<?> keys = subProportionJsonObject.keys();
		while (keys.hasNext()) {
		    String numOfChanges = keys.next().toString();
		    map.put(Integer.parseInt(numOfChanges), subProportionJsonObject.getDouble(numOfChanges));
		}
		return map;
	}

	public UnexpectedChangeParameter(double changeProportion, Map<Integer, Double> subProportionMap) {
		if (Double.isInfinite(changeProportion))
			throw new RuntimeException();
		if (Double.isNaN(changeProportion))
			throw new RuntimeException();
		if (subProportionMap == null)
			throw new NullPointerException();
		if (subProportionMap.keySet().size() < 1)
			throw new RuntimeException("Need at least one sub proportion");
		
		double total = 0;
		for (Integer key : subProportionMap.keySet()) {
			double v = subProportionMap.get(key);
			if (v < 0 || v > 1)
				throw new RuntimeException("Invalid sub proportion [" + subProportionMap.get(key) + "] : " + v);
			total += v;
		}
		if (total > 1)
			throw new RuntimeException("Invalid total proportion : " + total);
		
		this.overallPatientsVisitedByAnotherNurseProportion = changeProportion;
		this.patientsVisitedByAnotherNurseSubProportionMap = new HashMap<>(subProportionMap);
	}

	public double getOverallPatientsVisitedByAnotherNurseProportion() {
		return overallPatientsVisitedByAnotherNurseProportion;
	}

	public double getPatientsVisitedByAnotherNurseSubProportion(int numOfChanges) {
		return patientsVisitedByAnotherNurseSubProportionMap.get(numOfChanges);
	}

	public JSONObject generateJsonObject() {
		try {
			JSONObject obj = new JSONObject();
			obj.put(CHANGE_PROPORTION, this.overallPatientsVisitedByAnotherNurseProportion);
			obj.put(SUB_PROPORTION, generateMapJsonObject(this.patientsVisitedByAnotherNurseSubProportionMap));
			return obj;
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	private static JSONObject generateMapJsonObject(Map<Integer, Double> map) {
		JSONObject obj = new JSONObject();
		for (Integer i : map.keySet()) {
			try {
				obj.put(Integer.toString(i), map.get(i));
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		return obj;
	}
	
	@Override
	public String toString() {
		return this.overallPatientsVisitedByAnotherNurseProportion 
						+ "-[" + getSubProportionString() + "]";
	}

	private String getSubProportionString() {
		ArrayList<Integer> keys = new ArrayList<>(this.patientsVisitedByAnotherNurseSubProportionMap.keySet());
		Collections.sort(keys);
		
		String s = Double.toString(this.patientsVisitedByAnotherNurseSubProportionMap.get(keys.get(0)));
		for (int i = 1 ; i < keys.size() ; ++i)
			s += "-" + Double.toString(this.patientsVisitedByAnotherNurseSubProportionMap.get(keys.get(i)));
		return s;
	}
}
