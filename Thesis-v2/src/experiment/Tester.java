package experiment;

import support.SupportFunction;

public class Tester {
	public void performTest(String allTestCasesDirPath, String reportDirPath) {
		if (allTestCasesDirPath == null)
			throw new NullPointerException();
		if (reportDirPath == null)
			throw new NullPointerException();
		ExperimentHandler handler = new ExperimentHandler(true);
		SupportFunction.createDirectory(reportDirPath);
		for (String testCasesDirWithParamName : SupportFunction.getSubDirectoriesName(allTestCasesDirPath)) {
			String testCasesDirWithParamDirPath = allTestCasesDirPath + "/" + testCasesDirWithParamName;
			String reportTestCasesWithParamDirPath = reportDirPath + "/report-" + testCasesDirWithParamName;
			SupportFunction.createDirectory(reportTestCasesWithParamDirPath);
			
			handler.performExperiments(testCasesDirWithParamDirPath, reportTestCasesWithParamDirPath);
		}
	}
}
