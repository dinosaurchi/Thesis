package experiment;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import model.VivianeModel;
import model_entity.ModelEntity;
import model_entity.Nurse;
import model_entity.Patient;
import model_entity.Visit;
import model_state.HomecareAssignmentState;

public class NurseDistrictingChange implements StateChange<HomecareAssignmentState, VivianeModel> {
	private static final String REMOVED_NURSE_IDS = "removed-nurse-ids";

	private static final String REMOVED_PATIENT_IDS = "removed-patient-ids";

	private static final String NEW_NURSES = "new-nurses";

	private static final String NEW_PATIENTS = "new-patients";

	private static final String NEW_VISITS_DAY_LIST = "new-visits-day-list";

	private static final String UNEXPECTED_CHANGE_PARAMS = "unexpected-change-params";

	private final ArrayList<ArrayList<Visit>> newDailyVisits;
	
	private final ArrayList<Patient> newPatients;
	private final ArrayList<String> removedPatientIds;
	
	private final ArrayList<Nurse> newNurses;
	private final ArrayList<String> removedNurseIds;
	
	private final UnexpectedChangeParameter parameter;
		
	public NurseDistrictingChange(ArrayList<Patient> newPatients, ArrayList<String> removedPatientIds,
								  ArrayList<Nurse> newNurses, ArrayList<String> removedNurseIds,
								  ArrayList<Visit> newVisits, int totalDays,
								  UnexpectedChangeParameter parameter) {
		this (newPatients, removedPatientIds,
			  newNurses, removedNurseIds,
			  getAdditionalDailyVisits(newVisits, totalDays),
			  parameter);
	}
	
	private static ArrayList<ArrayList<Visit>> getAdditionalDailyVisits(ArrayList<Visit> newVisits, int totalDays) {
		if (newVisits == null)
			throw new NullPointerException();
		if (totalDays < 1)
			throw new RuntimeException("Invalid total days : " + totalDays);
		
		ArrayList<ArrayList<Visit>> newDailyVisits = new ArrayList<>();
		int i = totalDays;
		while (i-- > 0)
			newDailyVisits.add(new ArrayList<Visit>());
					
		i = 0;
		newVisits = new ArrayList<>(newVisits);
		while (newVisits.size() > 0) {
			newDailyVisits.get(i++ % totalDays).add(newVisits.remove(0));
		}
		return newDailyVisits;
	}

	private NurseDistrictingChange(ArrayList<Patient> newPatients, ArrayList<String> removedPatientIds,
								   ArrayList<Nurse> newNurses, ArrayList<String> removedNurseIds,
								   ArrayList<ArrayList<Visit>> newDailyVisits,
								   UnexpectedChangeParameter parameter) {
		if (newPatients == null)
			throw new NullPointerException();
		if (removedPatientIds == null)
			throw new NullPointerException();
		if (newNurses == null)
			throw new NullPointerException();
		if (removedNurseIds == null)
			throw new NullPointerException();
		if (newDailyVisits == null)
			throw new NullPointerException();
		if (parameter == null)
			throw new NullPointerException();
		
		this.newPatients = new ArrayList<>(newPatients);
		this.newNurses = new ArrayList<>(newNurses);
		this.removedPatientIds = new ArrayList<>(removedPatientIds);
		this.removedNurseIds = new ArrayList<>(removedNurseIds);
		this.newDailyVisits = getCopied(newDailyVisits);
		this.parameter = parameter;
	}
	
	private static ArrayList<ArrayList<Visit>> getCopied(ArrayList<ArrayList<Visit>> newDailyVisits) {
		ArrayList<ArrayList<Visit>> result = new ArrayList<>();
		for (ArrayList<Visit> list : newDailyVisits) {
			result.add(new ArrayList<>(list));
		}
		return result;
	}

	public NurseDistrictingChange(JSONObject obj) throws JSONException {
		this (Patient.parsePatientsListFromJsonArray(obj.getJSONArray(NEW_PATIENTS)),
			  getStringsListFromJsonArray(obj, REMOVED_PATIENT_IDS),
			  Nurse.parseNursesListFromJsonArray (obj.getJSONArray(NEW_NURSES)),
			  getStringsListFromJsonArray(obj, REMOVED_NURSE_IDS),
			  getDailyVisits(obj.getJSONArray(NEW_VISITS_DAY_LIST)),
			  new UnexpectedChangeParameter(obj.getJSONObject(UNEXPECTED_CHANGE_PARAMS)));
	}	

	private static ArrayList<ArrayList<Visit>> getDailyVisits(JSONArray array) {
		if (array == null)
			throw new NullPointerException();
		
		ArrayList<ArrayList<Visit>> newDailyVisits = new ArrayList<>();
		for (int i = 0 ; i < array.length(); ++i) {
			try {
				newDailyVisits.add(Visit.parseVisitsListFromJsonArray(array.getJSONArray(i)));
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		return newDailyVisits;
	}

	private static ArrayList<String> getStringsListFromJsonArray(JSONObject obj, String key) {
		if (obj == null)
			throw new NullPointerException();
		if (key == null)
			throw new NullPointerException();
		
		try {
			ArrayList<String> array = new ArrayList<>();
			JSONArray newPatients = obj.getJSONArray(key);
			for (int i = 0 ; i < newPatients.length(); ++i) {
				array.add(newPatients.getString(i));
			}
			return array;
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	public ArrayList<String> getRemovedPatientIds() {
		return new ArrayList<>(removedPatientIds);
	}	

	public ArrayList<String> getRemovedNurseIds() {
		return new ArrayList<>(removedNurseIds);
	}

	public ArrayList<ArrayList<Visit>> getNewDailyVisits() {
		return new ArrayList<>(newDailyVisits);
	}

	public ArrayList<Patient> getNewPatients() {
		return new ArrayList<>(newPatients);
	}

	public ArrayList<Nurse> getNewNurses() {
		return new ArrayList<>(newNurses);
	}

	public UnexpectedChangeParameter getParameter() {
		return parameter;
	}
	
	public JSONObject generateJsonObject() {
		try {
			JSONObject obj = new JSONObject();
			obj.put(UNEXPECTED_CHANGE_PARAMS, this.parameter.generateJsonObject());
			obj.put(NEW_PATIENTS, ModelEntity.generateModelEntitiesJsonArray(newPatients));
			obj.put(NEW_NURSES, ModelEntity.generateModelEntitiesJsonArray(newNurses));
			obj.put(NEW_VISITS_DAY_LIST, generateNewVisitsDayListJsonArray(newDailyVisits));
			obj.put(REMOVED_PATIENT_IDS, generateStringListsJsonArray(removedPatientIds));
			obj.put(REMOVED_NURSE_IDS, generateStringListsJsonArray(removedNurseIds));
			return obj;
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	private static JSONArray generateStringListsJsonArray(ArrayList<String> strings) {
		if (strings == null)
			throw new NullPointerException();
		JSONArray array = new JSONArray();
		for (String s : strings)
			array.put(s);
		return array;
	}

	private static JSONArray generateNewVisitsDayListJsonArray(ArrayList<ArrayList<Visit>> newDailyVisits) {
		if (newDailyVisits == null)
			throw new NullPointerException();
		
		JSONArray array = new JSONArray();
		for (ArrayList<Visit> dayVisits : newDailyVisits) {
			array.put(ModelEntity.generateModelEntitiesJsonArray(dayVisits));
		}
		return array;
	}
}
