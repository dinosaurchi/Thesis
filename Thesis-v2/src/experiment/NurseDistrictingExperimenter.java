package experiment;

import java.util.ArrayList;

import javax.swing.JFrame;

import context.Problem;
import experiment.Experimenter;
import global.CHI_CONSTANT;
import model.VivianeModel;
import model_info.VivianeModelInfo;
import model_state.HomecareAssignmentState;
import non_model_entity.Coordinate2D;
import non_model_entity.DataPoint;
import non_model_entity.DirectedPair;
import nurse_districting_problem.NurseDistrictingProblem;
import plotting.Plotter;
import support.ExtendedPlotter;
import support.RunningInfo;
import support.SupportFunction;

public class NurseDistrictingExperimenter extends Experimenter<HomecareAssignmentState, VivianeModel> {
	
	public NurseDistrictingExperimenter(Problem<HomecareAssignmentState, VivianeModel> problem, ArrayList<DirectedPair<String, RunningInfo<HomecareAssignmentState>>> priorRunningInfo) {
		super(problem, priorRunningInfo);
	}
	
	public NurseDistrictingExperimenter(Problem<HomecareAssignmentState, VivianeModel> problem) {
		super(problem);
	}

	@Override
	public void exportExperimentalReports(String directory) {
		super.exportExperimentalReports(directory);
		
		ArrayList<Coordinate2D> denseCoords = SupportFunction.convertToCoordinate2DsList(problem.getModel().getModelInfo().getDataPointsList());
		JFrame denseFrame = Plotter.plot2DPointsWithDense(denseCoords, CHI_CONSTANT.FRAME_INFO, false);
		JFrame workloadCharFrame = ExtendedPlotter.plotBarCharWorkload(problem.getModel(), initialState, false);
		JFrame solution = Plotter.plotSolution(problem.getModel(), initialState, CHI_CONSTANT.FRAME_INFO, false);
		
		plotInitialAcutualAssignment(directory + "/" + "initial-actual-assignment-plots");
		problem.getModel().getModelInfo().getEntityInfo().getVisitInfo().generateJsonsInfoDirectory(directory + "/input-visit-info");
		
		SupportFunction.savePlot(denseFrame, directory + "/initial-location-dense");	
		SupportFunction.savePlot(workloadCharFrame, directory + "/initial-workload-distribution");	
		SupportFunction.savePlot(solution, directory + "/initial-solution");
	}

	private void plotInitialAcutualAssignment(String outputDirPath) {
		SupportFunction.createDirectory(outputDirPath);
		VivianeModelInfo modelInfo = problem.getModel().getModelInfo();
		ArrayList<DataPoint> dataPoints = modelInfo.getDataPointsList();
		for (String nid : modelInfo.getNurseIds()) {
			ArrayList<String> selectedPids = modelInfo.getEntityInfo().getVisitInfo().getVisitedPatientIds(nid);
			ArrayList<DirectedPair<Coordinate2D, Boolean>> selectedDataPoints = Plotter.highlighSelectedDataPoints(modelInfo.getDataPoints(selectedPids), dataPoints);
			JFrame frame = Plotter.plotHighlightSelectedPoints(selectedDataPoints, CHI_CONSTANT.FRAME_INFO, false);
			SupportFunction.savePlot(frame, outputDirPath + "/" + nid);
		}
	}
	
	@Override
	public NurseDistrictingProblem getProblem() {
		return (NurseDistrictingProblem) super.getProblem();
	}
}
