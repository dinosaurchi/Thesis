package experiment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import model.VivianeModel;
import model_state.HomecareAssignmentState;
import move.UnitMove;
import nurse_districting_problem.NurseDistrictingProblem;
import optimizer.AssigningOptimizer;
import optimizer.SimulatedAnnealing;
import optimizer_creator.ExtendedOptimizerCreator;
import problem_creator.NurseDistrictingProblemCreator;
import support.SupportFunction;

public class ExperimentHandler {
	private static final String WEIGHT_INFO = "weight-info";
	private static final String ITERATION_INFO = "iteration-info";
	private static final String REPORT_CHANGE = "report-change";
	private static final String CHANGES = "change-info";
	private static final String PROBLEM_INFO = "problem-info";
	private static final String SIMPLE_TABU = "simple-tabu";
	private static final String GREEDY_TABU = "greedy-tabu";
		
	private final NurseDistrictingProblemCreator creator = new NurseDistrictingProblemCreator();
	private final ExtendedOptimizerCreator optimizerCreator = new ExtendedOptimizerCreator();
	private final boolean hasReport;
	
	public ExperimentHandler(boolean hasReport) {
		this.hasReport = hasReport;
	}
	
	public void performExperiments(String experimentsDirPath, String reportTestCasesDirPath) {
		if (hasReport)
			SupportFunction.createDirectory(reportTestCasesDirPath);
		if (experimentsDirPath == null)
			throw new NullPointerException();
		if (reportTestCasesDirPath == null)
			throw new NullPointerException();
		
		for (String testCaseName : SupportFunction.getSubDirectoriesName(experimentsDirPath)) {
			performTestCase(experimentsDirPath + "/" + testCaseName, reportTestCasesDirPath);
		}
	}

	private void performTestCase(String testCaseDir, String reportTestCasesDirPath) {
		assert testCaseDir != null;
		
		NurseDistrictingProblem problem = creator.create(testCaseDir + "/" + PROBLEM_INFO);
		ArrayList<NurseDistrictingChange> changesList = loadChanges(testCaseDir + "/" + CHANGES);
		
		JSONObject obj = SupportFunction.parseJsonObjectFromFilePath(testCaseDir + "/" 
																	+ ITERATION_INFO + ".json");
		Map<String, Integer> iterationMap = new HashMap<>();
		try {
			iterationMap.put(SIMPLE_TABU, obj.getInt(SIMPLE_TABU));
			iterationMap.put(GREEDY_TABU, obj.getInt(GREEDY_TABU));
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		obj = SupportFunction.parseJsonObjectFromFilePath(testCaseDir + "/" 
														+ WEIGHT_INFO + ".json");
		Map<String, Double> weightMap = new HashMap<>();
		Iterator<?> iter = obj.keys();
		while (iter.hasNext()) {
			String key = iter.next().toString();
			try {
				weightMap.put(key, obj.getDouble(key));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		NurseDistrictingExperimenter experimenter = perform(problem, weightMap, iterationMap, true);
		String testCaseReportDir = reportTestCasesDirPath + "/" + "report-" + SupportFunction.getFileNameFromFilePath(testCaseDir);
		if (hasReport) {
			SupportFunction.createDirectory(testCaseReportDir);
			experimenter.exportExperimentalReports(testCaseReportDir + "/" + REPORT_CHANGE + "-" + 0);
		}
		int i = 1;
		for (NurseDistrictingChange change : changesList) {
			experimenter = perform(experimenter.getProblem().changeProblem(change), weightMap, iterationMap, false);
			
			if (hasReport) {
				experimenter.exportExperimentalReports(testCaseReportDir + "/" + REPORT_CHANGE + "-" + i);
				++i;
			}
		}
	}

	private ArrayList<NurseDistrictingChange> loadChanges(String changesDir) {
		if (changesDir == null)
			throw new NullPointerException();
		ArrayList<NurseDistrictingChange> changes = new ArrayList<>();
		String [] changeFileNames = SupportFunction.getFilesName(changesDir, "json");
		for (int i = 1 ; i <= changeFileNames.length ; ++i) {
			JSONObject obj = SupportFunction.parseJsonObjectFromFilePath(changesDir + "/" + "change-" + i + ".json");
			try {
				changes.add(new NurseDistrictingChange(obj));
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		return changes;
	}

	private NurseDistrictingExperimenter perform(NurseDistrictingProblem problem, 
												 Map<String, Double> weights,
												 Map<String, Integer> iterationInfo,
												 boolean isInitialRun) {
		assert problem != null;
		assert weights != null;
		assert weights.size() > 0;
		assert iterationInfo != null;
		assert iterationInfo.size() > 0;
		
		if (isInitialRun)
			problem.setModelWeights(getInitialWeights(weights));
		else
			problem.setModelWeights(weights);
		
		NurseDistrictingExperimenter experimenter = new NurseDistrictingExperimenter(problem);
		int totalNurses = experimenter.getProblem().getModel().getModelInfo().getNurseIds().size();
		if (isInitialRun) {
			experimenter.perform(optimizerCreator.createSimpleKMean(experimenter.getProblem().getModel(), 100, totalNurses));
			
			// Because we want to keep the clustering solution as the best solution at the moment, thus we have to refresh 
			// ... the problem to clean all the previous bestState
			experimenter = new NurseDistrictingExperimenter(experimenter.getProblem().refreshState(), experimenter.getRunningInfos());
		}
		
		int counter = 0;
		double bestObjective = experimenter.getProblem().getModel().getObjectiveScore(experimenter.getProblem().getBestState());
		while (counter++ < 2) {
			ArrayList<UnitMove<HomecareAssignmentState>> tabuMoves = new ArrayList<>();
			if (!isInitialRun) {
				AssigningOptimizer<HomecareAssignmentState, 
								   VivianeModel> followUpAssigningOptimizer = optimizerCreator.createFollowUpNurseAssigningOptimizer(experimenter.getProblem().getModel());
				experimenter.perform(followUpAssigningOptimizer);
				tabuMoves = followUpAssigningOptimizer.getLastMoves();
			}
			
//			SimpleTabuSearch<HomecareAssignmentState, 
//							 VivianeModel> optimizer = optimizerCreator.createSimpleTabuSearch(experimenter.getProblem().getModel(), 
//									 															   iterationInfo.get(SIMPLE_TABU),
//									 															   tabuMoves);
			
			SimulatedAnnealing<HomecareAssignmentState, VivianeModel> optimizer = optimizerCreator.createSimulatedAnnealing(experimenter.getProblem().getModel(), 300, 0.03);	
			
			experimenter.perform(optimizer);
			
			double score = experimenter.getProblem().getModel().getObjectiveScore(experimenter.getProblem().getBestState());
			if (score < bestObjective) {
				bestObjective = score;
				counter = 0;
			}
//			else {
//				if (!isInitialRun) {
//					AssigningOptimizer<HomecareAssignmentState, 
//									   VivianeModel> followUpAssigningOptimizer = optimizerCreator.createFollowUpNurseAssigningOptimizer(experimenter.getProblem().getModel());
//					experimenter.perform(followUpAssigningOptimizer);
//					tabuMoves = followUpAssigningOptimizer.getLastMoves();
//				}
//				experimenter.perform(optimizerCreator.createGreedyTabuSearch(experimenter.getProblem().getModel(), 
//																			 iterationInfo.get(GREEDY_TABU),
//																			 chiTabuSearch.getLastTabuMoves()));
//				score = experimenter.getProblem().getModel().getObjectiveScore(experimenter.getProblem().getBestState());
//				if (score < bestObjective) {
//					bestObjective = score;
//					counter = 0;
//				}
//			}
		}
		return experimenter;
	}

	private Map<String, Double> getInitialWeights(Map<String, Double> weights) {
		/*
		 * At the first run, we do not try to optimize the follow-up and moving objectives. 
		 *   We focus on minimize the traveling time and the balance workload
		 * */
		Map<String, Double> newWeights = new HashMap<>(weights);
		newWeights.put("w_follow_up", 0.0);
		newWeights.put("w_moving", 0.0);
		return newWeights;
	}	
}
