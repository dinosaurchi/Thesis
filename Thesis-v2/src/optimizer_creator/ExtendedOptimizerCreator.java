package optimizer_creator;

import java.util.ArrayList;

import model.VivianeModel;
import model_state.HomecareAssignmentState;
import move.UnitMove;
import move_generator.AdvancedChiMoveGenerator;
import move_generator.AdvancedChiRandomSingleMoveGenerator;
import move_generator.ChiMoveGenerator;
import move_generator.FollowUpNurseAssigningMoveGenerator;
import move_generator.GreedyMoveGenerator;
import optimizer.AssigningOptimizer;
import optimizer.SimpleTabuSearch;
import optimizer.SimulatedAnnealing;

public class ExtendedOptimizerCreator extends OptimizerCreator<HomecareAssignmentState, VivianeModel>{
	public SimpleTabuSearch<HomecareAssignmentState, VivianeModel> createSimpleTabuSearch(VivianeModel model, int iterations,
																						  ArrayList<UnitMove<HomecareAssignmentState>> initialTabuMoves) {
		if (model == null)
			throw new NullPointerException();
		
		//ChiMoveGenerator moveGenerator = new ChiMoveGenerator(model);
		AdvancedChiMoveGenerator moveGenerator = new AdvancedChiMoveGenerator(model);
		return new SimpleTabuSearch<HomecareAssignmentState, VivianeModel> (moveGenerator, iterations, initialTabuMoves);
	}
	
	public SimulatedAnnealing<HomecareAssignmentState, VivianeModel> createSimulatedAnnealing(VivianeModel model, double temperature, double coolingRate) {
		if (model == null)
			throw new NullPointerException();
		
		//ChiMoveGenerator moveGenerator = new ChiMoveGenerator(model);
		//AdvancedChiMoveGenerator moveGenerator = new AdvancedChiMoveGenerator(model);
		AdvancedChiRandomSingleMoveGenerator moveGenerator = new AdvancedChiRandomSingleMoveGenerator(model);
		return new SimulatedAnnealing<HomecareAssignmentState, VivianeModel> (moveGenerator, temperature, coolingRate);
	}
	
	public SimpleTabuSearch<HomecareAssignmentState, VivianeModel> createGreedyTabuSearch(VivianeModel model, int iterations, ArrayList<UnitMove<HomecareAssignmentState>> initialTabuMoves) {
		if (model == null)
			throw new NullPointerException();
		
		GreedyMoveGenerator moveGenerator = new GreedyMoveGenerator(model);
		//AdvancedGreedyMoveGenerator moveGenerator = new AdvancedGreedyMoveGenerator(model);
		return new SimpleTabuSearch<HomecareAssignmentState, VivianeModel> (moveGenerator, iterations, initialTabuMoves);
	}
	
	public AssigningOptimizer<HomecareAssignmentState, VivianeModel> createFollowUpNurseAssigningOptimizer(VivianeModel model) {
		if (model == null)
			throw new NullPointerException();
		
		FollowUpNurseAssigningMoveGenerator moveGenerator = new FollowUpNurseAssigningMoveGenerator(model);
		return new AssigningOptimizer<>(moveGenerator);
	}
}
