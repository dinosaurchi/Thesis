package solution_reporter;

import java.util.ArrayList;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import clustering.Cluster;
import global.CHI_CONSTANT;
import model.VivianeModel;
import model_entity.CLSC;
import model_state.HomecareAssignmentState;
import non_model_entity.Coordinate2D;
import non_model_entity.DataPoint;
import non_model_entity.DirectedPair;
import plotting.Plotter;
import solution_report.SolutionReporter;
import support.ExtendedPlotter;
import support.RunningInfo;
import support.SupportFunction;

public class VivianeModelSolutionReporter extends SolutionReporter<HomecareAssignmentState, VivianeModel> {
	private static final String FOLLOW_UP_NOT_IMPORTANT = "follow_up_not_important";
	private static final String FOLLOW_UP_IMPORTANT = "follow_up_important";
	private static final String PATIENT_ID = "patient_id";
	private static final String ASSIGNED_GRAPH = "assigned-graph";
	private static final String SUMMARY = "summary";
	private static final String OBJECTIVE_COMPONENTS = "objective_components";
	private static final String MEDIAN = "median";
	private static final String FOLLOWUP_NOT_IMPORTANT_CASES_ERROR = "followup_not_important_cases_error";
	private static final String FOLLOWUP_IMPORTANT_CASES_ERROR = "followup_important_cases_error";
	private static final String OVERAL_ERROR = "overal_error";
	private static final String NURSE_OVERVIEW = "nurse_overview";
	private static final String ASSIGNED_PATIENT_DETAILS_DIRECTORY = "assigned-patient-details";
	private static final String NURSE_SUMMARY = "nurse_summary";
	private static final String WEIGHTS = "weights";
	private static final String TIME_TO_CLSC = "time_to_clsc";
	private static final String AVERAGE_TRAVEL_TIME = "average_travel_time";
	private static final String AVERAGE_TREATMENT_TIME = "average_treatment_time";
	private static final String ASSIGNED_PATIENT_DETAILS = "assigned_patient_details";
	private static final String DISTANCE_TO_CENTROID = "distance_to_centroid";
	private static final String CHANGING_PERCENTAGE = "changing_percentage";
	private static final String TOTAL_OLD_PATIENTS = "total_old_patients";
	private static final String TOTAL_NEW_PATIENTS = "total_new_patients";
	private static final String TOTAL_MOVED_PATIENTS = "total_moved_patients";
	private static final String AVERAGE_WORKLOAD = "average_workload";
	private static final String NURSE_ID = "nurse_id";
	private static final String DETAILS = "nurses-detail";
	private static final String FOLLOW_UP_INFO = "follow_up_info";
	private static final String WORKLOAD = "workload";
	private static final String MAX_VARIANCE = "max_variance";
	private static final String STD = "std";
	private static final String LOWEST = "lowest";
	private static final String HIGHEST = "highest";
	private static final String AVERAGE = "average";
	private static final String SUM = "sum";
	private static final String TOTAL_RECORDS = "total_records";
	private static final String CONTEXT_INFO = "context_info";
	private static final String TOTAL_NURSES = "total_nurses";
	private static final String TOTAL_PATIENTS = "total_patients";
	private static final String MODEL = "model";
	
	public VivianeModelSolutionReporter(VivianeModel model) {
		super(model);
	}
	
	@Override
	public void exportReport(RunningInfo<HomecareAssignmentState> info) {
		JSONObject summary = generateSummary(info);
		JSONObject contextInfo = getContextInfo(summary);
		try {
			SupportFunction.saveFile(contextInfo.toString(3), 		
									 getReportDirectory() + "/" + CONTEXT_INFO + ".json");
		} catch (JSONException e) {
			e.printStackTrace();
		}		
		JSONArray details = getDetails(summary);
		exportNursesReport(getReportDirectory(), details);
		
		SupportFunction.savePlot(Plotter.plotSolution(model, info.getCurrentState(), CHI_CONSTANT.FRAME_INFO, false), getReportDirectory() + "/solution-graph");
		SupportFunction.savePlot(ExtendedPlotter.plotBarCharWorkload(model, info.getCurrentState(), false), getReportDirectory() + "/workload-distribution");
	}

	private void exportNursesReport(String reportDirectoryName, JSONArray nursesDetails) {
		assert reportDirectoryName != null;
		assert nursesDetails != null;
		
		String detailsDirPath = reportDirectoryName + "/" + ASSIGNED_PATIENT_DETAILS_DIRECTORY;		
		SupportFunction.createDirectory(detailsDirPath);
	
		JSONArray nursesSummary = new JSONArray();
		for (int i = 0 ; i < nursesDetails.length() ; ++i) {
			try {
				JSONObject obj = nursesDetails.getJSONObject(i);
				JSONObject nurseSummary = obj.getJSONObject(NURSE_SUMMARY);
				String nid = nurseSummary.getJSONObject(NURSE_OVERVIEW).getString(NURSE_ID);
				
				String nurseDetailDirPath = detailsDirPath + "/" + nid + "-detail";		
				SupportFunction.createDirectory(nurseDetailDirPath);
				
				exportPatientsDetail(nurseDetailDirPath, nid, obj.getJSONArray(ASSIGNED_PATIENT_DETAILS));
				
				JSONObject temp = new JSONObject(); 
				temp.put(nid + "-" + SUMMARY, nurseSummary);
				temp.put(ASSIGNED_PATIENT_DETAILS, nurseDetailDirPath);
				nursesSummary.put(temp);
				
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		try {
			SupportFunction.saveFile(nursesSummary.toString(3), reportDirectoryName + "/" + DETAILS + ".json");
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	private void exportPatientsDetail(String assignedPatientDetailsDirectory, String nid, JSONArray patientsArray) {
		try {
			ArrayList<DirectedPair<Coordinate2D, Boolean>> selected = new ArrayList<>();
			for (String pid : this.model.getModelInfo().getPatientIds()) {
				Coordinate2D coord = this.model.getModelInfo().getDataPoint(pid).getCoordinate();
				if (containsPid(pid, patientsArray)) {
					selected.add(new DirectedPair<Coordinate2D, Boolean>(coord, true));
				}
				else {
					selected.add(new DirectedPair<Coordinate2D, Boolean>(coord, false));
				}
			}
			
			//SupportFunction.savePlot(Plotter.plotHighlightSelectedPoints(selected, CHI_CONSTANT.FRAME_INFO, false), assignedPatientDetailsDirectory + "/" + ASSIGNED_GRAPH);
			SupportFunction.saveFile(patientsArray.toString(3), assignedPatientDetailsDirectory + "/" + getPatientDetailFileName(nid));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private boolean containsPid(String pid, JSONArray patientsArray) {
		assert pid != null;
		assert patientsArray != null;
		
		for (int i = 0 ; i < patientsArray.length() ; ++i) {
			try {
				if (pid.equals(patientsArray.getJSONObject(i).getString(PATIENT_ID)))
					return true;
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		return false;
	}

	private String getPatientDetailFileName(String nid) {
		assert nid != null;
		return nid + "-" + ASSIGNED_PATIENT_DETAILS + ".json";
	}

	private JSONObject generateSummary(RunningInfo<HomecareAssignmentState> info) {
		if (info == null)
			throw new NullPointerException();
		
		if (!model.getModelInfo().isConsistent(info.getCurrentState()))
			throw new RuntimeException("Inconsistent state");
		
		JSONObject contextInfo = new JSONObject();
		try {
			contextInfo.put(MODEL, model.getClass().getName());
		
			for (String key : info.getKeys()) {
				contextInfo.put(key, info.getValue(key));
			}
				
			contextInfo.put(TOTAL_PATIENTS, model.getModelInfo().getTotalPatients());
			contextInfo.put(TOTAL_NURSES, model.getModelInfo().getTotalNurses());
			
			ArrayList<String> nids = model.getModelInfo().getNurseIds();
			JSONArray nursesSummaryArray = new JSONArray();
			for (String nid : nids) {
				nursesSummaryArray.put(getSummaryForNurse(info.getCurrentState(), nid));
			}
			
			contextInfo.put(WEIGHTS, getWeightsSummary());
			contextInfo.put(OBJECTIVE_COMPONENTS, getObjectiveComponentsSummary(info.getCurrentState()));
			contextInfo.put(WORKLOAD, getWorkloadSummary(nursesSummaryArray));
			contextInfo.put(FOLLOW_UP_INFO, getFollowUpSummary(info.getCurrentState()));
			
			JSONObject summary = new JSONObject();
			summary.put(DETAILS, nursesSummaryArray);
			summary.put(CONTEXT_INFO, contextInfo);
			
			return summary;
		
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	private JSONObject getObjectiveComponentsSummary(HomecareAssignmentState state) {
		if (state == null)
			throw new NullPointerException();		
		return new JSONObject(this.model.getObjectiveComponentMap(state));
	}

	private JSONObject getWeightsSummary() throws JSONException {
		JSONObject summary = new JSONObject();
		
		Map<String, Double> weights = model.getWeightsMap();
		for (String key : weights.keySet()) {
			summary.put(key, weights.get(key));
		}
		
		return summary;
	}

	private static JSONObject getContextInfo(JSONObject summary) {
		if (summary == null)
			throw new NullPointerException();
		
		try {
			return summary.getJSONObject(CONTEXT_INFO);
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	private static JSONArray getDetails(JSONObject summary) {
		if (summary == null)
			throw new NullPointerException();
		try {
			return summary.getJSONArray(DETAILS);
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	private JSONObject getFollowUpSummary(HomecareAssignmentState currentState) throws JSONException {
		JSONObject followUpInfo = new JSONObject();
		followUpInfo.put(OVERAL_ERROR, getVisitProportionErrorsStatistic(
											getOverallAssignedPairs(currentState, null)));		
		followUpInfo.put(FOLLOWUP_IMPORTANT_CASES_ERROR, getVisitProportionErrorsStatistic(
															 getFollowUpImportantAssignedPairs(currentState, null)));
		followUpInfo.put(FOLLOWUP_NOT_IMPORTANT_CASES_ERROR, getVisitProportionErrorsStatistic(
																 getFollowUpNotImportantAssignedPairs(currentState, null)));
		followUpInfo.put(FOLLOW_UP_IMPORTANT, getFollowUpImportantCasesStatistic(currentState));
		followUpInfo.put(FOLLOW_UP_NOT_IMPORTANT, getFollowUpNotImportantCasesStatistic(currentState));
		return followUpInfo;
	}

	private JSONObject getFollowSummary(HomecareAssignmentState currentState, String nid) throws JSONException {
		JSONObject followUpInfo = new JSONObject();
		followUpInfo.put(OVERAL_ERROR, getVisitProportionErrorsStatistic(
											getOverallAssignedPairs(currentState, nid)));		
		followUpInfo.put(FOLLOWUP_IMPORTANT_CASES_ERROR, getVisitProportionErrorsStatistic(
															 getFollowUpImportantAssignedPairs(currentState, nid)));
		followUpInfo.put(FOLLOWUP_NOT_IMPORTANT_CASES_ERROR, getVisitProportionErrorsStatistic(
																 getFollowUpNotImportantAssignedPairs(currentState, nid)));
		followUpInfo.put(FOLLOW_UP_IMPORTANT, getFollowUpImportantCasesStatistic(currentState, nid));
		followUpInfo.put(FOLLOW_UP_NOT_IMPORTANT, getFollowUpNotImportantCasesStatistic(currentState, nid));
		return followUpInfo;
	}
	
	private JSONObject getFollowUpNotImportantCasesStatistic(HomecareAssignmentState currentState, String nid) {
		ArrayList<String> pids = new ArrayList<>();
		for (String pid : currentState.getAssignedPatientIdsList(nid)) {
			if (!this.model.getModelInfo().isFollowUpImportant(pid))
				pids.add(pid);
		}
		return getStatisticCorrectionAssignment(pids, currentState);
	}

	private JSONObject getFollowUpImportantCasesStatistic(HomecareAssignmentState currentState, String nid) {
		ArrayList<String> pids = new ArrayList<>();
		for (String pid : currentState.getAssignedPatientIdsList(nid)) {
			if (this.model.getModelInfo().isFollowUpImportant(pid))
				pids.add(pid);
		}
		return getStatisticCorrectionAssignment(pids, currentState);
	}

	private JSONObject getFollowUpImportantCasesStatistic(HomecareAssignmentState currentState) {
		ArrayList<String> pids = new ArrayList<>();
		for (String pid : this.model.getModelInfo().getEntityInfo().getPatientIds()) {
			if (this.model.getModelInfo().isFollowUpImportant(pid))
				pids.add(pid);
		}
		return getStatisticCorrectionAssignment(pids, currentState);
	}

	private JSONObject getFollowUpNotImportantCasesStatistic(HomecareAssignmentState currentState) {
		ArrayList<String> pids = new ArrayList<>();
		for (String pid : this.model.getModelInfo().getEntityInfo().getPatientIds()) {
			if (!this.model.getModelInfo().isFollowUpImportant(pid))
				pids.add(pid);
		}
		return getStatisticCorrectionAssignment(pids, currentState);
	}

	private JSONObject getStatisticCorrectionAssignment(ArrayList<String> pids, HomecareAssignmentState currentState) {
		int totalCorrect = 0;
		for (String pid : pids) {
			String previousNid = this.model.getModelInfo().getHighestProportionNurse(pid);
			if (previousNid.equals(currentState.getAssignedNurseId(pid)))
				++totalCorrect;
		}
		JSONObject obj = new JSONObject();
		try {
			obj.put("total_assigned_to_previous_nurse", totalCorrect);
			obj.put("total_cases", pids.size());
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return obj;
	}

	private JSONObject getVisitProportionErrorsStatistic(ArrayList<DirectedPair<String, String>> pidToNidPairs) {
		ArrayList<Double> proportions = new ArrayList<>();
		for (DirectedPair<String, String> pair : pidToNidPairs) {
			proportions.add(1.0 - this.model.getModelInfo()
											.getLastMonthVisitProportion(pair.getLeft(), pair.getRight()));
		}
		try {
			return getStatistic(SupportFunction.toDoubleArray(proportions));
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	private ArrayList<DirectedPair<String, String>> getFollowUpNotImportantAssignedPairs(HomecareAssignmentState currentState, String currentNid) {
		ArrayList<String> pids = new ArrayList<>();
		if (currentNid == null)
			pids = currentState.getPatientIds();
		else
			pids = currentState.getAssignedPatientIdsList(currentNid);
		
		ArrayList<DirectedPair<String, String>> pairs = new ArrayList<>();
		for (String pid : pids) {
			if (!this.model.getModelInfo().isFollowUpImportant(pid)) {
				String nid = currentState.getAssignedNurseId(pid);
				pairs.add(new DirectedPair<String, String>(pid, nid));
			}
		}
		return pairs;
	}

	private ArrayList<DirectedPair<String, String>> getFollowUpImportantAssignedPairs(HomecareAssignmentState currentState, String currentNid) {
		ArrayList<String> pids = new ArrayList<>();
		if (currentNid == null)
			pids = currentState.getPatientIds();
		else
			pids = currentState.getAssignedPatientIdsList(currentNid);
		
		ArrayList<DirectedPair<String, String>> pairs = new ArrayList<>();
		for (String pid : pids) {
			if (this.model.getModelInfo().isFollowUpImportant(pid)) {
				String nid = currentState.getAssignedNurseId(pid);
				pairs.add(new DirectedPair<String, String>(pid, nid));
			}
		}
		return pairs;
	}

	private ArrayList<DirectedPair<String, String>> getOverallAssignedPairs(HomecareAssignmentState currentState, String currentNid) {
		ArrayList<String> nids = new ArrayList<>();
		if (currentNid == null)
			nids = currentState.getNurseIds();
		else 
			nids.add(currentNid);
		
		ArrayList<DirectedPair<String, String>> pairs = new ArrayList<>();
		for (String nid : nids) {
			for (String pid : currentState.getAssignedPatientIdsList(nid)) {
				pairs.add(new DirectedPair<String, String>(pid, nid));
			}
		}
		return pairs;
	}

	private JSONObject getWorkloadSummary(JSONArray nursesSummaryArray) throws JSONException {
		double [] workloads = getWorkloads(nursesSummaryArray);
		return getStatistic(workloads);
	}
	
	private JSONObject getStatistic(double [] values) throws JSONException {
		JSONObject stats = new JSONObject();
		if (values.length > 0) {
			stats.put(TOTAL_RECORDS, values.length);
			stats.put(SUM, SupportFunction.sum(values));
			stats.put(AVERAGE, SupportFunction.mean(values));
			stats.put(HIGHEST, SupportFunction.max(values));
			stats.put(LOWEST, SupportFunction.min(values));
			stats.put(STD, SupportFunction.std(values));
			stats.put(MEDIAN, SupportFunction.median(values));
			stats.put(MAX_VARIANCE, SupportFunction.maxVariation(values));
		}
		else {
			stats.put("status", "no_record");
		}
		return stats;
	}

	private double[] getWorkloads(JSONArray nursesSummaryArray) throws JSONException {
		assert nursesSummaryArray != null;
		double[] workloads = new double [nursesSummaryArray.length()];
		
		for (int i = 0 ; i < workloads.length ; ++i) {
			JSONObject obj = nursesSummaryArray.getJSONObject(i);
			obj = obj.getJSONObject(NURSE_SUMMARY);
			obj = obj.getJSONObject(NURSE_OVERVIEW);
			workloads[i] = obj.getDouble(AVERAGE_WORKLOAD);
		}
		
		return workloads;
	}

	private JSONObject getSummaryForNurse(HomecareAssignmentState currentState, String nid) throws JSONException {
		JSONObject nurse_summary = new JSONObject();
		
		nurse_summary.put(NURSE_OVERVIEW, getOverviewForNurse(currentState, nid));
		
		Cluster cluster = new Cluster();
		
		ArrayList<String> pids = currentState.getAssignedPatientIdsList(nid);
		cluster.addDataPoints(model.getModelInfo().getDataPoints(pids));
		double [] distancesToCentroid = getTimeDistancesToCentroid(cluster);
		
		nurse_summary.put(DISTANCE_TO_CENTROID, getStatistic(distancesToCentroid));
		nurse_summary.put(FOLLOW_UP_INFO, getFollowSummary(currentState, nid));
		
		JSONArray array = new JSONArray();
		for (String pid : pids) {
			array.put(getSummaryForPatient(currentState, pid));
		}
		
		JSONObject summary = new JSONObject();
		summary.put(NURSE_SUMMARY, nurse_summary);
		summary.put(ASSIGNED_PATIENT_DETAILS, array);
		
		return summary;
	}
	
	private JSONObject getOverviewForNurse(HomecareAssignmentState currentState, String nid) throws JSONException {
		JSONObject nurse_overview = new JSONObject();
		
		nurse_overview.put(NURSE_ID, nid);
		nurse_overview.put(AVERAGE_WORKLOAD, model.getNurseAverageDailyWorkload(currentState, nid));
		
		ArrayList<String> pids = currentState.getAssignedPatientIdsList(nid);
		nurse_overview.put(TOTAL_PATIENTS, pids.size());
	
		double total_moved = getTotalMovedPatients(currentState, nid);
		nurse_overview.put(TOTAL_MOVED_PATIENTS, total_moved);
		double total_news = getTotalNewPatients(currentState, nid);
		nurse_overview.put(TOTAL_NEW_PATIENTS, total_news);
		double initial_total = pids.size() + total_moved - total_news;
		double total_remaining_olds = getTotalRemainingOldPatients(currentState, nid);
		nurse_overview.put(TOTAL_OLD_PATIENTS, total_remaining_olds);
		// The total_remaining_olds here is the total common patient between the current assignment and the previous assignment
		nurse_overview.put(CHANGING_PERCENTAGE, (1 - (total_remaining_olds / initial_total)) * (1 - (total_remaining_olds / (double) pids.size())));
		
		return nurse_overview;
	}

	private JSONObject getSummaryForPatient(HomecareAssignmentState currentState, String pid) throws JSONException {
		JSONObject summary = new JSONObject();
		
		summary.put(PATIENT_ID, pid);
		
		summary.put(AVERAGE_TREATMENT_TIME, this.model.getModelInfo().getAverageServiceTime(pid));
		
		String clscId = model.getModelInfo().getCLSC(CLSC.CLSC_ID).getId();
		summary.put(TIME_TO_CLSC, model.getModelInfo().getTimeDistance(pid, clscId));
		
		String nid = currentState.getAssignedNurseId(pid);
		ArrayList<String> pids = currentState.getAssignedPatientIdsList(nid);
		double totalTime = 0;
		int n = 0;
		for (String id : pids) {
			if (!id.equals(pid)) {
				totalTime += model.getModelInfo().getTimeDistance(pid, id);
				++n;
			}
		}
		// Avoid dividing by 0
		summary.put(AVERAGE_TRAVEL_TIME, totalTime / (double) ((n == 0) ? 1 : n));
		
		return summary;
	}

	private double[] getTimeDistancesToCentroid(Cluster cluster) {
		DataPoint centroid = cluster.getCentroid();
		ArrayList<DataPoint> dataPoints = cluster.getAssignDataPoints();
		double[] distances = new double[dataPoints.size()];
		for (int i = 0 ; i < distances.length ; ++i) {
			distances[i] = model.getModelInfo().getTimeDistance(centroid.getId(), dataPoints.get(i).getId());
		}
		return distances;
	}

	private int getTotalRemainingOldPatients(HomecareAssignmentState currentState, String nid) {
		HomecareAssignmentState previousState = model.getModelInfo().getPreviousMonthAssignment();
		ArrayList<String> previousAssignedPids = previousState.getAssignedPatientIdsList(nid);
		ArrayList<String> currentAssignedPids = currentState.getAssignedPatientIdsList(nid);
		currentAssignedPids.retainAll(previousAssignedPids);
		return currentAssignedPids.size();
	}

	private int getTotalNewPatients(HomecareAssignmentState currentState, String nid) {
		HomecareAssignmentState previousState = model.getModelInfo().getPreviousMonthAssignment();
		ArrayList<String> previousAssignedPids = previousState.getAssignedPatientIdsList(nid);
		ArrayList<String> currentAssignedPids = currentState.getAssignedPatientIdsList(nid);
		int currentTotal = currentAssignedPids.size();
		previousAssignedPids.retainAll(currentAssignedPids);
		return currentTotal - previousAssignedPids.size();
	}

	private int getTotalMovedPatients(HomecareAssignmentState currentState, String nid) {
		HomecareAssignmentState previousState = model.getModelInfo().getPreviousMonthAssignment();
		ArrayList<String> previousAssignedPids = previousState.getAssignedPatientIdsList(nid);
		ArrayList<String> currentAssignedPids = currentState.getAssignedPatientIdsList(nid);
		int previousTotal = previousAssignedPids.size();
		previousAssignedPids.retainAll(currentAssignedPids);
		
		return previousTotal - previousAssignedPids.size();
	}
}
