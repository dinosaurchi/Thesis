package distance_matrix;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import global.KEY_MAP;
import non_model_entity.IndirectedPair;

public class IndirectedDistanceMatrix extends AbstractDistanceMatrix {	
	
	// Public methods
	public IndirectedDistanceMatrix (String matrixDistanceJsonFileName) {
		super(matrixDistanceJsonFileName);
	}
	
	protected IndirectedDistanceMatrix(IndirectedDistanceMatrix dm) {
		super(dm);
	}
	
	@Override
	public IndirectedDistanceMatrix clone() {
		return this;
	}
	
	@Override
	public Double getDistance(String id1, String id2) {
		if (id1 == null) 
			throw new RuntimeException("Null id1");
		
		if (id2 == null) 
			throw new RuntimeException("Null id2");
				
		return super.getDistance(new IndirectedPair<String, String>(id1, id2));
	}
	
	// ----------------------------------------------------------------------------------------
	// Protected methods
	
	protected IndirectedDistanceMatrix() {
		// This constructor is used in the case when you want to implement an expandable distance matrix
		// .. with another inheriting class.
	}

	@Override
	protected void reconstructDistanceMatrix(JSONArray array) {
		if (array == null)
			throw new RuntimeException("Null input");
				
		for (int i = 0 ; i < array.length() ; ++i) {
			try {
				JSONObject obj = array.getJSONObject(i);
				String id1 = obj.getString(KEY_MAP.PATIENT_1_ID);
				String id2 = obj.getString(KEY_MAP.PATIENT_2_ID);
				IndirectedPair<String, String> pair = new IndirectedPair<String, String>(id1, id2);
				
				Double t = obj.getDouble(KEY_MAP.TIME_DISTANCE);
				assert (t != null) : "Null time distance";
				addPair(pair, t);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		if (this.getClass().equals(IndirectedDistanceMatrix.class)) {
			// We only check this consistency when we use exactly this IndirectedDistanceMatrix, not the ExpandableDistanceMatrix for instance
			int n = (int) getTotalIds();
			int m = n * (n + 1) / 2;
			if (m != size()) {
				throw new RuntimeException("Incomplete distance matrix: size = " + size() + "/" + m);
			}
		}
	}
	
	// ----------------------------------------------------------------------------------------
	// Private methods
	
	
}
