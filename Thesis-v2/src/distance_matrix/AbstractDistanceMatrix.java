package distance_matrix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import global.KEY_MAP;
import non_model_entity.IndirectedPair;
import non_model_entity.Pair;
import support.SupportFunction;

public abstract class AbstractDistanceMatrix {
	/*
	DOCUMENTATION

	AbstractDistanceMatrix object is an unmodifiable object, means that it is only defined in constructors. 
	From that assumption, we can apply pass by reference for AbstractDistanceMatrix object, do not need 
	to have a deep copy.
	
	*/
	
	private final Map<Pair<String, String>, Double> distanceMatrix = new HashMap<>();
	private final ArrayList<String> idsList = new ArrayList<>();
	
	protected AbstractDistanceMatrix(AbstractDistanceMatrix dm) {
		if (dm == null) 
			throw new RuntimeException("Null input");

		this.distanceMatrix.putAll(dm.distanceMatrix);
		this.idsList.addAll(dm.idsList);
	}
	
	protected AbstractDistanceMatrix(String matrixDistanceJsonFileName) {
		if (matrixDistanceJsonFileName == null) 
			throw new RuntimeException("Null input");
		
		JSONArray array = SupportFunction.parseJsonArrayFromFile(matrixDistanceJsonFileName);
		assert (array != null);
		reconstructDistanceMatrix(array);
	}
	
	protected AbstractDistanceMatrix() {
		// This constructor is used in the case when you want to implement an expandable distance matrix
		// .. with another inheriting class.
	}
	
	public abstract AbstractDistanceMatrix clone();
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof AbstractDistanceMatrix))
			return false;
		
		AbstractDistanceMatrix m = (AbstractDistanceMatrix) obj;
		if (!m.idsList.equals(this.idsList)) {
			return false;
		}
		
		if (!this.distanceMatrix.keySet().equals(m.distanceMatrix.keySet())) {
			return false;
		}
		
		// Check the equality of time at each block of the matrix
		for (Pair<String, String> pair : this.distanceMatrix.keySet()) {
			if (!this.distanceMatrix.get(pair).equals(m.distanceMatrix.get(pair)))
				return false;
		}
		
		return true;
	}
	
	abstract public Double getDistance(String id1, String id2);
	
	public ArrayList<String> getIdsList() {
		return new ArrayList<String>(this.idsList);
	}
	
	public long size () {
		return this.distanceMatrix.size();
	}
	
	public long getTotalIds() {
		return idsList.size();
	}
	
	public JSONArray generateJsonArray() {
		JSONArray array = new JSONArray();
		for (Pair<String, String> pair : this.distanceMatrix.keySet()) {
			
			try {
				JSONObject obj = new JSONObject();
				obj.put(KEY_MAP.PATIENT_1_ID, pair.getLeft());
				obj.put(KEY_MAP.PATIENT_2_ID, pair.getRight());
				obj.put(KEY_MAP.TIME_DISTANCE, this.distanceMatrix.get(pair).doubleValue());
				array.put(obj);
			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		assert array.length() == this.distanceMatrix.size();
		return array;
	}
	
	// Protected methods

	protected Double getDistance(Pair<String, String> pair) {
		if (pair == null) 
			throw new RuntimeException("Null pair");
				
		if (!idsList.contains(pair.getLeft()))
			throw new RuntimeException("Not exisitng id : " + pair.getLeft());

		if (!idsList.contains(pair.getRight())) 
			throw new RuntimeException("Not exisitng id : " + pair.getRight());
				
		if (distanceMatrix.containsKey(pair)) {
			return distanceMatrix.get(pair);
		}
		
		throw new RuntimeException("Not exisitng distance from " + pair.getLeft() + " to " + pair.getRight());
	}
	
	protected void addPair(IndirectedPair<String, String> pair, double time) {
		if (pair == null)
			throw new RuntimeException("Null input");

		if (!this.idsList.contains(pair.getLeft())) 
			this.idsList.add(pair.getLeft());
		if (!this.idsList.contains(pair.getRight())) 
			this.idsList.add(pair.getRight());

		if (!distanceMatrix.containsKey(pair)) {
			distanceMatrix.put(pair, time);
		}
	}
	
	protected boolean constainsPair(Pair<String, String> pair) {
		if (pair == null)
			throw new RuntimeException("Null input");
		
		if (distanceMatrix.containsKey(pair))
			return true;
		return false;
	}
	
	abstract protected void reconstructDistanceMatrix(JSONArray array);
}
