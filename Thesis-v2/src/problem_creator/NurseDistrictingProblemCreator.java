package problem_creator;

import distance_matrix.IndirectedDistanceMatrix;
import model.VivianeModel;
import model_info.EntityInfo;
import model_info.VivianeModelInfo;
import nurse_districting_problem.NurseDistrictingProblem;

public class NurseDistrictingProblemCreator {
	public NurseDistrictingProblem create(String problemInfoDirectory) {
		if (problemInfoDirectory == null)
			throw new NullPointerException();
				
		VivianeModelInfo info = new VivianeModelInfo(new EntityInfo(problemInfoDirectory + "/info"), 
													 new IndirectedDistanceMatrix(problemInfoDirectory + "/distance-matrix.json"), 
													 null);
		VivianeModel model = new VivianeModel(info);
		return new NurseDistrictingProblem(model);
	}
}
