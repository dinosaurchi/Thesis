package global;

public class KEY_MAP {
	// Patient keys map
	public static final String PATIENT_ID = "pid";
	public static final String IS_FOLLOW_UP_IMPORTANT = "isFollowUpImportant";
	public static final String COORDINATE_X = "locX";
	public static final String COORDINATE_Y = "locY";
	public static final String ADDRESS = "address";
	
	// CLSC keys map, the rest key map are coordinate X,Y, and Address of Patient key map
	public static final String CLSC_ID = "id";

	// Nurse keys map
	public static final String NURSE_ID = "nid";
	public static final String NURSE_COLOR = "nurseColor";

	// Visit keys map
	public static final String SERVICE_TIME = "serviceTime";
	public static final String VISIT_TYPE = "visitType";	
	public static final String VISIT_ID = "vid";
	
	// Visit change keys map
	public static final String OLD_VISIT = "oldVisit";
	public static final String NEW_VISIT = "newVisit";
	
	// Matrix distance keys map
	public static final String PATIENT_1_ID = "pid1";
	public static final String PATIENT_2_ID = "pid2";
	public static final String TIME_DISTANCE = "time";
}