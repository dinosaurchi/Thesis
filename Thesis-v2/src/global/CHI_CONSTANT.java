package global;

import plotting.FrameInfo;

public class CHI_CONSTANT {	
	public static final String CLSC_ID = "CLSC";
	
	/*
	 * The top-left and right-bottom points are from the patient-155.json (max and min of xs and ys)
	 * */
	public static final FrameInfo FRAME_INFO = new FrameInfo(-72.4407143999999, 46.298783, -72.7738188, 46.4686543);
}
