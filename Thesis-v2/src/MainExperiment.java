import experiment.Tester;
import support.SupportFunction;

public class MainExperiment {
	public static void main(String[] args) {		
		Tester tester = new Tester();
		
		//tester.performTest("./all-test-cases-20170709-224727", "report-" + SupportFunction.getCurrentTimeStamp());
		tester.performTest("./all-test-cases-20170709-224727", "report-" + SupportFunction.getCurrentTimeStamp());
	}
}
